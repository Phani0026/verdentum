//
//  AssetCell.swift
//  Verdentum
//
//  Created by Verdentum on 13/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class AssetCell: UITableViewCell {

    @IBOutlet var nameLabelObj: UILabel!
    @IBOutlet var imageViewObj: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
