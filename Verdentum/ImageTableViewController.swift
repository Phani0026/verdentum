//
//  ImageTableViewController.swift
//  Verdentum
//
//  Created by Verdentum on 16/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import SDWebImage
import SKPhotoBrowser
import AlamofireImage
//import Alamofire
import MBProgressHUD
import LGSideMenuController


class ImageTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SKPhotoBrowserDelegate {
      var allisLikesPersonalArray = NSMutableArray()
    var headerStr = String()
    var responsePostimageArray = NSArray()
    var responseUserPostsArray = NSMutableArray()
      var program_idstr = NSString()
    var images = [SKPhotoProtocol]()
    var downloadImage = [UIImage]()
    var verticalOffset: Int = 0
    var responsePostArray = NSMutableArray()
    var commentsImagesArray = NSArray()
    var imageResponseArray = NSArray()
    var image_IDArray = NSArray()
    var post_IDArray = NSArray()
    var imageStr_ID = NSString()
    var PostStr_ID = NSString()
    var commentsArray = NSMutableArray()
    var commentCoutStr = NSString()
    var navstr = NSString()
     var peopleReg_idStr = NSString()
    var selectedRow = NSIndexPath()
    var verticalOffsetIndex: Int = 0
    var ButtonIndex: Int = 0
    var lastpostIDStr = NSString()
    var usernameobjstr = NSString()
    var navStr = NSString()
    var userNameObjStr = NSString()
    var headerNameStr = NSString()
    var groupImageStr = NSString()
    var groupIDStr = NSString()
    var programArray = NSMutableArray()
    var userID_Registered = NSString()
     var programNamestr = NSString()
     var allcommentCountArray = NSMutableArray()
     var WorldViewheaderTitleStr = NSString()
    var allPersonalcommentsArray = NSMutableArray()
     var allFeedcommentCountArray = NSMutableArray()
     var allSupportArray = NSMutableArray()
     var headerTitleStr = NSString()
    var commentPost_ID = NSString()
    
     var allSupportPersonalArray = NSMutableArray()
     var allsupportFeedArray = NSMutableArray()
     var allsupportPeopleArray = NSMutableArray()
    var allsupportGroupArray = NSMutableArray()
     var allisLikesGroupArray = NSMutableArray()
     var allisLikesPeopleArray = NSMutableArray()
    var allisLikesFeedArray = NSMutableArray()
     var commentUsedID = String()
     var allisLikeArray = NSMutableArray()
    var personalTitleName = NSString()
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var tableViewobj: UITableView!
    
     // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.userNameLabel.text = self.usernameobjstr as String
        self.tableViewobj .register(UINib (nibName: "ImagePostCell", bundle: nil), forCellReuseIdentifier: "ImagePostCell")
        self.tableViewobj.reloadData()
    }
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.navstr == "NewPage"
        {
            self.userNameLabel.text = self.usernameobjstr as String
        }
        if self.navstr == "imageCommentStr"
        {
            self.userNameLabel.text = self.usernameobjstr as String
        }
        if self.navstr == "MyProgramPrsnlVC"
        {
            self.userNameLabel.text = self.usernameobjstr as String
        }
         sideMenuController?.isLeftViewSwipeGestureDisabled = true
        
    }
    
      // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.navstr == "imageCommentStr"
        {
            let indexPath = IndexPath(item: self.verticalOffsetIndex, section: 0)
            self.tableViewobj.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.downloadImage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView .dequeueReusableCell(withIdentifier: "ImagePostCell") as! ImagePostCell
        let progressIcon = UIActivityIndicatorView()
        progressIcon.translatesAutoresizingMaskIntoConstraints = false
        progressIcon.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        progressIcon.center = cell.imageViewObj.center
        cell.imageViewObj.addSubview(progressIcon)
        progressIcon.startAnimating()
        var constraints = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(
            item: progressIcon,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: cell.imageViewObj,
            attribute: .centerX,
            multiplier: 1,
            constant: 0)
        )
        constraints.append(NSLayoutConstraint(
            item: progressIcon,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: cell.imageViewObj,
            attribute: .centerY,
            multiplier: 1,
            constant: 0)
        )
        cell.imageViewObj.addConstraints(constraints)
        
        if downloadImage.count == 0 {
        }
        else
        {
            if self.commentsArray.count == 0 {
                
            }
            else
            {
                //                cell.userCommentOne.text = (self.commentsArray[0] as AnyObject) .value(forKey: "text") as? String
                //                cell.userCommentTwo.text = (self.commentsArray[1] as AnyObject) .value(forKey: "text") as? String
            }
            
            let imagedata : Data = UIImageJPEGRepresentation(self.downloadImage[indexPath.row], 0.7)!
            cell.imageViewObj.image = UIImage (data: imagedata)
            cell.commentActionBtn.tag = indexPath.row
            cell.commentActionBtn.addTarget(self, action: #selector(self.viewAllCommentBtnTapped), for: .touchUpInside)
            cell.commentCountBtn.tag = indexPath.row
            cell.commentCountBtn.addTarget(self, action: #selector(self.viewAllCommentBtnTapped), for: .touchUpInside)
            
            if cell.imageViewObj.image != nil
            {
                progressIcon.stopAnimating()
                progressIcon.hidesWhenStopped = true
            }
            else
            {
                progressIcon.hidesWhenStopped = false
            }
            
            let commentCountNO : NSNumber = self.commentsImagesArray[indexPath.row] as! NSNumber
            
            self.commentCoutStr = String(describing: commentCountNO) as NSString
            cell.commentCOuntLabel.text = self.commentCoutStr as String
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        SKCache.sharedCache.imageCache = CustomImageCache()
        let browser = SKPhotoBrowser(photos: createWebPhotos())
        browser.initializePageIndex(indexPath.row)
        browser.delegate = self
        present(browser, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 268
    }
    
    func createWebPhotos() -> [SKPhotoProtocol] {
        return (0..<self.downloadImage.count).map { (i: Int) -> SKPhotoProtocol in
            let photo = SKPhoto.photoWithImage(self.downloadImage[i])
            //let photo = SKPhoto.photoWithImageURL(IMAGE_UPLOAD .appending(self.responsePostimageArray[i] as! String))
            // photo.caption = caption[i%10]
            photo.shouldCachePhotoURLImage = true
            return photo
        }
    }
    
    func viewAllCommentBtnTapped(_ sender: UIButton) {
        if self.navstr == "dashBoardComment" {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
         if self.navstr == "dashboarpersonalprofileComment" {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
         if self.navstr == "worldViewFeedprofileComment" {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
         if self.navstr == "WorldViewGroupCommentPF" {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
         if self.navstr == "WorldIndividualCMTView" {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
            
        }
        if self.navstr == "networkpeopleCMTView" {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "PeoplesNewPageCMTView" {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "personalUserPost" {
            
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCallingPostUser(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "personalUserPostUpdateComment" {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCallingPostUser(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "imageCommentStr"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCallingPostUser(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "NewPage"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "updateDashboardNewsImage" {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "PeoplesNewPage"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "PeoplesimageCommentStr"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "GroupPageVC"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "GroupPageimageCommentStr"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "MyProgramFeedVC"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCallingPostUser(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "MyProgramFeedCommentStr"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCallingPostUser(param: params, andSelectedRow: sender.tag)
            
        }
        if self.navstr == "MyProgramPrsnlVC"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCallingPostUser(param: params, andSelectedRow: sender.tag)
            
        }
        if self.navstr == "MyProgramFeedCommentStr"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCallingPostUser(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "WorldViewGroup"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "joinProgram"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "joinProgramVC" {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "WorldViewGroupImageVC"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCallingPostUser(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "WorldViewpersonalGroup"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
            
        }
        if self.navstr == "WorldViewGrouppersonalImageVC"
        {
            
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "WorldViewIndividualVC"
        {
            
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
        }
        if self.navstr == "WorldViewIndividualpersonalImageVC"
        {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewobj)
            let indexPathRow = self.tableViewobj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            self.imageStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "image_id") as AnyObject) as! NSString
            self.PostStr_ID = ((self.imageResponseArray[sender.tag] as AnyObject) .value(forKey: "post_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.PostStr_ID as AnyObject,
                "image_id" : self.imageStr_ID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
            
        }
        
    }
    
    func CommentsApiCalling(param:[String: AnyObject],andSelectedRow selectedRow: Int)
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true
        
        {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let url = NSURL(string:BaseURL_AllCommentsAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                 if self.navstr == "dashBoardComment" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "dashBoardComment"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.allcommentCountArray =  self.allcommentCountArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "dashboarpersonalprofileComment" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "dashboarpersonalprofileComment"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.allcommentCountArray =  self.allcommentCountArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.personalTitleName = self.personalTitleName
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                 if self.navstr == "worldViewFeedprofileComment" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "worldViewFeedprofileComment"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.allcommentCountArray =  self.allcommentCountArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.personalTitleName = self.personalTitleName
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                 if self.navstr == "WorldViewGroupCommentPF" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "WorldViewGroupCommentPF"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.allcommentCountArray =  self.allcommentCountArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.personalTitleName = self.personalTitleName
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                
                                 if self.navstr == "WorldIndividualCMTView" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "WorldIndividualCMTView"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.allcommentCountArray =  self.allcommentCountArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.personalTitleName = self.personalTitleName
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "networkpeopleCMTView" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "networkpeopleCMTView"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.allcommentCountArray =  self.allcommentCountArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.personalTitleName = self.personalTitleName
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                 if self.navstr == "PeoplesNewPageCMTView" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "PeoplesNewPageCMTView"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.allcommentCountArray =  self.allcommentCountArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.personalTitleName = self.personalTitleName
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                if self.navstr == "NewPage"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "NewPage"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                  //  CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                   // CommentsVC.allcommentCountArray =  self.allcommentCountArray
                                   // CommentsVC.allSupportArray = self.allSupportArray
                                   //  CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "updateDashboardNewsImage" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "NewPage"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                  //  CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                  //  CommentsVC.allcommentCountArray =  self.allcommentCountArray
                                  //  CommentsVC.allSupportArray = self.allSupportArray
                                  //  CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "PeoplesNewPage"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "PeoplesNewPage"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.commentPost_ID = self.commentPost_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.allsupportPeopleArray = self.allsupportPeopleArray
                                     CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                    CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    CommentsVC.allisLikesPeopleArray = self.allisLikesPeopleArray
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "PeoplesimageCommentStr"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "PeoplesNewPage"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                     CommentsVC.commentPost_ID = self.commentPost_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                    CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                     CommentsVC.allsupportPeopleArray = self.allsupportPeopleArray
                                     CommentsVC.allisLikesPeopleArray = self.allisLikesPeopleArray
                                     CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                     CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "GroupPageVC"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "GroupPageVC"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programArray = self.programArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "GroupPageimageCommentStr"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "GroupPageVC"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programArray = self.programArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "WorldViewGroup"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "WorldViewGroup"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.allsupportGroupArray = self.allsupportGroupArray
                                    CommentsVC.programArray = self.programArray
                                    CommentsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                                    
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "joinProgram"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "joinProgram"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.programArray = self.programArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.allsupportGroupArray = self.allsupportGroupArray
                                    CommentsVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    CommentsVC.programArray = self.programArray
                                    CommentsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                                    
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                 if self.navstr == "joinProgramVC" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "joinProgramVC"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.programArray = self.programArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.allsupportGroupArray = self.allsupportGroupArray
                                    CommentsVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    CommentsVC.programArray = self.programArray
                                    CommentsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                                    
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "WorldViewGroupImageVC"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "WorldViewGroup"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.programArray = self.programArray
                                     CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.allsupportGroupArray = self.allsupportGroupArray
                                    CommentsVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    CommentsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                if self.navstr == "WorldViewpersonalGroup"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "WorldViewpersonalGroup"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.programArray = self.programArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                                    CommentsVC.allsupportGroupArray = self.allsupportGroupArray
                                    CommentsVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    CommentsVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                    CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                if self.navstr == "WorldViewGrouppersonalImageVC"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "WorldViewpersonalGroup"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.programArray = self.programArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.allsupportGroupArray = self.allsupportGroupArray
                                    CommentsVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    CommentsVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                     CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    CommentsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "WorldViewIndividualVC"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "WorldViewIndividualVC"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.programArray = self.programArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                     CommentsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                                    CommentsVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                     CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "WorldViewIndividualpersonalImageVC"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.navStr = "WorldViewIndividualVC"
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.programArray = self.programArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                                    CommentsVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                    CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                
                                
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func CommentsApiCallingPostUser(param:[String: AnyObject],andSelectedRow selectedRow: Int)
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true
        
        {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let url = NSURL(string:BaseURL_AllCommentsAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                 if self.navstr == "personalUserPost" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    CommentsVC.navStr = "userPostStr"
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                 if self.navstr == "personalUserPostUpdateComment" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    CommentsVC.navStr = "userPostStr"
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                     CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "imageCommentStr"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.navStr = "userPostStr"
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "MyProgramFeedVC"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                     CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                    CommentsVC.navStr = "MyProgramFeedVC"
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.program_idstr = self.program_idstr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                if self.navstr == "MyProgramFeedCommentStr"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                    CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                    CommentsVC.navStr = "MyProgramFeedVC"
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                if self.navstr == "MyProgramPrsnlVC"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                    CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC.navStr = "MyProgramPrsnlVC"
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.allcommentCountArray =  self.allcommentCountArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navstr == "MyProgramFeedCommentStr"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = ImageCommentViewController()
                                    CommentsVC.CommentCountMutableArr =  NSMutableArray(array:self.commentsImagesArray)
                                    CommentsVC.commentsArray = self.commentsArray
                                    CommentsVC.image_Idstr = self.imageStr_ID
                                    CommentsVC.post_idStr = self.PostStr_ID
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                    CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC.navStr = "MyProgramPrsnlVC"
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.imageResponseArray = self.imageResponseArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.usernameobjstr = self.usernameobjstr
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.verticalOffsetIndex = selectedRow
                                    CommentsVC.downloadImage = self.downloadImage
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                            
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func backBtnTapped(_ sender: Any) {
         DispatchQueue.main.async {
        if self.navstr == "dashBoardComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "dashBoardComment"
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.headerStr = self.headerStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "dashboarpersonalprofileComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "dashboarpersonalprofileComment"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "worldViewFeedprofileComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "worldViewFeedprofileComment"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "WorldViewGroupCommentPF" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "WorldViewGroupCommentPF"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "WorldIndividualCMTView" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "WorldViewIndividualVC"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "networkpeopleCMTView" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "networkpeople"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "PeoplesNewPageCMTView" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "PeoplesNewPage"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "PeoplesNewPage" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PeoplePostsViewController()
            dashboardVC.navStr = "PeoplesNewPageVC"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.allsupportPeopleArray = self.allsupportPeopleArray
             dashboardVC.allisLikesPeopleArray = self.allisLikesPeopleArray
            dashboardVC.commentPost_ID = self.commentPost_ID
            dashboardVC.peopleReg_idStr = self.peopleReg_idStr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.usernameObjstr = self.headerTitleStr
           
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        
        if self.navstr == "PeoplesimageCommentStr"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PeoplePostsViewController()
            dashboardVC.navStr = "PeoplesNewPageVC"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
             dashboardVC.allsupportPeopleArray = self.allsupportPeopleArray
             dashboardVC.allisLikesPeopleArray = self.allisLikesPeopleArray
            //  dashboardVC.verticalOffsetPostUser = verticalOffsetIndex
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.peopleReg_idStr = self.peopleReg_idStr
            dashboardVC.commentPost_ID = self.commentPost_ID
            // dashboardVC.userNameObjStr = self.userNameObjStr
             dashboardVC.usernameObjstr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        
        if self.navstr == "personalUserPost" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PersonalAccountViewController()
            dashboardVC.navStr = "NewPage"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.verticalOffsetPostUser = self.verticalOffsetIndex
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allisLikeArray = self.allisLikeArray
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.userNameObjStr = self.userNameObjStr
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.headerTitleStr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "personalUserPostUpdateComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PersonalAccountViewController()
            dashboardVC.navStr = "personalUserPostUpdateComment"
            dashboardVC.responsePostArray = self.responsePostArray
          //dashboardVC.responseUserPostsArray = self.responseUserPostsArray
          //dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.verticalOffsetPostUser = self.verticalOffsetIndex
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allisLikeArray = self.allisLikeArray
            dashboardVC.userID_Registered = self.userID_Registered
          //dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.userNameObjStr = self.userNameObjStr
            dashboardVC.allcommentCountArray = self.allcommentCountArray
          //dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.headerTitleStr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        
        
        if self.navstr == "userPostimageCommentStr" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PersonalAccountViewController()
            dashboardVC.navStr = "personalUserPost"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.headerTitleStr = self.headerTitleStr
            dashboardVC.verticalOffsetPostUser = self.verticalOffsetIndex
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.userNameObjStr = self.userNameObjStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "NewPage" {
            
            let mainViewController = self.sideMenuController!
            let dashboardVC = DashBoardViewController()
            dashboardVC.navStr = "NewPage"
            dashboardVC.selectedRow = self.selectedRow
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allSupportArray = self.allSupportArray
             dashboardVC.allisLikeArray = self.allisLikeArray
            dashboardVC.allcommentCountArray =  self.allcommentCountArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navstr == "updateDashboardNewsImage" {
           
            let mainViewController = self.sideMenuController!
            let dashboardVC = DashBoardViewController()
            dashboardVC.navStr = "DashboardVC"
           // dashboardVC.selectedRow = self.selectedRow
           // dashboardVC.responsePostArray = self.responsePostArray
           // dashboardVC.verticalOffset = self.verticalOffset
           // dashboardVC.allSupportArray = self.allSupportArray
           // dashboardVC.allisLikeArray = self.allisLikeArray
           // dashboardVC.allcommentCountArray =  self.allcommentCountArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
       
        }
        if self.navstr == "imageCommentStr"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = DashBoardViewController()
            dashboardVC.navStr = "Commentvc"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allisLikeArray = self.allisLikeArray
            dashboardVC.allcommentCountArray =  self.allcommentCountArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "GroupPageVC"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = GroupNewsViewController()
            dashboardVC.navStr = "Commentvc"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programArray = self.programArray
            dashboardVC.allsupportGroupArray = self.allsupportGroupArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navstr == "GroupPageimageCommentStr"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = GroupNewsViewController()
            dashboardVC.navStr = "Commentvc"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programArray = self.programArray
            dashboardVC.allsupportGroupArray = self.allsupportGroupArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "MyProgramFeedVC"
        {
            let mainViewController = self.sideMenuController!
            let ProgramSwipeVC = ProgramSwipeViewController()
            ProgramSwipeVC.navStr = "MyProgramFeedVC"
            ProgramSwipeVC.responsePostArray = self.responsePostArray
            ProgramSwipeVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            ProgramSwipeVC.allsupportFeedArray = self.allsupportFeedArray
            ProgramSwipeVC.allisLikesFeedArray = self.allisLikesFeedArray
            ProgramSwipeVC.verticalOffset = self.verticalOffset
            ProgramSwipeVC.program_idstr = self.program_idstr
             ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ProgramSwipeVC, animated: true)
        }
        if self.navstr == "MyProgramFeedCommentStr"
        {
            let mainViewController = self.sideMenuController!
            let ProgramSwipeVC = ProgramSwipeViewController()
            ProgramSwipeVC.navStr = "MyProgramFeedVC"
            //ProgramSwipeVC.responsePostArray = self.responsePostArray
            //ProgramSwipeVC.allsupportFeedArray = self.allsupportFeedArray
           // ProgramSwipeVC.allisLikesFeedArray = self.allisLikesFeedArray
           // ProgramSwipeVC.allFeedcommentCountArray = self.allFeedcommentCountArray
           // ProgramSwipeVC.verticalOffset = self.verticalOffset
            ProgramSwipeVC.program_idstr = self.program_idstr
            ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ProgramSwipeVC, animated: true)
            
        }
        if self.navstr == "MyProgramPrsnlVC"
        {
            let mainViewController = self.sideMenuController!
            let PersonalAccountVC = PersonalAccountViewController()
           // PersonalAccountVC.navStr = "MyProgramPrsnlImgVC"
            PersonalAccountVC.navStr = "MyProgramPrsnlVC"
            PersonalAccountVC.responsePostArray = self.responsePostArray
           // PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
           // PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            PersonalAccountVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            PersonalAccountVC.allsupportFeedArray = self.allsupportFeedArray
            PersonalAccountVC.allisLikesFeedArray = self.allisLikesFeedArray
            PersonalAccountVC.headerTitleStr = self.headerTitleStr
            PersonalAccountVC.verticalOffsetPostUser = self.verticalOffsetIndex
            PersonalAccountVC.verticalOffset = self.verticalOffset
            PersonalAccountVC.userNameObjStr = self.userNameObjStr
            PersonalAccountVC.program_idstr = self.program_idstr
            PersonalAccountVC.userID_Registered = self.userID_Registered
            PersonalAccountVC.headerTitleStr = self.headerTitleStr
            //PersonalAccountVC.userNameObjStr = self.userNameObjStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(PersonalAccountVC, animated: true)
        }
        if self.navstr == "MyProgramPrsnlImageVC"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PersonalAccountViewController()
            dashboardVC.navStr = "MyProgramPrsnlImgVC"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            dashboardVC.allsupportFeedArray = self.allsupportFeedArray
            dashboardVC.headerTitleStr = self.headerTitleStr
            //dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            //dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.verticalOffsetPostUser = self.verticalOffsetIndex
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.userNameObjStr = self.userNameObjStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.headerTitleStr = self.headerTitleStr
            dashboardVC.userNameObjStr = self.userNameObjStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "WorldViewGroup"
        {
            
            let mainViewController = self.sideMenuController!
            let GroupNewsVC = GroupNewsViewController()
            GroupNewsVC.navStr = "WorldViewGroupImages"
            GroupNewsVC.responsePostArray = self.responsePostArray
            GroupNewsVC.verticalOffset = self.verticalOffset
            GroupNewsVC.groupIDStr = self.groupIDStr
            GroupNewsVC.headerNameStr = self.headerNameStr
            GroupNewsVC.groupImageStr = self.groupImageStr
            GroupNewsVC.program_idstr = self.program_idstr
            GroupNewsVC.programArray = self.programArray
            GroupNewsVC.programNamestr = self.programNamestr
            GroupNewsVC.allsupportGroupArray = self.allsupportGroupArray
            GroupNewsVC.allisLikesGroupArray = self.allisLikesGroupArray
            GroupNewsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(GroupNewsVC, animated: true)
        }
        if self.navstr == "joinProgram"
        {
            let mainViewController = self.sideMenuController!
            let GroupNewsVC = GroupNewsViewController()
            GroupNewsVC.navStr = "joinProgram"
            GroupNewsVC.responsePostArray = self.responsePostArray
            GroupNewsVC.verticalOffset = self.verticalOffset
            GroupNewsVC.groupIDStr = self.groupIDStr
            GroupNewsVC.programArray = self.programArray
            GroupNewsVC.headerNameStr = self.headerNameStr
            GroupNewsVC.groupImageStr = self.groupImageStr
            GroupNewsVC.program_idstr = self.program_idstr
            GroupNewsVC.programArray = self.programArray
            GroupNewsVC.programNamestr = self.programNamestr
            GroupNewsVC.allsupportGroupArray = self.allsupportGroupArray
            GroupNewsVC.allisLikesGroupArray = self.allisLikesGroupArray
            GroupNewsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(GroupNewsVC, animated: true)
            
        }
         if self.navstr == "joinProgramVC" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PersonalAccountViewController()
            dashboardVC.navStr = "joinProgram"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.verticalOffsetPostUser = self.verticalOffsetIndex
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.userNameObjStr = self.userNameObjStr
             dashboardVC.program_idstr = self.program_idstr
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
             dashboardVC.programArray = self.programArray
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.allsupportGroupArray = self.allsupportGroupArray
             dashboardVC.allisLikesGroupArray = self.allisLikesGroupArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.headerTitleStr = self.headerTitleStr
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "WorldViewGroupImageVC"
        {
            let mainViewController = self.sideMenuController!
            let GroupNewsVC = GroupNewsViewController()
            GroupNewsVC.navStr = "WorldViewGroup"
           // GroupNewsVC.responsePostArray = self.responsePostArray
            GroupNewsVC.verticalOffset = self.verticalOffset
            GroupNewsVC.groupIDStr = self.groupIDStr
            GroupNewsVC.headerNameStr = self.headerNameStr
            GroupNewsVC.groupImageStr = self.groupImageStr
            GroupNewsVC.program_idstr = self.program_idstr
            GroupNewsVC.programArray = self.programArray
            GroupNewsVC.programNamestr = self.programNamestr
           // GroupNewsVC.allsupportGroupArray = self.allsupportGroupArray
            //GroupNewsVC.allisLikesGroupArray = self.allisLikesGroupArray
            GroupNewsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(GroupNewsVC, animated: true)
        }
        if self.navstr == "WorldViewpersonalGroup"
        {
            
            let mainViewController = self.sideMenuController!
            let dashboardVC = PersonalAccountViewController()
            //dashboardVC.navStr = "WorldViewpersonalGroup"
            dashboardVC.navStr = "WorldViewGroup"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.headerTitleStr = self.headerTitleStr
            dashboardVC.verticalOffsetPostUser = self.verticalOffsetIndex
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.userNameObjStr = self.userNameObjStr
           // dashboardVC.program_idstr = self.program_idstr
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
           // dashboardVC.programArray = self.programArray
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.allsupportGroupArray = self.allsupportGroupArray
               dashboardVC.allisLikesGroupArray = self.allisLikesGroupArray
           // dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
           // dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
           // dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "WorldViewGrouppersonalImageVC"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PersonalAccountViewController()
            dashboardVC.navStr = "WorldViewpersonalGroup"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.headerTitleStr = self.headerTitleStr
            dashboardVC.verticalOffsetPostUser = self.verticalOffsetIndex
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.userNameObjStr = self.userNameObjStr
            dashboardVC.headerNameStr = self.headerNameStr
            // dashboardVC.program_idstr = self.program_idstr
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            // dashboardVC.programArray = self.programArray
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.userID_Registered = self.userID_Registered
             dashboardVC.allsupportGroupArray = self.allsupportGroupArray
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navstr == "WorldViewIndividualVC"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PersonalAccountViewController()
            dashboardVC.navStr = "WorldViewIndividualVC"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.headerTitleStr = self.headerTitleStr
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.verticalOffsetPostUser = self.verticalOffsetIndex
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.userNameObjStr = self.userNameObjStr
            // dashboardVC.program_idstr = self.program_idstr
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.headerTitleStr = self.headerTitleStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            // dashboardVC.programArray = self.programArray
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.userID_Registered = self.userID_Registered
             dashboardVC.WorldViewheaderTitleStr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navstr == "WorldViewIndividualpersonalImageVC"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PersonalAccountViewController()
            dashboardVC.navStr = "WorldViewIndividualVC"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.headerTitleStr = self.headerTitleStr
             dashboardVC.headerTitleStr = self.headerTitleStr
            
            dashboardVC.verticalOffsetPostUser = self.verticalOffsetIndex
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.userNameObjStr = self.userNameObjStr
            // dashboardVC.program_idstr = self.program_idstr
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            // dashboardVC.programArray = self.programArray
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.userID_Registered = self.userID_Registered
             dashboardVC.WorldViewheaderTitleStr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
    }
    }
}

// MARK: - SKPhotoBrowserDelegate



extension ImageTableViewController {
    func didDismissAtPageIndex(_ index: Int) {
    }
    
    func didDismissActionSheetWithButtonIndex(_ buttonIndex: Int, photoIndex: Int) {
    }
    
    func removePhoto(index: Int, reload: (() -> Void)) {
        SKCache.sharedCache.removeImageForKey("somekey")
        reload()
    }
}


