  //
//  CommentsViewController.swift
//  Verdentum
//
//  Created by Verdentum on 04/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import SDWebImage
import LGSideMenuController
  
class CommentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate {
    var headerStr = String()
     var commentUsedID = String()
    var arrMessage = NSMutableArray()
    var commentArray = NSMutableArray()
    var post_id = NSString()
    var placeholderLabel : UILabel!
    var verticalOffset: Int = 0
    var verticalOffsetPostUser: Int = 0
    var responsePostArray = NSMutableArray()
    var lastpostIDStr = NSString()
    var navstr = NSString()
    var responseUserPostsArray = NSMutableArray()
    var commentPost_ID = NSString()
    var userCommentIDstr = NSString()
    var userNameObjStr = NSString()
    var peopleReg_idStr = NSString()
    var headerNameStr = NSString()
    var groupImageStr = NSString()
    var groupIDStr = NSString()
    var program_idstr = NSString()
    var userID_Registered = NSString()
    var programNamestr = NSString()
    var allcommentCountArray = NSMutableArray()
    var allPersonalcommentsArray = NSMutableArray()
    var WorldViewheaderTitleStr = NSString()
    var allFeedcommentCountArray = NSMutableArray()
    var allSupportArray = NSMutableArray()
    var headerTitleStr = NSString()
    var allSupportPersonalArray = NSMutableArray()
    var allsupportFeedArray = NSMutableArray()
    var allsupportPeopleArray = NSMutableArray()
    var allsupportGroupArray = NSMutableArray()
     var allisLikesPeopleArray = NSMutableArray()
     var allisLikesGroupArray = NSMutableArray()
     var programArray = NSMutableArray()
    var personalTitleName = NSString()
    var commentpost_ID = String()
      var allisLikesPersonalArray = NSMutableArray()
     var allisLikeArray = NSMutableArray()
    var allisLikesFeedArray = NSMutableArray()
    @IBOutlet var chatTableViewObj: UITableView!
    @IBOutlet var dummyHeightConstarint: NSLayoutConstraint!
    @IBOutlet var maxHeight: NSLayoutConstraint!
    @IBOutlet var minHeight: NSLayoutConstraint!
    @IBOutlet var equalHeight: NSLayoutConstraint!
    @IBOutlet var chatTextViewOBj: UITextField!
    
    @IBOutlet var sendBtnOBj: UIButton!
    
    @IBOutlet var imageViewSendObj: UIImageView!
    
    
    
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.sendBtnOBj.isEnabled = false
        self.imageViewSendObj.image = UIImage (named: "sentHide")
        
        self.chatTableViewObj.estimatedRowHeight = 113
        self.chatTableViewObj.rowHeight = UITableViewAutomaticDimension
        
        self.chatTableViewObj.register(UINib(nibName: "ConversationCell", bundle: nil), forCellReuseIdentifier: "ConversationCell")
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CommentsViewController.dismissKeyboard))
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        self.chatTableViewObj.reloadData()
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
     // MARK: - viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        sideMenuController?.isLeftViewSwipeGestureDisabled = true
        chatTableViewObj.estimatedRowHeight = 44
        chatTableViewObj.rowHeight = UITableViewAutomaticDimension
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Show and Hide Keyboard
    
    func keyboardWillShow(_ notification: Notification) {
        let info: [AnyHashable: Any]? = notification.userInfo
        let animationDuration = CDouble(info?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? TimeInterval())
        let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect ?? CGRect.zero
        let window = UIApplication.shared.windows[0] as? UIWindow ?? UIWindow()
        let mainSubviewOfWindow: UIView? = window.rootViewController?.view
        let keyboardFrameConverted: CGRect = (mainSubviewOfWindow?.convert(keyboardFrame, from: window as? UIView ?? UIView()))!
        let tableSize: CGSize = chatTableViewObj.contentSize
        chatTableViewObj.contentOffset = CGPoint(x: 0, y: tableSize.height)
        dummyHeightConstarint.constant = keyboardFrameConverted.size.height
        UIView.animate(withDuration: animationDuration, animations: {() -> Void in
            self.view.layoutIfNeeded()
            
        })
    }
    
    func keyboardWillHide(_ notification: Notification) {
        let info: [AnyHashable: Any]? = notification.userInfo
        let animationDuration = CDouble(info?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? TimeInterval())
        view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.layoutIfNeeded()
        dummyHeightConstarint.constant = 0
        UIView.animate(withDuration: animationDuration, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.commentArray.count == 0 {
            let rect = CGRect(x: 0,
                              y: 0,
                              width: view.bounds.size.width,
                              height: view.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            
            noDataLabel.text = "No Comment found"
            noDataLabel.textColor = UIColor.black
            noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            noDataLabel.sizeToFit()
            noDataLabel.textAlignment = NSTextAlignment.center
            self.chatTableViewObj.backgroundView = noDataLabel
            
            return 0
        }
        else
        {
            let rect = CGRect(x: 0,
                              y: 0,
                              width: view.bounds.size.width,
                              height: view.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            
            noDataLabel.text = ""
            noDataLabel.textColor = UIColor.black
            noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            noDataLabel.sizeToFit()
            noDataLabel.textAlignment = NSTextAlignment.center
            self.chatTableViewObj.backgroundView = noDataLabel
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ConversationCell = tableView.dequeueReusableCell(withIdentifier: "ConversationCell") as! ConversationCell
        
        let url = URL(string: IMAGE_UPLOAD.appending(((self.commentArray[indexPath.row] as AnyObject).value(forKey: "avatar") as? String!)!))
        cell.imageViewObj.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        cell.imageViewObj.layer.borderWidth = 1.0
        cell.imageViewObj.layer.borderColor = UIColor.gray.cgColor
        cell.imageViewObj.layer.cornerRadius = cell.imageViewObj.bounds.width/2
        cell.imageViewObj.layer.masksToBounds = true
        
        let titlename: String = (self.commentArray[indexPath.row] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.commentArray[indexPath.row] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.commentArray[indexPath.row] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.commentArray[indexPath.row] as AnyObject).value(forKey: "reg_lname") as! String
        
        let imagePathstr : String = (self.commentArray[indexPath.row] as AnyObject).value(forKey: "image") as! String
        
        if imagePathstr.isEqual("") {
            cell.imageHeightConstant.constant = 0
        }
        else {
            cell.imageHeightConstant.constant = 150
            let imageURLstr : NSString = IMAGE_UPLOAD.appending(((self.commentArray[indexPath.row]  as AnyObject).value(forKey: "image") as? String!)!) as NSString
            let imageurl = URL(string: imageURLstr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            
            cell.commentImageObj.sd_setImage(with: imageurl!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                
            })
        }
      
        let usernameobjstr : String = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as String
        cell.nameLabelObj.text = usernameobjstr
        
        let capitalized:String = (cell.nameLabelObj.text!.capitalized)
        cell.nameLabelObj.text = capitalized
        
        let myDate = (self.commentArray[indexPath.row] as AnyObject).value(forKey: "date") as? String
        let myDateString = myDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        let Dateobj = dateFormatter.date(from: myDateString!)!
        dateFormatter.dateFormat = "MMM dd, YYYY HH:mm a"
        let somedateString = dateFormatter.string(from: Dateobj)
        cell.timeAndDateObj.text = somedateString
        
        cell.commentLabelObj.text = (self.commentArray[indexPath.row] as AnyObject).value(forKey: "text") as? String
        cell.profileViewBtn.tag = indexPath.row
        cell.profileViewBtn.addTarget(self, action: #selector(self.commentProfileView), for: .touchUpInside)
        return cell
    }
    
    //MARK: - Comment Profile API Calling
    
    func commentProfileView(_ sender: UIButton) {
     // print(self.commentArray)
        
        
        let titlename: String = (self.commentArray[sender.tag] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.commentArray[sender.tag] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.commentArray[sender.tag] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.commentArray[sender.tag] as AnyObject).value(forKey: "reg_lname") as! String
        
        
        let usernameobjstr : String = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as String
        
        let comment_userID : String = ((self.commentArray .object(at: sender.tag) as AnyObject) .value(forKey: "user_id") as AnyObject) as! String
        
        let userID : NSString = UserDefaults.standard.value(forKey: "userID") as! NSString
        
        if  userID .isEqual(to: comment_userID){
            let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
            CommentsVC.commentUsedID = comment_userID
            CommentsVC.headerStr = usernameobjstr
            CommentsVC.navStr = "dashBoardComment"
            self.navigationController?.pushViewController(CommentsVC , animated: true)
        }
        else {
            if self.navstr == "NewPage" {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navstr == "dashBoardComment" {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navstr == "userpostvc" {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.userID_Registered = self.userID_Registered
                CommentsVC.personalTitleName = self.personalTitleName
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashboarpersonalprofileComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navstr == "MyProgramFeedVC" {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.program_idstr = self.program_idstr
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "worldViewFeedprofileComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navstr == "peoplepostVC"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navstr == "PeoplesNewPage"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "PeoplesNewPage"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navstr == "networkpeople"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "networkpeople"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
                
            }
            if self.navstr ==  "GroupPageVC"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navstr ==  "WorldViewGroup"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.program_idstr = self.program_idstr
                CommentsVC.programNamestr = self.programNamestr
                CommentsVC.navStr = "WorldViewGroupCommentPF"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navstr == "joinProgram"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navstr ==  "WorldViewpersonalGroup"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navstr ==  "WorldViewIndividualVC"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.program_idstr = self.program_idstr
                CommentsVC.programNamestr = self.programNamestr
                CommentsVC.navStr = "WorldViewIndividualVC"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
                
            }
        }
        
       
    }
    
    
    // MARK: - ScrollView
    
    func scrollTableView(atCurrentMessage arrayObj: [Any]) {
        // [self.converstionTableView reloadData];
        let lastRowNumber = Int(chatTableViewObj.numberOfRows(inSection: 0)) - 1
        if lastRowNumber >= 0 {
            let ip = IndexPath(row: lastRowNumber, section: 0)
            chatTableViewObj.scrollToRow(at: ip, at: .bottom, animated: false)
            //   [self.converstionTableView reloadData];
        }
    }
    
    func scrollingTableView(atCurrentMessage arrayObj: [Any]) {
        chatTableViewObj.reloadData()
        let lastRowNumber = Int(chatTableViewObj.numberOfRows(inSection: 0)) - 1
        if lastRowNumber >= 0 {
            let ip = IndexPath(row: lastRowNumber, section: 0)
            chatTableViewObj.scrollToRow(at: ip, at: .bottom, animated: false)
            //   [self.converstionTableView reloadData];
        }
    }
    
   
    
    // MARK: - UITextfield Delegate Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.sendBtnOBj.isEnabled = true
        self.imageViewSendObj.image = UIImage (named: "sentShow")
        if (string == "\n") {
            textField.resignFirstResponder()
            return false
        }
      
        return true
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    func getTextFieldString(_ string: String) {
        var searchString: String
        if (string.characters.count ) > 0 {
            searchString = "\(chatTextViewOBj.text)\(string)"
        }
        else {
            searchString = ((chatTextViewOBj.text as NSString?)?.substring(to: (chatTextViewOBj.text?.characters.count ?? 0) - 1))!
        }
    }
    
 
     // MARK: - SendBtnTapped
    
    @IBAction func sendBtnTappedOBj(_ sender: Any) {
        
    if !(self.chatTextViewOBj.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "addcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.post_id as AnyObject,
                "comment" : self.chatTextViewOBj.text as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params)
            self.chatTextViewOBj.text = ""
        }
        else {
            self.presentAlertWithTitle(title: "", message:"Please Enter Comment")
        }
        
    }
    
    func CommentsApiCalling(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
        
            Alamofire.request(BaseURL_AddCommentsAPI, method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let JSON = response.result.value as? NSDictionary {
                        //print(JSON)
                        if (JSON["status"] as AnyObject) .isEqual(to: 1)
                        {
                            self.commentArray.insert((JSON["comment"] as AnyObject), at: 0)
                            self.chatTableViewObj.reloadData()
                              self.sendBtnOBj.isEnabled = false
                             self.imageViewSendObj.image = UIImage (named: "sentHide")
                        }
                        else {
                            
                        }
                    }
            }
        }
        else {
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
       
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        if self.navstr == "dashBoardComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "dashBoardComment"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.commentUsedID = self.commentUsedID
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "dashboarpersonalprofileComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "dashboarpersonalprofileComment"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navstr == "worldViewFeedprofileComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "worldViewFeedprofileComment"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
           
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "WorldViewGroupCommentPF" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "WorldViewGroupCommentPF"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.programNamestr = self.programNamestr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "WorldIndividualCMTView" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "WorldViewIndividualVC"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "networkpeopleCMTView" {
            
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "networkpeople"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "PeoplesNewPageCMTView" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "PeoplesNewPage"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "worldVCGroupprofileComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = CommentProfileVC()
            dashboardVC.navStr = "worldViewFeedprofileComment"
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.personalTitleName = self.personalTitleName
            
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navstr == "userpostvc"
        {
            self.commentPost_ID = ((self.responseUserPostsArray[(sender as AnyObject).tag] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "userpost" as AnyObject,
                "id" : self.commentPost_ID as AnyObject,
                "myid" : userID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.userpostsApiCalling(param: params)
        }
        if self.navstr == "NewPage" 
        {
            //  homeServiceApiCalling()
            let mainViewController = self.sideMenuController!
            let dashboardVC = DashBoardViewController()
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.responsePostArray = self.responsePostArray
            let commentStrupdate : NSString = String(self.commentArray.count) as NSString
            self.allcommentCountArray.replaceObject(at: self.verticalOffset, with: commentStrupdate)
            dashboardVC.allcommentCountArray =  self.allcommentCountArray
             dashboardVC.allisLikeArray = self.allisLikeArray
            dashboardVC.navStr = "NewPage"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: false)
        }
        if self.navstr == "peoplepostVC"
        {
            self.commentPost_ID = ((self.responsePostArray[(sender as AnyObject).tag] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "userpost" as AnyObject,
                "id" : self.commentPost_ID as AnyObject,
                "myid" : userID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.userpostsApiCalling(param: params)
        }
        if self.navstr == "PeoplesNewPage"
        {
            self.commentPost_ID = ((self.responsePostArray[(sender as AnyObject).tag] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "userpost" as AnyObject,
                "id" : self.peopleReg_idStr as AnyObject,
                "myid" : userID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.userpostsApiCalling(param: params)
        }
        if self.navstr == "networkpeople"
        {
            self.commentPost_ID = ((self.responsePostArray[(sender as AnyObject).tag] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "userpost" as AnyObject,
                "id" : self.peopleReg_idStr as AnyObject,
                "myid" : userID as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.userpostsApiCalling(param: params)
        }
        if self.navstr ==  "GroupPageVC"
        {
            let mainViewController = self.sideMenuController!
            let GroupNewsVC = GroupNewsViewController()
            GroupNewsVC.navStr = "GroupPageVC"
            GroupNewsVC.responsePostArray = self.responsePostArray
            GroupNewsVC.verticalOffset = self.verticalOffset
            GroupNewsVC.headerNameStr = self.headerNameStr
            GroupNewsVC.groupImageStr = self.groupImageStr
            GroupNewsVC.groupIDStr = self.groupIDStr
             GroupNewsVC.allsupportGroupArray = self.allsupportGroupArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(GroupNewsVC, animated: true)
        }
        if self.navstr ==  "WorldViewGroup"
        {
            let mainViewController = self.sideMenuController!
            let GroupNewsVC = GroupNewsViewController()
            GroupNewsVC.navStr = "WorldViewGroup"
            GroupNewsVC.responsePostArray = self.responsePostArray
            GroupNewsVC.verticalOffset = self.verticalOffset
            GroupNewsVC.headerNameStr = self.headerNameStr
            GroupNewsVC.groupImageStr = self.groupImageStr
            GroupNewsVC.groupIDStr = self.groupIDStr
            GroupNewsVC.program_idstr = self.program_idstr
            GroupNewsVC.programNamestr = self.programNamestr
            GroupNewsVC.allsupportGroupArray = self.allsupportGroupArray
            GroupNewsVC.allisLikesGroupArray = self.allisLikesGroupArray
            GroupNewsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(GroupNewsVC, animated: true)
        }
        if self.navstr == "joinProgramGroup"
        {
            let mainViewController = self.sideMenuController!
            let PeopleVC = PersonalAccountViewController()
            PeopleVC.program_idstr = self.program_idstr
            PeopleVC.commentPost_ID = self.post_id
            PeopleVC.verticalOffset = self.verticalOffset
            PeopleVC.navStr = "joinProgram"
            //  PeopleVC.responseUserPostsArray = self.responseUserPostsArray
            PeopleVC.commentPost_ID = self.userCommentIDstr
            PeopleVC.programArray = programArray
            PeopleVC.userID_Registered = self.userID_Registered
            PeopleVC.program_idstr = self.program_idstr
            PeopleVC.programNamestr = self.programNamestr
            PeopleVC.groupIDStr = self.groupIDStr
            PeopleVC.headerNameStr = self.headerNameStr
            PeopleVC.groupImageStr = self.groupImageStr
            PeopleVC.headerTitleStr = self.headerTitleStr
            PeopleVC.allsupportGroupArray = self.allsupportGroupArray
            PeopleVC.allisLikesGroupArray = self.allisLikesGroupArray
            PeopleVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            PeopleVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            PeopleVC.allSupportPersonalArray = self.allSupportPersonalArray
            //PeopleVC.allisLikesGroupArray = self.allisLikesGroupArray
            PeopleVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(PeopleVC, animated: true)
        }
        
        if self.navstr == "joinProgram"
        {
            let mainViewController = self.sideMenuController!
            let PeopleVC = GroupNewsViewController()
            PeopleVC.program_idstr = self.program_idstr
            PeopleVC.commentPost_ID = self.post_id
            PeopleVC.verticalOffset = self.verticalOffset
            PeopleVC.navStr = "joinProgram"
            //  PeopleVC.responseUserPostsArray = self.responseUserPostsArray
            PeopleVC.commentPost_ID = self.userCommentIDstr
            PeopleVC.programArray = programArray
           // PeopleVC.userID_Registered = self.userID_Registered
            PeopleVC.program_idstr = self.program_idstr
            PeopleVC.programNamestr = self.programNamestr
            PeopleVC.groupIDStr = self.groupIDStr
            PeopleVC.headerNameStr = self.headerNameStr
            PeopleVC.groupImageStr = self.groupImageStr
           // PeopleVC.headerTitleStr = self.headerTitleStr
            PeopleVC.allsupportGroupArray = self.allsupportGroupArray
            PeopleVC.allisLikesGroupArray = self.allisLikesGroupArray
            PeopleVC.allPersonalcommentsArray = self.allPersonalcommentsArray
           // PeopleVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            PeopleVC.allSupportPersonalArray = self.allSupportPersonalArray
            //PeopleVC.allisLikesGroupArray = self.allisLikesGroupArray
            PeopleVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(PeopleVC, animated: true)
        }
        
        if self.navstr ==  "MyProgramFeedVC"
        {
            let mainViewController = self.sideMenuController!
            let ProgramSwipeVC = ProgramSwipeViewController()
            ProgramSwipeVC.navStr = "MyProgramFeedVC"
            ProgramSwipeVC.responsePostArray = self.responsePostArray
            let commentStrupdate : NSString = String(self.commentArray.count) as NSString
            self.allFeedcommentCountArray.replaceObject(at: self.verticalOffset, with: commentStrupdate)
            ProgramSwipeVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            ProgramSwipeVC.allsupportFeedArray = self.allsupportFeedArray
             ProgramSwipeVC.allisLikesFeedArray = self.allisLikesFeedArray
            ProgramSwipeVC.verticalOffset = self.verticalOffset
             ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            ProgramSwipeVC.program_idstr = self.program_idstr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ProgramSwipeVC, animated: true)
        }
        if self.navstr ==  "MyProgramPrsnlVC"
        {
            let mainViewController = self.sideMenuController!
            let PersonalAccountVC = PersonalAccountViewController()
            PersonalAccountVC.program_idstr = self.program_idstr
            PersonalAccountVC.commentPost_ID = self.post_id
            PersonalAccountVC.verticalOffset = self.verticalOffset
            PersonalAccountVC.navStr = "MyProgramPrsnlVC"
            PersonalAccountVC.responsePostArray = self.responsePostArray
            PersonalAccountVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            PersonalAccountVC.allisLikesFeedArray = self.allisLikesFeedArray
            PersonalAccountVC.allsupportFeedArray = self.allsupportFeedArray
            PersonalAccountVC.verticalOffset = self.verticalOffset
            PersonalAccountVC.commentPost_ID = self.userCommentIDstr
            PersonalAccountVC.userID_Registered = self.userID_Registered
            PersonalAccountVC.headerTitleStr = self.headerTitleStr
            PersonalAccountVC.userNameObjStr = self.userNameObjStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(PersonalAccountVC, animated: true)
            
            /* self.commentPost_ID = ((self.responsePostArray[(sender as AnyObject).tag] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
             let userID = UserDefaults.standard.value(forKey: "userID")
             let params:[String: AnyObject] = [
             "purpose" : "userpost" as AnyObject,
             "id" : self.peopleReg_idStr as AnyObject,
             "myid" : userID as AnyObject]
             MBProgressHUD.showAdded(to: self.view, animated: true)
             self.userpostsApiCalling(param: params)*/
            
        }
        if self.navstr ==  "WorldViewpersonalGroup"
        {
            let mainViewController = self.sideMenuController!
            let PeopleVC = PersonalAccountViewController()
            
            PeopleVC.program_idstr = self.program_idstr
            PeopleVC.commentPost_ID = self.post_id
            PeopleVC.verticalOffset = self.verticalOffset
            PeopleVC.navStr = "WorldViewpersonalGroup"
            PeopleVC.userNameObjStr = self.userNameObjStr
            PeopleVC.commentPost_ID = self.userCommentIDstr
            PeopleVC.userID_Registered = self.userID_Registered
            PeopleVC.program_idstr = self.program_idstr
            PeopleVC.programNamestr = self.programNamestr
            PeopleVC.groupIDStr = self.groupIDStr
            PeopleVC.headerNameStr = self.headerNameStr
            PeopleVC.groupImageStr = self.groupImageStr
            PeopleVC.allsupportGroupArray = self.allsupportGroupArray
            PeopleVC.allisLikesGroupArray = self.allisLikesGroupArray
            PeopleVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            PeopleVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            PeopleVC.allSupportPersonalArray = self.allSupportPersonalArray
            PeopleVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(PeopleVC, animated: true)
            
        }
        if self.navstr ==  "WorldViewIndividualVC"
        {
            let mainViewController = self.sideMenuController!
            let PeopleVC = PersonalAccountViewController()
            
            PeopleVC.program_idstr = self.program_idstr
            PeopleVC.commentPost_ID = self.post_id
            // PeopleVC.verticalOffset = self.verticalOffset
            PeopleVC.navStr = "WorldViewIndividualVC"
            PeopleVC.responseUserPostsArray = self.responseUserPostsArray
            let commentStrupdate : NSString = String(self.commentArray.count) as NSString
            self.allPersonalcommentsArray.replaceObject(at: self.verticalOffset, with: commentStrupdate)
            print( self.allPersonalcommentsArray)
            PeopleVC.allPersonalcommentsArray =  self.allPersonalcommentsArray
            PeopleVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            PeopleVC.allSupportPersonalArray = self.allSupportPersonalArray
            PeopleVC.verticalOffset = self.verticalOffsetPostUser
            PeopleVC.commentPost_ID = self.userCommentIDstr
            PeopleVC.userID_Registered = self.userID_Registered
            PeopleVC.program_idstr = self.program_idstr
            PeopleVC.programNamestr = self.programNamestr
            PeopleVC.groupIDStr = self.groupIDStr
            PeopleVC.headerNameStr = self.headerNameStr
            PeopleVC.headerTitleStr = self.headerTitleStr
            PeopleVC.groupImageStr = self.groupImageStr
            PeopleVC.WorldViewheaderTitleStr = self.headerTitleStr
            PeopleVC.allsupportGroupArray = self.allsupportGroupArray
           
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(PeopleVC, animated: true)
        }
    }
    
    //MARK: - UserPostsAPI Calling
    
    func userpostsApiCalling(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let url = NSURL(string:BaseURL_USERPOSTSAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                
                                self.responseUserPostsArray = (responsejson["posts"] as AnyObject) as! NSArray as! NSMutableArray
                                self.allSupportPersonalArray.removeAllObjects()
                                self.allPersonalcommentsArray.removeAllObjects()
                                self.allsupportPeopleArray.removeAllObjects()
                                for var i in 0..<self.responseUserPostsArray.count
                                {
                                    let commentsArr : NSArray = ((self.responseUserPostsArray [i] as AnyObject).value(forKey: "comments") as? NSArray)!
                                    self.allPersonalcommentsArray.add(String(commentsArr.count))
                                    let likesStr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "likes") as? String)!
                                    self.allSupportPersonalArray.add(likesStr)
                                    let islikestr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                    self.allisLikesPersonalArray.add(islikestr)
                                }
                                if (self.responseUserPostsArray .value(forKey: "post_id") as AnyObject).contains(self.userCommentIDstr) {
                                    
                                    if self.navstr == "userpostvc"
                                    {
                                        let mainViewController = self.sideMenuController!
                                        let dashboardVC = PersonalAccountViewController()
                                        dashboardVC.navStr = "NewPage"
                                        dashboardVC.responsePostArray = self.responsePostArray
                                        dashboardVC.responseUserPostsArray = self.responseUserPostsArray
                                        dashboardVC.verticalOffset = self.verticalOffset
                                        dashboardVC.verticalOffsetPostUser = self.verticalOffsetPostUser
                                         dashboardVC.allSupportArray = self.allSupportArray
                                        dashboardVC.allisLikeArray = self.allisLikeArray
                                        dashboardVC.allcommentCountArray = self.allcommentCountArray
                                        dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                        dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
                                        dashboardVC.userNameObjStr = self.userNameObjStr
                                        dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        dashboardVC.headerTitleStr = self.headerTitleStr
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(dashboardVC, animated: true)
                                    }
                                    if self.navstr == "peoplepostVC"
                                    {
                                        let mainViewController = self.sideMenuController!
                                        let CommentsVC = PeoplePostsViewController()
                                        // CommentsVC .commentArray = self.commentsArray
                                        // CommentsVC.post_id = self.commentPost_ID
                                        CommentsVC.navStr = "peopleCommentpostVC"
                                        CommentsVC.verticalOffset = self.verticalOffset
                                        CommentsVC.responsePostArray =  self.responseUserPostsArray
                                        CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(CommentsVC, animated: true)
                                    }
                                    if self.navstr == "PeoplesNewPage"
                                    {
                                        let mainViewController = self.sideMenuController!
                                        let CommentsVC = PeoplePostsViewController()
                                        // CommentsVC .commentArray = self.commentsArray
                                        // CommentsVC.post_id = self.commentPost_ID
                                        CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                        CommentsVC.navStr = "peopleVC"
                                        CommentsVC.verticalOffset = self.verticalOffset
                                        CommentsVC.responsePostArray =  self.responseUserPostsArray
                                        CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                        CommentsVC.allsupportPeopleArray = self.allSupportPersonalArray
                                        CommentsVC.allisLikesPeopleArray = self.allisLikesPersonalArray
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(CommentsVC, animated: true)
                                    }
                                    if self.navstr == "networkpeople"
                                    {
                                        let mainViewController = self.sideMenuController!
                                        let CommentsVC = PeoplePostsViewController()
                                        // CommentsVC .commentArray = self.commentsArray
                                        // CommentsVC.post_id = self.commentPost_ID
                                        CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                        CommentsVC.navStr = "networkpeople"
                                        CommentsVC.verticalOffset = self.verticalOffset
                                        CommentsVC.responsePostArray =  self.responseUserPostsArray
                                        CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                         CommentsVC.allsupportPeopleArray = self.allSupportPersonalArray
                                         CommentsVC.allisLikesPeopleArray = self.allisLikesPersonalArray
                                         CommentsVC.usernameObjstr = self.headerTitleStr
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(CommentsVC, animated: true)
                                    }
                                    if self.navstr ==  "MyProgramPrsnlVC"
                                    {
                                        let mainViewController = self.sideMenuController!
                                        let PeopleVC = PersonalAccountViewController()
                                        
                                        PeopleVC.program_idstr = self.program_idstr
                                        PeopleVC.commentPost_ID = self.post_id
                                        PeopleVC.verticalOffset = self.verticalOffset
                                        PeopleVC.navStr = "MyProgramPrsnlVC"
                                        PeopleVC.responseUserPostsArray = self.responseUserPostsArray
                                        PeopleVC.commentPost_ID = self.userCommentIDstr
                                        PeopleVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                        PeopleVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        PeopleVC.allSupportPersonalArray = self.allSupportPersonalArray
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(PeopleVC, animated: true)
                                    }
                                }
                                else
                                {
                                    
                                    if self.navstr == "userpostvc"
                                    {
                                        // self.commentPost_ID = ((self.responseUserPostsArray[indexPath.row] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
                                        
                                        let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                                        self.lastpostIDStr = postarray.lastObject as! NSString
                                        let userID = UserDefaults.standard.value(forKey: "userID")
                                        let params:[String: AnyObject] = [
                                            "purpose" : "userpost" as AnyObject,
                                            "id" : self.commentPost_ID as AnyObject,
                                            "direction" : "down" as AnyObject,
                                            "myid" : userID as AnyObject,
                                            "limit" : self.lastpostIDStr as AnyObject]
                                        MBProgressHUD.showAdded(to: self.view, animated: true)
                                        self.userpostsApiCalling1(param: params)
                                    }
                                    if self.navstr == "peoplepostVC"
                                    {
                                        let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                                        self.lastpostIDStr = postarray.lastObject as! NSString
                                        let userID = UserDefaults.standard.value(forKey: "userID")
                                        let params:[String: AnyObject] = [
                                            "purpose" : "userpost" as AnyObject,
                                            "id" : self.commentPost_ID as AnyObject,
                                            "direction" : "down" as AnyObject,
                                            "myid" : userID as AnyObject,
                                            "limit" : self.lastpostIDStr as AnyObject]
                                        MBProgressHUD.showAdded(to: self.view, animated: true)
                                        self.userpostsApiCalling1(param: params)
                                    }
                                    if self.navstr == "PeoplesNewPage"
                                    {
                                        let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                                        self.lastpostIDStr = postarray.lastObject as! NSString
                                        let userID = UserDefaults.standard.value(forKey: "userID")
                                        let params:[String: AnyObject] = [
                                            "purpose" : "userpost" as AnyObject,
                                            "id" : self.peopleReg_idStr as AnyObject,
                                            "direction" : "down" as AnyObject,
                                            "myid" : userID as AnyObject,
                                            "limit" : self.lastpostIDStr as AnyObject]
                                        MBProgressHUD.showAdded(to: self.view, animated: true)
                                        self.userpostsApiCalling1(param: params)
                                        
                                    }
                                    if self.navstr == "networkpeople"
                                    {
                                        let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                                        self.lastpostIDStr = postarray.lastObject as! NSString
                                        let userID = UserDefaults.standard.value(forKey: "userID")
                                        let params:[String: AnyObject] = [
                                            "purpose" : "userpost" as AnyObject,
                                            "id" : self.peopleReg_idStr as AnyObject,
                                            "direction" : "down" as AnyObject,
                                            "myid" : userID as AnyObject,
                                            "limit" : self.lastpostIDStr as AnyObject]
                                        MBProgressHUD.showAdded(to: self.view, animated: true)
                                        self.userpostsApiCalling1(param: params)
                                        
                                    }
                                    if self.navstr ==  "MyProgramPrsnlVC"
                                    {
                                        let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                                        self.lastpostIDStr = postarray.lastObject as! NSString
                                        let userID = UserDefaults.standard.value(forKey: "userID")
                                        let params:[String: AnyObject] = [
                                            "purpose" : "userpost" as AnyObject,
                                            "id" : self.peopleReg_idStr as AnyObject,
                                            "direction" : "down" as AnyObject,
                                            "myid" : userID as AnyObject,
                                            "limit" : self.lastpostIDStr as AnyObject]
                                        MBProgressHUD.showAdded(to: self.view, animated: true)
                                        self.userpostsApiCalling1(param: params)
                                    }
                                }
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    //MARK: - UserPostsAPI Calling
    
    func userpostsApiCalling1(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_USERPOSTSAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let arra  = (responsejson["posts"] as AnyObject) as! NSArray
                                if arra.count == 0
                                {
                                    
                                }
                                else
                                {
                                    self.responseUserPostsArray.addObjects(from: arra as! [Any])
                                    for var i in 0..<arra.count
                                    {
                                        let commentsArr : NSArray = ((arra [i] as AnyObject).value(forKey: "comments") as? NSArray)!
                                        self.allPersonalcommentsArray.add(String(commentsArr.count))
                                        let likesStr : String = ((arra[i] as AnyObject).value(forKey: "likes") as? String)!
                                        self.allSupportPersonalArray.add(likesStr)
                                        let islikestr : String = ((arra[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                        self.allisLikesPersonalArray.add(islikestr)
                                    }
                                    if (self.responseUserPostsArray .value(forKey: "post_id") as AnyObject).contains(self.userCommentIDstr) {
                                        
                                        if self.navstr == "userpostvc"
                                        {
                                            let mainViewController = self.sideMenuController!
                                            let dashboardVC = PersonalAccountViewController()
                                            dashboardVC.navStr = "NewPage"
                                            dashboardVC.responsePostArray = self.responsePostArray
                                            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
                                            dashboardVC.verticalOffset = self.verticalOffset
                                            dashboardVC.allcommentCountArray = self.allcommentCountArray
                                            dashboardVC.allSupportArray = self.allSupportArray
                                            dashboardVC.allisLikeArray = self.allisLikeArray
                                            dashboardVC.userNameObjStr = self.userNameObjStr
                                            dashboardVC.verticalOffsetPostUser = self.verticalOffsetPostUser
                                           
                                            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
                                            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                            dashboardVC.headerTitleStr = self.headerTitleStr
                                            let navigationController = mainViewController.rootViewController as! NavigationController
                                            navigationController.pushViewController(dashboardVC, animated: true)
                                        }
                                        if self.navstr == "peoplepostVC"
                                        {
                                            let mainViewController = self.sideMenuController!
                                            let CommentsVC = PeoplePostsViewController()
                                            // CommentsVC .commentArray = self.commentsArray
                                            // CommentsVC.post_id = self.commentPost_ID
                                            CommentsVC.navStr = "peopleCommentpostVC"
                                            CommentsVC.verticalOffset = self.verticalOffset
                                            CommentsVC.responsePostArray =  self.responseUserPostsArray
                                            CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                            let navigationController = mainViewController.rootViewController as! NavigationController
                                            navigationController.pushViewController(CommentsVC, animated: true)
                                            
                                        }
                                        if self.navstr == "PeoplesNewPage"
                                        {
                                            let mainViewController = self.sideMenuController!
                                            let CommentsVC = PeoplePostsViewController()
                                            // CommentsVC .commentArray = self.commentsArray
                                            // CommentsVC.post_id = self.commentPost_ID
                                            CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                            CommentsVC.navStr = "peopleVC"
                                            CommentsVC.verticalOffset = self.verticalOffset
                                            CommentsVC.responsePostArray =  self.responseUserPostsArray
                                            CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                            CommentsVC.allsupportPeopleArray = self.allSupportPersonalArray
                                            CommentsVC.allisLikesPeopleArray = self.allisLikesPersonalArray
                                            let navigationController = mainViewController.rootViewController as! NavigationController
                                            navigationController.pushViewController(CommentsVC, animated: true)
                                        }
                                        if self.navstr == "networkpeople"
                                        {
                                            let mainViewController = self.sideMenuController!
                                            let CommentsVC = PeoplePostsViewController()
                                            // CommentsVC .commentArray = self.commentsArray
                                            // CommentsVC.post_id = self.commentPost_ID
                                            CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                            CommentsVC.navStr = "networkpeople"
                                            CommentsVC.verticalOffset = self.verticalOffset
                                            CommentsVC.responsePostArray =  self.responseUserPostsArray
                                            CommentsVC.peopleReg_idStr = self.peopleReg_idStr
                                            CommentsVC.allsupportPeopleArray = self.allSupportPersonalArray
                                            CommentsVC.allisLikesPeopleArray = self.allisLikesPersonalArray
                                            CommentsVC.usernameObjstr = self.headerTitleStr
                                            let navigationController = mainViewController.rootViewController as! NavigationController
                                            navigationController.pushViewController(CommentsVC, animated: true)
                                        }
                                        if self.navstr ==  "MyProgramPrsnlVC"
                                        {
                                            let mainViewController = self.sideMenuController!
                                            let PeopleVC = PersonalAccountViewController()
                                            
                                            PeopleVC.program_idstr = self.program_idstr
                                            PeopleVC.commentPost_ID = self.post_id
                                            PeopleVC.verticalOffset = self.verticalOffset
                                            PeopleVC.navStr = "MyProgramPrsnlVC"
                                            PeopleVC.responseUserPostsArray = self.responseUserPostsArray
                                            PeopleVC.commentPost_ID = self.userCommentIDstr
                                            PeopleVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                            PeopleVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                            PeopleVC.allSupportPersonalArray = self.allSupportPersonalArray
                                            let navigationController = mainViewController.rootViewController as! NavigationController
                                            navigationController.pushViewController(PeopleVC, animated: true)
                                        }
                                    }
                                    else
                                    {
                                        if self.navstr == "userpostvc"
                                        {
                                            // self.commentPost_ID = ((self.responseUserPostsArray[indexPath.row] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
                                            let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                                            self.lastpostIDStr = postarray.lastObject as! NSString
                                            let userID = UserDefaults.standard.value(forKey: "userID")
                                            let params:[String: AnyObject] = [
                                                "purpose" : "userpost" as AnyObject,
                                                "id" : self.commentPost_ID as AnyObject,
                                                "direction" : "down" as AnyObject,
                                                "myid" : userID as AnyObject,
                                                "limit" : self.lastpostIDStr as AnyObject]
                                            MBProgressHUD.showAdded(to: self.view, animated: true)
                                            self.userpostsApiCalling(param: params)
                                        }
                                        if self.navstr == "peoplepostVC"
                                        {
                                            let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                                            self.lastpostIDStr = postarray.lastObject as! NSString
                                            let userID = UserDefaults.standard.value(forKey: "userID")
                                            let params:[String: AnyObject] = [
                                                "purpose" : "userpost" as AnyObject,
                                                "id" : self.commentPost_ID as AnyObject,
                                                "direction" : "down" as AnyObject,
                                                "myid" : userID as AnyObject,
                                                "limit" : self.lastpostIDStr as AnyObject]
                                            MBProgressHUD.showAdded(to: self.view, animated: true)
                                            self.userpostsApiCalling(param: params)
                                            
                                        }
                                        if self.navstr == "PeoplesNewPage"
                                        {
                                            let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                                            self.lastpostIDStr = postarray.lastObject as! NSString
                                            let userID = UserDefaults.standard.value(forKey: "userID")
                                            let params:[String: AnyObject] = [
                                                "purpose" : "userpost" as AnyObject,
                                                "id" : self.peopleReg_idStr as AnyObject,
                                                "direction" : "down" as AnyObject,
                                                "myid" : userID as AnyObject,
                                                "limit" : self.lastpostIDStr as AnyObject]
                                            MBProgressHUD.showAdded(to: self.view, animated: true)
                                            self.userpostsApiCalling(param: params)
                                        }
                                        if self.navstr == "networkpeople"
                                        {
                                            let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                                            self.lastpostIDStr = postarray.lastObject as! NSString
                                            let userID = UserDefaults.standard.value(forKey: "userID")
                                            let params:[String: AnyObject] = [
                                                "purpose" : "userpost" as AnyObject,
                                                "id" : self.peopleReg_idStr as AnyObject,
                                                "direction" : "down" as AnyObject,
                                                "myid" : userID as AnyObject,
                                                "limit" : self.lastpostIDStr as AnyObject]
                                            MBProgressHUD.showAdded(to: self.view, animated: true)
                                            self.userpostsApiCalling(param: params)
                                            
                                        }
                                        if self.navstr ==  "MyProgramPrsnlVC"
                                        {
                                            
                                            let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                                            self.lastpostIDStr = postarray.lastObject as! NSString
                                            let userID = UserDefaults.standard.value(forKey: "userID")
                                            let params:[String: AnyObject] = [
                                                "purpose" : "userpost" as AnyObject,
                                                "id" : self.peopleReg_idStr as AnyObject,
                                                "direction" : "down" as AnyObject,
                                                "myid" : userID as AnyObject,
                                                "limit" : self.lastpostIDStr as AnyObject]
                                            MBProgressHUD.showAdded(to: self.view, animated: true)
                                            self.userpostsApiCalling(param: params)
                                        }
                                    }
                                }
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
}
