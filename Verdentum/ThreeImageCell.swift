//
//  ThreeImageCell.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 22/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class ThreeImageCell: UICollectionViewCell {

    @IBOutlet var firstImageThreeBtn: UIButton!
    
    @IBOutlet var thirdImageThreeBtn: UIButton!
    @IBOutlet var secondImageThreeBtn: UIButton!
    @IBOutlet var imageViewone3: UIImageView!
    
    @IBOutlet var imageViewtwo3: UIImageView!
    
    @IBOutlet var imageviewthree3: UIImageView!
    
     #if os(tvOS)
    override func awakeFromNib() {
        super.awakeFromNib()
       imageViewone3.adjustsImageWhenAncestorFocused = true
         imageViewtwo3.adjustsImageWhenAncestorFocused = true
         imageviewthree3.adjustsImageWhenAncestorFocused = true
    }
     #endif
}
