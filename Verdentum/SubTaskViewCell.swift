//
//  SubTaskViewCell.swift
//  Verdentum
//
//  Created by Verdentum on 25/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class SubTaskViewCell: UITableViewCell {

    @IBOutlet var countLabel: UILabel!
    @IBOutlet var titleNameLabel: UILabel!
    @IBOutlet var heightLabelNumberObj: NSLayoutConstraint!
    @IBOutlet var numberHeightObj: NSLayoutConstraint!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet weak var numberTopHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelCountTopHeight: NSLayoutConstraint!
    @IBOutlet weak var topTitleLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var subViewHEightCell: NSLayoutConstraint!
    @IBOutlet weak var heightBottomline: NSLayoutConstraint!
    @IBOutlet var valueTextfieldObj: UITextField!
    @IBOutlet weak var desTopLabelHeigth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
