//
//  NewsImages5Cell.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 22/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class NewsImages5Cell: UICollectionViewCell {

    
    @IBOutlet var firstImageView: UIImageView!
    @IBOutlet var fivethImageView: UIImageView!
    
    @IBOutlet var secondImageView: UIImageView!
    
    @IBOutlet var fourthImageView: UIImageView!
    @IBOutlet var thirdImageView: UIImageView!
    
    
    @IBOutlet var firstimageBtn: UIButton!
    
    @IBOutlet var secondimageBtn: UIButton!
    
    @IBOutlet var thirdImageBtn: UIButton!
    
    @IBOutlet var fourthimageBtn: UIButton!
    
    @IBOutlet var fivthimageBtn: UIButton!
    
    
    
     #if os(tvOS)
    override func awakeFromNib() {
        super.awakeFromNib()
        
        firstImageView.adjustsImageWhenAncestorFocused = true
        secondImageView.adjustsImageWhenAncestorFocused = true
        thirdImageView.adjustsImageWhenAncestorFocused = true
        fourthImageView.adjustsImageWhenAncestorFocused = true
        fivethImageView.adjustsImageWhenAncestorFocused = true
        
        
    }
  #endif
}
