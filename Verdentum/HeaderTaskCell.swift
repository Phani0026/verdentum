//
//  HeaderTaskCell.swift
//  Verdentum
//
//  Created by Verdentum on 25/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class HeaderTaskCell: UITableViewCell,UITextFieldDelegate {
    @IBOutlet var nameLabel: UILabel!

    @IBOutlet var descriptionLabel: UILabel!
    
    @IBOutlet var hourLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    
    @IBOutlet var hourTextfieldHeight: NSLayoutConstraint!
    
    @IBOutlet var desptBottmLIneHeight: NSLayoutConstraint!
    @IBOutlet var deptLabelheight: NSLayoutConstraint!
    @IBOutlet var hourLabelHeight: NSLayoutConstraint!
    @IBOutlet var hoursTextfieldObj: UITextField!
    @IBOutlet var numberTextfieldObjs: UITextField!
    
    @IBOutlet weak var numberLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var numberTextfieldhieght: NSLayoutConstraint!
    
    @IBOutlet weak var numberBottomLineLabel: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
}
