//
//  DashBoardViewController.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 19/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import LGSideMenuController
import MBProgressHUD
import AlamofireImage
import Alamofire
import SDWebImage
import SKPhotoBrowser
import FirebaseMessaging
//import Reachability

class DashBoardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,SKPhotoBrowserDelegate {
    var downloadImage = [UIImage]()
    var selected = Bool()
    var storeImagesURl = [AnyObject]()
    var images = [SKPhotoProtocol]()
    var cell = WallCell()
    var verticalOffset: Int = 0
    var likeIndexOffSet: Int = 0
    var imageArrayURLCol = NSMutableArray()
    var refreshControl = UIRefreshControl()
    var responsePostArray = NSMutableArray()
    var responseUploadActivityArray = NSMutableArray()
    var responseUserPostsArray = NSMutableArray()
    var newimagesArray = NSMutableArray()
    private var lastContentOffset: CGFloat = 0
    var lastpostIDStr = NSString()
    var firstPostIDstr = NSString()
    var postusedID = NSString()
    var postID = NSString()
    var commentPost_ID = NSString()
    var result = NSString()
    var selectedRow = NSIndexPath()
    var navStr = NSString()
    var groupNameArray = NSArray()
    var commentsArray = NSMutableArray()
    var imagesArrayURl = NSArray()
    var commentsImagesArray = NSArray()
    var arrcontainstate = NSMutableArray()
    var imageResponseArray = NSArray()
    var usernameobjstr = NSString()
    var postUserNamestr = NSString()
    var checkImageORNotStr : String?
    var StatuStr = NSString()
    var allcommentCountArray = NSMutableArray()
    var allPersonalcommentsArray = NSMutableArray()
    var timer = Timer()
    var supportselectedStr = NSString()
    var allSupportArray = NSMutableArray()
    var allisLikeArray = NSMutableArray()
    var allSupportPersonalArray = NSMutableArray()
    var likeStr = NSString()
    var allisLikesPersonalArray = NSMutableArray()
    @IBOutlet var newTableViewObj: UITableView!
    @IBOutlet var sideMenuBtn: UIButton!
    
    // MARK: - viewDidLoad
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkImageORNotStr = nil
        // [START log_fcm_reg_token]
     //   let token = Messaging.messaging().fcmToken
        // print("FCM token: \(token ?? "")")
        // [END log_fcm_reg_token]
        
        self.selected = true
        self.automaticallyAdjustsScrollViewInsets = false
        
        // path to documents directory
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        if let documentDirectoryPath = documentDirectoryPath {
            // create the custom folder path
            let imagesDirectoryPath = documentDirectoryPath.appending("/Verdentum")
            let fileManager = FileManager.default
            
            
            var isDir : ObjCBool = false
            if fileManager.fileExists(atPath: imagesDirectoryPath, isDirectory:&isDir) {
                if isDir.boolValue {
                    // file exists and is a directory
                } else {
                    
                }
            } else {
                do {
                    try fileManager.createDirectory(atPath: imagesDirectoryPath,
                                                    withIntermediateDirectories: false,
                                                    attributes: nil)
                } catch {
                    print("Error creating images folder in documents dir: \(error)")
                }            }
            
            if !fileManager.fileExists(atPath: imagesDirectoryPath) {
            }
        }
        self.navigationController?.isNavigationBarHidden = true
        self.newTableViewObj.estimatedRowHeight = 100
        self.newTableViewObj.rowHeight = UITableViewAutomaticDimension
        
        sideMenuController?.leftViewAlwaysVisibleOptions = [.onPadLandscape, .onPadPortrait]
        self.sideMenuBtn.addTarget(self, action: #selector(showLeftView(sender:)), for: UIControlEvents.touchUpInside)
        
        // -----Refresh Control----
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(DashBoardViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        self.newTableViewObj.addSubview(refreshControl)
        
        if self.navStr == "DashboardVC"
        {
            self.homeServiceApiCalling()
        }
        if self.navStr == "HomereportPostNav" {
            self.newTableViewObj.reloadData()
        }
        if self.navStr == "reportPostVC" {
            self.newTableViewObj.reloadData()
        }
        if self.navStr == "reportPostNav"
        {
            self.newTableViewObj.reloadData()
        }
        if self.navStr == "Commentvc"
        {
            //self.homeServiceApiCalling()
            self.newTableViewObj.reloadData()
        }
        if self.navStr == "userpostvc"
        {
            // self.homeServiceApiCalling()
            self.newTableViewObj.reloadData()
        }
        if self.navStr == "NewPageCommentvc"
        {
            self.newTableViewObj.reloadData()
        }
        if self.navStr == "NewPage"
        {
            self.newTableViewObj.reloadData()
        }
        //else
        
        if self.navStr == "uploadVC" {
            self.homeServiceApiCalling()
        }
        if self.navStr == "AddPostVC" {
            self.homeServiceApiCalling()
        }
        if self.navStr == "supportApply" {
            self.homeServiceApiCalling()
        }
        
        
        /* let userID = UserDefaults.standard.value(forKey: "userID")
         let params:[String: AnyObject] = [
         "purpose" : "allpost" as AnyObject,
         "id" : userID as AnyObject ]
         self.postsApiCalling(param: params)*/
        
        self.newTableViewObj .register(UINib (nibName: "WallCell", bundle: nil), forCellReuseIdentifier: "WallCell")
        
        //----- NSNotificationCenter ------ //
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectItem(fromCollectionView:)), name: NSNotification.Name(rawValue: "didSelectItemFromCollectionView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectItemPDF(fromCollectionView:)), name: NSNotification.Name(rawValue: "didSelectItemFromCollectionViewPDF"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectAddPostView(fromCollectionView:)), name: NSNotification.Name(rawValue: "AddPostView"), object: nil)
    }
    
    // MARK: - refresh
    
    func refresh(_ sender:AnyObject)
    {
        
        homeServiceApiCalling()
        // self.newTableViewObj.reloadData()
        self.refreshControl .endRefreshing()
    }
    
    
    func applicationWillResign(notification : NSNotification) {
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didSelectItemFromCollectionView"), object: nil)
    }
    
    // MARK: - viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
         sideMenuController?.isLeftViewSwipeGestureDisabled = false
        
        if self.navStr == "NewPage"
        {
            self.newTableViewObj.reloadData()
        }
        if self.navStr == "Commentvc"
        {
            self.downloadImage.removeAll()
            // self.newTableViewObj.reloadData()
        }
        if self.navStr == "userpostvc"
        {
            self.downloadImage.removeAll()
            self.newTableViewObj.reloadData()
        }
        if self.navStr == "NewPageCommentvc"
        {
            self.downloadImage.removeAll()
            self.newTableViewObj.reloadData()
        }
    }
    
    // MARK: - viewDidAppear
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.navStr == "HomereportPostNav" {
            let indexPath = IndexPath(item: self.verticalOffset, section: 0)
            self.newTableViewObj.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        if self.navStr == "NewPage"
        {
            let indexPath = IndexPath(item: self.verticalOffset, section: 0)
            self.newTableViewObj.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        if self.navStr == "NewPageCommentvc"
        {
            let indexPath = IndexPath(item: self.verticalOffset, section: 0)
            self.newTableViewObj.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        if self.navStr == "userpostvc"
        {
            let indexPath = IndexPath(item: self.verticalOffset, section: 0)
            self.newTableViewObj.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        
        if self.navStr == "Commentvc"
        {
            let indexPath = IndexPath(item: self.verticalOffset, section: 0)
            //            //DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            //            //self.newTableViewObj.scrollToRow(at: indexPath, at: .top, animated: false)
            self.newTableViewObj.scrollToRow(at: indexPath, at: .top, animated: false)
            // }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: - showLeftView
    
    func showLeftView(sender: AnyObject?) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    //MARK: - HomeServiceApiCalling
    
    func homeServiceApiCalling() {

        self.allSupportArray.removeAllObjects()
        self.allcommentCountArray.removeAllObjects()
        self.responsePostArray.removeAllObjects()
        
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allpost" as AnyObject,
                "id" : userID as AnyObject ]
            let url = NSURL(string:VERDENTUMB_BaseURL)
            let request = NSMutableURLRequest(url: url! as URL)
            
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
                
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    
                   
                    self.presentAlertWithTitle(title: "", message:"The request timed out")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        // print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                
                                self.responsePostArray = (responsejson["posts"] as AnyObject) as! NSArray as! NSMutableArray
                                
                                if self.responsePostArray.count == 0
                                {
                                    // self.presentAlertWithTitle(title: "", message:"This is your development dashbooard -a place where you can find updates from your network around the word, friends, and organizations you interact with.")
                                    
                                    self.StatuStr = "This is your development dashboard -a place where you can find updates from your network around the word, friends, and organizations you interact with."
                                    self.newTableViewObj.reloadData()
                                }
                                else
                                {
                                    UserDefaults.standard.set((((responsejson["posts"] as AnyObject) .value(forKey: "post_id") as AnyObject) .firstObject) as Any, forKey: "firstPostID")
                                    
                                    for var i in 0..<self.responsePostArray.count
                                    {
                                        let commentsArr : NSArray = ((self.responsePostArray[i] as AnyObject).value(forKey: "comments") as? NSArray)!
                                        self.allcommentCountArray.add(String(commentsArr.count))
                                        
                                        let likesStr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "likes") as? String)!
                                        self.allSupportArray.add(likesStr)
                                        
                                        let islikestr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                        self.allisLikeArray.add(islikestr)
                                        
                                    }
                                    // print(self.allcommentCountArray)
                                    self.newTableViewObj.reloadData()
                                }                                
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.StatuStr = ((responsejson["msg"] as AnyObject) as! String as NSString)
                                self.newTableViewObj.reloadData()
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    //MARK: - SecondServiceApiCalling
    
    func postsApiCalling(param:[String: AnyObject])
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:VERDENTUMB_BaseURL)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
                
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                   
                    self.presentAlertWithTitle(title: "", message:"The request timed out")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        // print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let arra  = (responsejson["posts"] as AnyObject) as! NSArray
                                if arra.count == 0
                                {
                                    
                                }
                                else
                                {
                                    self.responsePostArray.addObjects(from: arra as! [Any])
                                    for var i in 0..<arra.count
                                    {
                                        let commentsArr : NSArray = ((arra[i] as AnyObject).value(forKey: "comments") as? NSArray)!
                                        self.allcommentCountArray.add(String(commentsArr.count))
                                        
                                        let likesStr : String = ((arra[i] as AnyObject).value(forKey: "likes") as? String)!
                                        self.allSupportArray.add(likesStr)
                                        
                                        let islikestr : String = ((arra[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                        self.allisLikeArray.add(islikestr)
                                    }
                                    //print(self.allcommentCountArray)
                                    // print(self.allSupportArray)
                                    //print(self.allisLikeArray)
                                    self.newTableViewObj.reloadData()
                                }
                                
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    //MARK: - UserPostsAPI Calling
    
    func userpostsApiCalling(param:[String: AnyObject],andSelectedRow selectedRow: Int)
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_USERPOSTSAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
                
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    
                    self.presentAlertWithTitle(title: "", message:"The request timed out")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                
                                self.responseUserPostsArray = (responsejson["posts"] as AnyObject) as! NSArray as! NSMutableArray
                                for var i in 0..<self.responseUserPostsArray.count
                                {
                                    let commentsArr : NSArray = ((self.responseUserPostsArray [i] as AnyObject).value(forKey: "comments") as? NSArray)!
                                    self.allPersonalcommentsArray.add(String(commentsArr.count))
                                    let likesStr : String = ((self.responseUserPostsArray[i] as AnyObject).value(forKey: "likes") as? String)!
                                    self.allSupportPersonalArray.add(likesStr)
                                    
                                    let islikestr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                    self.allisLikesPersonalArray.add(islikestr)
                                    
                                }
                                let mainViewController = self.sideMenuController!
                                let PersonalAccountVC = PersonalAccountViewController()
                                PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
                                PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                PersonalAccountVC.headerTitleStr = self.postUserNamestr
                                PersonalAccountVC.allSupportPersonalArray = self.allSupportPersonalArray
                                PersonalAccountVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                PersonalAccountVC.responsePostArray = self.responsePostArray
                                PersonalAccountVC.allcommentCountArray = self.allcommentCountArray
                                PersonalAccountVC.allSupportArray = self.allSupportArray
                                PersonalAccountVC.allisLikeArray = self.allisLikeArray
                                PersonalAccountVC.userID_Registered = self.commentPost_ID
                                PersonalAccountVC.navStr = "NewPage"
                                PersonalAccountVC.verticalOffset = selectedRow
                                PersonalAccountVC.userNameObjStr = self.postUserNamestr
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(PersonalAccountVC, animated: true)
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        
        if self.responsePostArray.count == 0 {
            let rect = CGRect(x: 0,
                              y: 0,
                              width: view.bounds.size.width,
                              height: view.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            
            noDataLabel.text = self.StatuStr as String
            noDataLabel.textColor = UIColor.black
            noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            noDataLabel.sizeToFit()
            noDataLabel.textAlignment = NSTextAlignment.center
            self.newTableViewObj.backgroundView = noDataLabel
            
            return 0
        }
        else
        {
            return 1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.responsePostArray.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = tableView .dequeueReusableCell(withIdentifier: "WallCell") as! WallCell
        let url = URL(string: IMAGE_UPLOAD.appending(((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!))
        cell.imageViewObj.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
        cell.imageViewObj.layer.borderWidth = 2.0
        cell.imageViewObj.layer.borderColor = UIColor.white.cgColor
        cell.imageViewObj.layer.cornerRadius = 25
        cell.imageViewObj.layer.masksToBounds = true
        
        let titlename: String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_lname") as! String
        self.usernameobjstr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        cell.userNameLabel.text = usernameobjstr as String
        let capitalized:String = (cell.userNameLabel.text!.capitalized)
        cell.userNameLabel.text = capitalized
        
        let htmlStr  = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "text") as? String
        let attributeString1 = try? NSAttributedString(data: (htmlStr?.data(using: String.Encoding.unicode)!)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil) as NSAttributedString
        
        cell.textLabelObj.attributedText = attributeString1
        cell.imageViewAboveLIneObj.isHidden = true
        let newAttributedString = NSMutableAttributedString(attributedString: cell.textLabelObj.attributedText!)
        
        // Enumerate through all the font ranges
        newAttributedString.enumerateAttribute(NSFontAttributeName, in: NSMakeRange(0, newAttributedString.length), options: []) { value, range, stop in
            guard let currentFont = value as? UIFont else {
                return
            }
            
            // An NSFontDescriptor describes the attributes of a font: family name, face name, point size, etc.
            // Here we describe the replacement font as coming from the "Hoefler Text" family
            let fontDescriptor = currentFont.fontDescriptor.addingAttributes([UIFontDescriptorFamilyAttribute: "Helvetica"])
            
            // Ask the OS for an actual font that most closely matches the description above
            if let newFontDescriptor = fontDescriptor.matchingFontDescriptors(withMandatoryKeys: [UIFontDescriptorFamilyAttribute]).first {
                let newFont = UIFont(descriptor: newFontDescriptor, size: currentFont.pointSize)
                newAttributedString.addAttributes([NSFontAttributeName: newFont], range: range)
            }
        }
        
        cell.textLabelObj.attributedText = newAttributedString
        
        // cell.textLabelObj.text =
        
        let myDate = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "date") as? String
        let myDateString = myDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        let Dateobj = dateFormatter.date(from: myDateString!)!
        dateFormatter.dateFormat = "MMM dd, YYYY HH:mm a"
        let somedateString = dateFormatter.string(from: Dateobj)
        cell.dataTimeLabel.text = somedateString
        
        // ---- Show All Comments --- //
        
        // let commentsArr : NSArray = ((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "comments") as? NSArray)!
        //  cell.viewAllCmtCount.text =  String(commentsArr.count)
        
        //------ Add New Comments ------ //
        
        cell.viewAllCmtCount.text = self.allcommentCountArray[indexPath.row] as? String
        
        
        cell.likeCountLabel.tag = indexPath.row
        cell.supportLabel.tag = indexPath.row
        //let likesStr : NSString = ((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "is_liked") as? NSString)!
        
        let likesStr : NSString = ((self.allisLikeArray[indexPath.row] as AnyObject) as? NSString)!
        
        if likesStr.isEqual(to: "1") {
            
            cell.heartImageview.tag = indexPath.row
            cell.heartImageview.image = UIImage (named: "Hand Filled")
            // cell.likeCountLabel.text = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "likes") as? String
            cell.likeCountLabel.text = self.allSupportArray[indexPath.row] as? String
            cell.supportLabel.text = "Supported"
        }
        if likesStr.isEqual(to: "0")
        {
            cell.heartImageview.tag = indexPath.row
            cell.heartImageview.image = UIImage (named: "Hand")
            cell.likeCountLabel.text = self.allSupportArray[indexPath.row] as? String
            cell.supportLabel.text = "Support"
        }
        
        // ------ Sending Images to CollectionView(WallCell) ----//
        
        // cell.setCollectionData(collection: ((self.responsePostArray[indexPath.row] as! AnyObject) .value(forKey: "images") as? NSMutableArray)!)
        
        cell.setCollectionData(collection: ((self.responsePostArray[indexPath.row] as AnyObject) .value(forKey: "images") as? NSMutableArray)!)
        
        //-----------------------------//
        
        if (((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "images") as? NSMutableArray)!.count) == 0
        {
            cell.heightCollectionView.constant = 0
            cell.imageBtnHeight.constant = 0
        }
        else
        {
            let imageorPDFstr = ((((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "image_name") as AnyObject) .firstObject) as! NSString
            // print(imageorPDFstr as Any)
            let fileArray = imageorPDFstr.components(separatedBy: ".")
            let finalFileNameextension = fileArray.last
            if finalFileNameextension == "pdf"
            {
                cell.heightCollectionView.constant = 80
                cell.imageBtnHeight.constant = 0
            }
            if finalFileNameextension == "jpg"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
            if finalFileNameextension == "PNG"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
            if finalFileNameextension == "png"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
            if finalFileNameextension == "jpeg"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
        }
        cell.dotMoreBtn.tag = indexPath.row
        cell.dotMoreBtn.addTarget(self, action: #selector(self.DotbuttonClicked), for: .touchUpInside)
        
        self.checkImageORNotStr = ((((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "image_name") as AnyObject) .firstObject) as? String
        
        if  self.checkImageORNotStr != nil {
            let fileArray = self.checkImageORNotStr?.components(separatedBy: ".")
            let finalFileNameextension = fileArray?.last
            let countStr : NSString = finalFileNameextension! as NSString
            let youtubestrpath  = countStr.length
            
            if youtubestrpath as NSInteger == 11
            {
                cell.imageTappedBtn.isHidden = true
            }
            if finalFileNameextension == "pdf" {
                cell.imageTappedBtn.isHidden = true
            }
            if finalFileNameextension == "jpeg"{
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
            if finalFileNameextension == "png" {
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
            
            if finalFileNameextension == "PNG" {
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
            
            if finalFileNameextension == "jpg" {
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
        }
        else
        {
            
        }
        
        cell.likeBtnTapped.tag = indexPath.row
        cell.likeBtnTapped.addTarget(self, action: #selector(self.likeBtnTapped), for: .touchUpInside)
        cell.viewAllBtnTapped.tag = indexPath.row
        cell.viewAllBtnTapped.addTarget(self, action: #selector(self.viewAllCommentBtnTapped), for: .touchUpInside)
        cell.commentBtnTap.tag = indexPath.row
        cell.commentBtnTap.addTarget(self, action: #selector(self.viewAllCommentBtnTapped), for: .touchUpInside)
        cell.userPostsBtnTapped.tag = indexPath.row
        cell.userPostsBtnTapped.addTarget(self, action: #selector(self.userPostsBtnTapped), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView:UITableView, willDisplay cell:UITableViewCell, forRowAt indexPath:IndexPath)
    {
        /// print(self.responsePostArray.count - 1)
        if indexPath.row == (self.responsePostArray.count - 1)
        {
            if self.navStr == "Commentvc"
            {
                let postarray : NSArray = self.responsePostArray .value(forKey: "post_id") as! NSArray
                self.lastpostIDStr = postarray.lastObject as! NSString
                let userID = UserDefaults.standard.value(forKey: "userID")
                let params:[String: AnyObject] = [
                    "purpose" : "allpost" as AnyObject,
                    "id" : userID as AnyObject,
                    "direction" : "down" as AnyObject,
                    "limit" : self.lastpostIDStr as AnyObject]
                // MBProgressHUD.showAdded(to: self.view, animated: true)
                self.postsApiCalling(param: params)
            }
            else
            {
                let postarray : NSArray = self.responsePostArray .value(forKey: "post_id") as! NSArray
                self.lastpostIDStr = postarray.lastObject as! NSString
                let userID = UserDefaults.standard.value(forKey: "userID")
                let params:[String: AnyObject] = [
                    "purpose" : "allpost" as AnyObject,
                    "id" : userID as AnyObject,
                    "direction" : "down" as AnyObject,
                    "limit" : self.lastpostIDStr as AnyObject]
                
                self.postsApiCalling(param: params)
            }
        }
    }
    
    func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        cell.imageCollectionViewObj.collectionViewLayout.invalidateLayout()
    }
    
    func userPostsBtnTapped(_ sender: UIButton) {
        
        self.commentPost_ID = ((self.responsePostArray[sender.tag] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
        
        let titlename: String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_lname") as! String
        
        self.postUserNamestr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        
        let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "userpost" as AnyObject,
            "id" : self.commentPost_ID as AnyObject,
            "myid" : userID as AnyObject]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.userpostsApiCalling(param: params, andSelectedRow: sender.tag)
    }
    
    
    func likeBtnTapped(_ sender: UIButton)
    {
        let likesStr : NSString = ((self.allisLikeArray[sender.tag] as AnyObject) as? NSString)!
        if likesStr.isEqual(to: "1") {
            
        }
        if likesStr.isEqual(to: "0")
        {
            let indexCheck1 : Int = sender.tag
            let supportselectedStr1 : String = String(indexCheck1)
            if self.supportselectedStr .isEqual(to: supportselectedStr1) {
                
            } else {
                var message = NSNumber()
                let button: UIButton? = sender
                button?.backgroundColor = UIColor.clear
                button?.setTitleColor(UIColor.clear, for: UIControlState.normal)
                button?.isSelected = !(button?.isSelected)!
                if (button?.isSelected)!
                {
                    let post_IDStr : NSString = (self.responsePostArray .object(at: sender.tag) as AnyObject) .value(forKey: "post_id") as! NSString
                    let userID = UserDefaults.standard.value(forKey: "userID")
                    let params:[String: AnyObject] = [
                        "purpose" : "support" as AnyObject,
                        "post_id" : post_IDStr as AnyObject,
                        "user_id" : userID as AnyObject]
                    
                    if Reachability.isConnectedToNetwork() == true {
                        Alamofire.request(BaseURL_SupportAPI, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                            .responseJSON { response in
                                
                                if let JSON = response.result.value as? [String: Any] {
                                    print(JSON)
                                    message = JSON["status"]  as! NSNumber
                                    //message = JSON["likes"] as! NSNumber
                                    //print(message)
                                    //self.homeServiceApiCalling()
                                    let indexPath1 = IndexPath(row: sender.tag, section: 0)
                                    self.cell = (self.newTableViewObj.cellForRow(at: indexPath1) as? WallCell)!
                                    let indexCheck : Int = sender.tag
                                    
                                    // Replace allisLikeArray
                                    
                                    let updatecommentCount : String =  "1"
                                    self.allisLikeArray.replaceObject(at: indexCheck, with: updatecommentCount)
                                    
                                    // Replace AllSupportArray
                                    
                                    
                                    var likesStr:String?
                                    
                                    if let likesStrNo = JSON["likes"] as? NSNumber {
                                        likesStr = "\(likesStrNo)"
                                    } else if let likesString = JSON["likes"] as? String {
                                        likesStr =  likesString
                                    }
                                    
                                    self.allSupportArray.replaceObject(at: indexCheck, with: likesStr!)
                                    
                                    self.supportselectedStr = String(indexCheck) as NSString
                                    self.cell.heartImageview.tag = sender.tag
                                    self.cell.heartImageview.image = UIImage (named: "Hand Filled")
                                    self.cell.likeCountLabel.text = message.stringValue as String
                                    self.cell.likeCountLabel.tag = sender.tag
                                    self.cell.supportLabel.text = "Supported"
                                    self.cell.supportLabel.tag = sender.tag
                                    
                                    // self.newTableViewObj.reloadRows(at: [indexPath1], with: UITableViewRowAnimation.none)
                                    DispatchQueue.main.async(execute: {
                                        
                                        self.newTableViewObj.reloadData()
                                        self.newTableViewObj.contentOffset = .zero
                                        
                                    })
                                    
                                }
                                
                        }
                    }
                    else {
                        self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
                    }
                    
                }
            }           
        }
    }
    
    func viewAllCommentBtnTapped(_ sender: UIButton) {
        
        self.commentPost_ID = ((self.responsePostArray[sender.tag] as! NSObject) .value(forKey: "post_id") as AnyObject) as! NSString
        let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "allcomment" as AnyObject,
            "id" : userID as AnyObject,
            "post_id" : self.commentPost_ID as AnyObject]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
    }
    
    func CommentsApiCalling(param:[String: AnyObject],andSelectedRow selectedRow: Int)
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true
        
        {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let url = NSURL(string:BaseURL_AllCommentsAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                let mainViewController = self.sideMenuController!
                                let CommentsVC = CommentsViewController()
                                CommentsVC .commentArray = self.commentsArray
                                CommentsVC.post_id = self.commentPost_ID
                                CommentsVC.allcommentCountArray = self.allcommentCountArray
                                CommentsVC.navstr = "NewPage"
                                CommentsVC.verticalOffset = selectedRow
                                CommentsVC.responsePostArray = self.responsePostArray
                                CommentsVC.allSupportArray = self.allSupportArray
                                CommentsVC.allisLikeArray = self.allisLikeArray
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(CommentsVC, animated: true)
                                
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    func imageTappedBtn(_ sender: UIButton)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let buttonPosition = sender.convert(CGPoint.zero, to: self.newTableViewObj)
        let indexPathRow = self.newTableViewObj.indexPathForRow(at: buttonPosition)
        self.selectedRow = indexPathRow! as NSIndexPath
        self.verticalOffset = sender.tag
        
        let titlename: String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_lname") as! String
        
        self.postUserNamestr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        
        self.commentsImagesArray = (((self.responsePostArray[self.selectedRow.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "comments") as AnyObject) as! NSArray
        self.imagesArrayURl = (((self.responsePostArray[self.selectedRow.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "image_name") as AnyObject) as! NSArray
        self.imageResponseArray = ((self.responsePostArray[self.selectedRow.row] as AnyObject).value(forKey: "images") as AnyObject)  as! NSArray
        self.downloadImage.removeAll()
        for var i in (0..<imagesArrayURl.count)
        {
            let imageURLstr2 : NSString = IMAGE_UPLOAD.appending((self.imagesArrayURl[i] as AnyObject) as! String) as NSString
            let url = URL(string: imageURLstr2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            // let url = URL(string: IMAGE_UPLOAD.appending((self.imagesArrayURl[i] as AnyObject) as! String))
            if url != nil {
                Alamofire.request(url!).responseImage { response in
                    
                    if let image = response.result.value {
                        
                        self.downloadImage.append(image)
                        if self.downloadImage.count == self.imagesArrayURl.count
                        {
                            let mainViewController = self.sideMenuController!
                            let ImageTableVC = ImageTableViewController()
                            ImageTableVC.downloadImage = self.downloadImage
                            ImageTableVC.selectedRow = self.selectedRow
                            ImageTableVC.verticalOffset = self.verticalOffset
                            ImageTableVC.allSupportArray = self.allSupportArray
                            ImageTableVC.allisLikeArray = self.allisLikeArray
                            ImageTableVC.usernameobjstr = self.postUserNamestr
                            ImageTableVC.navstr = "NewPage"
                            ImageTableVC.imageResponseArray = self.imageResponseArray
                            ImageTableVC.responsePostArray = self.responsePostArray
                            ImageTableVC.commentsImagesArray = self.commentsImagesArray
                            ImageTableVC.allcommentCountArray = self.allcommentCountArray
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(ImageTableVC, animated: true)
                        }
                        else
                        {
                            
                        }
                    }
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.presentAlertWithTitle(title: "", message:"Please Try again later.")
            }
            
        }
    }
    
    func createWebPhotos() -> [SKPhotoProtocol] {
        return (0..<self.imagesArrayURl.count).map { (i: Int) -> SKPhotoProtocol in
            let photo = SKPhoto.photoWithImageURL(IMAGE_UPLOAD .appending(self.imagesArrayURl[i] as! String))
            //photo.caption = caption[i%10]
            photo.shouldCachePhotoURLImage = true
            return photo
        }
    }
    
    func DotbuttonClicked(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.newTableViewObj)
        let indexPathRow = self.newTableViewObj.indexPathForRow(at: buttonPosition)
        self.selectedRow = indexPathRow! as NSIndexPath
        self.verticalOffset = sender.tag
        self.result = UserDefaults.standard.value(forKey: "userID") as! NSString
        self.postusedID = ((self.responsePostArray[sender.tag] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
        self.postID = ((self.responsePostArray[sender.tag] as! NSObject) .value(forKey: "post_id") as AnyObject) as! NSString
        if self.result == self.postusedID
        {
            // Finding Array Index
            let indexPath = IndexPath(row: sender.tag, section: 0)
            cell = (self.newTableViewObj.cellForRow(at: indexPath) as? WallCell)!
            let quoteDictionary = self.responsePostArray[sender.tag] as! Dictionary<String,AnyObject>
            print(quoteDictionary)
            if indexPathRow != nil
            {
                let actionSheet = UIActionSheet(title: "Delete Post", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Delete")
                
                actionSheet.show(in: self.view)
            }
        }
        else
        {
            let actionSheet = UIActionSheet(title: "Report Post", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Report")
            actionSheet.show(in: self.view)
        }
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        print("\(buttonIndex)")
        switch (buttonIndex){
        case 0:
            print("Cancel")
        case 1:
            if self.result == self.postusedID
            {
                let alertController = UIAlertController(title: "Delete Post", message: "Are you sure you want to delete this post?", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                    
                }
                alertController.addAction(cancelAction)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    let userID = UserDefaults.standard.value(forKey: "userID")
                    let params:[String: AnyObject] = [
                        "purpose" : "deletepost" as AnyObject,
                        "post_id" : self.postID as AnyObject,
                        "id" : userID as AnyObject]
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    self.deletePostID(param: params)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
            else
            {
                let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                ReportuserVC.userpostIdstr = self.postusedID
                ReportuserVC.post_idStr = self.postID
                ReportuserVC.responseArray = self.responsePostArray
                ReportuserVC.allcommentCountArray = self.allcommentCountArray
                ReportuserVC.allSupportArray = allSupportArray
                ReportuserVC.allisLikeArray = self.allisLikeArray
                ReportuserVC.selectedRow = self.selectedRow
                ReportuserVC.verticalOffset = self.verticalOffset
                ReportuserVC.navstr = "HomereportPostNav"
                self.navigationController?.pushViewController(ReportuserVC, animated: true)
            }
        default:
            print("Default")
        }
    }
    
    
    func deletePostID(param:[String: AnyObject])
    {
        //  MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:VERDENTUMB_BaseURL.appending(BaseURL_VERDENTUMDeletepostAPI))
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                                
                            {
                                //self.presentAlertWithTitle(title: "", message:"Deleted Successfully.")
                                self.responsePostArray .removeObject(at: self.selectedRow.row)
                                self.newTableViewObj.reloadData()
                            }
                            else
                            {
                                self.presentAlertWithTitle(title: " Delete Failed!", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func didSelectItem(fromCollectionView notification: Notification)
    {
        let newsImageURLstr = notification .object as! NSString
        
        self.imageArrayURLCol.add(newsImageURLstr)
        print(self.imageArrayURLCol)
        if newsImageURLstr.isEqual(to: "")
        {
            let alertController = UIAlertController(title: "", message: "News Image unavailable. Please try again.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            let ZoomImageVC = ZoomImageViewController(nibName: "ZoomImageViewController", bundle: nil)
            ZoomImageVC.newImageURlStr =  newsImageURLstr
            self.navigationController?.pushViewController(ZoomImageVC, animated: true)
        }
    }
    
    func didSelectItemPDF(fromCollectionView notification: Notification)
    {
        let newsImageURLstr = notification .object as! NSString
        if newsImageURLstr.isEqual(to: "")
        {
            let alertController = UIAlertController(title: "", message: "PDF unavailable. Please try again.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            let ZoomImageVC = ZoomImageViewController(nibName: "ZoomImageViewController", bundle: nil)
            ZoomImageVC.newImageURlStr =  newsImageURLstr
            self.navigationController?.pushViewController(ZoomImageVC, animated: true)
        }
    }
    func didSelectAddPostView(fromCollectionView notification: Notification)
    {
        let newsImageURLstr = notification .object as! NSString
        if newsImageURLstr.isEqual(to: "")
        {
            let alertController = UIAlertController(title: "", message: "Please Try again Later.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            self.allSupportArray.removeAllObjects()
            self.allcommentCountArray.removeAllObjects()
            self.responsePostArray.removeAllObjects()
            self.homeServiceApiCalling()
            
        }
    }
    
    
    func UPnewApiCalling() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "allpost" as AnyObject,
            "id" : userID as AnyObject,
            "direction" : "up" as AnyObject,
            "limit" : self.lastpostIDStr as AnyObject]
        
        let url = NSURL(string:"http://52.40.207.123/api/mobpost")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        // var err: NSError?
        //request.HTTPBody = JSONSerialization.dataWithJSONObject(params, options: JSONSerialization.WritingOptions.allZeros, error: &err)
        
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    print("response was not 200: \(String(describing: response))")
                    return
                }
            }
            if (error != nil) {
                print("error submitting request: \(String(describing: error))")
                return
            }
            
            do {
                if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                {
                    
                    DispatchQueue.main.async (execute:{ () -> Void in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            
                        {
                            self.responsePostArray = (responsejson["posts"] as AnyObject) as! NSArray as! NSMutableArray
                            self.newTableViewObj.reloadData()
                        }
                        else
                        {
                            
                        }
                    })
                }
                // pass dictionary to nsdata object and set it as request body
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    
    @IBAction func addPostBtnTapped(_ sender: Any) {
        
        let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "allgroup" as AnyObject,
            "id" : userID as AnyObject]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.addPostAPI(param: params)
    }
    
    @IBAction func uploadActivityBtntapped(_ sender: Any) {
        
        let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "fastprograms" as AnyObject,
            "id" : userID as AnyObject]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.uploadActivityAPICalling(param: params)
        
    }
    
    func uploadActivityAPICalling(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_UPLOADACTIVITYAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        //      print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.presentAlertWithTitle(title: "", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                            else
                            {
                                // self.responseUploadActivityArray = (responsejson["programs"] as AnyObject) as! NSArray as! NSMutableArray
                                let mainViewController = self.sideMenuController!
                                let UploadActivityVC = UploadActivityViewController()
                                
                                
                                UploadActivityVC.mainProgramArray = (responsejson["programs"] as AnyObject) as! NSArray
                                UploadActivityVC.mainGroupArray = ((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject)  as! NSArray
                                UploadActivityVC.programArray = ((responsejson["programs"] as AnyObject) .value(forKey: "prog_name") as AnyObject) as! NSArray
                                UploadActivityVC.joiningArray = ((responsejson["programs"] as AnyObject) .value(forKey: "joining") as AnyObject) as! NSArray
                                UploadActivityVC.defaultProgramStr = (((responsejson["programs"] as AnyObject) .value(forKey: "prog_name") as AnyObject) .firstObject) as! NSString
                                UploadActivityVC.programIDArray = ((responsejson["programs"] as AnyObject) .value(forKey: "prog_id") as AnyObject) as! NSArray
                                UploadActivityVC.teamArray = ((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject)  as! NSArray
                                UploadActivityVC.defaultTeamStr = (((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject) .value(forKey: "group_name") as AnyObject) as! NSArray
                                UploadActivityVC.groupAdminArray = (((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject) .value(forKey: "group_admin") as AnyObject) as! NSArray
                                UploadActivityVC.teamIDArray = ((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject)  as! NSArray
                                UploadActivityVC.selectActivityArray = ((responsejson["programs"] as AnyObject) .value(forKey: "tasks") as AnyObject)  as! NSArray
                                UploadActivityVC.selectActivityIDArray = ((responsejson["programs"] as AnyObject) .value(forKey: "tasks") as AnyObject)  as! NSArray
                                UploadActivityVC.navstr = "uploadVC"
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(UploadActivityVC, animated: true)
                            }                           
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func addPostAPI(param:[String: AnyObject])
    {
        
        /* if Reachability.isConnectedToNetwork() == true {
         
         Alamofire.request(BaseURL_ADDPostGroupAPI, method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil)
         .responseJSON { response in
         // print(response.request as Any)  // original URL request
         // print(response.response as Any) // URL response
         // print(response.result.value as Any)   // result of response serialization
         if let JSON = response.result.value as? [String: AnyObject] {
         
         
         self.groupNameArray = JSON["groups"]  as! NSArray
         // self.groupNameArray.add(arra)
         print(self.groupNameArray.count)
         let mainViewController = self.sideMenuController!
         let AddPostVC = AddPostViewController()
         AddPostVC.responsePostArray = self.responsePostArray
         AddPostVC.allcommentCountArray = self.allcommentCountArray
         AddPostVC.allSupportArray = self.allSupportArray
         AddPostVC.allisLikeArray = self.allisLikeArray
         AddPostVC.groupnameArray = self.groupNameArray
         let navigationController = mainViewController.rootViewController as! NavigationController
         navigationController.pushViewController(AddPostVC, animated: true)
         }
         }
         }
         else {
         self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
         }*/
        
        //  MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_ADDPostGroupAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            self.groupNameArray = (responsejson["groups"] as AnyObject)  as! NSMutableArray
                            let mainViewController = self.sideMenuController!
                            let AddPostVC = AddPostViewController()
                            AddPostVC.responsePostArray = self.responsePostArray
                            AddPostVC.allcommentCountArray = self.allcommentCountArray
                            AddPostVC.allSupportArray = self.allSupportArray
                            AddPostVC.allisLikeArray = self.allisLikeArray
                            AddPostVC.groupnameArray = self.groupNameArray
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(AddPostVC, animated: true)
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
}

