 //
 //  UploadActivityViewController.swift
 //  Verdentum
 //
 //  Created by Praveen Kuruganti on 29/08/17.
 //  Copyright © 2017 Verdentum. All rights reserved.
 //
 
 import UIKit
 import MBProgressHUD
 import Photos
 import AlamofireImage
 import Alamofire
 import AVFoundation
 
 class UploadActivityViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CellInfoDelegate {
    
    
    
    @IBOutlet var programLabelHeight: NSLayoutConstraint!
    
    @IBOutlet var programArrowImageHeight: NSLayoutConstraint!
    @IBOutlet var programTextfieldheight: NSLayoutConstraint!
    @IBOutlet var programViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet var tableViewHeaderOBj: UIView!
    
    @IBOutlet var arrowHeightObj: NSLayoutConstraint!
    @IBOutlet var teamTextFieldHeight: NSLayoutConstraint!
    @IBOutlet var teamViewHeight: NSLayoutConstraint!
    
    @IBOutlet var teamHeightLabelObj: NSLayoutConstraint!
    
    @IBOutlet var headerBottomLabelHeight: NSLayoutConstraint!
    @IBOutlet var headerBottomLineObj: UILabel!
    @IBOutlet var programbottmLineHeight: NSLayoutConstraint!
    @IBOutlet var pickerViewObj: UIPickerView!
    
    @IBOutlet var footerViewObj: UIView!
    var rowIndex = Int()
    var inComponentintex = Int()
    var tagcheckInt = Int()
    var rootListAssets = SwiftAssetsViewController()
    var responseUploadActivityArray = NSMutableArray()
    var  programArray = NSArray()
    var  teamArray = NSArray()
    var  selectActivityArray = NSArray()
    var  programIDArray = NSArray()
    var  teamIDArray = NSArray()
    var  selectActivityIDArray = NSArray()
    var datePicker: UIDatePicker?
    var toolBar:  UIToolbar?
    var tempTxtField: UITextField?
    var selectedString: String? = nil
    var  groupArray = NSArray()
    var taskArray = NSArray()
    var taskDescriptionArray = NSArray()
    var indexOfObj = Int()
    var programIDStr = NSString()
    var taskIDStr = NSString()
    var taskIDArray = NSArray()
    var selected = Bool()
    var subtaskTitleArray = NSArray()
    var subtaskDescripionArray = NSArray()
    var subtaskNumberArray = NSArray()
    var subtaskAct_TypeArray = NSArray()
    var subtaskIDArray = NSArray()
    var subTaskCountArray = NSArray()
    var subTaskSelectCount = NSString()
    var footercell = FooterCell()
    var headerTaskCell = HeaderTaskCell()
    var subtaskCell = SubTaskViewCell()
    var subtaskCell1 = SubTaskCell1()
    var taskCell = TaskCell()
    var imageArray = NSMutableArray()
    var galleryImages = [UIImage]()
    var base64Images = [AnyObject]()
    var profilepicImageStr = NSString()
    var camImageArray = [UIImage]()
    var selectedTaskName = NSString()
    var selectedTaskDespName = NSString()
    var subActivityArray = NSArray()
    var subAct_TypeArray = NSArray()
    var act_TypeStr = NSString()
    var subTaskAct_TypeStr = NSString()
    var defaultProgramStr = NSString()
    var defaultTeamStr  = NSArray()
    var groupAdminArray = NSArray()
    var joiningArray = NSArray()
    var isRowHidden = Bool()
    var mainProgramArray = NSArray()
    var mainGroupArray = NSArray()
    var mainTaskArray = NSArray()
    var SelectedTaskArray = NSArray()
    var SelectedgroupArray = NSArray()
    var SelectedProgramArray = NSMutableArray()
    var subTaskvaluesStr = NSMutableArray()
    var videoArray = NSArray()
    var navstr = NSString()
    var params = [String: Any]()
    var params1 = [String: AnyObject]()
    var dictionary =  [String: AnyObject]()
    var Unitarry : [[String:AnyObject]] = []
    var taskAndSubtaskArray = NSArray()
    var taskAndSubtaskMutableDic = [String : AnyObject]()
    var SubtaskMutableDic = [String : AnyObject]()
    var programGroupTaskDic = [String: AnyObject]()
    var programTaskDict = [String: AnyObject]()
    var taskAndSubtaskMutableArray = NSMutableArray()
    var selectedIndex = Int()
    var SubtaskMutableArray = NSMutableArray()
    var noGeneratArray = NSMutableArray()
    let program = "program"
    let team = "team"
    var myPickerView : UIPickerView!
    var openAndCloseStr = NSString()
    var selectActivityChecked = NSString()
    var navigatePushStr = NSString()
    var allcommentCountArray = NSMutableArray()
    var programIDStrIndex = NSString()
    var programIDElementArray = [String]()
    var isTrue = Bool()
    var selectedPickerIndex = Int()
    @IBOutlet var programTextfield: UITextField!
    @IBOutlet var tableViewHeightObj: NSLayoutConstraint!
    @IBOutlet var teamTextfieldObj: UITextField!
    
    @IBAction func sideMenuBtn(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isRowHidden = true
        self.tableViewObj.beginUpdates()
        self.tableViewObj.endUpdates()
        // self.tableViewHeightObj.constant = 0
        self.selected = true
        isTrue = false
        self.rowIndex = 0
        self.inComponentintex = 0
        self.openAndCloseStr = "1"
        //        self.tableViewObj.estimatedRowHeight = 100
        //        self.tableViewObj.rowHeight = UITableViewAutomaticDimension
        
        // --- TableHeaderView And FooterView -- ///
        
        self.tableViewObj.tableHeaderView = self.tableViewHeaderOBj
        self.tableViewObj.tableFooterView = self.footerViewObj
        
        self.tableViewObj .register(UINib (nibName: "TaskCell", bundle: nil), forCellReuseIdentifier: "TaskCell")
        self.tableViewObj .register(UINib (nibName: "SubTaskViewCell", bundle: nil), forCellReuseIdentifier: "SubTaskViewCell")
        self.tableViewObj .register(UINib (nibName: "HeaderTaskCell", bundle: nil), forCellReuseIdentifier: "HeaderTaskCell")
        self.tableViewObj .register(UINib (nibName: "FooterCell", bundle: nil), forCellReuseIdentifier: "FooterCell")
        self.tableViewObj .register(UINib (nibName: "SelectCell", bundle: nil), forCellReuseIdentifier: "SelectCell")
        // self.tableViewObj .register(UINib (nibName: "UploadBtnCell", bundle: nil), forCellReuseIdentifier: "UploadBtnCell")
        self.tableViewObj .register(UINib (nibName: "SubTaskCell1", bundle: nil), forCellReuseIdentifier: "SubTaskCell1")
        
        if self.navigatePushStr .isEqual(to: "pushNavigateStr") {
            
            //---- Notification checking -----//
            
            let elements = self.programIDElementArray
            if elements.contains(self.programIDStrIndex as String) {
                let indexConvertStr : Int = self.programIDElementArray.index(of: self.programIDStrIndex as String)!
                
                // -- index checking -- //
                let joiningStr : NSString = (self.joiningArray .object(at: indexConvertStr) as AnyObject) as! NSString
                
                if joiningStr .isEqual(to: "1") {
                    
                    self.programLabelHeight.constant = 30
                    self.programViewHeight.constant = 30
                    self.programTextfieldheight.constant = 30
                    self.programArrowImageHeight.constant = 20
                    self.teamHeightLabelObj.constant = 0
                    self.teamViewHeight.constant = 0
                    self.teamTextFieldHeight.constant = 0
                    self.arrowHeightObj.constant = 0
                    self.headerBottomLabelHeight.constant = 2
                    self.programbottmLineHeight.constant = 0
                    self.tableViewHeaderOBj.frame = CGRect(x:0, y: 0, width:self.tableViewHeaderOBj.frame.size.width, height:50)
                }
                else
                {
                    self.programLabelHeight.constant = 30
                    self.programViewHeight.constant = 30
                    self.programTextfieldheight.constant = 30
                    self.programArrowImageHeight.constant = 20
                    self.teamHeightLabelObj.constant = 30
                    self.teamViewHeight.constant = 30
                    self.teamTextFieldHeight.constant = 30
                    self.arrowHeightObj.constant = 20
                    self.headerBottomLabelHeight.constant = 2
                    self.programbottmLineHeight.constant = 1
                    self.tableViewHeaderOBj.frame = CGRect(x:0, y: 0, width:self.tableViewHeaderOBj.frame.size.width, height:100)
                }
                
                
                self.programTextfield.text = self.programArray .object(at: indexConvertStr) as? String
                // print(self.defaultTeamStr)
                self.teamTextfieldObj.text = ((self.defaultTeamStr .object(at: indexConvertStr) as AnyObject) .object(at: 0)) as? String
                let selectedStr = (self.programArray .object(at: indexConvertStr) as AnyObject) as? String
                self.indexOfObj = (self.programArray.index(of: selectedStr!))
                self.programIDStr = (self.programIDArray .object(at: indexConvertStr) as AnyObject) as! NSString
                self.SelectedgroupArray = (self.teamArray .object(at: indexConvertStr) as AnyObject) as! NSArray
                taskArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_name") as AnyObject)  as! NSArray
                
                self.taskAndSubtaskArray = (self.selectActivityArray .object(at: self.indexOfObj) as AnyObject) as! NSArray
                
                taskDescriptionArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_description") as AnyObject)  as! NSArray
                self.subActivityArray = ((self.selectActivityArray .object(at: self.indexOfObj)
                    as AnyObject).value(forKey: "subtasks") as AnyObject)  as! NSArray
                self.subAct_TypeArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "act_type") as AnyObject)  as! NSArray
                
                groupArray = ((self.teamArray .object(at: indexConvertStr) as AnyObject).value(forKey: "group_name") as AnyObject)  as! NSArray
                
                
                let selectProgramStr : NSString = (self.programArray[self.indexOfObj] as AnyObject)  as! NSString
                
                let predicate = NSPredicate(format:"prog_name==%@",selectProgramStr)
                let arr : NSArray = self.mainProgramArray.filtered(using: predicate) as NSArray
                if arr.count > 0
                {
                    self.SelectedProgramArray.removeAllObjects()
                    for item in arr {
                        self.SelectedProgramArray.add(item)
                    }
                }
                // --- reload Tableview --- //
                self.tableViewObj.reloadData()
            }
            else {
                self.presentAlertWithTitle(title: "", message:"Program not updated")
            }
            
            
        } else {
            // -- Default Selection --//
            
            let joiningStr : NSString = (self.joiningArray .object(at: 0) as AnyObject) as! NSString
            
            if joiningStr .isEqual(to: "1") {
                
                self.programLabelHeight.constant = 30
                self.programViewHeight.constant = 30
                self.programTextfieldheight.constant = 30
                self.programArrowImageHeight.constant = 20
                self.teamHeightLabelObj.constant = 0
                self.teamViewHeight.constant = 0
                self.teamTextFieldHeight.constant = 0
                self.arrowHeightObj.constant = 0
                self.headerBottomLabelHeight.constant = 2
                self.programbottmLineHeight.constant = 0
                self.tableViewHeaderOBj.frame = CGRect(x:0, y: 0, width:self.tableViewHeaderOBj.frame.size.width, height:50)
            }
            else
            {
                self.programLabelHeight.constant = 30
                self.programViewHeight.constant = 30
                self.programTextfieldheight.constant = 30
                self.programArrowImageHeight.constant = 20
                self.teamHeightLabelObj.constant = 30
                self.teamViewHeight.constant = 30
                self.teamTextFieldHeight.constant = 30
                self.arrowHeightObj.constant = 20
                self.headerBottomLabelHeight.constant = 2
                self.programbottmLineHeight.constant = 1
                self.tableViewHeaderOBj.frame = CGRect(x:0, y: 0, width:self.tableViewHeaderOBj.frame.size.width, height:100)
            }
            
            
            //self.programTextfield.text = self.defaultProgramStr as String
            self.programTextfield.text = self.programArray .object(at: 0) as? String
            self.teamTextfieldObj.text = ((self.defaultTeamStr .object(at: 0) as AnyObject) .object(at: 0)) as? String
            let selectedStr = (self.programArray .object(at: 0) as AnyObject) as? String
            self.indexOfObj = (self.programArray.index(of: selectedStr!))
            self.programIDStr = (self.programIDArray .object(at: 0) as AnyObject) as! NSString
            self.SelectedgroupArray = (self.teamArray .object(at: 0) as AnyObject) as! NSArray
            taskArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_name") as AnyObject)  as! NSArray
            
            self.taskAndSubtaskArray = (self.selectActivityArray .object(at: self.indexOfObj) as AnyObject) as! NSArray
            
            taskDescriptionArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_description") as AnyObject)  as! NSArray
            self.subActivityArray = ((self.selectActivityArray .object(at: self.indexOfObj)
                as AnyObject).value(forKey: "subtasks") as AnyObject)  as! NSArray
            self.subAct_TypeArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "act_type") as AnyObject)  as! NSArray
            
            groupArray = ((self.teamArray .object(at: 0) as AnyObject).value(forKey: "group_name") as AnyObject)  as! NSArray
            
            
            let selectProgramStr : NSString = (self.programArray[self.indexOfObj] as AnyObject)  as! NSString
            
            let predicate = NSPredicate(format:"prog_name==%@",selectProgramStr)
            let arr : NSArray = self.mainProgramArray.filtered(using: predicate) as NSArray
            if arr.count > 0
            {
                self.SelectedProgramArray.removeAllObjects()
                for item in arr {
                    self.SelectedProgramArray.add(item)
                }
            }
            /////////////////////////////////////
            // print(self.SelectedProgramArray)
            
            self.tableViewObj.reloadData()
        }
        
        //----- NSNotificationCenter ------ //
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectItem(fromCollectionView:)), name: NSNotification.Name(rawValue: "didSelectImageFromCollectionViewHeight"), object: nil)
    }
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didSelectImageFromCollectionViewHeight"), object: nil)
    }
    
    // MARK: - Base64images
    
    func Base64images(Base64Images: [AnyObject]) {
        print(self.camImageArray)
        
        self.base64Images = Base64Images
    }
    
    // MARK: - viewDidLayoutSubviews
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let height = min(self.view.bounds.size.height, self.tableViewObj.contentSize.height)
        self.tableViewHeightObj.constant = height
        self.view.layoutIfNeeded()
    }
    // MARK: - viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         sideMenuController?.isLeftViewSwipeGestureDisabled = false
        self.tableViewObj.beginUpdates()
        self.tableViewObj.endUpdates()
        // self.tableViewObj.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func didSelectItem(fromCollectionView notification: Notification)
    {
        self.tableViewObj.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func donePicker(_ sender: UIBarButtonItem) {
        //self.datePicker?.isHidden = true
        //self.pickerViewObj .removeFromSuperview()
        view.endEditing(true)
        // toolBar.isHidden = true
    }
    
    func backgroundTap(_ tapGR: UITapGestureRecognizer)
    {
        self.pickerViewObj .removeFromSuperview()
        self.pickerViewObj.isHidden = true
        self.pickerViewObj.resignFirstResponder()
        toolBar?.isHidden = true
    }
    
    // MARK: - UITextfieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        pickerViewObj.isHidden = false
        toolBar?.isHidden = false
        tempTxtField = textField
        if self.programTextfield == textField
        {
            selectedString = program
            if self.programArray.count == 0
            {
                
            }
            else
            {
                self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                self.myPickerView.delegate = self
                self.myPickerView.dataSource = self
                self.myPickerView.backgroundColor = UIColor.white
                self.programTextfield.inputView = self.myPickerView
                
                // ToolBar
                let toolBar = UIToolbar()
                toolBar.barStyle = .default
                toolBar.isTranslucent = true
                toolBar.tintColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1)
                toolBar.backgroundColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1)
                toolBar.sizeToFit()
                
                // Adding Button ToolBar
                let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
                doneButton.setTitleTextAttributes([
                    NSFontAttributeName : UIFont(name: "HelveticaNeue", size: 17)!,
                    NSForegroundColorAttributeName : UIColor.black,
                    ], for: .normal)
                let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
                cancelButton.setTitleTextAttributes([
                    NSFontAttributeName : UIFont(name: "HelveticaNeue", size: 17)!,
                    NSForegroundColorAttributeName : UIColor.black,
                    ], for: .normal)
                toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
                self.programTextfield.inputAccessoryView = toolBar
                if self.rowIndex != nil {
                    self.myPickerView.selectRow(self.rowIndex, inComponent: self.inComponentintex, animated: false)
                }
                else {
                    
                }
                
                self.myPickerView.reloadAllComponents()
            }
        }
        if self.teamTextfieldObj == textField
        {
            selectedString = team
            if teamArray.count == 0
            {
                
            }
            else
            {
                self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                self.myPickerView.delegate = self
                self.myPickerView.dataSource = self
                self.myPickerView.backgroundColor = UIColor.white
                self.teamTextfieldObj.inputView = self.myPickerView
                
                // ToolBar
                let toolBar = UIToolbar()
                toolBar.barStyle = .default
                toolBar.isTranslucent = true
                toolBar.tintColor = UIColor(red: 169.0/255.0, green: 169.0/255.0, blue: 169.0/255.0, alpha: 1.0)
                toolBar.sizeToFit()
                
                // Adding Button ToolBar
                let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
                doneButton.setTitleTextAttributes([
                    NSFontAttributeName : UIFont(name: "HelveticaNeue", size: 17)!,
                    NSForegroundColorAttributeName : UIColor.black,
                    ], for: .normal)
                let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
                cancelButton.setTitleTextAttributes([
                    NSFontAttributeName : UIFont(name: "HelveticaNeue", size: 17)!,
                    NSForegroundColorAttributeName : UIColor.black,
                    ], for: .normal)
                toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
                self.teamTextfieldObj.inputAccessoryView = toolBar
                if self.rowIndex != nil {
                    self.myPickerView.selectRow(self.rowIndex, inComponent: self.inComponentintex, animated: false)
                }
                else {
                    
                }
                self.myPickerView.reloadAllComponents()
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == headerTaskCell.numberTextfieldObjs.tag {
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if textField.tag == headerTaskCell.numberTextfieldObjs.tag {
            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            if filtered == string {
                return true
            } else {
                
                if string == "." {
                    let countdots = textField.text!.components(separatedBy:".").count - 1
                    if countdots == 0 {
                        return true
                    }else{
                        if countdots > 0 && string == "." {
                            return false
                        } else {
                            return true
                        }
                    }
                }else{
                    return false
                }
            }
        }
        if textField.tag == headerTaskCell.hoursTextfieldObj.tag  {
            // Create an `NSCharacterSet` set which includes everything *but* the digits
            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            // At every character in this "inverseSet" contained in the string,
            // split the string up into components which exclude the characters
            // in this inverse set
            let components = string.components(separatedBy: inverseSet)
            // Rejoin these components
            let filtered = components.joined(separator: "")  // use join("", components) if you are using Swift 1.2
            // If the original string is equal to the filtered string, i.e. if no
            // inverse characters were present to be eliminated, the input is valid
            // and the statement returns true; else it returns false
            return string == filtered
        }
        if textField.tag == footercell.youtubeTextfieldUrl.tag {
        }
        if  self.tagcheckInt == 0 {
            if textField.tag ==  subtaskCell.valueTextfieldObj.tag  {
                var v : UIView = textField
                repeat { v = v.superview! } while !(v is SubTaskViewCell)
                let cell = v as! SubTaskViewCell
                if cell != nil
                {
                    let index : NSIndexPath = self.tableViewObj.indexPath(for:cell)! as NSIndexPath
                    let tagNo : Int = index.row
                    if textField.tag == tagNo {
                        let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
                        let components = string.components(separatedBy: inverseSet)
                        let filtered = components.joined(separator: "")
                        if filtered == string {
                            return true
                        } else {
                            if string == "." {
                                let countdots = textField.text!.components(separatedBy:".").count - 1
                                if countdots == 0 {
                                    return true
                                }else{
                                    if countdots > 0 && string == "." {
                                        return false
                                    } else {
                                        return true
                                    }
                                }
                            }else{
                                return false
                            }
                        }
                    }
                }
            }
        }
        return true
    }
    
    //MARK:- Button
    func doneClick() {
        self.programTextfield.resignFirstResponder()
        self.teamTextfieldObj.resignFirstResponder()
    }
    func cancelClick() {
        self.programTextfield.resignFirstResponder()
        self.teamTextfieldObj.resignFirstResponder()
    }
    
    // MARK: - UIPickerViewDataSourse
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if (selectedString?.isEqual(program))! {
            return self.programArray.count
        }
        if (selectedString?.isEqual(team))! {
            
            return self.groupArray.count
        }
        return 0
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if (selectedString?.isEqual(program))! {
            return 1
        }
        if (selectedString?.isEqual(team))! {
            return 1
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        if (selectedString?.isEqual(program))!
        {
            return (self.programArray[row]  as AnyObject) as? String
        }
        if (selectedString?.isEqual(team))!
        {
            
            return (self.groupArray[row]  as AnyObject) as? String
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        headerTaskCell.numberTextfieldObjs.text = ""
        headerTaskCell.hoursTextfieldObj.text = ""
        isTrue = false
        
        if (selectedString?.isEqual(program))!
        {
            tempTxtField?.text = (self.programArray[row] as AnyObject) as? String
            self.rowIndex = row
            self.inComponentintex = component
            
            self.openAndCloseStr = "1"
            let joiningStr1 : NSString = (self.joiningArray[row] as AnyObject) as! NSString
            if joiningStr1 .isEqual(to: "1") {
                self.programLabelHeight.constant = 30
                self.programViewHeight.constant = 30
                self.programTextfieldheight.constant = 30
                self.programArrowImageHeight.constant = 20
                self.teamHeightLabelObj.constant = 0
                self.teamViewHeight.constant = 0
                self.teamTextFieldHeight.constant = 0
                self.arrowHeightObj.constant = 0
                self.headerBottomLabelHeight.constant = 2
                self.programbottmLineHeight.constant = 0
                self.tableViewHeaderOBj.frame = CGRect(x:0, y: 0, width:self.tableViewHeaderOBj.frame.size.width, height:50)
            }
            if joiningStr1 .isEqual(to: "2")
            {
                self.programLabelHeight.constant = 30
                self.programViewHeight.constant = 30
                self.programTextfieldheight.constant = 30
                self.programArrowImageHeight.constant = 20
                self.teamHeightLabelObj.constant = 30
                self.teamViewHeight.constant = 30
                self.headerBottomLabelHeight.constant = 2
                self.teamTextFieldHeight.constant = 30
                self.arrowHeightObj.constant = 20
                self.programbottmLineHeight.constant = 1
                self.tableViewHeaderOBj.frame = CGRect(x:0, y: 0, width:self.tableViewHeaderOBj.frame.size.width, height:100)
            }
            // --- Select particular Program --- //
            let selectProgramStr : NSString = (self.programArray[row] as AnyObject)  as! NSString
            let predicate = NSPredicate(format:"prog_name==%@",selectProgramStr)
            let arr : NSArray = self.mainProgramArray.filtered(using: predicate) as NSArray
            if arr.count > 0
            {
                self.SelectedProgramArray.removeAllObjects()
                for item in arr {
                    
                    self.SelectedProgramArray.add(item)
                }
            }
            /////////////////////////////////////
            self.programTaskDict = self.SelectedProgramArray .object(at: 0) as! [String:AnyObject]
            //  let joiningStr : NSString = (self.joiningArray .object(at: 0) as AnyObject) as! NSString
            let joiningStr : NSString = (self.joiningArray[row] as AnyObject) as! NSString
            let selectedStr = (self.programArray[row] as AnyObject) as? String
            self.programIDStr = (self.programIDArray[row] as AnyObject) as! NSString
            self.indexOfObj = (self.programArray.index(of: selectedStr!))
            if joiningStr .isEqual(to: "1") {
                self.getgroupName(indexOfObj)
                self.subActivityArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "subtasks") as AnyObject)  as! NSArray
                taskArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_name") as AnyObject)  as! NSArray
                taskDescriptionArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_description") as AnyObject)  as! NSArray
                self.subActivityArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "subtasks") as AnyObject)  as! NSArray
                self.taskAndSubtaskArray = (self.selectActivityArray .object(at: self.indexOfObj) as AnyObject) as! NSArray
                print(self.taskAndSubtaskArray)
                self.subAct_TypeArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "act_type") as AnyObject)  as! NSArray
                self.tableViewObj.reloadData()
                self.programLabelHeight.constant = 30
                self.programViewHeight.constant = 30
                self.programTextfieldheight.constant = 30
                self.programArrowImageHeight.constant = 20
                self.teamHeightLabelObj.constant = 0
                self.teamViewHeight.constant = 0
                self.teamTextFieldHeight.constant = 0
                self.arrowHeightObj.constant = 0
                self.headerBottomLabelHeight.constant = 2
                self.programbottmLineHeight.constant = 0
                self.tableViewHeaderOBj.frame = CGRect(x:0, y: 0, width:self.tableViewHeaderOBj.frame.size.width, height:50)
            }
            if joiningStr .isEqual(to: "2")
            {
                self.getgroupName(indexOfObj)
                
                // 2nd Textfield and TableView Reload
                
                let teamDefaultStr : NSString = ((((self.teamArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "group_name") as AnyObject) .firstObject) as? NSString)!
                
                self.teamTextfieldObj?.text =  teamDefaultStr as String
                self.subActivityArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "subtasks") as AnyObject)  as! NSArray
                // self.tableViewObj.reloadData()
                
                // ----- ///
                
                
                self.subActivityArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "subtasks") as AnyObject)  as! NSArray
                taskArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_name") as AnyObject)  as! NSArray
                taskDescriptionArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_description") as AnyObject)  as! NSArray
                self.subActivityArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "subtasks") as AnyObject)  as! NSArray
                self.taskAndSubtaskArray = (self.selectActivityArray .object(at: self.indexOfObj) as AnyObject) as! NSArray
                print(self.taskAndSubtaskArray)
                self.subAct_TypeArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "act_type") as AnyObject)  as! NSArray
                self.tableViewObj.reloadData()
                self.programLabelHeight.constant = 30
                self.programViewHeight.constant = 30
                self.programTextfieldheight.constant = 30
                self.programArrowImageHeight.constant = 20
                self.teamHeightLabelObj.constant = 30
                self.teamViewHeight.constant = 30
                self.teamTextFieldHeight.constant = 30
                self.arrowHeightObj.constant = 20
                self.headerBottomLabelHeight.constant = 2
                self.programbottmLineHeight.constant = 1
                self.tableViewHeaderOBj.frame = CGRect(x:0, y: 0, width:self.tableViewHeaderOBj.frame.size.width, height:100)
            }
            //self.programTextfield .resignFirstResponder()
        }
        if (selectedString?.isEqual(team))!
        {
            tempTxtField?.text = (self.groupArray[row] as AnyObject) as? String
            self.rowIndex = row
            self.inComponentintex = component
            taskArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_name") as AnyObject)  as! NSArray
            taskDescriptionArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_description") as AnyObject)  as! NSArray
            self.subActivityArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "subtasks") as AnyObject)  as! NSArray
            self.taskAndSubtaskArray = (self.selectActivityArray .object(at: self.indexOfObj) as AnyObject) as! NSArray
            print(self.taskAndSubtaskArray)
            self.subAct_TypeArray = ((self.selectActivityArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "act_type") as AnyObject)  as! NSArray
            self.tableViewObj.reloadData()
            // self.teamTextfieldObj .resignFirstResponder()
        }
    }
    
    
    
    /// ---------  Get Team Name ----- ///
    
    func getgroupName(_ index: Int) {
        groupArray = ((self.teamArray .object(at: index) as AnyObject).value(forKey: "group_name") as AnyObject)  as! NSArray
        self.SelectedgroupArray = (self.teamArray .object(at: index) as AnyObject) as! NSArray
        self.taskAndSubtaskArray = (self.selectActivityArray .object(at: index) as AnyObject) as! NSArray
        print(self.taskAndSubtaskArray)
        
        //pickerViewObj.reloadAllComponents()
    }
    
    
    
    
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if section == 1 {
            return self.taskArray.count
        }
        if section == 2 {
            return 1
        }
        if section == 3 {
            return self.subtaskTitleArray.count
        }
        if section == 4 {
            return 1
        }
        /* if section == 5 {
         return 1
         }*/
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell : SelectCell = tableView .dequeueReusableCell(withIdentifier: "SelectCell") as! SelectCell
            cell.selectActivityBtn.tag = indexPath.row
            cell.selectActivityBtn.addTarget(self, action: #selector(self.selectActivityBtn), for: .touchUpInside)
            return cell
        }
        if indexPath.section == 1 {
            taskCell = tableView .dequeueReusableCell(withIdentifier: "TaskCell") as! TaskCell
            
            let zeroStr : NSString = self.subActivityArray[indexPath.row] as! NSString
            
            if zeroStr.isEqual(to: "0")
            {
                taskCell.taskNameLabelObj.text = self.taskArray[indexPath.row] as? String
            }
            else
            {
                taskCell.taskNameLabelObj.text = (self.taskArray[indexPath.row] as? String)! + " Has " + (self.subActivityArray[indexPath.row] as? String)! + " Sub Activities"
            }
            return taskCell
        }
        
        if indexPath.section == 2 {
            headerTaskCell = tableView .dequeueReusableCell(withIdentifier: "HeaderTaskCell") as! HeaderTaskCell
            headerTaskCell.nameLabel.text = self.selectedTaskName as String
            
            if self.selectedTaskDespName .isEqual(to: "") {
                headerTaskCell.descriptionLabel.text =  self.selectedTaskDespName as String
                headerTaskCell.deptLabelheight.constant = 0
                headerTaskCell.desptBottmLIneHeight.constant = 0
            }
            else {
                headerTaskCell.descriptionLabel.text =  self.selectedTaskDespName as String
                headerTaskCell.deptLabelheight.constant = 21
                headerTaskCell.desptBottmLIneHeight.constant = 1
            }
            
            
            
            headerTaskCell.numberTextfieldObjs.delegate = self
            // headerTaskCell.numberTextfieldObjs.tag = indexPath.row
            headerTaskCell.hoursTextfieldObj.delegate = self
            // headerTaskCell.hoursTextfieldObj.tag = indexPath.row
            headerTaskCell.numberTextfieldObjs.addTarget(self, action: #selector(HeadertextFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            
            
            if self.act_TypeStr .isEqual(to: "2") {
                headerTaskCell.numberLabelHeight.constant = 0
                headerTaskCell.numberTextfieldhieght.constant = 0
                headerTaskCell.numberBottomLineLabel.constant = 0
                headerTaskCell.hourLabelHeight.constant = 30
                headerTaskCell.hourTextfieldHeight.constant = 30
            }
            if self.act_TypeStr .isEqual(to: "1")
            {
                headerTaskCell.numberLabelHeight.constant = 30
                headerTaskCell.numberTextfieldhieght.constant = 30
                headerTaskCell.numberBottomLineLabel.constant = 1
            }
            
            if self.subtaskNumberArray.count == 0 {
                
            }
            else
            {
                headerTaskCell.numberLabel.text = (self.subtaskNumberArray .object(at: 0) as AnyObject) as? String
                let taskUnitStr = (self.subtaskNumberArray .object(at: 0) as AnyObject) as! NSString
                if taskUnitStr .isEqual(to: "Text") {
                    
                    headerTaskCell.numberLabel.text = "Number"
                }
                if taskUnitStr .isEqual(to: "Other") {
                    
                    headerTaskCell.numberLabel.text = "Number"
                }
                if taskUnitStr .isEqual(to: "Minutes") {
                    
                    headerTaskCell.numberLabel.text = "Number"
                }
                //Minutes
                if taskUnitStr .isEqual(to: "Minutes") {
                    headerTaskCell.hourLabelHeight.constant = 0
                    headerTaskCell.hourTextfieldHeight.constant = 0
                }
                else
                {
                    
                }
            }
            return headerTaskCell
        }
        if indexPath.section == 3 {
            
            if self.act_TypeStr .isEqual(to: "2")
            {
                subtaskCell1 = tableView .dequeueReusableCell(withIdentifier: "SubTaskCell1") as! SubTaskCell1
                if  self.openAndCloseStr .isEqual(to: "1") {
                    subtaskCell1.isHidden = true
                } else {
                    subtaskCell1.isHidden = false
                }
                subtaskCell1.countLabel.text = self.noGeneratArray[indexPath.row] as? String
                subtaskCell1.titleNameLabel.text = self.subtaskTitleArray[indexPath.row] as? String
                subtaskCell1.descriptionLabel.text = self.subtaskDescripionArray[indexPath.row] as? String
                return subtaskCell1
            }
            if self.act_TypeStr .isEqual(to: "1")
            {
                subtaskCell = tableView .dequeueReusableCell(withIdentifier: "SubTaskViewCell") as! SubTaskViewCell
                
                subtaskCell.valueTextfieldObj.delegate = self
                subtaskCell.valueTextfieldObj.tag = indexPath.row
                
                if  self.openAndCloseStr .isEqual(to: "1") {
                    subtaskCell.isHidden = true
                } else {
                    subtaskCell.isHidden = false
                }
                
                subtaskCell.countLabel.text = self.noGeneratArray[indexPath.row] as? String
                subtaskCell.titleNameLabel.text = self.subtaskTitleArray[indexPath.row] as? String
                subtaskCell.descriptionLabel.text = self.subtaskDescripionArray[indexPath.row] as? String
                subtaskCell.numberLabel.text = self.subtaskNumberArray[indexPath.row] as? String
                
                
                
                self.subTaskAct_TypeStr = (self.subtaskAct_TypeArray .object(at: indexPath.row) as AnyObject) as! NSString
                
                if isTrue == false {
                    subtaskCell.valueTextfieldObj.text = ""
                }
                
                subtaskCell.valueTextfieldObj.addTarget(self, action: #selector(SubTasktextFieldDidChange(_:)), for: UIControlEvents.editingChanged)
                
                if subtaskCell.numberLabel.text != "Number" {
                    
                    subtaskCell.numberLabel.text = "Number"
                }
                
                /*  let userID : NSString = (UserDefaults.standard.value(forKey: "userID") as? NSString)!
                 let groupAdminIDStr : NSString = (((self.groupAdminArray .object(at: 0) as AnyObject) .object(at: 0)) as? NSString)!
                 if !(groupAdminIDStr == userID) {
                 subtaskCell.numberHeightObj.constant = 0
                 subtaskCell.heightLabelNumberObj.constant = 0
                 }
                 else
                 {
                 
                 }*/
                return subtaskCell
            }
        }
        if indexPath.section == 4 {
            
            footercell = tableView .dequeueReusableCell(withIdentifier: "FooterCell") as! FooterCell
            
            if  self.openAndCloseStr .isEqual(to: "1") {
                footercell.isHidden = true
            } else {
                footercell.isHidden = false
            }
            
            footercell.camBtnTaped.tag = indexPath.row
            footercell.camBtnTaped.addTarget(self, action: #selector(self.camBtnTapped), for: .touchUpInside)
            footercell.youtubeTextfieldUrl.delegate = self
            footercell.youtubeTextfieldUrl.addTarget(self, action: #selector(youtubeTextfieldUrl), for: .editingDidBegin)
            footercell.delegate = self
            footercell.youtubeTextfieldUrl.tag = indexPath.row
            footercell.youtubeTextfieldUrl.delegate = self
            self.footercell.setCollectionData(collection: self.imageArray)
            if self.imageArray.count == 0
            {
                footercell.heightColView.constant = 0
                self.base64Images.removeAll()
                self.isTrue = true
            }
            if self.imageArray.count == 1
            {
                footercell.heightColView.constant = 110
                self.isTrue = true
            }
            if self.imageArray.count == 2
            {
                footercell.heightColView.constant = 205
                self.isTrue = true
            }
            if self.imageArray.count == 3
            {
                footercell.heightColView.constant = 305
                self.isTrue = true
            }
            if self.imageArray.count == 4
            {
                footercell.heightColView.constant = 420
                self.isTrue = true
            }
            if self.imageArray.count == 5
            {
                footercell.heightColView.constant = 520
                self.isTrue = true
            }
            if self.imageArray.count == 6
            {
                footercell.heightColView.constant = 0
            }
            return footercell
        }
        return headerTaskCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 44
        }
        if indexPath.section == 1 {
            
            var height:CGFloat = 40.0
            
            if (self.isRowHidden == true)
            {
                self.isRowHidden = true
                height = 0.0
            }
            else
            {
                self.isRowHidden = false
                height = 45.0
            }
            return height
        }
        if indexPath.section == 2 {
            //return 155
            var height:CGFloat = 155.0
            
            if  self.openAndCloseStr .isEqual(to: "1") {
                height = 0.0
            }
            if  self.openAndCloseStr .isEqual(to: "0") {
                if (self.isRowHidden == true)
                {
                    self.isRowHidden = true
                    height = 0.0
                }
                else
                {
                    if self.act_TypeStr .isEqual(to: "2") {
                        self.isRowHidden = false
                        height = 115.0
                    }
                    else
                    {
                        self.isRowHidden = false
                        height = 155.0
                    }
                    
                    
                }
            }
            return height
        }
        if indexPath.section == 3 {
            // return 130
            var height:CGFloat = 0.0
            
            if  self.openAndCloseStr .isEqual(to: "1") {
                height = 0.0
            }
            else {
                if (self.isRowHidden == true) {
                    self.isRowHidden = true
                    height = 0.0
                }
                else
                {
                    if  self.subtaskTitleArray.count == 0 {
                        self.isRowHidden = false
                        height = 0.0
                    }
                    else
                    {
                        if self.act_TypeStr .isEqual(to: "2") {
                            self.isRowHidden = false
                            height = 80.0
                        }
                        if self.act_TypeStr .isEqual(to: "1") {
                            self.isRowHidden = false
                            height = 130.0
                        }
                    }
                }
            }
            return height
        }
        if indexPath.section == 4 {
            var height:CGFloat = 206.0
            
            if  self.openAndCloseStr .isEqual(to: "1") {
                height = 0.0
            }
            if  self.openAndCloseStr .isEqual(to: "0") {
                if self.imageArray.count == 0
                {
                    // return 206
                    if  self.openAndCloseStr .isEqual(to: "1") {
                        height = 0.0
                    }
                    else {
                        height = 206.0
                    }
                    
                }
                if self.imageArray.count == 1
                {
                    //return 320
                    height = 320.0
                }
                if self.imageArray.count == 2
                {
                    //return 415
                    height = 415.0
                }
                if self.imageArray.count == 3
                {
                    // return 510
                    height = 510.0
                }
                if self.imageArray.count == 4
                {
                    // return 605
                    height = 605.0
                }
                if self.imageArray.count == 5
                {
                    // return 705
                    height = 705.0
                }
            }
            return height
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            
        }
        if indexPath.section == 1 {
            
            self.selectActivityChecked = "0"
            self.openAndCloseStr = "0"
            self.selectedIndex = indexPath.row
            headerTaskCell.numberTextfieldObjs.text = ""
            headerTaskCell.hoursTextfieldObj.text = ""
            isTrue = false
            
            self.taskAndSubtaskMutableDic = self.taskAndSubtaskArray .object(at: indexPath.row) as! [String : AnyObject]
            
            // print(self.taskAndSubtaskMutableDic)
            self.taskAndSubtaskMutableDic["tasks"] = "" as AnyObject
            //  print(self.taskAndSubtaskMutableDic)
            
            (self.SelectedProgramArray.value(forKey: "tasks") as AnyObject).setValue("", forKey: "tasks")
            //  print(self.SelectedProgramArray)
            // print(self.taskArray)
            self.selectedTaskName = self.taskArray[indexPath.row] as! NSString
            self.selectedTaskDespName = self.taskDescriptionArray[indexPath.row] as! NSString
            self.tableViewObj.reloadData()
            self.taskIDArray = ((self.selectActivityIDArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "task_id") as AnyObject)  as! NSArray
            
            self.subTaskCountArray = (self.selectActivityIDArray .object(at: self.indexOfObj) as AnyObject).value(forKey: "subtasks") as! NSArray
            
            self.subTaskSelectCount =  (self.subTaskCountArray .object(at: indexPath.row) as AnyObject) as! NSString
            
            self.taskIDStr = (self.taskIDArray .object(at: indexPath.row) as AnyObject) as! NSString
            
            self.act_TypeStr = (self.subAct_TypeArray .object(at: indexPath.row) as AnyObject) as! NSString
            
            
            
            let params:[String: AnyObject] = [
                "purpose" : "getsubtasks" as AnyObject,
                "progid" : self.programIDStr as AnyObject,
                "taskid" : self.taskIDStr as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            
            
            
            // ------ SubActivityAPI Calling ------ //
            self.SubActivitiesAPICalling(param: params)
            
            DispatchQueue.main.async {
                let indexPath = IndexPath(item: 0, section: 2)
                self.tableViewObj.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        if indexPath.section == 2 {
            
        }
        if indexPath.section == 3 {
            
        }
        if indexPath.section == 4 {
            
        }
        /*  if indexPath.section == 5 {
         
         }*/
    }
    
    // MARK: - TextFields Action
    
    func HeadertextFieldDidChange(_ textField:UITextField) {
        dictionary.updateValue(textField.text! as AnyObject, forKey: self.taskIDStr as String)
        self.Unitarry.append(dictionary)
        // self.subTaskvaluesStr.add(textField.text!)
        headerTaskCell.numberTextfieldObjs.tag = textField.tag
        print(self.dictionary)
        let indexPath = IndexPath(row: self.dictionary.count, section: 2)
        self.tableViewObj.cellForRow(at: indexPath)
    }
    
    
    func SubTasktextFieldDidChange(_ textField:UITextField) {
        
        // print(textField.tag)
        // print("\(String(describing: textField.text))")
        
        
        var v : UIView = textField
        repeat { v = v.superview! } while !(v is SubTaskViewCell)
        let cell = v as! SubTaskViewCell
        if cell != nil
        {
            _ = self.tableViewObj.indexPath(for:cell)!
            self.tagcheckInt = 0
            dictionary.updateValue(textField.text! as AnyObject, forKey: self.subtaskIDArray .object(at: textField.tag) as! String)
            print(dictionary)
            self.Unitarry.append(dictionary)
            // self.subTaskvaluesStr.add(textField.text!)
            //subtaskCell.valueTextfieldObj.tag = textField.tag
            print(self.dictionary)
            let indexPath = IndexPath(row: self.dictionary.count, section: 3)
            self.tableViewObj.cellForRow(at: indexPath)
        }
    }
    
    
    
    func youtubeTextfieldUrl() {
        
        if self.imageArray.count == 0 {
            
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Are you want to delete All Images", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                self.imageArray.removeAllObjects()
                self.footercell.collectionViewObj.reloadData()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    func selectActivityBtn(_ sender: UIButton) {
        
        if (self.isRowHidden == false)
        {
            self.isRowHidden = true
            //  subtaskCell1.isHidden = true
            self.openAndCloseStr = "1"
            self.tableViewObj.reloadData()
        }
        else
        {
            self.isRowHidden = false
            self.openAndCloseStr = "1"
            self.tableViewObj.reloadData()
        }
    }
    
    @IBAction func uploadActivityBtnTapped(_ sender: Any) {
        
        
        let userID = UserDefaults.standard.value(forKey: "userID")
        let userName = UserDefaults.standard.value(forKey: "userName")
        
        if self.selectActivityChecked .isEqual(to: "0") {
            
            //   if (headerTaskCell.numberTextfieldObjs.text?.isEmpty)! ||  (subtaskCell.valueTextfieldObj.text?.isEmpty)! {
            
            if self.act_TypeStr .isEqual(to: "2") {
                if (headerTaskCell.hoursTextfieldObj.text?.isEmpty)!
                    // ||  (subtaskCell.valueTextfieldObj.text?.isEmpty)!
                {
                    self.presentAlertWithTitle(title: "", message:"You have not provide any activity details or documents. Activity not updated")
                    
                } else {
                    if headerTaskCell.hoursTextfieldObj.text?.isEmpty ?? true && footercell.youtubeTextfieldUrl.text?.isEmpty ?? true && self.dictionary.count == 0
                    {
                        let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                            "task" : self.taskAndSubtaskMutableDic ,
                            "imgURI" : self.base64Images,
                            "program" : self.SelectedProgramArray.firstObject!,
                            "group" : self.SelectedgroupArray.firstObject!,
                            "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                        MBProgressHUD.showAdded(to: self.view, animated: true)
                        self.uploadActivityApiCalling(param: params as [String : AnyObject])
                    }
                    else
                    {
                        if headerTaskCell.hoursTextfieldObj.text?.isEmpty ?? true && (footercell.youtubeTextfieldUrl.text?.isEmpty)!
                        {
                            let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                                "task" : self.taskAndSubtaskMutableDic ,
                                "imgURI" : self.base64Images,
                                "program" : self.SelectedProgramArray.firstObject!,
                                "group" : self.SelectedgroupArray.firstObject!,
                                "unit" : self.dictionary,
                                "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            self.uploadActivityApiCalling(param: params as [String : AnyObject])
                        }
                        else
                        {
                            if headerTaskCell.hoursTextfieldObj.text?.isEmpty ?? true  {
                                let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                                    "task" : self.taskAndSubtaskMutableDic ,
                                    "imgURI" : self.base64Images,
                                    "program" : self.SelectedProgramArray.firstObject!,
                                    "group" : self.SelectedgroupArray.firstObject!,
                                    "unit" : self.dictionary,
                                    "video" : footercell.youtubeTextfieldUrl.text!,
                                    "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                                MBProgressHUD.showAdded(to: self.view, animated: true)
                                self.uploadActivityApiCalling(param: params as [String : AnyObject])
                            }
                            else
                            {
                                if footercell.youtubeTextfieldUrl.text?.isEmpty ?? true
                                {
                                    let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                                        "task" : self.taskAndSubtaskMutableDic ,
                                        "imgURI" : self.base64Images,
                                        "program" : self.SelectedProgramArray.firstObject!,
                                        "group" : self.SelectedgroupArray.firstObject!,
                                        "unit" : self.dictionary,
                                        "hours" : headerTaskCell.hoursTextfieldObj.text!,
                                        "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    self.uploadActivityApiCalling(param: params as [String : AnyObject])
                                    
                                }
                                else
                                {
                                    if ( self.dictionary.count == 0 )  {
                                        
                                        let params = [
                                            "purpose" : "fastupload",
                                            "id" : userID as Any,
                                            "name" : userName as Any,
                                            "upload" : [
                                                "task" : self.taskAndSubtaskMutableDic,
                                                "imgURI" : self.base64Images,
                                                "program" : self.SelectedProgramArray.firstObject!,
                                                "group" : self.SelectedgroupArray.firstObject! ,
                                                "hours" : headerTaskCell.hoursTextfieldObj.text!,
                                                "video" : footercell.youtubeTextfieldUrl.text!,
                                                "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                                        MBProgressHUD.showAdded(to: self.view, animated: true)
                                        self.uploadActivityApiCalling(param: params as [String : AnyObject])
                                    }
                                    else {
                                        
                                        if self.SelectedgroupArray.count == 0 {
                                            
                                        }
                                        else {
                                            let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                                                "task" : self.taskAndSubtaskMutableDic ,
                                                "imgURI" : self.base64Images,
                                                "program" : self.SelectedProgramArray.firstObject!,
                                                "group" : self.SelectedgroupArray.firstObject!,
                                                "unit" : self.dictionary,
                                                "hours" : headerTaskCell.hoursTextfieldObj.text!,
                                                "video" : footercell.youtubeTextfieldUrl.text!,
                                                "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                                            
                                            MBProgressHUD.showAdded(to: self.view, animated: true)
                                            self.uploadActivityApiCalling(param: params as [String : AnyObject])
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                if (headerTaskCell.numberTextfieldObjs.text?.isEmpty)!
                    // ||  (subtaskCell.valueTextfieldObj.text?.isEmpty)!
                {
                    if (headerTaskCell.hoursTextfieldObj.text?.isEmpty)!
                        // ||  (subtaskCell.valueTextfieldObj.text?.isEmpty)!
                    {
                        
                        if self.base64Images.count == 0 {
                            
                            if self.dictionary.count == 0 {
                                self.presentAlertWithTitle(title: "", message:"You have not provide any activity details or documents. Activity not updated")
                            }
                            else {
                                let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                                    "task" : self.taskAndSubtaskMutableDic ,
                                    "imgURI" : self.base64Images,
                                    "unit" : self.dictionary,
                                    "program" : self.SelectedProgramArray.firstObject!,
                                    "group" : self.SelectedgroupArray.firstObject!,
                                    "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                                MBProgressHUD.showAdded(to: self.view, animated: true)
                                self.uploadActivityApiCalling(param: params as [String : AnyObject])
                            }
                        }
                        else {
                            let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                                "task" : self.taskAndSubtaskMutableDic ,
                                "imgURI" : self.base64Images,
                                "program" : self.SelectedProgramArray.firstObject!,
                                "group" : self.SelectedgroupArray.firstObject!,
                                "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            self.uploadActivityApiCalling(param: params as [String : AnyObject])
                        }
                    }
                    else {
                        let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                            "task" : self.taskAndSubtaskMutableDic ,
                            "imgURI" : self.base64Images,
                            "program" : self.SelectedProgramArray.firstObject!,
                            "hours" : headerTaskCell.hoursTextfieldObj.text!,
                            "group" : self.SelectedgroupArray.firstObject!,
                            "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                        MBProgressHUD.showAdded(to: self.view, animated: true)
                        self.uploadActivityApiCalling(param: params as [String : AnyObject])
                    }
                    
                    
                } else {
                    if headerTaskCell.hoursTextfieldObj.text?.isEmpty ?? true && footercell.youtubeTextfieldUrl.text?.isEmpty ?? true && self.dictionary.count == 0
                    {
                        let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                            "task" : self.taskAndSubtaskMutableDic ,
                            "imgURI" : self.base64Images,
                            "program" : self.SelectedProgramArray.firstObject!,
                            "group" : self.SelectedgroupArray.firstObject!,
                            "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                        MBProgressHUD.showAdded(to: self.view, animated: true)
                        self.uploadActivityApiCalling(param: params as [String : AnyObject])
                    }
                    else
                    {
                        if headerTaskCell.hoursTextfieldObj.text?.isEmpty ?? true && (footercell.youtubeTextfieldUrl.text?.isEmpty)!
                        {
                            let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                                "task" : self.taskAndSubtaskMutableDic ,
                                "imgURI" : self.base64Images,
                                "program" : self.SelectedProgramArray.firstObject!,
                                "group" : self.SelectedgroupArray.firstObject!,
                                "unit" : self.dictionary,
                                "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            self.uploadActivityApiCalling(param: params as [String : AnyObject])
                        }
                        else
                        {
                            if headerTaskCell.hoursTextfieldObj.text?.isEmpty ?? true  {
                                let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                                    "task" : self.taskAndSubtaskMutableDic ,
                                    "imgURI" : self.base64Images,
                                    "program" : self.SelectedProgramArray.firstObject!,
                                    "group" : self.SelectedgroupArray.firstObject!,
                                    "unit" : self.dictionary,
                                    "video" : footercell.youtubeTextfieldUrl.text!,
                                    "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                                MBProgressHUD.showAdded(to: self.view, animated: true)
                                self.uploadActivityApiCalling(param: params as [String : AnyObject])
                                
                            }
                            else
                            {
                                if footercell.youtubeTextfieldUrl.text?.isEmpty ?? true
                                {
                                    let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                                        "task" : self.taskAndSubtaskMutableDic ,
                                        "imgURI" : self.base64Images,
                                        "program" : self.SelectedProgramArray.firstObject!,
                                        "group" : self.SelectedgroupArray.firstObject!,
                                        "unit" : self.dictionary,
                                        "hours" : headerTaskCell.hoursTextfieldObj.text!,
                                        "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    self.uploadActivityApiCalling(param: params as [String : AnyObject])
                                    
                                }
                                else
                                {
                                    if ( self.dictionary.count == 0 )  {
                                        
                                        let params = [
                                            "purpose" : "fastupload",
                                            "id" : userID as Any,
                                            "name" : userName as Any,
                                            "upload" : [
                                                "task" : self.taskAndSubtaskMutableDic,
                                                "imgURI" : self.base64Images,
                                                "program" : self.SelectedProgramArray.firstObject!,
                                                "group" : self.SelectedgroupArray.firstObject! ,
                                                "hours" : headerTaskCell.hoursTextfieldObj.text!,
                                                "video" : footercell.youtubeTextfieldUrl.text!,
                                                "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                                        MBProgressHUD.showAdded(to: self.view, animated: true)
                                        self.uploadActivityApiCalling(param: params as [String : AnyObject])
                                    }
                                    else {
                                        
                                        if self.SelectedgroupArray.count == 0 {
                                            
                                        }
                                        else {
                                            let params = ["purpose": "fastupload", "id": userID as Any,"name" : userName as Any ,"upload": [
                                                "task" : self.taskAndSubtaskMutableDic ,
                                                "imgURI" : self.base64Images,
                                                "program" : self.SelectedProgramArray.firstObject!,
                                                "group" : self.SelectedgroupArray.firstObject!,
                                                "unit" : self.dictionary,
                                                "hours" : headerTaskCell.hoursTextfieldObj.text!,
                                                "video" : footercell.youtubeTextfieldUrl.text!,
                                                "actnotes" : footercell.commentTextviewObj.text!] as [String: Any]]
                                            
                                            MBProgressHUD.showAdded(to: self.view, animated: true)
                                            self.uploadActivityApiCalling(param: params as [String : AnyObject])
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            
        } else {
            self.presentAlertWithTitle(title: "", message:"Please Select an Activity to upload its data.")
        }
        
    }
    // MARK: - uploadActivityApiCalling
    
    func uploadActivityApiCalling(param:[String: AnyObject])  {
        
        if Reachability.isConnectedToNetwork() == true {
            
            Alamofire.request(BaseURL_FastUPloadActivitiesAPI, method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let JSON = response.result.value as? NSDictionary {
                        
                        let status : NSNumber  =  JSON .value(forKey: "status") as! NSNumber
                        let statusstr : NSString = status.stringValue as NSString
                        
                        if statusstr .isEqual(to: "0") {
                            let alertController = UIAlertController(title: "", message: ((JSON .value(forKey: "msg") as AnyObject) as! String), preferredStyle: .alert)
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(cancelAction)
                            
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.base64Images.removeAll()
                                self.taskAndSubtaskMutableDic.removeAll()
                                self.dictionary.removeAll()
                                // self.SelectedProgramArray.removeAllObjects()
                                self.imageArray.removeAllObjects()
                                self.openAndCloseStr = "1"
                                self.selectActivityChecked = "1"
                                self.tableViewObj.reloadData()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                        if statusstr .isEqual(to: "1") {
                            let alertController = UIAlertController(title: "", message: ((JSON .value(forKey: "msg") as AnyObject) as! String), preferredStyle: .alert)
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                
                            }
                            alertController.addAction(cancelAction)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.selectActivityChecked = "1"
                                self.base64Images.removeAll()
                                self.taskAndSubtaskMutableDic.removeAll()
                                self.dictionary.removeAll()
                                // self.SelectedProgramArray.removeAllObjects()
                                self.imageArray.removeAllObjects()
                                self.openAndCloseStr = "1"
                                self.tableViewObj.reloadData()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }                        
                    }
            }
        }
        else {
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
        
        /* if Reachability.isConnectedToNetwork() == true {
         
         let configuration = URLSessionConfiguration.default
         let session = URLSession(configuration: configuration)
         let url = NSURL(string:BaseURL_FastUPloadActivitiesAPI)
         let request = NSMutableURLRequest(url: url! as URL)
         request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
         
         request.addValue("application/json", forHTTPHeaderField: "Accept")
         request.timeoutInterval = 60.0
         request.httpMethod = "POST"
         do {
         request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
         }
         catch let error
         {
         print(error.localizedDescription)
         }
         let task = session.dataTask(with: request as URLRequest)
         {
         data, response, error in
         
         if let returnData = String(data: data!, encoding : .utf8) {
         print(returnData)
         }
         
         if let httpResponse = response as? HTTPURLResponse
         {
         if httpResponse.statusCode != 200
         {
         print("response was not 200: \(String(describing: response))")
         return
         }
         }
         if (error != nil)
         {
         print("error submitting request: \(String(describing: error))")
         return
         }
         do {
         if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
         {
         //  print(responsejson)
         DispatchQueue.main.async (execute:{ () -> Void in
         MBProgressHUD.hide(for: self.view, animated: true)
         if (responsejson["status"] as AnyObject) .isEqual(to: 0)
         
         {
         let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
         let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
         }
         alertController.addAction(cancelAction)
         
         let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
         
         }
         alertController.addAction(OKAction)
         self.present(alertController, animated: true, completion:nil)
         }
         if (responsejson["status"] as AnyObject) .isEqual(to: 1)
         
         {
         let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
         let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
         
         }
         alertController.addAction(cancelAction)
         let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
         self.openAndCloseStr = "1"
         self.tableViewObj.reloadData()
         /* let mainViewController = self.sideMenuController!
         let DashBoardVC = DashBoardViewController()
         DashBoardVC.navStr = self.navstr
         let navigationController = mainViewController.rootViewController as! NavigationController
         navigationController.pushViewController(DashBoardVC, animated: true)*/
         
         }
         alertController.addAction(OKAction)
         self.present(alertController, animated: true, completion:nil)
         }
         })
         }
         }
         catch let error
         {
         print(error.localizedDescription)
         DispatchQueue.main.async (execute:{ () -> Void in
         MBProgressHUD.hide(for: self.view, animated: true)
         })
         }
         }
         task.resume()
         } else {
         MBProgressHUD.hide(for: self.view, animated: true)
         self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
         }*/
        
    }
    
    func camBtnTapped(_ sender: UIButton) {
        
        if footercell.youtubeTextfieldUrl.text == "" {
            
            let actionSheetController: UIAlertController = UIAlertController(title: "Please choose a source type", message: nil, preferredStyle: .actionSheet)
            
            let editAction: UIAlertAction = UIAlertAction(title: "Take Photo", style: .default) { action -> Void in
                
                self.openCamera()
            }
            
            let deleteAction: UIAlertAction = UIAlertAction(title: "Choose Photo", style: .default) { action -> Void in
                
                self.showPhotoLibrary()
            }
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
            
            actionSheetController.addAction(editAction)
            actionSheetController.addAction(deleteAction)
            actionSheetController.addAction(cancelAction)
            
            //        present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad
            
            actionSheetController.popoverPresentationController?.sourceView = sender // works for both iPhone & iPad
            
            present(actionSheetController, animated: true) {
                print("option menu presented")
            }
            
            
            
            
            /*let actionSheet = UIActionSheet(title: "", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Take Picture","Choose Picture")
             actionSheet.show(in: self.view)*/
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Are you want to delete Youtube URL", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                
                self.footercell.youtubeTextfieldUrl.text = ""
                let actionSheetController: UIAlertController = UIAlertController(title: "Please choose a source type", message: nil, preferredStyle: .actionSheet)
                
                let editAction: UIAlertAction = UIAlertAction(title: "Take Photo", style: .default) { action -> Void in
                    
                    self.openCamera()
                }
                
                let deleteAction: UIAlertAction = UIAlertAction(title: "Choose Photo", style: .default) { action -> Void in
                    
                    self.showPhotoLibrary()
                }
                
                let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
                
                actionSheetController.addAction(editAction)
                actionSheetController.addAction(deleteAction)
                actionSheetController.addAction(cancelAction)
                
                //        present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad
                
                actionSheetController.popoverPresentationController?.sourceView = sender // works for both iPhone & iPad
                self.present(actionSheetController, animated: true) {
                }
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true, completion:nil)
        }        
    }
    
    func openCamera() {
        if self.imageArray.count == 5  {
            let alertSuccess = UIAlertController(title: "Maximum 5 Photos", message: "You can only take 5 photos", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction (title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
            alertSuccess.addAction(okAction)
            
            self.present(alertSuccess, animated: true, completion: nil)
        }
        else {
            let cameraMediaType = AVMediaTypeVideo
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
            
            switch cameraAuthorizationStatus {
            case .denied:
                alertPromptToAllowCameraAccessViaSetting()
                break
            case .authorized:
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate=self
                    imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                    imagePicker.allowsEditing = true
                    self .present(imagePicker, animated: true, completion: nil)
                } else {
                    noCamera()
                }
                break
            case .restricted: break
                
            case .notDetermined:
                // Prompting user for the permission to use the camera.
                AVCaptureDevice.requestAccess(forMediaType: cameraMediaType) { granted in
                    if granted {
                        print("Granted access to \(cameraMediaType)")
                        
                    } else {
                        print("Denied access to \(cameraMediaType)")
                        self.alertPromptToAllowCameraAccessViaSetting()
                    }
                }
            }
        }
    }
    
    
    func showPhotoLibrary()  {
        if self.imageArray.count == 5  {
            let alertSuccess = UIAlertController(title: "Maximum 5 Photos", message: "You can only take 5 photos", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction (title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
            alertSuccess.addAction(okAction)
            
            self.present(alertSuccess, animated: true, completion: nil)
        }
        else {
            let status = PHPhotoLibrary.authorizationStatus()
            
            if (status == PHAuthorizationStatus.authorized) {
                // Access has been granted.
                print("Access has been granted")
            }
                
            else if (status == PHAuthorizationStatus.denied) {
                // Access has been denied.
                print("Access has been denied")
                self.alertPromptToAllowCameraAccessViaSetting()
            }
                
            else if (status == PHAuthorizationStatus.notDetermined) {
                
                // Access has not been determined.
                PHPhotoLibrary.requestAuthorization({ (newStatus) in
                    
                    if (newStatus == PHAuthorizationStatus.authorized) {
                        print("Access has been granted.")
                    }
                    else {
                        print(" Access has been denied.")
                        self.alertPromptToAllowCameraAccessViaSetting()
                    }
                })
            }
                
            else if (status == PHAuthorizationStatus.restricted) {
                // Restricted access - normally won't happen.
                print(" Restricted access - normally won't happen")
            }
            
            self.rootListAssets.didSelectAssets = {(assets: Array<PHAsset?>) -> () in
                
                for var i in 0..<assets.count {
                    let manager = PHImageManager.default()
                    let asset = assets[i]
                    let requestOptions = PHImageRequestOptions()
                    requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
                    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
                    // this one is key
                    requestOptions.isSynchronous = true
                    if ((asset as AnyObject).mediaType == PHAssetMediaType.image)
                    {
                        PHImageManager.default().requestImage(for: asset!, targetSize: PHImageManagerMaximumSize, contentMode:  .aspectFill, options: requestOptions, resultHandler: { (pickedImage, info) in
                            self.galleryImages.append(pickedImage!)
                            // you can get image like this way
                        })
                    }
                    
                }
                for var i in 0..<self.galleryImages.count {
                    if self.imageArray.count >= 5 {
                        // self.imageArray.removeLastObject()
                        //self.base64Images.removeLast()
                    }
                    else {
                        self.imageArray.insert(self.galleryImages[i] , at: 0)
                        let imageData : NSData = UIImageJPEGRepresentation(self.galleryImages[i], 0.4)! as NSData
                        let base64Image : NSString = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
                        self.base64Images.insert(base64Image, at: 0)
                    }
                    
                }
                // self.base64Images.removeAll()
                self.galleryImages.removeAll()
                self.tableViewObj.reloadData()
            }
            let navigationController = UINavigationController(rootViewController:  self.rootListAssets)
            present(navigationController, animated: true, completion: nil)
        }
    }
    
    /* func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
     {
     print("\(buttonIndex)")
     switch (buttonIndex){
     case 0:
     print("Cancel")
     case 1:
     let cameraMediaType = AVMediaTypeVideo
     let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
     
     switch cameraAuthorizationStatus {
     case .denied:
     alertPromptToAllowCameraAccessViaSetting()
     break
     case .authorized:
     if UIImagePickerController.isSourceTypeAvailable(.camera) {
     let imagePicker = UIImagePickerController()
     imagePicker.delegate=self
     imagePicker.sourceType = UIImagePickerControllerSourceType.camera
     imagePicker.allowsEditing = true
     self .present(imagePicker, animated: true, completion: nil)
     } else {
     noCamera()
     }
     break
     case .restricted: break
     
     case .notDetermined:
     // Prompting user for the permission to use the camera.
     AVCaptureDevice.requestAccess(forMediaType: cameraMediaType) { granted in
     if granted {
     print("Granted access to \(cameraMediaType)")
     } else {
     print("Denied access to \(cameraMediaType)")
     }
     }
     }
     break
     case 2:
     self.rootListAssets.didSelectAssets = {(assets: Array<PHAsset?>) -> () in
     
     for var i in 0..<assets.count {
     let manager = PHImageManager.default()
     let asset = assets[i]
     let requestOptions = PHImageRequestOptions()
     requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
     requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
     // this one is key
     requestOptions.isSynchronous = true
     if ((asset as AnyObject).mediaType == PHAssetMediaType.image)
     {
     PHImageManager.default().requestImage(for: asset!, targetSize: PHImageManagerMaximumSize, contentMode:  .aspectFill, options: requestOptions, resultHandler: { (pickedImage, info) in
     self.galleryImages.append(pickedImage!)
     // you can get image like this way
     })
     }
     }
     for var i in 0..<self.galleryImages.count {
     if self.imageArray.count >= 5 {
     self.imageArray.removeLastObject()
     }
     self.imageArray.insert(self.galleryImages[i] , at: 0)
     }
     self.base64Images.removeAll()
     self.galleryImages.removeAll()
     self.tableViewObj.reloadData()
     }
     let navigationController = UINavigationController(rootViewController:  self.rootListAssets)
     present(navigationController, animated: true, completion: nil)
     break
     
     default:
     print("Default")
     }
     }*/
    
    func alertPromptToAllowCameraAccessViaSetting() {
        let alert = UIAlertController(title: "Alert", message: "Photo access required to...", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel) { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        
        present(alert, animated: true)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, This device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        let Camimages = info[UIImagePickerControllerEditedImage] as? UIImage ?? UIImage()
        if let updatedImage = Camimages.updateImageOrientionUpSide() {
            
            if self.imageArray.count >= 5 {
                //self.imageArray.removeLastObject()
                //self.base64Images.removeLast()
            } else {
                self.imageArray.insert(updatedImage , at: 0)
                let imageData : NSData = UIImageJPEGRepresentation(updatedImage, 0.4)! as NSData
                let base64Image : NSString = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
                self.base64Images.insert(base64Image, at: 0)
                //}
                
                self.galleryImages.removeAll()
                self.tableViewObj.reloadData()
            }
        }
        picker .dismiss(animated: true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        defer {
            picker.dismiss(animated: true)
        }
    }
    
    // MARK: -  SubActivityAPI Calling
    
    func SubActivitiesAPICalling(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_SubActivitiesAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            self.SelectedTaskArray = (responsejson["tasks"] as AnyObject) as! NSArray
                            self.dictionary.removeAll()
                            if self.SelectedTaskArray.count == 0 {
                                
                            }
                            else {
                                
                                // Generating Numbers
                                self.noGeneratArray.removeAllObjects()
                                for var i in 0 ..< self.SelectedTaskArray.count {
                                    
                                    let nogeneratestr : NSString = String(i+1) as NSString
                                    self.noGeneratArray.add(nogeneratestr)
                                    //  print(self.noGeneratArray)
                                }
                            }                            
                            
                            if self.SelectedTaskArray.count == 0
                            {
                                self.subtaskTitleArray = (responsejson["tasks"] as AnyObject) as! NSArray
                                self.taskAndSubtaskMutableDic["tasks"] = self.SelectedTaskArray as AnyObject
                                (self.SelectedProgramArray.value(forKey: "tasks") as AnyObject).setValue(self.SubtaskMutableArray, forKey: "tasks")
                                self.tableViewObj.reloadData()
                            }
                            else
                            {
                                let selectedTaskCount : Int = self.SelectedTaskArray.count
                                let selectedTaskCountstr = String(selectedTaskCount)
                                
                                self.taskAndSubtaskMutableDic["tasks"] = self.SelectedTaskArray as AnyObject
                                
                                let subTaskresponsearray = (responsejson["tasks"] as AnyObject) as! NSArray
                                self.subtaskTitleArray = ((responsejson["tasks"] as AnyObject) .value(forKey: "task_name") as AnyObject) as! NSArray
                                self.subtaskDescripionArray = ((responsejson["tasks"] as AnyObject) .value(forKey: "task_description") as AnyObject) as! NSArray
                                self.subtaskIDArray = ((responsejson["tasks"] as AnyObject) .value(forKey: "task_id") as AnyObject) as! NSArray
                                self.subtaskNumberArray = ((responsejson["tasks"] as AnyObject) .value(forKey: "task_unit") as AnyObject) as! NSArray
                                self.subtaskAct_TypeArray = ((responsejson["tasks"] as AnyObject) .value(forKey: "act_type") as AnyObject) as! NSArray
                                (self.SelectedProgramArray.value(forKey: "tasks") as AnyObject).setValue(self.SubtaskMutableArray, forKey: "tasks")
                                //                            (self.SelectedProgramArray.value(forKey: "tasks") as AnyObject).setValue("", forKey: "tasks")
                                print(self.SelectedProgramArray)
                                for var i in 0 ..< (((self.SelectedProgramArray.value(forKey: "tasks") as AnyObject) .firstObject) as AnyObject).count
                                {
                                    
                                    self.subTaskSelectCount =  (self.subTaskCountArray .object(at: i) as AnyObject) as! NSString
                                    //self.selectedIndex
                                    if self.subTaskSelectCount .isEqual(to: selectedTaskCountstr)
                                    {
                                        (((((self.SelectedProgramArray .value(forKey: "tasks") as AnyObject).firstObject) as AnyObject).object(at:self.selectedIndex)) as AnyObject).setValue(subTaskresponsearray, forKey: "tasks")
                                    }
                                    else
                                    {
                                        //(self.SelectedProgramArray.value(forKey: "tasks") as AnyObject).setValue("", forKey: "tasks")
                                        //self.SubtaskMutableArray.add("")
                                        // (self.SelectedProgramArray.value(forKey: "tasks") as AnyObject).setValue("", forKey: "tasks")
                                    }
                                }
                                
                                self.Unitarry.removeAll()
                                self.dictionary.removeAll()
                                self.subTaskvaluesStr.removeAllObjects()
                                self.tableViewObj.reloadData()
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func selectActivityBtnTapped(_ sender: Any) {
        
        if (self.selected == false)
        {
            self.tableViewHeightObj.constant = 0
            self.selected = true
        }
        else
        {
            // self.viewDidLayoutSubviews1()
            self.selected = false
        }
    }
 }
 
 //-------------------- Protocol for Delegate -----------------------
 
 protocol CellInfoDelegate {
    func Base64images(Base64Images: [AnyObject])
    
 }
