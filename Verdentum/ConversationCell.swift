//
//  ConversationCell.swift
//  ChartSwift
//
//  Created by admin on 17/07/17.
//  Copyright © 2017 VNSoftech. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell {

    @IBOutlet var profileViewBtn: UIButton!
    @IBOutlet var timeAndDateObj: UILabel!
    @IBOutlet var imageHeightConstant: NSLayoutConstraint!
    
    @IBOutlet var commentImageObj: UIImageView!
    @IBOutlet var commentLabelObj: UILabel!
    @IBOutlet var imageViewObj: UIImageView!
    
    @IBOutlet var nameLabelObj: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
