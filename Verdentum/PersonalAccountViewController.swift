//
//  PersonalAccountViewController.swift
//  Verdentum
//
//  Created by Verdentum on 13/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import SKPhotoBrowser
import AlamofireImage
import Alamofire

class PersonalAccountViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,SKPhotoBrowserDelegate  {
    
    
    @IBOutlet var tableViewUsePostsObj: UITableView!
     var programArray = NSMutableArray()
    var cell = WallCell()
    var responseUserPostsArray = NSMutableArray()
    var refreshControl = UIRefreshControl()
    var usernameobjstr = NSString()
    var lastpostIDStr = NSString()
    var images = [SKPhotoProtocol]()
    var navStr = NSString()
    var selectedRow = NSIndexPath()
    var commentsArray = NSMutableArray()
    var imagesArrayURl = NSArray()
    var commentsImagesArray = NSArray()
    var arrcontainstate = NSMutableArray()
    var commentPost_ID = NSString()
    var responsePostArray = NSMutableArray()
    var result = NSString()
    var postusedID = NSString()
    var postID = NSString()
    var verticalOffset: Int = 0
    var verticalOffsetPostUser: Int = 0
    var imageResponseArray = NSArray()
    var downloadImage = [UIImage]()
    var imageArrayURLCol = NSMutableArray()
    var userNameObjStr = NSString()
    var checkImageORNotStr : String?
    var program_idstr = NSString()
    var userID_Registered = NSString()
    var programNamestr = NSString()
    var groupIDStr = NSString()
    var headerNameStr = NSString()
    var groupImageStr = NSString()
    var StatuStr = NSString()
    var headerTitleStr = NSString()
    var WorldViewheaderTitleStr = NSString()
    var allcommentCountArray = NSMutableArray()
    var allPersonalcommentsArray = NSMutableArray()
    var allFeedcommentCountArray = NSMutableArray()
    var supportselectedStr = NSString()
     var allSupportArray = NSMutableArray()
    var allSupportPersonalArray = NSMutableArray()
     var allisLikesPersonalArray = NSMutableArray()
     var allsupportFeedArray = NSMutableArray()
    var allisLikesGroupArray = NSMutableArray()
   var allisLikesFeedArray = NSMutableArray()
    var personalTitleName = NSString()
    var suppportApply = NSString()
     var allsupportGroupArray = NSMutableArray()
     var allisLikeArray = NSMutableArray()
    @IBOutlet var userNameLabelObj: UILabel!
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkImageORNotStr = nil
        self.navigationController?.isNavigationBarHidden = true
        self.userNameLabelObj.text = self.userNameObjStr as String
        self.tableViewUsePostsObj.estimatedRowHeight = 100
        self.tableViewUsePostsObj.rowHeight = UITableViewAutomaticDimension
        self.tableViewUsePostsObj .register(UINib (nibName: "WallCell", bundle: nil), forCellReuseIdentifier: "WallCell")
        //if self.navStr == "WorldViewIndividualVC"
        //{
           // self.userNameLabelObj.text = self.headerTitleStr.capitalized
         self.userNameLabelObj.text = self.userNameObjStr.capitalized
        //}
        if self.navStr == "userpostvc"
        {
            self.tableViewUsePostsObj.reloadData()
        }
        if self.navStr == "WorldViewGroup"
        {
            self.tableViewUsePostsObj.reloadData()
            self.userNameLabelObj.text = self.userNameObjStr.capitalized
        }
        if self.navStr == "MyProgramFeedVC"
        {
            self.tableViewUsePostsObj.reloadData()
             self.userNameLabelObj.text = self.userNameObjStr.capitalized
        }
        if self.navStr == "MyProgramPrsnlVC"
        {
            self.homeServiceApiCalling()
        }
        if self.navStr == "MyProgramPrsnlImgVC"
        {
            self.homeServiceApiCalling()
        }
        if self.navStr == "WorldViewpersonalGroup"
        {
            self.homeServiceApiCalling()
            self.userNameLabelObj.text = self.userNameObjStr.capitalized
        }
        if self.navStr == "WorldViewIndividualVC"
        {
            self.homeServiceApiCalling()
            self.userNameLabelObj.text = self.headerTitleStr.capitalized
            // self.tableViewUsePostsObj.reloadData()
        }
        if self.navStr == "worldViewPersonalreportNav" {
            self.homeServiceApiCalling()
        }
        if self.navStr == "joinProgram"
        {
            self.homeServiceApiCalling()
        }
        if self.navStr == "dashboarpersonalprofileComment" {
            self.homeServiceApiCalling()
        }
        if self.navStr == "NewPage" {
            self.tableViewUsePostsObj.reloadData()
             self.userNameLabelObj.text = self.userNameObjStr.capitalized
        }
        if self.navStr == "personalUserPostUpdateComment" {
             self.homeServiceApiCalling()
        }
        
        // -----Refresh Control----
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(DashBoardViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        self.tableViewUsePostsObj.addSubview(refreshControl)
       
        //----- NSNotificationCenter ------ //
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectItem(fromCollectionView:)), name: NSNotification.Name(rawValue: "didSelectItemFromCollectionView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectItemPDF(fromCollectionView:)), name: NSNotification.Name(rawValue: "didSelectItemFromCollectionViewPDF"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectAddPostView(fromCollectionView:)), name: NSNotification.Name(rawValue: "AddPostView"), object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.navStr == "userpostvc"
        {
            self.tableViewUsePostsObj.reloadData()
        }
        if self.navStr == "personalUserPost"
        {
            self.tableViewUsePostsObj.reloadData()
        }
        
    }
    
    
    // MARK: - viewDidAppear
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.navStr == "userpostvc"
        {
            let indexPath = IndexPath(item: self.verticalOffsetPostUser, section: 0)
            self.tableViewUsePostsObj.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        if self.navStr == "NewPage"
        {
            let indexPath = IndexPath(item: self.verticalOffsetPostUser, section: 0)
            self.tableViewUsePostsObj.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        if self.navStr == "personalUserPost"
        {
            let indexPath = IndexPath(item: self.verticalOffsetPostUser, section: 0)
            self.tableViewUsePostsObj.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        //        if self.navStr == "WorldViewIndividualVC"
        //        {
        //            let indexPath = IndexPath(item: self.verticalOffset, section: 0)
        //            self.tableViewUsePostsObj.scrollToRow(at: indexPath, at: .top, animated: false)
        //        }
    }
    
    //MARK: - HomeServiceApiCalling
    
    func homeServiceApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            
            let params:[String: AnyObject] = [
                "purpose" : "userpost" as AnyObject,
                "id"   : self.userID_Registered as AnyObject,
                "myid" : userID as AnyObject]
            
            let url = NSURL(string:BaseURL_USERPOSTSAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                
//                                self.allPersonalcommentsArray.removeAllObjects()
//                                self.allSupportPersonalArray.removeAllObjects()
//                                self.responseUserPostsArray.removeAllObjects()
                                self.responseUserPostsArray = (responsejson["posts"] as AnyObject) as! NSArray as! NSMutableArray
                                
                                if self.responseUserPostsArray.count == 0
                                {
                                    // self.presentAlertWithTitle(title: "", message:"This is your development dashbooard -a place where you can find updates from your network around the word, friends, and organizations you interact with.")
                                    
                                    self.StatuStr = "This is your development dashboard -a place where you can find updates from your network around the word, friends, and organizations you interact with."
                                    self.tableViewUsePostsObj.reloadData()
                                }
                                else
                                {
                                    UserDefaults.standard.set((((responsejson["posts"] as AnyObject) .value(forKey: "post_id") as AnyObject) .firstObject) as Any, forKey: "firstPostID")
                                    
                                    for var i in 0..<self.responseUserPostsArray.count
                                    {
                                        let commentsArr : NSArray = ((self.responseUserPostsArray [i] as AnyObject).value(forKey: "comments") as? NSArray)!
                                        self.allPersonalcommentsArray.add(String(commentsArr.count))
                                        let likesStr : String = ((self.responseUserPostsArray[i] as AnyObject).value(forKey: "likes") as? String)!
                                        self.allSupportPersonalArray.add(likesStr)
                                        
                                        let islikestr : String = ((self.responseUserPostsArray[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                        self.allisLikesPersonalArray.add(islikestr)
                                    }
                                    
                                    self.tableViewUsePostsObj.reloadData()
                                }
                                
                                
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                //self.presentAlertWithTitle(title: "", message:((responsejson["msg"] as AnyObject) as! String))
                                self.StatuStr = ((responsejson["msg"] as AnyObject) as! String as NSString)
                                self.tableViewUsePostsObj.reloadData()
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
        
    }
    
     //MARK: - Refresh API
    
    func refresh(_ sender:AnyObject)
    {
        self.homeServiceApiCalling()
        self.refreshControl .endRefreshing()
    }
    
    func applicationWillResign(notification : NSNotification) {
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didSelectItemFromCollectionView"), object: nil)
    }
    
    //MARK: - UserPostsAPI Calling
    
    func userpostsApiCalling(param:[String: AnyObject],andSelectedRow selectedRow: Int)
        
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
            
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_USERPOSTSAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                
                                self.responseUserPostsArray = (responsejson["posts"] as AnyObject) as! NSArray as! NSMutableArray
                                self.tableViewUsePostsObj.reloadData()
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return 1
        
        if self.responseUserPostsArray.count > 0 {
            
            return 1
        }
        else
        {
            
            let rect = CGRect(x: 0,
                              y: 0,
                              width: view.bounds.size.width,
                              height: view.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            
            noDataLabel.text = self.StatuStr as String
            noDataLabel.textColor = UIColor.black
            noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            noDataLabel.sizeToFit()
            noDataLabel.textAlignment = NSTextAlignment.center
            tableView.backgroundView = noDataLabel
            
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.responseUserPostsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = tableView .dequeueReusableCell(withIdentifier: "WallCell") as! WallCell
        let url = URL(string: IMAGE_UPLOAD.appending(((self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!))
        cell.imageViewObj.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
        cell.imageViewObj.layer.borderWidth = 2.0
        cell.imageViewObj.layer.borderColor = UIColor.white.cgColor
        cell.imageViewObj.layer.cornerRadius = 25
        cell.imageViewObj.layer.masksToBounds = true
         cell.imageViewAboveLIneObj.isHidden = true
         cell.imageViewAboveLIneObj.isHidden = true
        let titlename: String = (self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "reg_lname") as! String
        
        
        self.usernameobjstr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        
        cell.userNameLabel.text = usernameobjstr as String
        
        let capitalized:String = (cell.userNameLabel.text!.capitalized)
        cell.userNameLabel.text = capitalized
        
        
        let htmlStr  = (self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "text") as? String
        let attributeString1 = try? NSAttributedString(data: (htmlStr?.data(using: String.Encoding.unicode)!)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil) as NSAttributedString
        
        cell.textLabelObj.attributedText = attributeString1
        
        let newAttributedString = NSMutableAttributedString(attributedString: cell.textLabelObj.attributedText!)
        
        // Enumerate through all the font ranges
        newAttributedString.enumerateAttribute(NSFontAttributeName, in: NSMakeRange(0, newAttributedString.length), options: []) { value, range, stop in
            guard let currentFont = value as? UIFont else {
                return
            }
            
            // An NSFontDescriptor describes the attributes of a font: family name, face name, point size, etc.
            // Here we describe the replacement font as coming from the "Hoefler Text" family
            let fontDescriptor = currentFont.fontDescriptor.addingAttributes([UIFontDescriptorFamilyAttribute: "Helvetica"])
            
            // Ask the OS for an actual font that most closely matches the description above
            if let newFontDescriptor = fontDescriptor.matchingFontDescriptors(withMandatoryKeys: [UIFontDescriptorFamilyAttribute]).first {
                let newFont = UIFont(descriptor: newFontDescriptor, size: currentFont.pointSize)
                newAttributedString.addAttributes([NSFontAttributeName: newFont], range: range)
            }
        }
        
        cell.textLabelObj.attributedText = newAttributedString
        
        
        let myDate = (self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "date") as? String
        let myDateString = myDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        let Dateobj = dateFormatter.date(from: myDateString!)!
        dateFormatter.dateFormat = "MMM dd, YYYY HH:mm a"
        let somedateString = dateFormatter.string(from: Dateobj)
        cell.dataTimeLabel.text = somedateString
        
        /* let commentsArr : NSArray = ((self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "comments") as? NSArray)!
         cell.viewAllCmtCount.text =  String(commentsArr.count)*/
        
        // ---- Show All Comments --- //
        
        // let commentsArr : NSArray = ((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "comments") as? NSArray)!
        //  cell.viewAllCmtCount.text =  String(commentsArr.count)
        
        //------ Add New Comments ------ //
        
        cell.viewAllCmtCount.text = self.allPersonalcommentsArray[indexPath.row] as? String
        
        
        
        cell.likeCountLabel.tag = indexPath.row
        cell.supportLabel.tag = indexPath.row
        let likesStr : NSString = ((self.allisLikesPersonalArray[indexPath.row] as AnyObject) as? NSString)!
     
        if likesStr.isEqual(to: "1") {
            
            cell.heartImageview.tag = indexPath.row
            cell.heartImageview.image = UIImage (named: "Hand Filled")
            cell.likeCountLabel.text = (self.allSupportPersonalArray[indexPath.row] as? NSString)! as String
            cell.supportLabel.text = "Supported"
        }
        if likesStr.isEqual(to: "0")
        {
            cell.heartImageview.tag = indexPath.row
            cell.heartImageview.image = UIImage (named: "Hand")
            cell.likeCountLabel.text = (self.allSupportPersonalArray[indexPath.row] as? NSString)! as String
            cell.supportLabel.text = "Support"
        }
        
        // ------ Sending Images to CollectionView(WallCell) ----//
        
        cell.setCollectionData(collection: ((self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "images") as? NSMutableArray)!)
        
        //-----------------------------//
        
        if (((self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "images") as? NSMutableArray)!.count) == 0
        {
            cell.heightCollectionView.constant = 0
            cell.imageBtnHeight.constant = 0
        }
        else
        {
            let imageorPDFstr = ((((self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "image_name") as AnyObject) .firstObject) as! NSString
           // print(imageorPDFstr as Any)
            let fileArray = imageorPDFstr.components(separatedBy: ".")
            let finalFileNameextension = fileArray.last
            if finalFileNameextension == "pdf"
            {
                cell.heightCollectionView.constant = 80
                cell.imageBtnHeight.constant = 0
            }
            if finalFileNameextension == "jpg"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
            if finalFileNameextension == "PNG"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
            if finalFileNameextension == "png"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
            if finalFileNameextension == "jpeg"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
        }
        
        self.checkImageORNotStr = ((((self.responseUserPostsArray[indexPath.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "image_name") as AnyObject) .firstObject) as? String
        
        if  self.checkImageORNotStr != nil {
            let fileArray = self.checkImageORNotStr?.components(separatedBy: ".")
            let finalFileNameextension = fileArray?.last
            let countStr : NSString = finalFileNameextension! as NSString
            let youtubestrpath  = countStr.length
            
            if youtubestrpath as NSInteger == 11
            {
                cell.imageTappedBtn.isHidden = true
            }
            if finalFileNameextension == "pdf" {
                cell.imageTappedBtn.isHidden = true
            }
            if finalFileNameextension == "jpeg"{
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
            if finalFileNameextension == "png" {
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
            
            if finalFileNameextension == "PNG" {
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
            
            if finalFileNameextension == "jpg" {
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
        }
        else
        {
            
        }
        
        cell.dotMoreBtn.tag = indexPath.row
        cell.dotMoreBtn.addTarget(self, action: #selector(self.DotbuttonClicked), for: .touchUpInside)
        cell.imageTappedBtn.tag = indexPath.row
        cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
        cell.likeBtnTapped.tag = indexPath.row
        cell.likeBtnTapped.addTarget(self, action: #selector(self.likeBtnTapped), for: .touchUpInside)
        cell.viewAllBtnTapped.tag = indexPath.row
        cell.viewAllBtnTapped.addTarget(self, action: #selector(self.viewAllCommentBtnTapped), for: .touchUpInside)
        cell.commentBtnTap.tag = indexPath.row
        cell.commentBtnTap.addTarget(self, action: #selector(self.viewAllCommentBtnTapped), for: .touchUpInside)
        return cell
    }
    
    
    
    func tableView(_ tableView:UITableView, willDisplay cell:UITableViewCell, forRowAt indexPath:IndexPath)
    {
        
        
        if indexPath.row == (self.responseUserPostsArray.count - 1)
        {
            if self.navStr == "Commentvc"
            {
                self.commentPost_ID = ((self.responseUserPostsArray[indexPath.row] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
                let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                self.lastpostIDStr = postarray.lastObject as! NSString
                let userID = UserDefaults.standard.value(forKey: "userID")
                let params:[String: AnyObject] = [
                    "purpose" : "userpost" as AnyObject,
                    "id" : self.commentPost_ID as AnyObject,
                    "direction" : "down" as AnyObject,
                    "myid" : userID as AnyObject,
                    "limit" : self.lastpostIDStr as AnyObject]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.userpostsApiCalling(param: params)
            }
            if self.navStr == "MyProgramPrsnlVC"
            {
                self.commentPost_ID = ((self.responseUserPostsArray[indexPath.row] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
                let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                self.lastpostIDStr = postarray.lastObject as! NSString
                let userID = UserDefaults.standard.value(forKey: "userID")
                let params:[String: AnyObject] = [
                    "purpose" : "userpost" as AnyObject,
                    "id" : self.userID_Registered as AnyObject,
                    "direction" : "down" as AnyObject,
                    "myid" : userID as AnyObject,
                    "limit" : self.lastpostIDStr as AnyObject]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.userpostsApiCalling(param: params)
                
            }
            else
            {
                self.commentPost_ID = ((self.responseUserPostsArray[indexPath.row] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
                let postarray : NSArray = self.responseUserPostsArray .value(forKey: "post_id") as! NSArray
                self.lastpostIDStr = postarray.lastObject as! NSString
                let userID = UserDefaults.standard.value(forKey: "userID")
                let params:[String: AnyObject] = [
                    "purpose" : "userpost" as AnyObject,
                    "id" : self.commentPost_ID as AnyObject,
                    "direction" : "down" as AnyObject,
                    "myid" : userID as AnyObject,
                    "limit" : self.lastpostIDStr as AnyObject]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.userpostsApiCalling(param: params)
            }
        }
    }
    
    //MARK: - UserPostsAPI Calling
    
    func userpostsApiCalling(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_USERPOSTSAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                      //  print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let arra  = (responsejson["posts"] as AnyObject) as! NSArray
                                if arra.count == 0
                                {
                                    
                                }
                                else
                                {
                                    self.responseUserPostsArray.addObjects(from: arra as! [Any])
                                    for var i in 0..<arra.count
                                    {
                                        let commentsArr : NSArray = ((arra [i] as AnyObject).value(forKey: "comments") as? NSArray)!
                                        self.allPersonalcommentsArray.add(String(commentsArr.count))
                                        let likesStr : String = ((arra[i] as AnyObject).value(forKey: "likes") as? String)!
                                        self.allSupportPersonalArray.add(likesStr)
                                        
                                        let islikestr : String = ((arra[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                        self.allisLikesPersonalArray.add(islikestr)
                                    }
                                    
                                    self.tableViewUsePostsObj.reloadData()
                                }
                                
                                
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        cell.imageCollectionViewObj.collectionViewLayout.invalidateLayout()
    }
    
     //MARK: - Support API Calling
    
    func likeBtnTapped(_ sender: UIButton)
    {
        let likesStr : NSString = ((self.allisLikesPersonalArray[sender.tag] as AnyObject) as? NSString)!
        
        if likesStr.isEqual(to: "1") {
            
        }
        if likesStr.isEqual(to: "0")
        {
            let indexCheck1 : Int = sender.tag
            let supportselectedStr1 : String = String(indexCheck1)
            
            if self.supportselectedStr .isEqual(to: supportselectedStr1) {
                
            } else {
                
                var message = NSNumber()
                let button: UIButton? = sender
                button?.backgroundColor = UIColor.clear
                button?.setTitleColor(UIColor.clear, for: UIControlState.normal)
                button?.isSelected = !(button?.isSelected)!
                if (button?.isSelected)!
                {
                    let post_IDStr : NSString = (self.responseUserPostsArray .object(at: sender.tag) as AnyObject) .value(forKey: "post_id") as! NSString
                    let userID = UserDefaults.standard.value(forKey: "userID")
                    let params:[String: AnyObject] = [
                        "purpose" : "support" as AnyObject,
                        "post_id" : post_IDStr as AnyObject,
                        "user_id" : userID as AnyObject]
                    
                    
                    if Reachability.isConnectedToNetwork() == true {
                        Alamofire.request(BaseURL_SupportAPI, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                            .responseJSON { response in
                               
                                if let JSON = response.result.value as? NSDictionary {
                                    print(JSON)
                                    message = JSON .value(forKey: "status")  as! NSNumber
                                    //message = JSON["likes"] as! NSNumber
                                    //print(message)
                                    let indexPath = IndexPath(row: sender.tag, section: 0)
                                    self.cell = (self.tableViewUsePostsObj.cellForRow(at: indexPath) as? WallCell)!
                                    let indexCheck : Int = sender.tag
                                    // Replace allisLikesPersonalArray
                                    let updatecommentCount : String =  "1"
                                    self.allisLikesPersonalArray.replaceObject(at: indexCheck, with: updatecommentCount)
                                    
                                    // Replace AllSupportArray
                                    
                                    
                                    var likesStr:String?
                                    
                                    if let likesStrNo = JSON["likes"] as? NSNumber {
                                        likesStr = "\(likesStrNo)"
                                    } else if let likesString = JSON["likes"] as? String {
                                        likesStr =  likesString
                                    }
                                    
                                    self.allSupportPersonalArray.replaceObject(at: indexCheck, with: likesStr!)
                                    
                                    self.suppportApply = "1"
                                    self.supportselectedStr = String(indexCheck) as NSString
                                    self.cell.heartImageview.tag = sender.tag
                                    self.cell.heartImageview.image = UIImage (named: "Hand Filled")
                                    self.cell.likeCountLabel.text = String(describing: message) as String
                                    self.cell.likeCountLabel.tag = sender.tag
                                    self.cell.supportLabel.text = "Supported"
                                    self.cell.supportLabel.tag = sender.tag
                                }
                                
                                
                        }
                    }
                    else {
                        self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
                    }
                }
            }
        }
     
    }
    
     //MARK: -  View All Comments
    
    func viewAllCommentBtnTapped(_ sender: UIButton) {
        
        self.commentPost_ID = ((self.responseUserPostsArray[sender.tag] as! NSObject) .value(forKey: "post_id") as AnyObject) as! NSString
        let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "allcomment" as AnyObject,
            "id" : userID as AnyObject,
            "post_id" : self.commentPost_ID as AnyObject]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
    }
    
     //MARK: - Comments API Calling
    
    func CommentsApiCalling(param:[String: AnyObject],andSelectedRow selectedRow: Int)
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true
        
        {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_AllCommentsAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                if self.navStr == "MyProgramFeedVC"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.verticalOffsetPostUser = selectedRow
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.navstr = "MyProgramPrsnlVC"
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                    CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userCommentIDstr = self.commentPost_ID
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.program_idstr = self.program_idstr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                if self.navStr == "MyProgramPrsnlVC"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.verticalOffsetPostUser = selectedRow
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.navstr = "MyProgramPrsnlVC"
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                   
                                     CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userCommentIDstr = self.commentPost_ID
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.program_idstr = self.program_idstr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                if self.navStr == "dashboarpersonalprofileComment" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.verticalOffsetPostUser = selectedRow
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.navstr = "userpostvc"
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.personalTitleName = self.headerTitleStr
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                     CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userCommentIDstr = self.commentPost_ID
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                if self.navStr == "NewPage"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.verticalOffsetPostUser = selectedRow
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.navstr = "userpostvc"
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userCommentIDstr = self.commentPost_ID
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                 if self.navStr == "personalUserPostUpdateComment" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.verticalOffsetPostUser = selectedRow
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.navstr = "userpostvc"
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userCommentIDstr = self.commentPost_ID
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navStr == "joinProgram"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.verticalOffsetPostUser = selectedRow
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userCommentIDstr = self.commentPost_ID
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.programArray = self.programArray
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.navstr = "joinProgramGroup"
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                                    CommentsVC.allsupportGroupArray = self.allsupportGroupArray
                                    CommentsVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    CommentsVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                    CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navStr == "WorldViewGroup"
                                {
                                    
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.verticalOffsetPostUser = selectedRow
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userCommentIDstr = self.commentPost_ID
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.navstr = "WorldViewpersonalGroup"
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                                    CommentsVC.allsupportGroupArray = self.allsupportGroupArray
                                     CommentsVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    CommentsVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                     CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navStr == "WorldViewpersonalGroup"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.verticalOffsetPostUser = selectedRow
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userCommentIDstr = self.commentPost_ID
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.navstr = "WorldViewpersonalGroup"
                                   // CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.allsupportGroupArray = self.allsupportGroupArray
                                     CommentsVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    CommentsVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                     CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navStr == "WorldViewIndividualVC"
                                {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC.headerTitleStr = self.WorldViewheaderTitleStr
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.verticalOffsetPostUser = selectedRow
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.userNameObjStr = self.userNameObjStr
                                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                     CommentsVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    CommentsVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    CommentsVC.userCommentIDstr = self.commentPost_ID
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.programNamestr = self.programNamestr
                                    CommentsVC.groupIDStr = self.groupIDStr
                                    CommentsVC.headerNameStr = self.headerNameStr
                                    CommentsVC.groupImageStr = self.groupImageStr
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    CommentsVC.navstr = "WorldViewIndividualVC"
                                   // CommentsVC.program_idstr = self.program_idstr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
     //MARK: - All Images API Calling
    
    func imageTappedBtn(_ sender: UIButton)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewUsePostsObj)
        let indexPathRow = self.tableViewUsePostsObj.indexPathForRow(at: buttonPosition)
        self.selectedRow = indexPathRow! as NSIndexPath
        self.verticalOffsetPostUser = sender.tag
        self.commentsImagesArray = (((self.responseUserPostsArray[self.selectedRow.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "comments") as AnyObject) as! NSArray
        self.imagesArrayURl = (((self.responseUserPostsArray[self.selectedRow.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "image_name") as AnyObject) as! NSArray
        
        self.imageResponseArray = ((self.responseUserPostsArray[self.selectedRow.row] as AnyObject).value(forKey: "images") as AnyObject)  as! NSArray
        self.downloadImage.removeAll()
        for var i in (0..<imagesArrayURl.count)
        {
        
            let imageURLstr2 : NSString = IMAGE_UPLOAD.appending((self.imagesArrayURl[i] as AnyObject) as! String) as NSString
            
            let url = URL(string: imageURLstr2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
         if url != nil {
            URLSession.shared.dataTask(with: url! , completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    print(error!)
                    return
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        self.downloadImage.append(image!)
                    }
//            Alamofire.request(url!).responseImage { response in
//                if let image = response.result.value {
//                    print("image downloaded: \(image)")
//                    self.downloadImage.append(image)
//                }
//                MBProgressHUD.hide(for: self.view, animated: true)
//            }
                    if self.downloadImage.count == self.imagesArrayURl.count
                    {
                        if self.navStr == "MyProgramPrsnlVC"
                        {
                            let mainViewController = self.sideMenuController!
                            let ImageTableVC = ImageTableViewController()
                            ImageTableVC.downloadImage = self.downloadImage
                            ImageTableVC.verticalOffsetIndex = self.verticalOffsetPostUser
                            ImageTableVC.verticalOffset = self.verticalOffset
                            ImageTableVC.usernameobjstr = self.usernameobjstr
                            ImageTableVC.imageResponseArray = self.imageResponseArray
                            ImageTableVC.responsePostArray = self.responsePostArray
                            ImageTableVC.commentsImagesArray = self.commentsImagesArray
                            ImageTableVC.allcommentCountArray = self.allcommentCountArray
                            ImageTableVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                            ImageTableVC.allSupportPersonalArray = self.allSupportPersonalArray
                            ImageTableVC.responseUserPostsArray = self.responseUserPostsArray
                            ImageTableVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                            ImageTableVC.allsupportFeedArray = self.allsupportFeedArray
                             ImageTableVC.allisLikesFeedArray = self.allisLikesFeedArray
                            ImageTableVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                            ImageTableVC.headerTitleStr = self.headerTitleStr
                            ImageTableVC.userNameObjStr = self.userNameObjStr
                            ImageTableVC.program_idstr = self.program_idstr
                            ImageTableVC.userID_Registered = self.userID_Registered
                            ImageTableVC.navstr = "MyProgramPrsnlVC"
                            ImageTableVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(ImageTableVC, animated: true)
                            
                        }
                        if self.navStr == "MyProgramPrsnlImgVC"
                        {
                            let mainViewController = self.sideMenuController!
                            let ImageTableVC = ImageTableViewController()
                            ImageTableVC.downloadImage = self.downloadImage
                            ImageTableVC.verticalOffsetIndex = self.verticalOffsetPostUser
                            ImageTableVC.verticalOffset = self.verticalOffset
                            ImageTableVC.usernameobjstr = self.usernameobjstr
                            ImageTableVC.imageResponseArray = self.imageResponseArray
                            ImageTableVC.responsePostArray = self.responsePostArray
                            ImageTableVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                            ImageTableVC.allsupportFeedArray = self.allsupportFeedArray
                            ImageTableVC.allisLikesFeedArray = self.allisLikesFeedArray
                            ImageTableVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                            ImageTableVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                            ImageTableVC.allSupportPersonalArray = self.allSupportPersonalArray
                            ImageTableVC.headerTitleStr = self.headerTitleStr
                            ImageTableVC.headerTitleStr = self.headerTitleStr
                            ImageTableVC.commentsImagesArray = self.commentsImagesArray
                            ImageTableVC.responseUserPostsArray = self.responseUserPostsArray
                            ImageTableVC.userNameObjStr = self.userNameObjStr
                            ImageTableVC.program_idstr = self.program_idstr
                            ImageTableVC.userID_Registered = self.userID_Registered
                            ImageTableVC.navstr = "MyProgramPrsnlVC"
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(ImageTableVC, animated: true)
                        }
                        if self.navStr == "dashboarpersonalprofileComment" {
                            let mainViewController = self.sideMenuController!
                            let ImageTableVC = ImageTableViewController()
                            ImageTableVC.downloadImage = self.downloadImage
                            ImageTableVC.verticalOffsetIndex = self.verticalOffsetPostUser
                            ImageTableVC.verticalOffset = self.verticalOffset
                            ImageTableVC.usernameobjstr = self.usernameobjstr
                            ImageTableVC.imageResponseArray = self.imageResponseArray
                            ImageTableVC.responsePostArray = self.responsePostArray
                            ImageTableVC.commentsImagesArray = self.commentsImagesArray
                            ImageTableVC.allcommentCountArray = self.allcommentCountArray
                            ImageTableVC.responseUserPostsArray = self.responseUserPostsArray
                            ImageTableVC.allcommentCountArray = self.allcommentCountArray
                            ImageTableVC.allSupportArray = self.allSupportArray
                            ImageTableVC.allSupportPersonalArray = self.allSupportPersonalArray
                            ImageTableVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                            ImageTableVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                            ImageTableVC.headerTitleStr = self.headerTitleStr
                            ImageTableVC.userNameObjStr = self.userNameObjStr
                            ImageTableVC.navstr = "personalUserPost"
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(ImageTableVC, animated: true)
                            
                        }
                        if self.navStr == "NewPage"
                        {
                            let mainViewController = self.sideMenuController!
                            let ImageTableVC = ImageTableViewController()
                            ImageTableVC.downloadImage = self.downloadImage
                            ImageTableVC.verticalOffsetIndex = self.verticalOffsetPostUser
                            ImageTableVC.verticalOffset = self.verticalOffset
                            ImageTableVC.usernameobjstr = self.usernameobjstr
                            ImageTableVC.imageResponseArray = self.imageResponseArray
                            ImageTableVC.responsePostArray = self.responsePostArray
                            ImageTableVC.commentsImagesArray = self.commentsImagesArray
                            ImageTableVC.allcommentCountArray = self.allcommentCountArray
                            ImageTableVC.responseUserPostsArray = self.responseUserPostsArray
                            ImageTableVC.allcommentCountArray = self.allcommentCountArray
                            ImageTableVC.allisLikeArray = self.allisLikeArray
                            ImageTableVC.allSupportArray = self.allSupportArray
                            ImageTableVC.allSupportPersonalArray = self.allSupportPersonalArray
                            ImageTableVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                            ImageTableVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                            ImageTableVC.headerTitleStr = self.headerTitleStr
                            ImageTableVC.userNameObjStr = self.userNameObjStr
                            ImageTableVC.userID_Registered = self.userID_Registered
                            ImageTableVC.navstr = "personalUserPost"
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(ImageTableVC, animated: true)
                        }
                         if self.navStr == "personalUserPostUpdateComment" {
                            let mainViewController = self.sideMenuController!
                            let ImageTableVC = ImageTableViewController()
                            ImageTableVC.downloadImage = self.downloadImage
                            ImageTableVC.verticalOffsetIndex = self.verticalOffsetPostUser
                            ImageTableVC.verticalOffset = self.verticalOffset
                            ImageTableVC.usernameobjstr = self.usernameobjstr
                            ImageTableVC.imageResponseArray = self.imageResponseArray
                            ImageTableVC.responsePostArray = self.responsePostArray
                            ImageTableVC.commentsImagesArray = self.commentsImagesArray
                            ImageTableVC.allcommentCountArray = self.allcommentCountArray
                            ImageTableVC.responseUserPostsArray = self.responseUserPostsArray
                            ImageTableVC.allcommentCountArray = self.allcommentCountArray
                            ImageTableVC.allisLikeArray = self.allisLikeArray
                            ImageTableVC.allSupportArray = self.allSupportArray
                            ImageTableVC.allSupportPersonalArray = self.allSupportPersonalArray
                            ImageTableVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                            ImageTableVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                            ImageTableVC.headerTitleStr = self.headerTitleStr
                            ImageTableVC.userNameObjStr = self.userNameObjStr
                            ImageTableVC.userID_Registered = self.userID_Registered
                            ImageTableVC.navstr = "personalUserPost"
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(ImageTableVC, animated: true)
                        }
                        if self.navStr == "joinProgram"
                        {
                            let mainViewController = self.sideMenuController!
                            let ImageTableVC = ImageTableViewController()
                            ImageTableVC.downloadImage = self.downloadImage
                            ImageTableVC.verticalOffsetIndex = self.verticalOffsetPostUser
                            ImageTableVC.verticalOffset = self.verticalOffset
                            ImageTableVC.usernameobjstr = self.usernameobjstr
                            ImageTableVC.imageResponseArray = self.imageResponseArray
                            ImageTableVC.responsePostArray = self.responsePostArray
                            ImageTableVC.commentsImagesArray = self.commentsImagesArray
                            ImageTableVC.responseUserPostsArray = self.responseUserPostsArray
                            ImageTableVC.userNameObjStr = self.userNameObjStr
                            ImageTableVC.program_idstr = self.program_idstr
                            ImageTableVC.programNamestr = self.programNamestr
                            ImageTableVC.groupIDStr = self.groupIDStr
                            ImageTableVC.headerNameStr = self.headerNameStr
                            ImageTableVC.groupImageStr = self.groupImageStr
                            ImageTableVC.userID_Registered = self.userID_Registered
                            ImageTableVC.programArray = self.programArray
                            ImageTableVC.navstr = "joinProgramVC"
                            ImageTableVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                            ImageTableVC.allsupportGroupArray = self.allsupportGroupArray
                            ImageTableVC.allisLikesGroupArray = self.allisLikesGroupArray
                            ImageTableVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                            ImageTableVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                            ImageTableVC.allSupportPersonalArray = self.allSupportPersonalArray
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(ImageTableVC, animated: true)
                        }
                        if self.navStr == "WorldViewGroup"
                        {
                            let mainViewController = self.sideMenuController!
                            let ImageTableVC = ImageTableViewController()
                            ImageTableVC.downloadImage = self.downloadImage
                            ImageTableVC.verticalOffsetIndex = self.verticalOffsetPostUser
                            ImageTableVC.verticalOffset = self.verticalOffset
                            ImageTableVC.usernameobjstr = self.usernameobjstr
                            ImageTableVC.imageResponseArray = self.imageResponseArray
                            ImageTableVC.responsePostArray = self.responsePostArray
                            ImageTableVC.commentsImagesArray = self.commentsImagesArray
                            ImageTableVC.responseUserPostsArray = self.responseUserPostsArray
                            ImageTableVC.userNameObjStr = self.userNameObjStr
                            ImageTableVC.program_idstr = self.program_idstr
                            ImageTableVC.programNamestr = self.programNamestr
                            ImageTableVC.groupIDStr = self.groupIDStr
                            ImageTableVC.headerNameStr = self.headerNameStr
                            ImageTableVC.groupImageStr = self.groupImageStr
                            ImageTableVC.userID_Registered = self.userID_Registered
                            ImageTableVC.navstr = "WorldViewpersonalGroup"
                            ImageTableVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                            ImageTableVC.allsupportGroupArray = self.allsupportGroupArray
                            ImageTableVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                            ImageTableVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                            ImageTableVC.headerTitleStr = self.headerTitleStr
                            ImageTableVC.allSupportPersonalArray = self.allSupportPersonalArray
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(ImageTableVC, animated: true)
                        }
                        if self.navStr ==  "WorldViewpersonalGroup" {
                            let mainViewController = self.sideMenuController!
                            let ImageTableVC = ImageTableViewController()
                            ImageTableVC.downloadImage = self.downloadImage
                            ImageTableVC.verticalOffsetIndex = self.verticalOffsetPostUser
                            ImageTableVC.verticalOffset = self.verticalOffset
                            ImageTableVC.usernameobjstr = self.usernameobjstr
                            ImageTableVC.imageResponseArray = self.imageResponseArray
                            ImageTableVC.responsePostArray = self.responsePostArray
                            ImageTableVC.commentsImagesArray = self.commentsImagesArray
                            ImageTableVC.responseUserPostsArray = self.responseUserPostsArray
                            ImageTableVC.userNameObjStr = self.userNameObjStr
                            ImageTableVC.program_idstr = self.program_idstr
                            ImageTableVC.programNamestr = self.programNamestr
                            ImageTableVC.groupIDStr = self.groupIDStr
                            ImageTableVC.headerNameStr = self.headerNameStr
                            ImageTableVC.groupImageStr = self.groupImageStr
                            ImageTableVC.userID_Registered = self.userID_Registered
                            ImageTableVC.navstr = "WorldViewpersonalGroup"
                            ImageTableVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
                            ImageTableVC.allsupportGroupArray = self.allsupportGroupArray
                            ImageTableVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                            ImageTableVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                            ImageTableVC.headerTitleStr = self.headerTitleStr
                            ImageTableVC.allSupportPersonalArray = self.allSupportPersonalArray
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(ImageTableVC, animated: true)
                        }

                        if self.navStr ==  "WorldViewIndividualVC"
                        {
                            let mainViewController = self.sideMenuController!
                            let ImageTableVC = ImageTableViewController()
                            ImageTableVC.downloadImage = self.downloadImage
                            ImageTableVC.verticalOffsetIndex = self.verticalOffsetPostUser
                            ImageTableVC.verticalOffset = self.verticalOffset
                            ImageTableVC.usernameobjstr = self.usernameobjstr
                            ImageTableVC.imageResponseArray = self.imageResponseArray
                            ImageTableVC.responsePostArray = self.responsePostArray
                            ImageTableVC.commentsImagesArray = self.commentsImagesArray
                            ImageTableVC.responseUserPostsArray = self.responseUserPostsArray
                            ImageTableVC.allSupportPersonalArray = self.allSupportPersonalArray
                            ImageTableVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                            ImageTableVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                            ImageTableVC.headerTitleStr = self.headerTitleStr
                            ImageTableVC.userNameObjStr = self.userNameObjStr
                            ImageTableVC.program_idstr = self.program_idstr
                            ImageTableVC.programNamestr = self.programNamestr
                            ImageTableVC.groupIDStr = self.groupIDStr
                            ImageTableVC.headerNameStr = self.headerNameStr
                            ImageTableVC.headerTitleStr = self.headerTitleStr
                            ImageTableVC.groupImageStr = self.groupImageStr
                            ImageTableVC.userID_Registered = self.userID_Registered
                            ImageTableVC.navstr = "WorldViewIndividualVC"
                            let navigationController = mainViewController.rootViewController as! NavigationController
                            navigationController.pushViewController(ImageTableVC, animated: true)
                        }
                    }
                    else
                    {
                        
                    }
              })
            }).resume()
             MBProgressHUD.hide(for: self.view, animated: true)
            }
         else {
            
        }
        }
    }
    
    func createWebPhotos() -> [SKPhotoProtocol] {
        return (0..<self.imagesArrayURl.count).map { (i: Int) -> SKPhotoProtocol in
            let photo = SKPhoto.photoWithImageURL(IMAGE_UPLOAD .appending(self.imagesArrayURl[i] as! String))
            //photo.caption = caption[i%10]
            photo.shouldCachePhotoURLImage = true
            return photo
        }
    }
    
     //MARK: - Dot Button
    
    func DotbuttonClicked(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewUsePostsObj)
        let indexPathRow = self.tableViewUsePostsObj.indexPathForRow(at: buttonPosition)
        self.selectedRow = indexPathRow! as NSIndexPath
        self.verticalOffsetPostUser = sender.tag
        self.result = UserDefaults.standard.value(forKey: "userID") as! NSString
        self.postusedID = ((self.responseUserPostsArray[sender.tag] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
        self.postID = ((self.responseUserPostsArray[sender.tag] as! NSObject) .value(forKey: "post_id") as AnyObject) as! NSString
        if self.result == self.postusedID
        {
            // Finding Array Index
            let indexPath = IndexPath(row: sender.tag, section: 0)
            cell = (self.tableViewUsePostsObj.cellForRow(at: indexPath) as? WallCell)!
            let quoteDictionary = self.responseUserPostsArray[sender.tag] as! Dictionary<String,AnyObject>
            print(quoteDictionary)
            if indexPathRow != nil
            {
                let actionSheet = UIActionSheet(title: "Delete Post", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Delete")
                
                actionSheet.show(in: self.view)
            }
        }
        else
        {
            let actionSheet = UIActionSheet(title: "Report Post", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Report")
            actionSheet.show(in: self.view)
        }
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        switch (buttonIndex){
        case 0:
            print("Cancel")
        case 1:
            if self.result == self.postusedID
            {
                let alertController = UIAlertController(title: "Delete Post", message: "Are you sure you want to delete this post?", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                    
                }
                alertController.addAction(cancelAction)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    let userID = UserDefaults.standard.value(forKey: "userID")
                    let params:[String: AnyObject] = [
                        "purpose" : "deletepost" as AnyObject,
                        "post_id" : self.postID as AnyObject,
                        "id" : userID as AnyObject]
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    self.deletePostID(param: params)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
            else
            {
                if self.navStr == "WorldViewGroup"
                {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.userNameObjStr = self.userNameObjStr
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.groupImageStr = self.groupImageStr
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.program_idstr = self.program_idstr
                    ReportuserVC.programNamestr = self.programNamestr
                    ReportuserVC.headerTitleStr = self.WorldViewheaderTitleStr
                    ReportuserVC.headerNameStr = self.headerNameStr
                    ReportuserVC.personalheaderTitleStr = self.headerTitleStr
                    ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
                    ReportuserVC.allSupportArray = self.allSupportArray
                    ReportuserVC.allSupportPersonalArray = self.allSupportPersonalArray
                    ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                    ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
                     ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
                    ReportuserVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                    ReportuserVC.groupIDStr = self.groupIDStr
                    ReportuserVC.allSupportPersonalArray = self.allSupportPersonalArray
                    ReportuserVC.navstr = "worldViewPersonalreportNav"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                    
                }
                 if self.navStr ==  "WorldViewpersonalGroup" {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.userNameObjStr = self.userNameObjStr
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.groupImageStr = self.groupImageStr
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.program_idstr = self.program_idstr
                    ReportuserVC.programNamestr = self.programNamestr
                    ReportuserVC.headerTitleStr = self.WorldViewheaderTitleStr
                    ReportuserVC.headerNameStr = self.headerNameStr
                    ReportuserVC.personalheaderTitleStr = self.headerTitleStr
                    ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
                    ReportuserVC.allSupportArray = self.allSupportArray
                    ReportuserVC.allSupportPersonalArray = self.allSupportPersonalArray
                    ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                    ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
                    ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
                    ReportuserVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                    ReportuserVC.groupIDStr = self.groupIDStr
                    ReportuserVC.allSupportPersonalArray = self.allSupportPersonalArray
                    ReportuserVC.navstr = "worldViewPersonalreportNav"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                }
                if self.navStr == "joinProgram"
                {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.groupImageStr = self.groupImageStr
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.program_idstr = self.program_idstr
                    ReportuserVC.programNamestr = self.programNamestr
                    ReportuserVC.headerTitleStr = self.WorldViewheaderTitleStr
                    ReportuserVC.personalheaderTitleStr = self.headerTitleStr
                    ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
                    ReportuserVC.allSupportArray = self.allSupportArray
                    ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                    ReportuserVC.programArray = self.programArray
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
                    ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
                    ReportuserVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                    ReportuserVC.groupIDStr = self.groupIDStr
                    ReportuserVC.allSupportPersonalArray = self.allSupportPersonalArray
                    ReportuserVC.navstr = "joinProgramReport"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                    
                }
                if self.navStr == "WorldViewIndividualVC"
                {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.program_idstr = self.program_idstr
                    ReportuserVC.programNamestr = self.programNamestr
                    ReportuserVC.headerTitleStr = self.WorldViewheaderTitleStr
                    ReportuserVC.personalheaderTitleStr = self.headerTitleStr
                    ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
                    ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                    ReportuserVC.allSupportPersonalArray = self.allSupportPersonalArray
                    ReportuserVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                    ReportuserVC.navstr = "WorldViewIndividualVC"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                }
                if self.navStr == "dashboarpersonalprofileComment" {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
                    ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                   
                    ReportuserVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                    ReportuserVC.verticalOffsetPostUser = self.verticalOffsetPostUser
                    ReportuserVC.allSupportArray = self.allSupportArray
                    ReportuserVC.verticalOffset = self.verticalOffset
                    ReportuserVC.navstr = "personalreportPostNav"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                    
                }
                if self.navStr == "NewPage"
                {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
                    ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                    ReportuserVC.verticalOffsetPostUser = self.verticalOffsetPostUser
                    ReportuserVC.allSupportPersonalArray = self.allSupportPersonalArray
                    ReportuserVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                    ReportuserVC.userNameObjStr = self.userNameObjStr
                    ReportuserVC.headerTitleStr = self.headerTitleStr
                    ReportuserVC.allSupportArray = self.allSupportArray
                    ReportuserVC.allisLikeArray = self.allisLikeArray
                    ReportuserVC.verticalOffset = self.verticalOffset
                    ReportuserVC.navstr = "personalreportPostNav"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                }
                  if self.navStr == "personalUserPostUpdateComment" {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
                    ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                    ReportuserVC.verticalOffsetPostUser = self.verticalOffsetPostUser
                    ReportuserVC.allSupportPersonalArray = self.allSupportPersonalArray
                    ReportuserVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                    ReportuserVC.headerTitleStr = self.headerTitleStr
                    ReportuserVC.allSupportArray = self.allSupportArray
                    ReportuserVC.allisLikeArray = self.allisLikeArray
                    ReportuserVC.verticalOffset = self.verticalOffset
                    ReportuserVC.navstr = "personalreportPostNav"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                }
                if self.navStr == "MyProgramPrsnlVC"
                {
                    let mainViewController = self.sideMenuController!
                    let CommentsVC = ReportViewController()
                    CommentsVC.verticalOffsetPostUser =  self.verticalOffsetPostUser
                    CommentsVC.verticalOffset = self.verticalOffset
                    CommentsVC.navstr = "MyProgramPrsnlVC"
                    CommentsVC.responseArray = self.responsePostArray
                    CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                    CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                    CommentsVC.headerTitleStr = self.headerTitleStr
                    CommentsVC.userNameObjStr = self.userNameObjStr
                    CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                    CommentsVC.userID_Registered = self.userID_Registered
                    CommentsVC.program_idstr = self.program_idstr
                    let navigationController = mainViewController.rootViewController as! NavigationController
                    navigationController.pushViewController(CommentsVC, animated: true)
                }
            }
        default:
            print("Default")
        }
    }
    
    
    func deletePostID(param:[String: AnyObject])
    {
        //  MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:VERDENTUMB_BaseURL.appending(BaseURL_VERDENTUMDeletepostAPI))
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                                
                            {
                                self.presentAlertWithTitle(title: "", message: (responsejson["msg"] as AnyObject) as! String)
                                self.responseUserPostsArray .removeObject(at: self.selectedRow.row)
                                self.tableViewUsePostsObj.reloadData()
                            }
                            else
                            {
                                self.presentAlertWithTitle(title: " Delete Failed!", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func didSelectItem(fromCollectionView notification: Notification)
    {
        let newsImageURLstr = notification .object as! NSString
        
        self.imageArrayURLCol.add(newsImageURLstr)
        if newsImageURLstr.isEqual(to: "")
        {
            let alertController = UIAlertController(title: "", message: "News Image unavailable. Please try again.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            let ZoomImageVC = ZoomImageViewController(nibName: "ZoomImageViewController", bundle: nil)
            ZoomImageVC.newImageURlStr =  newsImageURLstr
            self.navigationController?.pushViewController(ZoomImageVC, animated: true)
        }
    }
    
    func didSelectItemPDF(fromCollectionView notification: Notification)
    {
        let newsImageURLstr = notification .object as! NSString
        if newsImageURLstr.isEqual(to: "")
        {
            let alertController = UIAlertController(title: "", message: "PDF unavailable. Please try again.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            let ZoomImageVC = ZoomImageViewController(nibName: "ZoomImageViewController", bundle: nil)
            ZoomImageVC.newImageURlStr =  newsImageURLstr
            self.navigationController?.pushViewController(ZoomImageVC, animated: true)
        }
    }
    func didSelectAddPostView(fromCollectionView notification: Notification)
    {
        let newsImageURLstr = notification .object as! NSString
        if newsImageURLstr.isEqual(to: "")
        {
            let alertController = UIAlertController(title: "", message: "Please Try again Later.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            //self.homeServiceApiCalling()
        }
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        if self.navStr == "MyProgramFeedVC"
        {
            let mainViewController = self.sideMenuController!
            let ProgramSwipeVC = ProgramSwipeViewController()
            ProgramSwipeVC.navStr = "MyProgramFeedVC"
            ProgramSwipeVC.program_idstr = self.program_idstr
            ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            ProgramSwipeVC.responsePostArray = self.responsePostArray
            ProgramSwipeVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            ProgramSwipeVC.allsupportFeedArray = self.allsupportFeedArray
            ProgramSwipeVC.allisLikesFeedArray = self.allisLikesFeedArray
            ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            ProgramSwipeVC.verticalOffset = self.verticalOffset
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ProgramSwipeVC, animated: true)
        }
        if self.navStr == "MyProgramPrsnlVC"
        {
            let mainViewController = self.sideMenuController!
            let ProgramSwipeVC = ProgramSwipeViewController()
            ProgramSwipeVC.navStr = "MyProgramFeedVC"
            ProgramSwipeVC.program_idstr = self.program_idstr
            ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            ProgramSwipeVC.responsePostArray = self.responsePostArray
            ProgramSwipeVC.allisLikesFeedArray = self.allisLikesFeedArray
            ProgramSwipeVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            ProgramSwipeVC.allsupportFeedArray = self.allsupportFeedArray
            ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            ProgramSwipeVC.verticalOffset = self.verticalOffset
            //print( ProgramSwipeVC.verticalOffset)
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ProgramSwipeVC, animated: true)
        }
        if self.navStr == "NewPage"
        {
           /* let mainViewController = self.sideMenuController!
            let dashboardVC = DashBoardViewController()
            
            if self.suppportApply .isEqual(to: "1"){
                 dashboardVC.navStr = "supportApply"
            } else {
                dashboardVC.navStr = "userpostvc"
            }
           
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allisLikeArray = self.allisLikeArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: false)*/
            
            
            for DashBoardVC in (self.navigationController?.viewControllers ?? []) {
                if DashBoardVC is DashBoardViewController {
                    _ = self.navigationController?.popToViewController(DashBoardVC, animated: true)
                    break
                }
            }
            
            
            
            
        }
         if self.navStr == "personalUserPostUpdateComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = DashBoardViewController()
            
            if self.suppportApply .isEqual(to: "1"){
                dashboardVC.navStr = "supportApply"
            } else {
                dashboardVC.navStr = "userpostvc"
            }
            
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allisLikeArray = self.allisLikeArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: false)
        }
        if self.navStr == "dashboarpersonalprofileComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = DashBoardViewController()
            
            dashboardVC.navStr = "DashboardVC"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: false)
        }
        if self.navStr == "userpostvc"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = DashBoardViewController()
            dashboardVC.navStr = "userpostvc"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
             dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.verticalOffset = self.verticalOffset
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "personalUserPost"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = DashBoardViewController()
            dashboardVC.navStr = "userpostvc"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
             dashboardVC.allSupportArray = self.allSupportArray
             dashboardVC.allisLikeArray = self.allisLikeArray
            dashboardVC.verticalOffset = self.verticalOffset
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "MyProgramPrsnlImgVC"
        {
            let mainViewController = self.sideMenuController!
            let ProgramSwipeVC = ProgramSwipeViewController()
            ProgramSwipeVC.navStr = "MyProgramFeedVC"
            ProgramSwipeVC.program_idstr = self.program_idstr
            ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            ProgramSwipeVC.responsePostArray = self.responsePostArray
            ProgramSwipeVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            ProgramSwipeVC.allsupportFeedArray = self.allsupportFeedArray
            ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            ProgramSwipeVC.verticalOffset = self.verticalOffset
            ProgramSwipeVC.program_idstr = self.program_idstr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ProgramSwipeVC, animated: true)
        }
        if self.navStr == "joinProgram"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = GroupNewsViewController()
            dashboardVC.navStr = "joinProgram"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programArray = self.programArray
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            dashboardVC.allsupportGroupArray = self.allsupportGroupArray
            dashboardVC.allisLikesGroupArray = self.allisLikesGroupArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "WorldViewGroup"
        {
            
            let mainViewController = self.sideMenuController!
            let dashboardVC = GroupNewsViewController()
            dashboardVC.navStr = "WorldViewGroup"
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programArray = self.programArray
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            dashboardVC.allsupportGroupArray = self.allsupportGroupArray
             dashboardVC.allisLikesGroupArray = self.allisLikesGroupArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "WorldViewpersonalGroup"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = GroupNewsViewController()
            dashboardVC.navStr = "WorldViewGroup"
           // dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            // dashboardVC.programArray = self.programArray
            dashboardVC.programNamestr = self.programNamestr
            // dashboardVC.allsupportGroupArray = self.allsupportGroupArray
           //  dashboardVC.allisLikesGroupArray = self.allisLikesGroupArray
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navStr == "WorldViewIndividualVC"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ProgramSwipeViewController()
            dashboardVC.navStr = "WorldVC"
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.headerTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        
    }
}
