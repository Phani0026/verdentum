//
//  FourImagesCell.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 22/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class FourImagesCell: UICollectionViewCell {

    
    @IBOutlet var firstimageFourBtn: UIButton!
    
    @IBOutlet var fourthImageFourBtn: UIButton!
    @IBOutlet var thirdImageFourBtn: UIButton!
    @IBOutlet var secondimageFourBtn: UIButton!
    @IBOutlet var imageViewone4: UIImageView!
    
    @IBOutlet var imageViewfour4: UIImageView!
    @IBOutlet var imageViewTwo4: UIImageView!
    
    @IBOutlet var imageviewThree4: UIImageView!
    #if os(tvOS)
    override func awakeFromNib() {
        super.awakeFromNib()
        imageViewone4.adjustsImageWhenAncestorFocused = true
        imageViewfour4.adjustsImageWhenAncestorFocused = true
        imageViewTwo4.adjustsImageWhenAncestorFocused = true
        imageviewThree4.adjustsImageWhenAncestorFocused = true
    }
   #endif
}
