


//
//  AppDelegate.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 17/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import CoreData
//import FBSDKLoginKit
import LGSideMenuController
import Firebase
import UserNotifications
import FirebaseMessaging
import GooglePlaces
import GooglePlacePicker
import Alamofire
import SystemConfiguration
import IKEventSource
import Pushwoosh

@available(iOS 10.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,SWRevealViewControllerDelegate,UNUserNotificationCenterDelegate, PushNotificationDelegate {
    
    //MessagingDelegate,
    var window: UIWindow?
    var navVC : UINavigationController?
    var homeVC : HomePageViewController?
    var mainVC : UIViewController?
    var pushDict = NSMutableDictionary()
    var pushArray = NSMutableArray()
    var groupRequestDict = NSMutableDictionary()
    var groupRequestArray = NSMutableArray()
    var friendRequestDict = NSMutableDictionary()
    var friendRequestArray = NSMutableArray()
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let result = UserDefaults.standard.value(forKey: "userID")
         // FirebaseApp.configure()
        
       GMSPlacesClient.provideAPIKey("AIzaSyC8k1cScMXx8nrhD-zNcbLDnDT_t9KR1cU")
        
        // set custom delegate for push handling, in our case AppDelegate
        PushNotificationManager.push().delegate = self
        
        // set default Pushwoosh delegate for iOS10 foreground push handling
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = PushNotificationManager.push().notificationCenterDelegate
        }
        // track application open statistics
        PushNotificationManager.push().sendAppOpen()
        // register for push notifications!
        PushNotificationManager.push().registerForPushNotifications()
        // [END register_for_notifications]
        if result as? String == "" || result == nil
        {
          //  FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
            let homeVC = HomePageViewController() as UIViewController
            //            let navigationController = UINavigationController(rootViewController: homeVC)
            //            navigationController.isNavigationBarHidden = true
            let navigationController1 = NavigationController(rootViewController: homeVC)
            let mainViewController = MainSideViewController()
            mainViewController.rootViewController = navigationController1
            self.window?.rootViewController = mainViewController
            self.window?.makeKeyAndVisible()
        }
        else
        {
             self.checkNotifications()
          //  let viewController: UIViewController
          let  viewController = DashBoardViewController()
            viewController.navStr = "DashboardVC"
            let navigationController1 = NavigationController(rootViewController: viewController)
            let mainViewController = MainSideViewController()
            mainViewController.rootViewController = navigationController1
            mainViewController.setup(type: UInt(2))
            window?.rootViewController = mainViewController
            UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
            window?.makeKeyAndVisible()
        }
        return true
    }
    
    // MARK: EventSource Code Notifications
    
    func checkNotifications() {
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let now = dateformatter.string(from: NSDate() as Date)
        let fullName    = now
        let fullNameArr = fullName.components(separatedBy: " ")
        let timestamp = fullNameArr[2]
        let nsMutableString = NSMutableString(string: timestamp)
        let timestamp1 : NSMutableString = nsMutableString
        timestamp1.insert(":", at: 3)
        let emailAddressText = timestamp1
        let encodedtime = emailAddressText.addingPercentEncoding(withAllowedCharacters:.rfc3986Unreserved)
        let currentUsedID = UserDefaults.standard.value(forKey: "userID")
        let urlPath : NSString = "http://52.40.207.123/cyc/api/notification/startstream/\(currentUsedID!)/\(encodedtime!)" as NSString
        
        let eventSource : EventSource = EventSource(url: urlPath as String)
        
        eventSource.onOpen {
            // When opened
        }
        
        eventSource.onError { (error) in
            // When errors
        }
        
        eventSource.onMessage { (id, event, data) in
            // Here you get an event without event name!
            let data = data?.data(using: .utf8)!
            print(data!)
        }
        
        eventSource.addEventListener("groupRequest") { (id, event, data) in
          
            let data = data?.data(using: .utf8)!
            let json = try? JSONSerialization.jsonObject(with: data!)
            let groupArray : NSArray = (json as AnyObject) as! NSArray
            self.groupRequestDict .setValue(groupArray, forKey: "groupReq")
            let notificationName = Notification.Name("groupRequestArrayNotifications")
            NotificationCenter.default.post(name: notificationName, object: self.groupRequestDict)
        }
        eventSource.addEventListener("friendRequest") { (id, event, data) in
            let data = data?.data(using: .utf8)!
            let json = try? JSONSerialization.jsonObject(with: data!)
            let friendReqArray : NSArray = (json as AnyObject) as! NSArray
            self.friendRequestDict .setValue(friendReqArray, forKey: "frndreq")
            let notificationName = Notification.Name("friendRequestArrayNotifications")
            NotificationCenter.default.post(name: notificationName, object: self.friendRequestDict)
        }
        eventSource.addEventListener("pushRequest") { (id, event, data) in
            let data = data?.data(using: .utf8)!
            let json = try? JSONSerialization.jsonObject(with: data!)
            let pushdataArray : NSArray = (json as AnyObject) as! NSArray
            self.pushDict .setValue(pushdataArray, forKey: "pushnotificationdata")
            let notificationName = Notification.Name("pushArrayNotifications")
            NotificationCenter.default.post(name: notificationName, object: self.pushDict)
        }
       // eventSource.close()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        PushNotificationManager.push().handlePushRegistration(deviceToken as Data!)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        PushNotificationManager.push().handlePushRegistrationFailure(error)
    }
    
    // this event is fired when the push is received in the app
    func onPushReceived(_ pushManager: PushNotificationManager!, withNotification pushNotification: [AnyHashable : Any]!, onStart: Bool) {
        print("Push notification received: \(pushNotification)")
        // shows a push is received. Implement passive reaction to a push, such as UI update or data download.
    }
    
    // this event is fired when user clicks on notification
    func onPushAccepted(_ pushManager: PushNotificationManager!, withNotification pushNotification: [AnyHashable : Any]!, onStart: Bool) {
        print("Push notification accepted: \(pushNotification)")
        // shows a user tapped the notification. Implement user interaction, such as showing push details
    }
    
    
   
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        PushNotificationManager.push().handlePushReceived(userInfo)
        completionHandler(UIBackgroundFetchResult.noData)
        switch application.applicationState {
        case .active:
            //app is currently active, can update badges count here
            
            
            
            break
        case .inactive:
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            
            
            let  viewController = NotificationsViewController()
            let navigationController1 = NavigationController(rootViewController: viewController)
            let mainViewController = MainSideViewController()
            mainViewController.rootViewController = navigationController1
            mainViewController.setup(type: UInt(2))
            window?.rootViewController = mainViewController
            UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
            window?.makeKeyAndVisible()
            
            break
        case .background:
            //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
            
            
            break
        }
    }
    
    
   /* func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
       // return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool
    {
       // FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        return true
        
    }*/
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        
      //  FBSDKAppEvents.activateApp()
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        // Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        // connectToFCM()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Verdentum")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}



