//
//  HomePageViewController.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 17/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
//import FBSDKLoginKit
import MBProgressHUD
import LGSideMenuController

class HomePageViewController: UIViewController {
    
    var dict : [String : AnyObject]!
    
    var fbAccessToken = NSString()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        sideMenuController?.isLeftViewSwipeGestureDisabled = true
        
//        self.fbAccessToken = FBSDKAccessToken.current().tokenString! as NSString
//
//        print(self.fbAccessToken as Any)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sideMenuController?.isLeftViewSwipeGestureDisabled = true
    }
    
    @IBAction func loginBtnTapped(_ sender: Any) {
        
        let loginVC = LoginViewController(nibName: "LoginViewController", bundle: nil)
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func signupBtnTapped(_ sender: Any) {
        
        let signUpvc = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        navigationController?.pushViewController(signUpvc, animated: true)
    }
    
    
  /*  @IBAction func facebookBtnTapped(_ sender: Any) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
        
    }
    
    func getFBUserData()
    {
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                    
                    let idstr : NSString = (self.dict["id"] as AnyObject) as! NSString
                     let firstnamestr : NSString = (self.dict["first_name"] as AnyObject) as! NSString
                     let lastnamestr : NSString = (self.dict["last_name"] as AnyObject) as! NSString
                     let emailstr : NSString = (self.dict["email"] as AnyObject) as! NSString
                     let genderstr : NSString = (self.dict["gender"] as AnyObject) as! NSString
                   
                    
                    // prepare json data
                    let parameters: [String: Any] = [
                        "purpose" : "fblogin",
                        "id" : idstr,
                        "email" : emailstr,
                        "first_name" : firstnamestr,
                        "last_name" : lastnamestr,
                        "gender" : genderstr ]
                    
                    //create the url with NSURL
                    let url = NSURL(string: BaseURL_FacebookLoginAPI)
                    
                    //create the session object
                    let session = URLSession.shared
                    
                    //now create the NSMutableRequest object using the url object
                    let request = NSMutableURLRequest(url: url! as URL)
                    request.httpMethod = "POST" //set http method as POST
                    
                    do {
                        request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
                        // pass dictionary to nsdata object and set it as request body
                        
                    } catch let error {
                        print(error.localizedDescription)
                    }
                    
                    //HTTP Headers
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                   // request.addValue("application/json", forHTTPHeaderField: "Accept")
                    
                    //create dataTask using the session object to send data to the server
                    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                        
                        guard error == nil else {
                            return
                        }
                        
                        guard let data = data else {
                            return
                        }
                        
                        do {
                            //create json object from data
                            if let responsejson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                                DispatchQueue.main.async (execute:{ () -> Void in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    print(responsejson)
                                    
//                                    let successMsgstr : NSString = (responsejson["msg"] as AnyObject) as! NSString
//                                    print(successMsgstr)
                                    
                                    if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                                        //&& (responsejson["status"] as AnyObject) .isEqual(to: 0)
                                    {
                                        UserDefaults.standard.set(((responsejson["user"] as AnyObject) .value(forKey: "id") as AnyObject), forKey: "userID")
                                        UserDefaults.standard.set(((responsejson["user"] as AnyObject) .value(forKey: "name") as AnyObject), forKey: "userName")
                                        UserDefaults.standard.set(((responsejson["user"] as AnyObject) .value(forKey: "role") as AnyObject), forKey: "userRole")
                                        UserDefaults.standard.set(((responsejson["user"] as AnyObject) .value(forKey: "image") as AnyObject), forKey: "userImage")
                                        UserDefaults.standard.set(((responsejson["user"] as AnyObject) .value(forKey: "email") as AnyObject), forKey: "userEmail")
                                        
                                     /*   let frontViewController = DashBoardViewController()
                                        let rearViewController = SideMenuViewController()
                                        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
                                        let rearNavigationController = UINavigationController(rootViewController: rearViewController)
                                        let mainRevealController = SWRevealViewController(rearViewController: rearNavigationController, frontViewController: frontNavigationController) as SWRevealViewController*/
                                        
                                        let dashboardVC = DashBoardViewController()
                                        let sidemenuVC = SideMenuViewController()
                                        
                                        let navigationController = UINavigationController(rootViewController: dashboardVC)
                                        
                                        let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                                                      leftViewController: sidemenuVC,
                                                                                      rightViewController: nil)
                                        
                                        sideMenuController.leftViewWidth = 250.0;
                                        sideMenuController.leftViewPresentationStyle = .slideBelow;

                                        
                                        
                                        self.navigationController?.pushViewController(sideMenuController, animated: true)
                                        
                                    }
                                    else if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                                    {
                                        self.presentAlertWithTitle(title: "Login failed!", message: (responsejson["msg"] as AnyObject) as! String)
                                    }
                                        
                                    else
                                    {
                                        
                                    }
                                })
                            }
                        }
                        catch let error
                        {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            print(error.localizedDescription)
                        }
                    })
                    
                    
                    task.resume()
                    
                    
                    
                    
                }
            })
        }
    }*/
    
    // UIAlertViewController
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
