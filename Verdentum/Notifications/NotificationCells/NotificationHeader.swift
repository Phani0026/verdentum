//
//  NotificationHeader.swift
//  Verdentum
//
//  Created by Verdentum on 01/12/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class NotificationHeader: UITableViewHeaderFooterView {
   
    @IBOutlet var countlabelObj: UILabel!
    
    @IBOutlet var titleNameObj: UILabel!
    
    override func awakeFromNib() {
        
        backgroundView = UIView()
        backgroundView?.backgroundColor = UIColor.init(red: 211/255.0, green: 211/255.0, blue: 211/255.0, alpha: 0.1)
    }

}
