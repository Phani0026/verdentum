//
//  RequestCell.swift
//  Verdentum
//
//  Created by Verdentum on 07/12/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class RequestCell: UITableViewCell {

    @IBOutlet var imageViewObj: UIImageView!
    
    @IBOutlet var reqLabelObj: UILabel!
    
    @IBOutlet var nameLabelObj: UILabel!
    
    @IBOutlet var acceptBtnObj: UIButton!
    @IBOutlet var nameTopHeightObj: NSLayoutConstraint!
    
    @IBOutlet var deleteReqBtnObj: UIButton!
    
    @IBOutlet var acceptheightbtn: NSLayoutConstraint!
    
    @IBOutlet var reqLabelHeight: NSLayoutConstraint!
    
    @IBOutlet var deleteheightBtn: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
