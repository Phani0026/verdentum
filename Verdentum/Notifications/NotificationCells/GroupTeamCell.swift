
//
//  GroupTeamCell.swift
//  Verdentum
//
//  Created by Verdentum on 09/12/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class GroupTeamCell: UITableViewCell {

    @IBOutlet var imageViewObj: UIImageView!
    
    @IBOutlet var connectReqLabel: UILabel!
    @IBOutlet var nameLabelObj: UILabel!
    
    @IBOutlet var deleteBtnGroup: UIButton!
    @IBOutlet var acceptBtnGroup: UIButton!
    
    @IBOutlet var reqHeightLabel: NSLayoutConstraint!
    
    @IBOutlet var acceptheightBtn: NSLayoutConstraint!
    
    
    @IBOutlet var deleteHeightBtn: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
