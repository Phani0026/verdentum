//
//  NotificationCell.swift
//  Verdentum
//
//  Created by Verdentum on 18/11/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet var imageViewObj: UIImageView!
    
    @IBOutlet var nameLabelObj: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
