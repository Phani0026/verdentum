//
//  NotificationsViewController.swift
//  Verdentum
//
//  Created by Verdentum on 11/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD
import Alamofire

class NotificationsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var pushArray = NSMutableArray()
    var notificationArray = NSArray()
    var frndArray = NSArray()
    var groupArray = NSArray()
    var groupRequestArray = NSMutableArray()
    var friendRequestArray = NSMutableArray()
    var progIDuserDefault : String? = nil
     var noConnectionStr : String? = nil
    var friendReqstr = NSString()
    var pushdic = NSDictionary()
     var frndDic = NSDictionary()
     var groupDic = NSDictionary()
    
    var frndReqidstr : String? = ""
    var groupReqidstr : String? = ""
     var pushstr : String? = ""
    var postIDStr = NSString()
    var progidstr = NSString()
    var notificationsArray = NSArray()
    
    @IBOutlet var tableViewnotification: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.progIDuserDefault = nil
        self.noConnectionStr = nil
       
       // self.tableViewnotification.estimatedRowHeight = 40
        //self.tableViewnotification.rowHeight = UITableViewAutomaticDimension
        self.tableViewnotification .register(UINib (nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        self.tableViewnotification .register(UINib (nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "RequestCell")
        self.tableViewnotification .register(UINib (nibName: "GroupTeamCell", bundle: nil), forCellReuseIdentifier: "GroupTeamCell")
        self.tableViewnotification.register(UINib(nibName: "NotificationHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "NotificationHeader")
        self.tableViewnotification.reloadData()
        //----- NSNotificationCenter ------ //
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.push(fromCollectionView:)), name: NSNotification.Name(rawValue: "pushArrayNotifications"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.groupRequest(fromCollectionView:)), name: NSNotification.Name(rawValue: "groupRequestArrayNotifications"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.friendRequest(fromCollectionView:)), name: NSNotification.Name(rawValue: "friendRequestArrayNotifications"), object: nil)
    }
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didSelectItemFromCollectionView"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sideMenuBtnTapped(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    func push(fromCollectionView notification: Notification)
    {
        self.pushdic = notification .object as! NSDictionary
       
        self.notificationArray = self.pushdic .value(forKey: "pushnotificationdata") as! NSArray
      
        if self.notificationArray.count == 0 {
            self.tableViewnotification.reloadData()
        }
        else {
            let checkProgidDic : NSDictionary = (self.notificationArray  .firstObject) as! NSDictionary
            let keys = checkProgidDic.allKeys as! [String]
            let progidfiltered = keys.filter { $0.contains("progid") }
            if progidfiltered.count == 0 {
                let keys = checkProgidDic.allKeys as! [String]
                let postidfiltered = keys.filter { $0.contains("postid") }
                if postidfiltered.count == 0 {
                    let keys = checkProgidDic.allKeys as! [String]
                    let datafiltered = keys.filter { $0.contains("data") }
                    if datafiltered.count == 0 {
                        
                    }
                    else {
                        pushstr = progidfiltered[0]
                    }
                }
                else {
                    pushstr = progidfiltered[0]
                }
            }
            else {
                pushstr = progidfiltered[0]
            }
        }
        
        
        if (pushstr? .isEqual("progid"))! {
            self.tableViewnotification.reloadData()
        }
        else {
            if (pushstr? .isEqual("postid"))! {
                self.tableViewnotification.reloadData()
            }
            else {
                if (pushstr? .isEqual("data"))! {
                    self.tableViewnotification.reloadData()
                }
            }
        }
    }
    
    func groupRequest(fromCollectionView notification: Notification)
    {
        self.groupDic = notification .object as! NSDictionary
        self.groupArray = self.groupDic .value(forKey: "groupReq") as! NSArray
      
        if self.groupArray.count == 0 {
            self.tableViewnotification.reloadData()
        }
        else {
            let checkProgidDic : NSDictionary = (self.groupArray  .firstObject) as! NSDictionary
            let keys = checkProgidDic.allKeys as! [String]
            let progidfiltered = keys.filter { $0.contains("req_id") }
            if progidfiltered.count == 0 {
                let keys = checkProgidDic.allKeys as! [String]
                let progidfiltered = keys.filter { $0.contains("data") }
                if progidfiltered.count == 0 {
                    
                }
                else {
                    groupReqidstr = progidfiltered[0]
                }
            }
            else {
                groupReqidstr = progidfiltered[0]
            }
        }
        
        
        if (groupReqidstr? .isEqual("req_id"))! {
            self.tableViewnotification.reloadData()
        }
        else {
            if (groupReqidstr? .isEqual("data"))! {
                self.tableViewnotification.reloadData()
            }
        }
        
    }
    
    func friendRequest(fromCollectionView notification: Notification)
    {
        self.frndDic = notification .object as! NSDictionary
        self.frndArray = self.frndDic .value(forKey: "frndreq") as! NSArray
        
        if self.frndArray.count == 0 {
            // self.tableViewnotification.reloadData()
        } else {
            let checkProgidDic : NSDictionary = (self.frndArray  .firstObject) as! NSDictionary
            let keys = checkProgidDic.allKeys as! [String]
            let progidfiltered = keys.filter { $0.contains("req_id") }
            if progidfiltered.count == 0 {
                let keys = checkProgidDic.allKeys as! [String]
                let progidfiltered = keys.filter { $0.contains("data") }
                if progidfiltered.count == 0 {
                    
                }
                else {
                    frndReqidstr = progidfiltered[0]
                }
            }
            else {
                frndReqidstr = progidfiltered[0]
            }
        }
        
        
        
        
        if (frndReqidstr? .isEqual("req_id"))! {
             self.tableViewnotification.reloadData()
        }
        else {
            if (frndReqidstr? .isEqual("data"))! {
                 self.tableViewnotification.reloadData()
            }
        }
    }
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if self.frndArray.count == 0 {
                return 1
            }
            else {
               return self.frndArray.count
            }
        }
        if section == 1 {
            if self.groupArray.count == 0 {
                return 1
            } else {
                return self.groupArray.count
            }
        }
        if section == 2 {
           
            if self.notificationArray.count == 0 {
                return 1
            } else {
                return self.notificationArray.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            
            let frndreqcell = tableView .dequeueReusableCell(withIdentifier: "RequestCell") as! RequestCell
            
            print(self.frndArray)
            if self.frndArray.count == 0 {
                frndreqcell.nameLabelObj.text = "No Connection Request Found."
               
                frndreqcell.acceptBtnObj.isHidden = true
                frndreqcell.deleteReqBtnObj.isHidden = true
               // frndreqcell.imageViewObj.isHidden = true
                frndreqcell.acceptheightbtn.constant = 0.0
                frndreqcell.deleteheightBtn.constant = 0.0
               
            }
            else {
                if (frndReqidstr? .isEqual("req_id"))! {
                  
                    frndreqcell.acceptBtnObj.isHidden = false
                    frndreqcell.deleteReqBtnObj.isHidden = false
                    frndreqcell.imageViewObj.layer.borderWidth = 1.0
                    frndreqcell.imageViewObj.layer.borderColor = UIColor.lightGray.cgColor
                    frndreqcell.imageViewObj.layer.cornerRadius = (frndreqcell.imageViewObj.frame.size.width) / 2
                    frndreqcell.imageViewObj.layer.masksToBounds = true
                    
                    let reqNameStr : NSString = ((self.frndArray .object(at: indexPath.row) as AnyObject) .value(forKey: "from") as AnyObject) as! NSString
                    
                    let reqConnectStr : NSString = "request to connect with you."
                    
                    frndreqcell.nameLabelObj.text = reqNameStr .appending(reqConnectStr as String)
                    
                    
                    let imageURLstr2 : NSString = IMAGE_UPLOAD.appending(((self.frndArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!) as NSString
                    
                    let url = URL(string: imageURLstr2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                    
                   // let url = URL(string: IMAGE_UPLOAD.appending(((self.frndArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!))
                    
                    frndreqcell.imageViewObj.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                        
                    })
                    frndreqcell.acceptBtnObj.tag = indexPath.row
                    frndreqcell.acceptBtnObj.addTarget(self, action: #selector(self.frndAcceptTapped), for: .touchUpInside)
                    frndreqcell.deleteReqBtnObj.tag = indexPath.row
                    frndreqcell.deleteReqBtnObj.addTarget(self, action: #selector(self.deleteFrndReqTapped), for: .touchUpInside)
                }
                else {
                    if (frndReqidstr? .isEqual("data"))! {
                        
                         frndreqcell.acceptBtnObj.isHidden = true
                         frndreqcell.deleteReqBtnObj.isHidden = true
                        // frndreqcell.imageViewObj.isHidden = true
                         frndreqcell.acceptheightbtn.constant = 0.0
                         frndreqcell.deleteheightBtn.constant = 0.0
                         frndreqcell.nameLabelObj.text = ((self.frndArray .object(at: indexPath.row) as AnyObject) .value(forKey: "data") as! String)
                    }
                }
            }
            return frndreqcell
        }
        if indexPath.section == 1 {
            let groupcell = tableView .dequeueReusableCell(withIdentifier: "GroupTeamCell") as! GroupTeamCell
            
            if self.groupArray.count == 0 {
                groupcell.nameLabelObj.text = "No team requests found"
                
                 groupcell.acceptBtnGroup.isHidden = true
                 groupcell.deleteBtnGroup.isHidden = true
                 groupcell.imageViewObj.isHidden = true
                 groupcell.acceptheightBtn.constant = 0.0
                 groupcell.deleteHeightBtn.constant = 0.0
            } else {
                
                if (groupReqidstr? .isEqual("req_id"))! {
                   
                    groupcell.acceptBtnGroup.isHidden = false
                    groupcell.deleteBtnGroup.isHidden = false
                    groupcell.imageViewObj.layer.borderWidth = 1.0
                    groupcell.imageViewObj.layer.borderColor = UIColor.lightGray.cgColor
                    groupcell.imageViewObj.layer.cornerRadius = (groupcell.imageViewObj.frame.size.width) / 2
                    groupcell.imageViewObj.layer.masksToBounds = true
                    groupcell.nameLabelObj.text = ((self.groupArray .object(at: indexPath.row) as AnyObject) .value(forKey: "text") as! String)
                    
                    let imageURLstr2 : NSString = IMAGE_UPLOAD.appending(((self.groupArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!) as NSString
                    
                    let url = URL(string: imageURLstr2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                    
                   // let url = URL(string: IMAGE_UPLOAD.appending(((self.groupArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!))
                    
                    groupcell.imageViewObj.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                        
                    })
                    
                    groupcell.acceptBtnGroup.tag = indexPath.row
                    groupcell.acceptBtnGroup.addTarget(self, action: #selector(self.acceptTeamBtnTapped), for: .touchUpInside)
                    groupcell.deleteBtnGroup.tag = indexPath.row
                    groupcell.deleteBtnGroup.addTarget(self, action: #selector(self.deleteTeamReqTapped), for: .touchUpInside)
                    
                }
                else {
                    if (groupReqidstr? .isEqual("data"))! {
                        groupcell.acceptBtnGroup.isHidden = true
                        groupcell.deleteBtnGroup.isHidden = true
                        groupcell.imageViewObj.isHidden   = true
                        groupcell.acceptheightBtn.constant = 0.0
                        groupcell.deleteHeightBtn.constant = 0.0
                        groupcell.nameLabelObj.text = ((self.groupArray .object(at: indexPath.row) as AnyObject) .value(forKey: "data") as! String)
                    }
                }
                
                
                
            }
            return groupcell
        }
        if indexPath.section == 2 {
            let notificationcell = tableView .dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
            
            if self.notificationArray.count == 0 {
              notificationcell.nameLabelObj.text = "No notification found"
            }
            else {
                
                if (pushstr? .isEqual("progid"))! {
                    notificationcell.imageViewObj.layer.borderWidth = 1.0
                    notificationcell.imageViewObj.layer.borderColor = UIColor.lightGray.cgColor
                    notificationcell.imageViewObj.layer.cornerRadius = (notificationcell.imageViewObj.frame.size.width) / 2
                    notificationcell.imageViewObj.layer.masksToBounds = true
                    notificationcell.nameLabelObj.text = ((self.notificationArray .object(at: indexPath.row) as AnyObject) .value(forKey: "text") as! String)
                    
                    let imageURLstr2 : NSString = IMAGE_UPLOAD.appending(((self.notificationArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!) as NSString
                    
                    let url = URL(string: imageURLstr2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                    
                    //let url = URL(string: IMAGE_UPLOAD.appending(((self.notificationArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!))
                    
                    notificationcell.imageViewObj.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                        
                    })
                }
                else {
                    if (pushstr? .isEqual("postid"))! {
                        notificationcell.imageViewObj.layer.borderWidth = 1.0
                        notificationcell.imageViewObj.layer.borderColor = UIColor.lightGray.cgColor
                        notificationcell.imageViewObj.layer.cornerRadius = (notificationcell.imageViewObj.frame.size.width) / 2
                        notificationcell.imageViewObj.layer.masksToBounds = true
                        notificationcell.nameLabelObj.text = ((self.notificationArray .object(at: indexPath.row) as AnyObject) .value(forKey: "text") as! String)
                        
                        let url = URL(string: IMAGE_UPLOAD.appending(((self.notificationArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!))
                        
                        notificationcell.imageViewObj.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                            
                        })
                    }
                    else {
                        if (pushstr? .isEqual("data"))! {
                            notificationcell.nameLabelObj.text = ((self.notificationArray .object(at: indexPath.row) as AnyObject) .value(forKey: "data") as! String)
                        }
                    }
                }
            }
            
            return notificationcell
        }
        let notificationcell = tableView .dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        
        return notificationcell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0 {
            let headerView = self.tableViewnotification.dequeueReusableHeaderFooterView(withIdentifier: "NotificationHeader") as! NotificationHeader
            headerView.layer.borderWidth = 0.5
            headerView.layer.borderColor = UIColor.gray.cgColor
            headerView.titleNameObj.text = "Connection Requests"
            if (frndReqidstr? .isEqual("req_id"))! {
                 headerView.countlabelObj.text = String(self.frndArray.count)
            }
            else {
                if (frndReqidstr? .isEqual("data"))! {
                     headerView.countlabelObj.text = "0"
                }
            }
            return headerView
        }
        if section == 1 {
            let headerView = self.tableViewnotification.dequeueReusableHeaderFooterView(withIdentifier: "NotificationHeader") as! NotificationHeader
            headerView.layer.borderWidth = 0.5
            headerView.layer.borderColor = UIColor.gray.cgColor
             headerView.titleNameObj.text = "Team Requests"
            if (groupReqidstr? .isEqual("req_id"))! {
               headerView.countlabelObj.text = String(self.groupArray.count)
            }
            else {
                if (groupReqidstr? .isEqual("data"))! {
                    headerView.countlabelObj.text = "0"
                }
            }
            return headerView
        }
        if section == 2 {
            let headerView = self.tableViewnotification.dequeueReusableHeaderFooterView(withIdentifier: "NotificationHeader") as! NotificationHeader
            headerView.layer.borderWidth = 0.5
            headerView.layer.borderColor = UIColor.gray.cgColor
             headerView.titleNameObj.text = "Notifications"
            if (pushstr? .isEqual("progid"))! {
                headerView.countlabelObj.text = String(self.notificationArray.count)
            }
            else {
                if (pushstr? .isEqual("postid"))! {
                     headerView.countlabelObj.text = String(self.notificationArray.count)
                }
                else {
                    if (pushstr? .isEqual("data"))! {
                         headerView.countlabelObj.text = "0"
                    }
                }
            }
            return headerView
        }
         let headerView = self.tableViewnotification.dequeueReusableHeaderFooterView(withIdentifier: "NotificationHeader") as! NotificationHeader
         return headerView
    }
    
    
    private func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
           return 50.0
        }
        if indexPath.section == 1 {
           return 50.0
        }
        if indexPath.section == 2 {
          return 50.0
        }
        return 0.0
    }
    
    private func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
           return UITableViewAutomaticDimension
        }
        if indexPath.section == 1 {
            return UITableViewAutomaticDimension
        }
        if indexPath.section == 2 {
           return UITableViewAutomaticDimension
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
        }
        if indexPath.section == 1 {
            
        }
        if indexPath.section == 2 {
            
            if self.notificationArray.count == 0 {
                
            }
            else {
                let dicNotification : NSDictionary = self.notificationArray .object(at: indexPath.row) as! NSDictionary
                
                
                let keys = dicNotification.allKeys as! [String]
                print(keys)
                let progidfiltered = keys.filter { $0.contains("progid") }
                print(progidfiltered)
                
                if progidfiltered.count == 0 {
                    let keys = dicNotification.allKeys as! [String]
                    print(keys)
                    let postidfiltered = keys.filter { $0.contains("postid") }
                    print(postidfiltered)
                    if postidfiltered.count == 0 {
                        let keys = dicNotification.allKeys as! [String]
                        print(keys)
                        let datafiltered = keys.filter { $0.contains("data") }
                        print(datafiltered)
                        
                        if datafiltered.count == 0 {
                            
                        }
                        else {
                            pushstr = progidfiltered[0]
                            print(pushstr!)
                        }
                    }
                    else {
                        pushstr = progidfiltered[0]
                        print(pushstr!)
                    }
                }
                else {
                    pushstr = progidfiltered[0]
                    print(pushstr!)
                }
                
                if (pushstr? .isEqual("progid"))! {
                    
                    self.progidstr = dicNotification .value(forKey: "progid") as! NSString
                    let userID = UserDefaults.standard.value(forKey: "userID")
                    let params:[String: AnyObject] = [
                        "purpose" : "fastprograms" as AnyObject,
                        "id" : userID as AnyObject]
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    self.uploadActivityAPICalling(param: params)
                    
                } else {
                    
                    if (pushstr? .isEqual("postid"))! {
                        self.postIDStr = (self.notificationArray .object(at: indexPath.row) as AnyObject) .value(forKey: "postid") as! NSString
                        
                        let userID = UserDefaults.standard.value(forKey: "userID")
                        let params:[String: AnyObject] = [
                            "purpose" : "fastprograms" as AnyObject,
                            "id" : self.postIDStr as AnyObject,
                            "userid" : userID as AnyObject]
                        MBProgressHUD.showAdded(to: self.view, animated: true)
                        self.pushPostAPICaling(param: params)
                    }
                    else {
                        if (pushstr? .isEqual("data"))! {
                            
                        }
                    }
                }
            }           
        }
    }
    
    func uploadActivityAPICalling(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_UPLOADACTIVITYAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                              print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.presentAlertWithTitle(title: "", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                            else
                            {
                               
                                let mainViewController = self.sideMenuController!
                                let UploadActivityVC = UploadActivityViewController()
                                
                                
                                UploadActivityVC.mainProgramArray = (responsejson["programs"] as AnyObject) as! NSArray
                                UploadActivityVC.programIDStrIndex = self.progidstr
                                UploadActivityVC.navigatePushStr = "pushNavigateStr"
                                
                                UploadActivityVC.mainGroupArray = ((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject)  as! NSArray
                                UploadActivityVC.programArray = ((responsejson["programs"] as AnyObject) .value(forKey: "prog_name") as AnyObject) as! NSArray
                                UploadActivityVC.joiningArray = ((responsejson["programs"] as AnyObject) .value(forKey: "joining") as AnyObject) as! NSArray
                                UploadActivityVC.defaultProgramStr = (((responsejson["programs"] as AnyObject) .value(forKey: "prog_name") as AnyObject) .firstObject) as! NSString
                                UploadActivityVC.programIDArray = ((responsejson["programs"] as AnyObject) .value(forKey: "prog_id") as AnyObject) as! NSArray
                                UploadActivityVC.programIDElementArray = (((responsejson["programs"] as AnyObject) .value(forKey: "prog_id") as AnyObject) as! [String])
                                
                                
                                UploadActivityVC.teamArray = ((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject)  as! NSArray
                                UploadActivityVC.defaultTeamStr = (((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject) .value(forKey: "group_name") as AnyObject) as! NSArray
                                UploadActivityVC.groupAdminArray = (((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject) .value(forKey: "group_admin") as AnyObject) as! NSArray
                                UploadActivityVC.teamIDArray = ((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject)  as! NSArray
                                UploadActivityVC.selectActivityArray = ((responsejson["programs"] as AnyObject) .value(forKey: "tasks") as AnyObject)  as! NSArray
                                UploadActivityVC.selectActivityIDArray = ((responsejson["programs"] as AnyObject) .value(forKey: "tasks") as AnyObject)  as! NSArray
                                UploadActivityVC.navstr = "uploadVC"
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(UploadActivityVC, animated: true)
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func pushPostAPICaling(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_PushPostAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        //      print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.presentAlertWithTitle(title: "", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                            else
                            {
                                
                                let mainViewController = self.sideMenuController!
                                let DashBoardVC = DashBoardViewController()
                                
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(DashBoardVC, animated: true)
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    // ---- Team Request Button Actions --- //
    
    func acceptTeamBtnTapped(_ sender: UIButton) {
        let reqIDno : NSNumber = (self.groupArray .object(at: sender.tag) as AnyObject) .value(forKey: "req_id") as! NSNumber
        let reqIDStr : NSString = reqIDno.stringValue as NSString
        let params:[String: AnyObject] = [
            "purpose" : "updategroup" as AnyObject,
            "flag" : "reject" as AnyObject,
            "data" : reqIDStr as AnyObject]
        
        if Reachability.isConnectedToNetwork() == true {
        
            Alamofire.request(BaseURL_GroupReqAcceptAPI, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    // print(response.request as Any)  // original URL request
                    // print(response.response as Any) // URL response
                    // print(response.result.value as Any)   // result of response serialization
                    if let JSON = response.result.value as? [String: Any] {
                        print(JSON)
                        if JSON.count == 0 {
                            
                        }
                        else {
//                            let statusCheckStr : NSString = JSON["status"] as! NSString
//                            if statusCheckStr .isEqual(to: "1") {
                                self.tableViewnotification.reloadData()
//                            }
//                            else {
//
//                            }
                            
                        }
                        
                    }
            }
        }
        else {
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
    }
    func deleteTeamReqTapped(_ sender: UIButton) {
        let reqIDno : NSNumber = (self.groupArray .object(at: sender.tag) as AnyObject) .value(forKey: "req_id") as! NSNumber
        let reqIDStr : NSString = reqIDno.stringValue as NSString
        let params:[String: AnyObject] = [
            "purpose" : "updategroup" as AnyObject,
            "flag" : "reject" as AnyObject,
            "data" : reqIDStr as AnyObject]
        
        if Reachability.isConnectedToNetwork() == true {
        
            Alamofire.request(BaseURL_GroupReqAcceptAPI, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    // print(response.request as Any)  // original URL request
                    // print(response.response as Any) // URL response
                    // print(response.result.value as Any)   // result of response serialization
                    if let JSON = response.result.value as? [String: Any] {
                        if JSON.count == 0 {
                            
                        }
                        else {
//                            let statusCheckStr : NSString = JSON["status"] as! NSString
//                            if statusCheckStr .isEqual(to: "1") {
                                self.tableViewnotification.reloadData()
//                            }
//                            else {
//
//                            }
                            
                        }
                    }
            }
        }
        else {
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    // ----- FriendRequest Button Actions --- //
    
    func frndAcceptTapped(_ sender: UIButton) {
        
        let reqIDno : NSNumber = (self.frndArray .object(at: sender.tag) as AnyObject) .value(forKey: "req_id") as! NSNumber
        let reqIDStr : NSString = reqIDno.stringValue as NSString
        let params:[String: AnyObject] = [
            "purpose" : "updatefriend" as AnyObject,
            "flag" : "accept" as AnyObject,
            "data" : reqIDStr as AnyObject]
        
        if Reachability.isConnectedToNetwork() == true {
        
            Alamofire.request(BaseURL_FriendReqAcceptAPI, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    // print(response.request as Any)  // original URL request
                    // print(response.response as Any) // URL response
                    // print(response.result.value as Any)   // result of response serialization
                    if let JSON = response.result.value as? [String: Any] {
                        if JSON.count == 0 {
                            
                        }
                        else {
//                            let statusCheckStr : NSString = JSON["status"] as! NSString
//                            if statusCheckStr .isEqual(to: "1") {
                                self.tableViewnotification.reloadData()
//                            }
//                            else {
//
//                            }
                            
                        }
                    }
            }
        }
        else {
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
    }
    func deleteFrndReqTapped(_ sender: UIButton) {
        let reqIDno : NSNumber = (self.frndArray .object(at: sender.tag) as AnyObject) .value(forKey: "req_id") as! NSNumber
        let reqIDStr : NSString = reqIDno.stringValue as NSString
        let params:[String: AnyObject] = [
            "purpose" : "updatefriend" as AnyObject,
            "flag" : "reject" as AnyObject,
            "data" : reqIDStr as AnyObject]
        
        if Reachability.isConnectedToNetwork() == true {
        
            Alamofire.request(BaseURL_FriendReqAcceptAPI, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    // print(response.request as Any)  // original URL request
                    // print(response.response as Any) // URL response
                    // print(response.result.value as Any)   // result of response serialization
                    if let JSON = response.result.value as? [String: Any] {
                        if JSON.count == 0 {
                            
                        }
                        else {
//                            let statusCheckStr : NSString = JSON["status"] as! NSString
//                            if statusCheckStr .isEqual(to: "1") {
                                self.tableViewnotification.reloadData()
//                            }
//                            else {
//
//                            }
                            
                        }
                    }
            }
        }
        else {
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
    }
    
}
