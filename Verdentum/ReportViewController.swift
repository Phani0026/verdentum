//
//  ReportViewController.swift
//  Verdentum
//
//  Created by Verdentum on 01/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD
import LGSideMenuController

class ReportViewController: UIViewController {
     var personalTitleName = NSString()
     var headerStr = String()
     var commentUsedID = String()
    var selectedReasonStr = NSString()
    var selected = Bool()
    var reportuserStr = NSString()
    var userpostIdstr = NSString()
    var userNameObjStr = NSString()
    var responseArray = NSMutableArray()
    var navstr = NSString()
    var selectedRow = NSIndexPath()
    var peopleReg_idStr = NSString()
    var usernameobjstr = NSString()
    var postUserNamestr = NSString()
    var checkImageORNotStr : String?
    var programArray = NSMutableArray()
    var program_idstr = NSString()
    var headerNameStr = NSString()
    var groupImageStr = NSString()
    var groupIDStr = NSString()
    var userID_Registered = NSString()
    var verticalOffset: Int = 0
    var verticalOffsetPostUser: Int = 0
    var programNamestr = NSString()
    var allcommentCountArray = NSMutableArray()
    var responseUserPostsArray = NSMutableArray()
    var allPersonalcommentsArray = NSMutableArray()
    var headerTitleStr = NSString()
    var personalheaderTitleStr = NSString()
    var post_idStr = NSString()
    // var peopleReg_idStr = NSString()
    var commentPost_ID = NSString()
    var userReportCheck = Bool()
    
    var allSupportArray = NSMutableArray()
    var allSupportPersonalArray = NSMutableArray()
    var allFeedcommentCountArray = NSMutableArray()
    var allsupportFeedArray = NSMutableArray()
    var allsupportPeopleArray = NSMutableArray()
    var allsupportGroupArray = NSMutableArray()
    var allisLikesGroupArray = NSMutableArray()
    var allisLikeArray = NSMutableArray()
    var allisLikesPersonalArray = NSMutableArray()
    var allisLikesPeopleArray = NSMutableArray()
    var allisLikesFeedArray = NSMutableArray()
    
    
    
    
    @IBOutlet var imageViewCheck1: UIImageView!
    
    @IBOutlet var radioBtnOne: UIButton!
    @IBOutlet var imageViewCheck2: UIImageView!
    @IBOutlet var imageViewCheck3: UIImageView!
    @IBOutlet var checkboxImageViewObj: UIImageView!
    @IBOutlet var radioBtnTwo: UIButton!
    @IBOutlet var radioBtnThree: UIButton!
    
    @IBOutlet var viewfourobj: UIView!
    @IBOutlet var viewoneobj: UIView!
    @IBOutlet var viewtwoobj: UIView!
    @IBOutlet var viewthreeobj: UIView!
    
    
    //MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.userReportCheck = false
        self.selected = true
        self.selectedReasonStr = "1"
        self.imageViewCheck1.image = UIImage (named: "checkMark")
        self.viewoneobj.layer.borderWidth = 1
        self.viewoneobj.layer.borderColor = UIColor.init(red:170/255.0, green:170/255.0, blue:170/255.0, alpha: 1.0).cgColor
        self.viewtwoobj.layer.borderWidth = 1
        self.viewtwoobj.layer.borderColor = UIColor.init(red:170/255.0, green:170/255.0, blue:170/255.0, alpha: 1.0).cgColor
        self.viewthreeobj.layer.borderWidth = 1
        self.viewthreeobj.layer.borderColor = UIColor.init(red:170/255.0, green:170/255.0, blue:170/255.0, alpha: 1.0).cgColor
        self.viewfourobj.layer.borderWidth = 1
        self.viewfourobj.layer.borderColor = UIColor.init(red:170/255.0, green:170/255.0, blue:170/255.0, alpha: 1.0).cgColor
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sideMenuController?.isLeftViewSwipeGestureDisabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - checkboxBtnTapped
    
    @IBAction func checkboxBtnTapped(_ sender: Any) {
        
        // Instead of specifying each button we are just using the sender (button that invoked) the method
        
        
        if (self.selected == false)
        {
            self.checkboxImageViewObj.image = UIImage (named: "unchecked")
            self.selected = true
            self.reportuserStr = ""
            self.userReportCheck = false
        }
        else
        {
            self.checkboxImageViewObj.image = UIImage (named: "Checkbox Filled")
            self.selected = false
            self.reportuserStr = "true"
            self.userReportCheck = true
        }
    }
    //MARK: - radioBtnTapped
    
    @IBAction func radioBtnTapped(_ sender: Any) {
        
        switch (sender as AnyObject).tag {
        case 1:
            if self.radioBtnOne.isSelected == true {
                self.radioBtnOne.isSelected = false
                self.imageViewCheck1.image = UIImage (named: "checkMark")
                self.imageViewCheck2.image = UIImage (named: "")
                self.imageViewCheck3.image = UIImage (named: "")
                self.selectedReasonStr = ""
            }
            else {
                self.radioBtnOne.isSelected = true
                self.radioBtnTwo.isSelected = false
                self.radioBtnThree.isSelected = false
                self.selectedReasonStr = "1"
                self.imageViewCheck1.image = UIImage (named: "checkMark")
                self.imageViewCheck3.image = UIImage (named: "")
                self.imageViewCheck2.image = UIImage (named: "")
            }
        case 2:
            if self.radioBtnTwo.isSelected == true {
                self.radioBtnTwo.isSelected = false
                self.selectedReasonStr = ""
                self.imageViewCheck2.image = UIImage (named: "checkMark")
                self.imageViewCheck1.image = UIImage (named: "")
                self.imageViewCheck1.image = UIImage (named: "")
            }
            else {
                self.radioBtnTwo.isSelected = true
                self.radioBtnOne.isSelected = false
                self.radioBtnThree.isSelected = false
                self.selectedReasonStr = "2"
                self.imageViewCheck2.image = UIImage (named: "checkMark")
                self.imageViewCheck1.image = UIImage (named: "")
                self.imageViewCheck3.image = UIImage (named: "")
                
            }
        case 3:
            if self.radioBtnThree.isSelected == true {
                self.radioBtnThree.isSelected = false
                self.selectedReasonStr = ""
                self.imageViewCheck3.image = UIImage (named: "checkMark")
                self.imageViewCheck2.image = UIImage (named: "")
                self.imageViewCheck1.image = UIImage (named: "")
            }
            else {
                self.radioBtnTwo.isSelected = false
                self.radioBtnOne.isSelected = false
                self.radioBtnThree.isSelected = true
                self.selectedReasonStr = "3"
                self.imageViewCheck3.image = UIImage (named: "checkMark")
                self.imageViewCheck1.image = UIImage (named: "")
                self.imageViewCheck2.image = UIImage (named: "")
            }
        default:
            break
        }
    }
    
    //MARK: - SubmitrepotBtntapped
    
    @IBAction func submitrepotBtntapped(_ sender: Any) {
        let userID = UserDefaults.standard.value(forKey: "userID")
        if self.reportuserStr == "true"
        {
                let params:[String: AnyObject] = [
                    "purpose" : "reportpost" as AnyObject,
                    "report" : [
                        "type" : self.selectedReasonStr as AnyObject,
                        "post" : self.post_idStr as AnyObject,
                        "reportuser" : self.userReportCheck as AnyObject] as AnyObject,
                    "id" : userID as AnyObject]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.postsApiCalling(param: params)
        }
        if self.reportuserStr == ""
        {
                let params:[String: AnyObject] = [
                    "purpose" : "reportpost" as AnyObject,
                    "report" : [
                        "type" : self.selectedReasonStr as AnyObject,
                        "post" : self.post_idStr as AnyObject,
                        "reportuser" : self.userReportCheck as AnyObject] as AnyObject,
                    "id" : userID as AnyObject]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.postsApiCalling(param: params)
        }
    }
    
    //MARK: - Report API Calling
    
    func postsApiCalling(param:[String: AnyObject])
    {
        
        if Reachability.isConnectedToNetwork() == true {
      

            // MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:VERDENTUMB_BaseURL.appending(BaseURL_VERDENTUMReportpostAPI))
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        
                        DispatchQueue.main.async (execute:{ () -> Void in
                             MBProgressHUD.hide(for: self.view, animated: true)
                            //print(responsejson)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                                
                            {
                                if self.navstr == "dashBoardComment" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "dashBoardComment"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "WorldIndividualCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "WorldViewIndividualVC"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "networkpeopleCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "networkpeople"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "PeoplesNewPageCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "PeoplesNewPage"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "dashboarpersonalprofileComment" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "dashboarpersonalprofileComment"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "worldViewFeedprofileComment" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "worldViewFeedprofileComment"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "WorldViewGroupCommentPF" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "WorldViewGroupCommentPF"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "WorldIndividualCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "WorldViewIndividualVC"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr .isEqual(to: "peoplereportPost") {
                                    
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                   // let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                   // }
                                   // alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        let addpostVC = PeoplePostsViewController(nibName: "PeoplePostsViewController", bundle: nil)
                                        
                                        // self.responseArray .removeObject(at: self.selectedRow.row)
                                        
                                        addpostVC.navStr = self.navstr
                                        addpostVC.responsePostArray = self.responseArray
                                         addpostVC.allsupportPeopleArray = self.allsupportPeopleArray
                                         addpostVC.allisLikesPeopleArray = self.allisLikesPeopleArray
                                         addpostVC.usernameObjstr = self.headerTitleStr
                                         addpostVC.peopleReg_idStr = self.peopleReg_idStr
                                         addpostVC.commentPost_ID = self.commentPost_ID
                                          addpostVC.navStr = "PeoplesNewPage"
                                        //addpostVC.selectedRow = self.selectedRow
                                        self.navigationController?.pushViewController(addpostVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "joinProgramReport" {
                                    let mainViewController = self.sideMenuController!
                                    let PersonalAccountVC = PersonalAccountViewController()
                                    PersonalAccountVC.postID = self.post_idStr
                                    PersonalAccountVC.postusedID = self.userpostIdstr
                                    PersonalAccountVC.groupIDStr = self.groupIDStr
                                    PersonalAccountVC.groupImageStr = self.groupImageStr
                                    PersonalAccountVC.userID_Registered = self.userID_Registered
                                    PersonalAccountVC.program_idstr = self.program_idstr
                                    PersonalAccountVC.programNamestr = self.programNamestr
                                    PersonalAccountVC.WorldViewheaderTitleStr = self.headerTitleStr
                                    PersonalAccountVC.headerTitleStr = self.personalheaderTitleStr
                                    PersonalAccountVC.responsePostArray = self.responseArray
                                    PersonalAccountVC.allcommentCountArray = self.allcommentCountArray
                                    PersonalAccountVC.selectedRow = self.selectedRow
                                    PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
                                    PersonalAccountVC.programArray = self.programArray
                                    PersonalAccountVC.allSupportArray = self.allSupportArray
                                    PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                    PersonalAccountVC.allsupportGroupArray = self.allsupportGroupArray
                                    PersonalAccountVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    PersonalAccountVC.allisLikeArray = self.allisLikeArray
                                    PersonalAccountVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    PersonalAccountVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    PersonalAccountVC.userID_Registered = self.userID_Registered
                                    PersonalAccountVC.programArray = self.programArray
                                    PersonalAccountVC.navStr = "joinProgram"
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(PersonalAccountVC, animated: true)
                                }
                                if self.navstr == "joinProgram"
                                {
                                    let mainViewController = self.sideMenuController!
                                    let ReportuserVC = GroupNewsViewController()
                                    ReportuserVC.postusedID = self.userpostIdstr
                                    ReportuserVC.responsePostArray = self.responseArray
                                    ReportuserVC.selectedRow = self.selectedRow
                                    ReportuserVC.groupIDStr = self.groupIDStr
                                    ReportuserVC.headerNameStr = self.headerNameStr
                                    ReportuserVC.groupImageStr = self.groupImageStr
                                    ReportuserVC.program_idstr = self.program_idstr
                                    ReportuserVC.programArray = self.programArray
                                    ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
                                     ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    ReportuserVC.navStr = "joinProgram"
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(ReportuserVC, animated: true)
                                    
                                }
                                if self.navstr == "worldViewPersonalreportNav"
                                {
                                    let mainViewController = self.sideMenuController!
                                    let PersonalAccountVC = PersonalAccountViewController()
                                    PersonalAccountVC.postID = self.post_idStr
                                    PersonalAccountVC.postusedID = self.userpostIdstr
                                    PersonalAccountVC.groupIDStr = self.groupIDStr
                                    PersonalAccountVC.userNameObjStr = self.userNameObjStr
                                    PersonalAccountVC.headerNameStr = self.headerNameStr
                                    PersonalAccountVC.userID_Registered = self.userID_Registered
                                    PersonalAccountVC.program_idstr = self.program_idstr
                                    PersonalAccountVC.groupImageStr = self.groupImageStr
                                    PersonalAccountVC.programNamestr = self.programNamestr
                                     PersonalAccountVC.programArray = self.programArray
                                    PersonalAccountVC.WorldViewheaderTitleStr = self.headerTitleStr
                                    PersonalAccountVC.headerTitleStr = self.personalheaderTitleStr
                                    PersonalAccountVC.responsePostArray = self.responseArray
                                    PersonalAccountVC.allcommentCountArray = self.allcommentCountArray
                                    PersonalAccountVC.selectedRow = self.selectedRow
                                    PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
                                    PersonalAccountVC.allSupportArray = self.allSupportArray
                                    PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                    PersonalAccountVC.allsupportGroupArray = self.allsupportGroupArray
                                    PersonalAccountVC.allSupportPersonalArray = self.allSupportPersonalArray
                                    PersonalAccountVC.allisLikeArray = self.allisLikeArray
                                    PersonalAccountVC.allisLikesGroupArray = self.allisLikesGroupArray
                                    PersonalAccountVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                     PersonalAccountVC.groupIDStr = self.groupIDStr
                                    PersonalAccountVC.navStr = "WorldViewGroup"
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(PersonalAccountVC, animated: true)
                                }
                                if self.navstr == "GroupreportPostNav"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                   // let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                   // }
                                  //  alertController.addAction(cancelAction)
                                  
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        
                                        let mainViewController = self.sideMenuController!
                                        let ReportuserVC = GroupNewsViewController()
                                        ReportuserVC.postusedID = self.userpostIdstr
                                        ReportuserVC.responsePostArray = self.responseArray
                                        ReportuserVC.selectedRow = self.selectedRow
                                        ReportuserVC.groupIDStr = self.groupIDStr
                                        ReportuserVC.headerNameStr = self.headerNameStr
                                        ReportuserVC.groupImageStr = self.groupImageStr
                                        ReportuserVC.program_idstr = self.program_idstr
                                        ReportuserVC.programArray = self.programArray
                                         ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
                                         ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
                                        ReportuserVC.navStr = "GroupreportPostNav"
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(ReportuserVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr ==  "FeedreportPostNav" {
                                    
                                    
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                   // let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                   // }
                                  //  alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        let mainViewController = self.sideMenuController!
                                        let ProgramSwipeVC = ProgramSwipeViewController()
                                        ProgramSwipeVC.navStr = "MyProgramFeedVC"
                                        ProgramSwipeVC.responsePostArray = self.responseArray
                                        ProgramSwipeVC.verticalOffset = self.verticalOffset
                                        ProgramSwipeVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                        ProgramSwipeVC.allisLikesFeedArray = self.allisLikesFeedArray
                                        ProgramSwipeVC.allsupportFeedArray = self.allsupportFeedArray
                                        ProgramSwipeVC.headerTitleStr = self.headerTitleStr
                                        ProgramSwipeVC.program_idstr = self.program_idstr
                                         ProgramSwipeVC.headerTitleStr = self.headerTitleStr
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(ProgramSwipeVC, animated: true)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr ==  "WorldViewIndividualVC"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                   // let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                   // }
                                   // alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        let ReportuserVC = PersonalAccountViewController(nibName: "PersonalAccountViewController", bundle: nil)
                                        ReportuserVC.postusedID = self.userpostIdstr
                                        // ReportuserVC.responsePostArray = self.responseArray
                                        // ReportuserVC.allcommentCountArray = self.allcommentCountArray
                                        ReportuserVC.selectedRow = self.selectedRow
                                        ReportuserVC.userID_Registered = self.userID_Registered
                                        ReportuserVC.program_idstr = self.program_idstr
                                        ReportuserVC.programNamestr = self.programNamestr
                                        ReportuserVC.WorldViewheaderTitleStr = self.headerTitleStr
                                        ReportuserVC.headerTitleStr = self.personalheaderTitleStr
                                        // ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
                                        // ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        ReportuserVC.navStr = "WorldViewIndividualVC"
                                        self.navigationController?.pushViewController(ReportuserVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                    
                                    
                                }
                                
                                if self.navstr == "HomereportPostNav"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                  //  let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                  //  }
                                  //  alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        
                                        let addpostVC = DashBoardViewController(nibName: "DashBoardViewController", bundle: nil)
                                        addpostVC.navStr = self.navstr
                                        addpostVC.responsePostArray = self.responseArray
                                        addpostVC.allcommentCountArray = self.allcommentCountArray
                                        addpostVC.allSupportArray = self.allSupportArray
                                        addpostVC.allisLikeArray = self.allisLikeArray
                                        addpostVC.verticalOffset = self.verticalOffset
                                        //addpostVC.selectedRow = self.selectedRow
                                        self.navigationController?.pushViewController(addpostVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "personalreportPostNav"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                  //  let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                  //  }
                                  //  alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        
                                        let addpostVC = PersonalAccountViewController(nibName: "PersonalAccountViewController", bundle: nil)
                                        
                                        addpostVC.navStr = "NewPage"
                                        addpostVC.postusedID = self.userpostIdstr
                                        addpostVC.responsePostArray = self.responseArray
                                        addpostVC.allcommentCountArray = self.allcommentCountArray
                                        addpostVC.responseUserPostsArray = self.responseUserPostsArray
                                        addpostVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        addpostVC.verticalOffsetPostUser = self.verticalOffsetPostUser
                                        addpostVC.verticalOffset = self.verticalOffset
                                        addpostVC.allisLikeArray = self.allisLikeArray
                                        addpostVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                        addpostVC.headerTitleStr = self.headerTitleStr
                                         addpostVC.userNameObjStr = self.userNameObjStr
                                        self.navigationController?.pushViewController(addpostVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                
                                if self.navstr == "MyProgramPrsnlVC"
                                {
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = PersonalAccountViewController()
                                    CommentsVC.verticalOffsetPostUser =  self.verticalOffsetPostUser
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.navStr = "MyProgramPrsnlVC"
                                    CommentsVC.responsePostArray = self.responseArray
                                    CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                    CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                   // CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.program_idstr = self.program_idstr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                
                                
                                // self.presentAlertWithTitle(title: "", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                                
                            {
                                if self.navstr == "dashBoardComment" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "dashBoardComment"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "WorldIndividualCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "WorldViewIndividualVC"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "networkpeopleCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "networkpeople"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "PeoplesNewPageCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "PeoplesNewPage"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "dashboarpersonalprofileComment" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "dashboarpersonalprofileComment"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "worldViewFeedprofileComment" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "worldViewFeedprofileComment"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "WorldViewGroupCommentPF" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "WorldViewGroupCommentPF"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "WorldIndividualCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "WorldViewIndividualVC"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr .isEqual(to: "peoplereportPost") {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                  //  let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                        
                                   // }
                                   // alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        self.responseArray .removeObject(at: self.selectedRow.row)
                                        let addpostVC = PeoplePostsViewController(nibName: "PeoplePostsViewController", bundle: nil)
                                        
                                        addpostVC.navStr = self.navstr
                                        addpostVC.responsePostArray = self.responseArray
                                        addpostVC.allsupportPeopleArray = self.allsupportPeopleArray
                                         addpostVC.allisLikesPeopleArray = self.allisLikesPeopleArray
                                         addpostVC.usernameObjstr = self.headerTitleStr
                                         addpostVC.peopleReg_idStr = self.peopleReg_idStr
                                        addpostVC.commentPost_ID = self.commentPost_ID
                                         addpostVC.navStr = "PeoplesNewPage"
                                        //addpostVC.selectedRow = self.selectedRow
                                        self.navigationController?.pushViewController(addpostVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "joinProgramReport" {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                 //   let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                        
                                 //   }
                                 //   alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                         self.responseUserPostsArray .removeObject(at: self.selectedRow.row)
                                    let mainViewController = self.sideMenuController!
                                    let PersonalAccountVC = PersonalAccountViewController()
                                    PersonalAccountVC.postID = self.post_idStr
                                    PersonalAccountVC.postusedID = self.userpostIdstr
                                    PersonalAccountVC.groupIDStr = self.groupIDStr
                                    PersonalAccountVC.groupImageStr = self.groupImageStr
                                    PersonalAccountVC.userID_Registered = self.userID_Registered
                                    PersonalAccountVC.program_idstr = self.program_idstr
                                    PersonalAccountVC.programNamestr = self.programNamestr
                                    PersonalAccountVC.WorldViewheaderTitleStr = self.headerTitleStr
                                    PersonalAccountVC.headerTitleStr = self.personalheaderTitleStr
                                    PersonalAccountVC.responsePostArray = self.responseArray
                                    PersonalAccountVC.allcommentCountArray = self.allcommentCountArray
                                    PersonalAccountVC.selectedRow = self.selectedRow
                                    PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
                                    PersonalAccountVC.programArray = self.programArray
                                    PersonalAccountVC.allSupportArray = self.allSupportArray
                                    PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                    PersonalAccountVC.allsupportGroupArray = self.allsupportGroupArray
                                    PersonalAccountVC.allSupportPersonalArray = self.allSupportPersonalArray
                                        PersonalAccountVC.allisLikeArray = self.allisLikeArray
                                        PersonalAccountVC.allisLikesGroupArray = self.allisLikesGroupArray
                                        PersonalAccountVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                    PersonalAccountVC.userID_Registered = self.userID_Registered
                                    PersonalAccountVC.programArray = self.programArray
                                    PersonalAccountVC.navStr = "joinProgram"
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(PersonalAccountVC, animated: true)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "joinProgram"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                //    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                        
                                //    }
                                //    alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                    self.responseArray .removeObject(at: self.selectedRow.row)
                                        let mainViewController = self.sideMenuController!
                                        let ReportuserVC = GroupNewsViewController()
                                        ReportuserVC.postusedID = self.userpostIdstr
                                        ReportuserVC.responsePostArray = self.responseArray
                                        ReportuserVC.selectedRow = self.selectedRow
                                        ReportuserVC.groupIDStr = self.groupIDStr
                                        ReportuserVC.headerNameStr = self.headerNameStr
                                        ReportuserVC.groupImageStr = self.groupImageStr
                                        ReportuserVC.program_idstr = self.program_idstr
                                        ReportuserVC.programArray = self.programArray
                                        ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
                                         ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
                                        ReportuserVC.navStr = "joinProgram"
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(ReportuserVC, animated: true)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "worldViewPersonalreportNav"
                                {
                                    
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                //    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                        
                                 //   }
                                //    alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                    self.responseUserPostsArray .removeObject(at: self.selectedRow.row)
                                        let mainViewController = self.sideMenuController!
                                        let PersonalAccountVC = PersonalAccountViewController()
                                        PersonalAccountVC.postID = self.post_idStr
                                         PersonalAccountVC.programArray = self.programArray
                                        PersonalAccountVC.userNameObjStr = self.userNameObjStr
                                        PersonalAccountVC.headerNameStr = self.headerNameStr
                                        PersonalAccountVC.postusedID = self.userpostIdstr
                                        PersonalAccountVC.groupIDStr = self.groupIDStr
                                         PersonalAccountVC.groupImageStr = self.groupImageStr
                                        PersonalAccountVC.userID_Registered = self.userID_Registered
                                        PersonalAccountVC.program_idstr = self.program_idstr
                                        PersonalAccountVC.programNamestr = self.programNamestr
                                        PersonalAccountVC.WorldViewheaderTitleStr = self.headerTitleStr
                                        PersonalAccountVC.headerTitleStr = self.personalheaderTitleStr
                                        PersonalAccountVC.responsePostArray = self.responseArray
                                        PersonalAccountVC.allcommentCountArray = self.allcommentCountArray
                                        PersonalAccountVC.selectedRow = self.selectedRow
                                        PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
                                        PersonalAccountVC.allSupportArray = self.allSupportArray
                                        PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        PersonalAccountVC.allsupportGroupArray = self.allsupportGroupArray
                                        PersonalAccountVC.allSupportPersonalArray = self.allSupportPersonalArray
                                        PersonalAccountVC.allisLikeArray = self.allisLikeArray
                                        PersonalAccountVC.allisLikesGroupArray = self.allisLikesGroupArray
                                        PersonalAccountVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                         PersonalAccountVC.groupIDStr = self.groupIDStr
                                        PersonalAccountVC.navStr = "WorldViewGroup"
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(PersonalAccountVC, animated: true)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "GroupreportPostNav"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                               //     let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                //    }
                               //     alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        self.responseArray .removeObject(at: self.selectedRow.row)
                                        
                                        let mainViewController = self.sideMenuController!
                                        let ReportuserVC = GroupNewsViewController()
                                        ReportuserVC.postusedID = self.userpostIdstr
                                        ReportuserVC.responsePostArray = self.responseArray
                                        ReportuserVC.selectedRow = self.selectedRow
                                        ReportuserVC.groupIDStr = self.groupIDStr
                                        ReportuserVC.headerNameStr = self.headerNameStr
                                        ReportuserVC.groupImageStr = self.groupImageStr
                                        ReportuserVC.program_idstr = self.program_idstr
                                        ReportuserVC.programArray = self.programArray
                                         ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
                                         ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
                                        ReportuserVC.navStr = "GroupreportPostNav"
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(ReportuserVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr ==  "FeedreportPostNav" {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                 //   let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                 //   }
                                  //  alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        self.responseArray .removeObject(at: self.verticalOffset)
                                        let mainViewController = self.sideMenuController!
                                        let ProgramSwipeVC = ProgramSwipeViewController()
                                        ProgramSwipeVC.navStr = "MyProgramFeedVC"
                                        ProgramSwipeVC.responsePostArray = self.responseArray
                                        ProgramSwipeVC.verticalOffset = self.verticalOffset
                                        ProgramSwipeVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                        ProgramSwipeVC.allsupportFeedArray = self.allsupportFeedArray
                                        ProgramSwipeVC.allisLikesFeedArray = self.allisLikesFeedArray
                                        ProgramSwipeVC.headerTitleStr = self.headerTitleStr
                                        ProgramSwipeVC.program_idstr = self.program_idstr
                                         ProgramSwipeVC.headerTitleStr = self.headerTitleStr
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(ProgramSwipeVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr ==  "WorldViewIndividualVC"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                //    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) //{ (action:UIAlertAction!) in
                               //     }
                                //    alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        
                                        let ReportuserVC = PersonalAccountViewController(nibName: "PersonalAccountViewController", bundle: nil)
                                        ReportuserVC.postusedID = self.userpostIdstr
                                        // ReportuserVC.responsePostArray = self.responseArray
                                        // ReportuserVC.allcommentCountArray = self.allcommentCountArray
                                        ReportuserVC.headerTitleStr = self.personalheaderTitleStr
                                        ReportuserVC.selectedRow = self.selectedRow
                                        ReportuserVC.userID_Registered = self.userID_Registered
                                        ReportuserVC.program_idstr = self.program_idstr
                                        ReportuserVC.programNamestr = self.programNamestr
                                        ReportuserVC.WorldViewheaderTitleStr = self.headerTitleStr
                                        // ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
                                        // ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        ReportuserVC.navStr = "WorldViewIndividualVC"
                                        self.navigationController?.pushViewController(ReportuserVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                    
                                    
                                }
                                if self.navstr == "HomereportPostNav"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                //    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                        
                                 ///   }
                                //    alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        
                                        self.responseArray .removeObject(at: self.verticalOffset)
                                        
                                        let addpostVC = DashBoardViewController(nibName: "DashBoardViewController", bundle: nil)
                                        
                                        addpostVC.navStr = self.navstr
                                        addpostVC.responsePostArray = self.responseArray
                                        addpostVC.allcommentCountArray = self.allcommentCountArray
                                        addpostVC.verticalOffset = self.verticalOffset
                                        addpostVC.allSupportArray = self.allSupportArray
                                        addpostVC.allisLikeArray = self.allisLikeArray
                                        //addpostVC.selectedRow = self.selectedRow
                                        self.navigationController?.pushViewController(addpostVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "personalreportPostNav"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                //    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                 //   }
                                 //   alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        self.responseArray .removeObject(at: self.verticalOffset)
                                        
                                        let addpostVC = PersonalAccountViewController(nibName: "PersonalAccountViewController", bundle: nil)
                                        
                                        // self.responseArray .removeObject(at: self.selectedRow.row)
                                        
                                        addpostVC.navStr = "NewPage"
                                        addpostVC.postusedID = self.userpostIdstr
                                        addpostVC.responsePostArray = self.responseArray
                                        addpostVC.allcommentCountArray = self.allcommentCountArray
                                        addpostVC.responseUserPostsArray = self.responseUserPostsArray
                                        addpostVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        addpostVC.verticalOffsetPostUser = self.verticalOffsetPostUser
                                        addpostVC.verticalOffset = self.verticalOffset
                                        addpostVC.allisLikeArray = self.allisLikeArray
                                        addpostVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                        addpostVC.headerTitleStr = self.headerTitleStr
                                        addpostVC.userNameObjStr = self.userNameObjStr
                                        self.navigationController?.pushViewController(addpostVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "MyProgramPrsnlVC"
                                {
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = PersonalAccountViewController()
                                    CommentsVC.verticalOffsetPostUser =  self.verticalOffsetPostUser
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.navStr = "MyProgramPrsnlVC"
                                    CommentsVC.responsePostArray = self.responseArray
                                    CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                    CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                   // CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.program_idstr = self.program_idstr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 2)
                                
                            {
                                if self.navstr == "dashBoardComment" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "dashBoardComment"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "WorldIndividualCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "WorldViewIndividualVC"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "networkpeopleCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "networkpeople"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "PeoplesNewPageCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "PeoplesNewPage"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "dashboarpersonalprofileComment" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "dashboarpersonalprofileComment"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "worldViewFeedprofileComment" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "worldViewFeedprofileComment"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "WorldViewGroupCommentPF" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "WorldViewGroupCommentPF"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr == "WorldIndividualCMTView" {
                                    let mainViewController = self.sideMenuController!
                                    let DashBoardVC = CommentProfileVC()
                                    DashBoardVC.navStr = "WorldViewIndividualVC"
                                    DashBoardVC.commentUsedID = self.commentUsedID
                                    DashBoardVC.headerStr = self.headerStr
                                    DashBoardVC.program_idstr = self.program_idstr
                                    DashBoardVC.programNamestr = self.programNamestr
                                    DashBoardVC.userID_Registered = self.userID_Registered
                                    DashBoardVC.personalTitleName = self.personalTitleName
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(DashBoardVC, animated: true)
                                }
                                if self.navstr .isEqual(to: "peoplereportPost") {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                  //  let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                        
                                 //   }
                                 //   alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        let predicate = NSPredicate(format:"user_id==%@",self.userpostIdstr)
                                        let arr : NSArray = self.responseArray.filtered(using: predicate) as NSArray
                                        if arr.count > 0
                                        {
                                            for item in arr {
                                                
                                                self.responseArray.remove(item)
                                            }
                                            print(self.responseArray)
                                        }
                                        let addpostVC = PeoplePostsViewController(nibName: "PeoplePostsViewController", bundle: nil)
                                        addpostVC.navStr = self.navstr
                                        addpostVC.responsePostArray = self.responseArray
                                        addpostVC.allsupportPeopleArray = self.allsupportPeopleArray
                                        addpostVC.allisLikesPeopleArray = self.allisLikesPeopleArray
                                        addpostVC.usernameObjstr = self.headerTitleStr
                                         addpostVC.peopleReg_idStr = self.peopleReg_idStr
                                        addpostVC.commentPost_ID = self.commentPost_ID
                                         addpostVC.navStr = "PeoplesNewPage"
                                        //addpostVC.selectedRow = self.selectedRow
                                        self.navigationController?.pushViewController(addpostVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "joinProgramReport" {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                //    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                        
                                //    }
                                //    alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        let predicate = NSPredicate(format:"user_id==%@",self.userpostIdstr)
                                        let arr : NSArray = self.responseUserPostsArray.filtered(using: predicate) as NSArray
                                        if arr.count > 0
                                        {
                                            for item in arr {
                                                self.responseUserPostsArray.remove(item)
                                            }
                                            print(self.responseUserPostsArray)
                                        }
                                        let mainViewController = self.sideMenuController!
                                        let PersonalAccountVC = PersonalAccountViewController()
                                        PersonalAccountVC.postID = self.post_idStr
                                        PersonalAccountVC.postusedID = self.userpostIdstr
                                        PersonalAccountVC.groupIDStr = self.groupIDStr
                                        PersonalAccountVC.groupImageStr = self.groupImageStr
                                        PersonalAccountVC.userID_Registered = self.userID_Registered
                                        PersonalAccountVC.program_idstr = self.program_idstr
                                        PersonalAccountVC.programNamestr = self.programNamestr
                                        PersonalAccountVC.WorldViewheaderTitleStr = self.headerTitleStr
                                        PersonalAccountVC.headerTitleStr = self.personalheaderTitleStr
                                        PersonalAccountVC.responsePostArray = self.responseArray
                                        PersonalAccountVC.allcommentCountArray = self.allcommentCountArray
                                        PersonalAccountVC.selectedRow = self.selectedRow
                                        PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
                                        PersonalAccountVC.programArray = self.programArray
                                        PersonalAccountVC.allSupportArray = self.allSupportArray
                                        PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        PersonalAccountVC.allsupportGroupArray = self.allsupportGroupArray
                                        PersonalAccountVC.allSupportPersonalArray = self.allSupportPersonalArray
                                        PersonalAccountVC.allisLikeArray = self.allisLikeArray
                                        PersonalAccountVC.allisLikesGroupArray = self.allisLikesGroupArray
                                        PersonalAccountVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                        PersonalAccountVC.userID_Registered = self.userID_Registered
                                        PersonalAccountVC.programArray = self.programArray
                                        PersonalAccountVC.navStr = "joinProgram"
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(PersonalAccountVC, animated: true)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "joinProgram"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                               //     let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                        
                               //     }
                               //     alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        let predicate = NSPredicate(format:"user_id==%@",self.userpostIdstr)
                                        let arr : NSArray = self.responseArray.filtered(using: predicate) as NSArray
                                        if arr.count > 0
                                        {
                                            for item in arr {
                                                self.responseArray.remove(item)
                                            }
                                            print(self.responseArray)
                                        }
                                        let mainViewController = self.sideMenuController!
                                        let ReportuserVC = GroupNewsViewController()
                                        ReportuserVC.postusedID = self.userpostIdstr
                                        ReportuserVC.responsePostArray = self.responseArray
                                        ReportuserVC.selectedRow = self.selectedRow
                                        ReportuserVC.groupIDStr = self.groupIDStr
                                        ReportuserVC.headerNameStr = self.headerNameStr
                                        ReportuserVC.groupImageStr = self.groupImageStr
                                        ReportuserVC.program_idstr = self.program_idstr
                                        ReportuserVC.programArray = self.programArray
                                        ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
                                         ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
                                        ReportuserVC.navStr = "joinProgram"
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(ReportuserVC, animated: true)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "worldViewPersonalreportNav"
                                {
                                    
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                               //     let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                        
                               //     }
                               //     alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        let predicate = NSPredicate(format:"user_id==%@",self.userpostIdstr)
                                        let arr : NSArray = self.responseUserPostsArray.filtered(using: predicate) as NSArray
                                        if arr.count > 0
                                        {
                                            for item in arr {
                                                self.responseUserPostsArray.remove(item)
                                            }
                                            print(self.responseUserPostsArray)
                                        }
                                        let mainViewController = self.sideMenuController!
                                        let PersonalAccountVC = PersonalAccountViewController()
                                        PersonalAccountVC.postID = self.post_idStr
                                        PersonalAccountVC.postusedID = self.userpostIdstr
                                        PersonalAccountVC.programArray = self.programArray
                                        PersonalAccountVC.groupImageStr = self.groupImageStr
                                        PersonalAccountVC.userNameObjStr = self.userNameObjStr
                                        PersonalAccountVC.headerNameStr = self.headerNameStr
                                        PersonalAccountVC.groupIDStr = self.groupIDStr
                                        PersonalAccountVC.userID_Registered = self.userID_Registered
                                        PersonalAccountVC.program_idstr = self.program_idstr
                                        PersonalAccountVC.programNamestr = self.programNamestr
                                        PersonalAccountVC.WorldViewheaderTitleStr = self.headerTitleStr
                                        PersonalAccountVC.headerTitleStr = self.personalheaderTitleStr
                                        PersonalAccountVC.responsePostArray = self.responseArray
                                        PersonalAccountVC.allcommentCountArray = self.allcommentCountArray
                                         PersonalAccountVC.groupIDStr = self.groupIDStr
                                        PersonalAccountVC.selectedRow = self.selectedRow
                                        PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
                                        PersonalAccountVC.allSupportArray = self.allSupportArray
                                        
                                        PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        PersonalAccountVC.allisLikeArray = self.allisLikeArray
                                        PersonalAccountVC.allisLikesGroupArray = self.allisLikesGroupArray
                                        PersonalAccountVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                        PersonalAccountVC.allsupportGroupArray = self.allsupportGroupArray
                                        
                                        PersonalAccountVC.allSupportPersonalArray = self.allSupportPersonalArray
                                        PersonalAccountVC.navStr = "WorldViewGroup"
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(PersonalAccountVC, animated: true)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "GroupreportPostNav"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                              //      let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                              //      }
                              //      alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        let predicate = NSPredicate(format:"user_id==%@",self.userpostIdstr)
                                        let arr : NSArray = self.responseArray.filtered(using: predicate) as NSArray
                                        if arr.count > 0
                                        {
                                            for item in arr {
                                                
                                                self.responseArray.remove(item)
                                            }
                                            print(self.responseArray)
                                        }
                                        let mainViewController = self.sideMenuController!
                                        let ReportuserVC = GroupNewsViewController()
                                        ReportuserVC.postusedID = self.userpostIdstr
                                        ReportuserVC.responsePostArray = self.responseArray
                                        ReportuserVC.selectedRow = self.selectedRow
                                        ReportuserVC.groupIDStr = self.groupIDStr
                                        ReportuserVC.headerNameStr = self.headerNameStr
                                        ReportuserVC.groupImageStr = self.groupImageStr
                                        ReportuserVC.program_idstr = self.program_idstr
                                        ReportuserVC.programArray = self.programArray
                                         ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
                                         ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
                                        ReportuserVC.navStr = "GroupreportPostNav"
                                        let navigationController = mainViewController.rootViewController as! NavigationController
                                        navigationController.pushViewController(ReportuserVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr ==  "FeedreportPostNav" {
                                    let predicate = NSPredicate(format:"user_id==%@",self.userpostIdstr)
                                    let arr : NSArray = self.responseArray.filtered(using: predicate) as NSArray
                                    if arr.count > 0
                                    {
                                        for item in arr {
                                            
                                            self.responseArray.remove(item)
                                        }
                                        print(self.responseArray)
                                    }
                                    let mainViewController = self.sideMenuController!
                                    let ProgramSwipeVC = ProgramSwipeViewController()
                                    ProgramSwipeVC.navStr = "MyProgramFeedVC"
                                    ProgramSwipeVC.responsePostArray = self.responseArray
                                    ProgramSwipeVC.verticalOffset = self.verticalOffset
                                    ProgramSwipeVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                    ProgramSwipeVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    ProgramSwipeVC.allsupportFeedArray = self.allsupportFeedArray
                                    ProgramSwipeVC.headerTitleStr = self.headerTitleStr
                                     ProgramSwipeVC.headerTitleStr = self.headerTitleStr
                                    ProgramSwipeVC.program_idstr = self.program_idstr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(ProgramSwipeVC, animated: false)
                                }
                                if self.navstr ==  "WorldViewIndividualVC"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                               //     let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                               //     }
                               //     alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        let predicate = NSPredicate(format:"user_id==%@",self.userpostIdstr)
                                        let arr : NSArray = self.responseUserPostsArray.filtered(using: predicate) as NSArray
                                        if arr.count > 0
                                        {
                                            for item in arr {
                                                
                                                self.responseUserPostsArray.remove(item)
                                            }
                                            print(self.responseUserPostsArray)
                                        }
                                        
                                        let ReportuserVC = PersonalAccountViewController(nibName: "PersonalAccountViewController", bundle: nil)
                                        ReportuserVC.postusedID = self.userpostIdstr
                                        // ReportuserVC.responsePostArray = self.responseArray
                                        // ReportuserVC.allcommentCountArray = self.allcommentCountArray
                                        ReportuserVC.headerTitleStr = self.personalheaderTitleStr
                                        ReportuserVC.selectedRow = self.selectedRow
                                        ReportuserVC.userID_Registered = self.userID_Registered
                                        ReportuserVC.program_idstr = self.program_idstr
                                        ReportuserVC.programNamestr = self.programNamestr
                                        ReportuserVC.WorldViewheaderTitleStr = self.headerTitleStr
                                        // ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
                                        // ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        ReportuserVC.navStr = "WorldViewIndividualVC"
                                        self.navigationController?.pushViewController(ReportuserVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                    
                                    
                                }
                                if self.navstr == "HomereportPostNav"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                //    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                        
                                //    }
                                 //   alertController.addAction(cancelAction)
                                 
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        
                                        let predicate = NSPredicate(format:"user_id==%@",self.userpostIdstr)
                                        let arr : NSArray = self.responseArray.filtered(using: predicate) as NSArray
                                        if arr.count > 0
                                        {
                                            for item in arr {
                                                
                                                self.responseArray.remove(item)
                                            }
                                            print(self.responseArray)
                                        }
                                        let addpostVC = DashBoardViewController(nibName: "DashBoardViewController", bundle: nil)
                                        addpostVC.navStr = "reportPostVC"
                                        addpostVC.responsePostArray = self.responseArray
                                        addpostVC.allcommentCountArray = self.allcommentCountArray
                                        addpostVC.allSupportArray = self.allSupportArray
                                        addpostVC.verticalOffset = self.verticalOffset
                                         addpostVC.allisLikeArray = self.allisLikeArray
                                        //addpostVC.selectedRow = self.selectedRow
                                    self.navigationController?.pushViewController(addpostVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "personalreportPostNav"
                                {
                                    let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                 //   let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                //    }
                                 //   alertController.addAction(cancelAction)
                                    
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        let addpostVC = PersonalAccountViewController(nibName: "PersonalAccountViewController", bundle: nil)
                                        
                                        // self.responseArray .removeObject(at: self.selectedRow.row)
                                        
                                        addpostVC.navStr = "NewPage"
                                        addpostVC.postusedID = self.userpostIdstr
                                        addpostVC.responsePostArray = self.responseArray
                                        addpostVC.allcommentCountArray = self.allcommentCountArray
                                        addpostVC.responseUserPostsArray = self.responseUserPostsArray
                                        addpostVC.allPersonalcommentsArray = self.allPersonalcommentsArray
                                        addpostVC.verticalOffsetPostUser = self.verticalOffsetPostUser
                                        addpostVC.verticalOffset = self.verticalOffset
                                        addpostVC.allisLikeArray = self.allisLikeArray
                                        addpostVC.allisLikesPersonalArray = self.allisLikesPersonalArray
                                        addpostVC.headerTitleStr = self.headerTitleStr
                                        addpostVC.userNameObjStr = self.userNameObjStr
                                        self.navigationController?.pushViewController(addpostVC, animated: false)
                                    }
                                    alertController.addAction(OKAction)
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                if self.navstr == "MyProgramPrsnlVC"
                                {
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = PersonalAccountViewController()
                                    CommentsVC.verticalOffsetPostUser =  self.verticalOffsetPostUser
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.navStr = "MyProgramPrsnlVC"
                                    CommentsVC.responsePostArray = self.responseArray
                                    CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
                                    CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
                                    CommentsVC.allsupportFeedArray = self.allsupportFeedArray
                                    CommentsVC.headerTitleStr = self.headerTitleStr
                                    //CommentsVC.responseUserPostsArray = self.responseUserPostsArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    CommentsVC.program_idstr = self.program_idstr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        if self.navstr == "dashBoardComment" {
            let mainViewController = self.sideMenuController!
            let DashBoardVC = CommentProfileVC()
            DashBoardVC.navStr = "dashBoardComment"
            DashBoardVC.commentUsedID = self.commentUsedID
            DashBoardVC.headerStr = headerStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(DashBoardVC, animated: true)
        }
        if self.navstr == "dashboarpersonalprofileComment" {
            let mainViewController = self.sideMenuController!
            let DashBoardVC = CommentProfileVC()
            DashBoardVC.navStr = "dashboarpersonalprofileComment"
            DashBoardVC.commentUsedID = self.commentUsedID
            DashBoardVC.headerStr = headerStr
            DashBoardVC.userID_Registered = self.userID_Registered
            DashBoardVC.personalTitleName = self.personalTitleName
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(DashBoardVC, animated: true)
        }
        if self.navstr == "worldViewFeedprofileComment" {
            let mainViewController = self.sideMenuController!
            let DashBoardVC = CommentProfileVC()
            DashBoardVC.navStr = "worldViewFeedprofileComment"
            DashBoardVC.commentUsedID = self.commentUsedID
            DashBoardVC.headerStr = headerStr
            DashBoardVC.program_idstr = self.program_idstr
            DashBoardVC.userID_Registered = self.userID_Registered
            DashBoardVC.personalTitleName = self.personalTitleName
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(DashBoardVC, animated: true)
        }
        if self.navstr == "WorldViewGroupCommentPF" {
            let mainViewController = self.sideMenuController!
            let DashBoardVC = CommentProfileVC()
            DashBoardVC.navStr = "WorldViewGroupCommentPF"
            DashBoardVC.commentUsedID = self.commentUsedID
            DashBoardVC.headerStr = headerStr
            DashBoardVC.program_idstr = self.program_idstr
            DashBoardVC.programNamestr = self.programNamestr
            DashBoardVC.userID_Registered = self.userID_Registered
            DashBoardVC.personalTitleName = self.personalTitleName
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(DashBoardVC, animated: true)
        }
        if self.navstr == "WorldIndividualCMTView" {
            let mainViewController = self.sideMenuController!
            let DashBoardVC = CommentProfileVC()
            DashBoardVC.navStr = "WorldViewIndividualVC"
            DashBoardVC.commentUsedID = self.commentUsedID
            DashBoardVC.headerStr = headerStr
            DashBoardVC.program_idstr = self.program_idstr
            DashBoardVC.programNamestr = self.programNamestr
            DashBoardVC.userID_Registered = self.userID_Registered
            DashBoardVC.personalTitleName = self.personalTitleName
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(DashBoardVC, animated: true)
        }
        if self.navstr == "networkpeopleCMTView" {
            let mainViewController = self.sideMenuController!
            let DashBoardVC = CommentProfileVC()
            DashBoardVC.navStr = "networkpeople"
            DashBoardVC.commentUsedID = self.commentUsedID
            DashBoardVC.headerStr = headerStr
            DashBoardVC.program_idstr = self.program_idstr
            DashBoardVC.programNamestr = self.programNamestr
            DashBoardVC.userID_Registered = self.userID_Registered
            DashBoardVC.personalTitleName = self.personalTitleName
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(DashBoardVC, animated: true)
        }
        if self.navstr == "PeoplesNewPageCMTView" {
            let mainViewController = self.sideMenuController!
            let DashBoardVC = CommentProfileVC()
            DashBoardVC.navStr = "PeoplesNewPage"
            DashBoardVC.commentUsedID = self.commentUsedID
            DashBoardVC.headerStr = headerStr
            DashBoardVC.program_idstr = self.program_idstr
            DashBoardVC.programNamestr = self.programNamestr
            DashBoardVC.userID_Registered = self.userID_Registered
            DashBoardVC.personalTitleName = self.personalTitleName
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(DashBoardVC, animated: true)
        }
        if self.navstr == "HomereportPostNav"
        {
            let mainViewController = self.sideMenuController!
            let DashBoardVC = DashBoardViewController()
            DashBoardVC.navStr = self.navstr
            DashBoardVC.responsePostArray = self.responseArray
            DashBoardVC.allSupportArray = allSupportArray
            DashBoardVC.allisLikeArray = self.allisLikeArray
            DashBoardVC.allcommentCountArray = self.allcommentCountArray
            DashBoardVC.verticalOffset = self.verticalOffset
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(DashBoardVC, animated: true)
        }
        if self.navstr .isEqual(to: "peoplereportPost") {
            
            let dashBoardVC = PeoplePostsViewController(nibName: "PeoplePostsViewController", bundle: nil)
            dashBoardVC.navStr = self.navstr
            dashBoardVC.responsePostArray = self.responseArray
              dashBoardVC.allsupportPeopleArray = allsupportPeopleArray
            dashBoardVC.allisLikesPeopleArray = self.allisLikesPeopleArray
            dashBoardVC.usernameObjstr = self.headerTitleStr
            dashBoardVC.selectedRow = self.selectedRow
             dashBoardVC.peopleReg_idStr = self.peopleReg_idStr
             dashBoardVC.navStr = "PeoplesNewPage"
             dashBoardVC.commentPost_ID = self.commentPost_ID
            dashBoardVC.peopleReg_idStr = self.peopleReg_idStr
            self.navigationController?.pushViewController(dashBoardVC, animated: true)
            
        }
        if self.navstr == "MyProgramPrsnlVC"
        {
            let mainViewController = self.sideMenuController!
            let CommentsVC = PersonalAccountViewController()
            CommentsVC.verticalOffsetPostUser =  self.verticalOffsetPostUser
            CommentsVC.verticalOffset = self.verticalOffset
            CommentsVC.navStr = "MyProgramPrsnlVC"
            CommentsVC.responsePostArray = self.responseArray
            CommentsVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            CommentsVC.allisLikesFeedArray = self.allisLikesFeedArray
            CommentsVC.allsupportFeedArray = self.allsupportFeedArray
            CommentsVC.headerTitleStr = self.headerTitleStr
           // CommentsVC.responseUserPostsArray = self.responseUserPostsArray
            CommentsVC.userID_Registered = self.userID_Registered
            CommentsVC.program_idstr = self.program_idstr
            CommentsVC.userNameObjStr = self.userNameObjStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(CommentsVC, animated: true)
        }
        if self.navstr == "GroupreportPostNav"
        {
            let mainViewController = self.sideMenuController!
            let ReportuserVC = GroupNewsViewController()
            ReportuserVC.postusedID = self.userpostIdstr
            ReportuserVC.responsePostArray = self.responseArray
            ReportuserVC.selectedRow = self.selectedRow
            ReportuserVC.groupIDStr = self.groupIDStr
            ReportuserVC.headerNameStr = self.headerNameStr
            ReportuserVC.groupImageStr = self.groupImageStr
            ReportuserVC.program_idstr = self.program_idstr
            ReportuserVC.programArray = self.programArray
            ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
             ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
            ReportuserVC.navStr = "WorldViewGroup"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ReportuserVC, animated: true)
            
        }
        if self.navstr ==  "WorldViewIndividualVC"
        {
            let mainViewController = self.sideMenuController!
            let ReportuserVC = PersonalAccountViewController()
            ReportuserVC.postusedID = self.userpostIdstr
            ReportuserVC.responsePostArray = self.responseArray
            ReportuserVC.selectedRow = self.selectedRow
            ReportuserVC.groupIDStr = self.groupIDStr
            ReportuserVC.headerNameStr = self.headerNameStr
            ReportuserVC.groupImageStr = self.groupImageStr
            ReportuserVC.program_idstr = self.program_idstr
           
            // ReportuserVC.programArray = self.programArray
            ReportuserVC.programNamestr = self.programNamestr
            ReportuserVC.WorldViewheaderTitleStr = self.headerTitleStr
            ReportuserVC.userID_Registered = self.userID_Registered
            ReportuserVC.allSupportPersonalArray = self.allSupportPersonalArray
            ReportuserVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            ReportuserVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            ReportuserVC.responseUserPostsArray = self.responseUserPostsArray
            ReportuserVC.navStr = "WorldViewIndividualVC"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ReportuserVC, animated: true)
            
        }
        if self.navstr ==  "FeedreportPostNav" {
            let mainViewController = self.sideMenuController!
            let ProgramSwipeVC = ProgramSwipeViewController()
            ProgramSwipeVC.navStr = "MyProgramFeedVC"
            ProgramSwipeVC.responsePostArray = self.responseArray
            ProgramSwipeVC.verticalOffset = self.verticalOffset
            ProgramSwipeVC.allisLikesFeedArray = self.allisLikesFeedArray
            ProgramSwipeVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            ProgramSwipeVC.allsupportFeedArray = self.allsupportFeedArray
            ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            ProgramSwipeVC.program_idstr = self.program_idstr
             ProgramSwipeVC.headerTitleStr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ProgramSwipeVC, animated: true)
        }
        if self.navstr == "personalreportPostNav"
        {
            let mainViewController = self.sideMenuController!
            let PersonalAccountVC = PersonalAccountViewController()
            PersonalAccountVC.navStr = "NewPage"
            PersonalAccountVC.postusedID = self.userpostIdstr
            PersonalAccountVC.responsePostArray = self.responseArray
            PersonalAccountVC.allcommentCountArray = self.allcommentCountArray
            PersonalAccountVC.allSupportPersonalArray = self.allSupportPersonalArray
            PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
            PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
              PersonalAccountVC.userNameObjStr = self.userNameObjStr
            PersonalAccountVC.allSupportArray = self.allSupportArray
            PersonalAccountVC.allisLikeArray = self.allisLikeArray
            PersonalAccountVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            PersonalAccountVC.headerTitleStr = self.headerTitleStr
            PersonalAccountVC.verticalOffsetPostUser = self.verticalOffsetPostUser
            PersonalAccountVC.verticalOffset = self.verticalOffset
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(PersonalAccountVC, animated: true)
            
        }
        if self.navstr == "worldViewPersonalreportNav"
        {
            let mainViewController = self.sideMenuController!
            let PersonalAccountVC = PersonalAccountViewController()
            PersonalAccountVC.postID = self.post_idStr
            PersonalAccountVC.postusedID = self.userpostIdstr
            PersonalAccountVC.groupIDStr = self.groupIDStr
            PersonalAccountVC.userNameObjStr = self.userNameObjStr
            PersonalAccountVC.headerNameStr = self.headerNameStr
            PersonalAccountVC.groupImageStr = self.groupImageStr
            PersonalAccountVC.userID_Registered = self.userID_Registered
            PersonalAccountVC.program_idstr = self.program_idstr
            PersonalAccountVC.programNamestr = self.programNamestr
            PersonalAccountVC.WorldViewheaderTitleStr = self.headerTitleStr
            PersonalAccountVC.headerTitleStr = self.personalheaderTitleStr
            PersonalAccountVC.responsePostArray = self.responseArray
            PersonalAccountVC.allcommentCountArray = self.allcommentCountArray
            PersonalAccountVC.selectedRow = self.selectedRow
            PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
            PersonalAccountVC.programArray = self.programArray
            PersonalAccountVC.allSupportArray = self.allSupportArray
            PersonalAccountVC.allisLikeArray = self.allisLikeArray
            PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            PersonalAccountVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            PersonalAccountVC.allsupportGroupArray = self.allsupportGroupArray
            PersonalAccountVC.allisLikesGroupArray = self.allisLikesGroupArray
            PersonalAccountVC.allSupportPersonalArray = self.allSupportPersonalArray
           // PersonalAccountVC.navStr = "WorldViewGroup"
            PersonalAccountVC.navStr = "WorldViewpersonalGroup"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(PersonalAccountVC, animated: true)
        }
        if self.navstr == "joinProgram"
        {
            let mainViewController = self.sideMenuController!
            let ReportuserVC = GroupNewsViewController()
            ReportuserVC.postusedID = self.userpostIdstr
            ReportuserVC.responsePostArray = self.responseArray
            ReportuserVC.selectedRow = self.selectedRow
            ReportuserVC.groupIDStr = self.groupIDStr
            ReportuserVC.headerNameStr = self.headerNameStr
            ReportuserVC.groupImageStr = self.groupImageStr
            ReportuserVC.program_idstr = self.program_idstr
            ReportuserVC.programArray = self.programArray
            ReportuserVC.allsupportGroupArray = self.allsupportGroupArray
             ReportuserVC.allisLikesGroupArray = self.allisLikesGroupArray
            ReportuserVC.navStr = "joinProgram"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ReportuserVC, animated: true)
            
        }
        if self.navstr == "joinProgramReport" {
            let mainViewController = self.sideMenuController!
            let PersonalAccountVC = PersonalAccountViewController()
            PersonalAccountVC.postID = self.post_idStr
            PersonalAccountVC.postusedID = self.userpostIdstr
            PersonalAccountVC.groupIDStr = self.groupIDStr
            PersonalAccountVC.groupImageStr = self.groupImageStr
            PersonalAccountVC.userID_Registered = self.userID_Registered
            PersonalAccountVC.program_idstr = self.program_idstr
            PersonalAccountVC.programNamestr = self.programNamestr
            PersonalAccountVC.WorldViewheaderTitleStr = self.headerTitleStr
            PersonalAccountVC.headerTitleStr = self.personalheaderTitleStr
            PersonalAccountVC.responsePostArray = self.responseArray
            PersonalAccountVC.allcommentCountArray = self.allcommentCountArray
            PersonalAccountVC.selectedRow = self.selectedRow
            PersonalAccountVC.responseUserPostsArray = self.responseUserPostsArray
            PersonalAccountVC.programArray = self.programArray
            PersonalAccountVC.allSupportArray = self.allSupportArray
            PersonalAccountVC.allisLikeArray = self.allisLikeArray
            PersonalAccountVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            PersonalAccountVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            PersonalAccountVC.allsupportGroupArray = self.allsupportGroupArray
            PersonalAccountVC.allisLikesGroupArray = self.allisLikesGroupArray
            PersonalAccountVC.allSupportPersonalArray = self.allSupportPersonalArray
            PersonalAccountVC.userID_Registered = self.userID_Registered
            PersonalAccountVC.programArray = self.programArray
            PersonalAccountVC.navStr = "joinProgram"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(PersonalAccountVC, animated: true)
        }
    }
}
