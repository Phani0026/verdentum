//
//  InviteViewController.swift
//  Verdentum
//
//  Created by Verdentum on 11/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD

class InviteViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var tableViewInviteObj: UITableView!
    
    var inviteArray = NSMutableArray()
    var programArray = NSMutableArray()
    var Invitecell = InviteEmailCell()
    var Programcell = ProgramCell()
    var selectedIndex = NSIndexPath()
    var selectedRadioStr = NSString()
    var programIDstr = NSString()
    var checkSelection = Bool()
    var emailIDStr = NSString()
    var selected = Bool()
    
     //MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selected = true
        self.checkSelection = true
        self.InvitePeopleAPICalling()
        self.selectedRadioStr  = "2"
        self.tableViewInviteObj .register(UINib (nibName: "InviteEmailCell", bundle: nil), forCellReuseIdentifier: "InviteEmailCell")
        self.tableViewInviteObj .register(UINib (nibName: "ProgramCell", bundle: nil), forCellReuseIdentifier: "ProgramCell")
        self.tableViewInviteObj .register(UINib (nibName: "SendInviteCell", bundle: nil), forCellReuseIdentifier: "SendInviteCell")
        self.tableViewInviteObj .register(UINib (nibName: "InviteUsersListCell", bundle: nil), forCellReuseIdentifier: "InviteUsersListCell")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sideMenuBtnTaped(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    //MARK: - InvitePeopleAPICalling
    
    func InvitePeopleAPICalling() {
        if Reachability.isConnectedToNetwork() == true {
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let userRole = UserDefaults.standard.value(forKey: "userRole")
            let params:[String: AnyObject] = [
                "purpose" : "allinvites" as AnyObject,
                "id" : userID as AnyObject,
                "role" : userRole as AnyObject]
            let url = NSURL(string:BaseURL_InvitedAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.inviteArray = (responsejson["invites"] as AnyObject) as! NSMutableArray
                                self.programArray = (responsejson["programs"] as AnyObject) as! NSMutableArray
                                self.tableViewInviteObj.reloadData()
                            }
                            else
                            {
                                self.presentAlertWithTitle(title: "", message:"Please Try again.")
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        if section == 1 {
            return self.programArray.count
        }
        if section == 2 {
            return 1
        }
        if section == 3 {
            return self.inviteArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            Invitecell = tableView .dequeueReusableCell(withIdentifier: "InviteEmailCell") as! InviteEmailCell
            if (selectedIndex as IndexPath == indexPath) {
                Invitecell.checkBoxImageView.image = UIImage (named: "Checkmark Filled")
                if self.programArray.count == 0 {
                    
                }
                else {
                   Programcell.checkBoxImageObj.image = UIImage (named: "")
                }
                
            }
            else
            {
                Invitecell.checkBoxImageView.image = UIImage (named: "")
            }
            Invitecell.emailTextfieldObj.delegate = self
            Invitecell.emailTextfieldObj.tag = indexPath.row
            Invitecell.emailTextfieldObj.addTarget(self, action: #selector(HeadertextFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            return Invitecell
        }
        if indexPath.section == 1
        {
            Programcell = tableView .dequeueReusableCell(withIdentifier: "ProgramCell") as! ProgramCell
            
            Programcell.programLabelObj.text = (self.programArray  .object(at: indexPath.row) as AnyObject).value(forKey: "prog_name")  as? String
            
            Programcell.selectionStyle = UITableViewCellSelectionStyle.none;
            
            if (selectedIndex as IndexPath == indexPath) {
                Programcell.checkBoxImageObj.image = UIImage (named: "Checkmark Filled")
                Invitecell.checkBoxImageView.isHidden = true
            } else {
                Programcell.checkBoxImageObj.image = UIImage (named: "")
            }
            
            return Programcell
        }
        if indexPath.section == 2
        {
            let cell = tableView .dequeueReusableCell(withIdentifier: "SendInviteCell") as! SendInviteCell
            
            cell.sendInviteBtnTapped.tag = indexPath.row
            cell.sendInviteBtnTapped.addTarget(self, action: #selector(self.sendInviteBtnTapped), for: .touchUpInside)
            
            return cell
        }
        if indexPath.section == 3
        {
            let cell = tableView .dequeueReusableCell(withIdentifier: "InviteUsersListCell") as! InviteUsersListCell
            
            cell.emailLabelOBj.text = ((self.inviteArray  .object(at: indexPath.row) as AnyObject) .value(forKey: "email") as AnyObject) as? String
            cell.dateLabelObj.text = ((self.inviteArray .object(at: indexPath.row) as AnyObject) .value(forKey: "invite_date") as AnyObject) as? String
            
            let statuCheckstr =  ((self.inviteArray .object(at: indexPath.row) as AnyObject) .value(forKey: "status") as AnyObject) as! NSString
            
            if statuCheckstr .isEqual(to: "1") {
                cell.statusLabelObj.text = "Invitation Sent"
            }
            if statuCheckstr .isEqual(to: "2") {
                cell.statusLabelObj.text = "Invitation Accepted "
            }
            return cell
        }
        return Invitecell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            self.tableViewInviteObj.deselectRow(at: indexPath, animated: true)
            
            let cell = tableView.cellForRow(at:indexPath) as! InviteEmailCell
            
            cell.checkBoxImageView.isHidden = true
            Invitecell.checkBoxImageView.isHidden = false
            selectedIndex = indexPath as NSIndexPath
            self.selectedRadioStr  = "0"
            self.tableViewInviteObj.reloadData()
        }
        if indexPath.section == 1 {
            self.tableViewInviteObj.deselectRow(at: indexPath, animated: true)
            Programcell.checkBoxImageObj.isHidden = false
            Invitecell.checkBoxImageView.isHidden = true
            let row = indexPath.row
            self.programIDstr = (((self.programArray[row] as AnyObject) .value(forKey: "prog_id") as AnyObject) as? NSString)!
            selectedIndex = indexPath as NSIndexPath
            self.selectedRadioStr  = "1"
            self.tableViewInviteObj.reloadData()
        }
        if indexPath.section == 2 {
            
        }
        if indexPath.section == 3 {
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 121
        }
        if indexPath.section == 1 {
            return 44
        }
        if indexPath.section == 2 {
            return 117
        }
        if indexPath.section == 3 {
            return 40
        }
        return 0
        
    }
    
    func HeadertextFieldDidChange(_ textField:UITextField) {
        
        self.emailIDStr = textField.text! as NSString
    }
    
    func emailValidation(_ checkString: String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: checkString)
        return result
    }
    
    func sendInviteBtnTapped(_ sender: UIButton) {
        
         if emailValidation(Invitecell.emailTextfieldObj.text!) {
            if (Invitecell.emailTextfieldObj.text?.isEmpty)!  {
                self.presentAlertWithTitle(title: "", message:"Please Enter Email Id.")
            }
            else {
                if self.selectedRadioStr .isEqual(to: "2") {
                    self.presentAlertWithTitle(title: "", message:"Please Select Join A Program or Join Verdentum")
                }
                else {
                    if self.selectedRadioStr .isEqual(to: "0") {
                        
                        let programstr : NSString = "0"
                        let userID = UserDefaults.standard.value(forKey: "userID")
                        let userRole = UserDefaults.standard.value(forKey: "userRole")
                        let params:[String: AnyObject] = [
                            "purpose" : "invite" as AnyObject,
                            "id" : userID as AnyObject,
                            "role" : userRole as AnyObject,
                            "email" : self.emailIDStr as AnyObject,
                            "program" : programstr as AnyObject]
                        MBProgressHUD.showAdded(to: self.view, animated: true)
                        self.InviteApiCalling(param: params)
                    }
                    if self.selectedRadioStr .isEqual(to: "1")
                    {
                        let userID = UserDefaults.standard.value(forKey: "userID")
                        let userRole = UserDefaults.standard.value(forKey: "userRole")
                        let params:[String: AnyObject] = [
                            "purpose" : "invite" as AnyObject,
                            "id" : userID as AnyObject,
                            "role" : userRole as AnyObject,
                            "email" : self.emailIDStr as AnyObject,
                            "program" :  self.programIDstr as AnyObject]
                        MBProgressHUD.showAdded(to: self.view, animated: true)
                        self.InviteApiCalling(param: params)
                    }
                }
            }
         }
         else
         {
           
            self.presentAlertWithTitle(title:"" , message:"You have entered an Iinvalid email-id")
        }
        
      
    }
    
    //MARK: - InviteApiCalling
    
    func InviteApiCalling(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
            
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_InvitedPeopleAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            self.presentAlertWithTitle(title: "", message:(responsejson["msg"] as AnyObject) as! String)
                            self.InvitePeopleAPICalling()
                            
                            self.Invitecell.emailTextfieldObj.text = ""
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
}
