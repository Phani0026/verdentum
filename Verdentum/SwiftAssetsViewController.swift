//
//  SwiftAssetsViewController.swift
//  Verdentum
//
//  Created by Verdentum on 13/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import Photos

class SwiftAssetsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PHPhotoLibraryChangeObserver {
    
    
    @IBOutlet var tableViewAssetPicker: UITableView!
    
    enum AlbumType: Int {
        case allPhotos
        case favorites
        case panoramas
        case videos
        case timeLapse
        case recentlyDeleted
        case userAlbum
        
        static let titles = ["All Photos", "Favorites", "Panoramas", "Videos", "Time Lapse", "Recently Deleted", "User Album"]
    }
    
    struct RootListItem {
        var title: String!
        var albumType: AlbumType
        var image: UIImage!
        var collection: PHAssetCollection?
    }
    
    var items: Array<RootListItem>!
    let thumbnailSize = CGSize(width: 64, height: 64)
    let reuseIdentifier = "RootListAssetsCell"
    
    var didSelectAssets: ((Array<PHAsset?>) -> ())?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.isNavigationBarHidden = true
        self.tableViewAssetPicker .register(UINib (nibName: "AssetCell", bundle: nil), forCellReuseIdentifier: "AssetCell")
        
        // Navigation bar
        navigationItem.title = NSLocalizedString("Photos", comment: "")
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(AssetsPickerController.cancelAction))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: UIBarButtonItemStyle.done, target: self, action: #selector(AssetsPickerController.doneAction))
        navigationItem.rightBarButtonItem?.isEnabled = false
        

        
        // Data
        items = Array()
        
        // Notifications
        PHPhotoLibrary.shared().register(self)
        
        // Load photo library
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    // MARK: Data loading
    
    func loadData()
    {
        self.tableViewAssetPicker.isUserInteractionEnabled = false
        
        DispatchQueue.global(qos: .default).async {
            
            self.items.removeAll(keepingCapacity: false)
            
            let allPhotosItem = RootListItem(title: AlbumType.titles[AlbumType.allPhotos.rawValue], albumType: AlbumType.allPhotos, image: self.lastImageFromCollection(nil), collection: nil)
            let assetsCount = self.assetsCountFromCollection(nil)
            if assetsCount > 0 {
                self.items.append(allPhotosItem)
            }
            
            let smartAlbums = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.smartAlbum, subtype: PHAssetCollectionSubtype.albumRegular, options: nil)
            for i: Int in 0 ..< smartAlbums.count {
                let smartAlbum = smartAlbums[i]
                var item: RootListItem? = nil
                
                let assetsCount = self.assetsCountFromCollection(smartAlbum)
                if assetsCount == 0 {
                    continue
                }
                
                switch smartAlbum.assetCollectionSubtype {
                case .smartAlbumFavorites:
                    item = RootListItem(title: AlbumType.titles[AlbumType.favorites.rawValue], albumType: AlbumType.favorites, image: self.lastImageFromCollection(smartAlbum), collection: smartAlbum)
                    break
                case .smartAlbumPanoramas:
                    item = RootListItem(title: AlbumType.titles[AlbumType.panoramas.rawValue], albumType: AlbumType.panoramas, image: self.lastImageFromCollection(smartAlbum), collection: smartAlbum)
                    break
                case .smartAlbumVideos:
                    item = RootListItem(title: AlbumType.titles[AlbumType.videos.rawValue], albumType: AlbumType.videos, image: self.lastImageFromCollection(smartAlbum), collection: smartAlbum)
                    break
                case .smartAlbumTimelapses:
                    item = RootListItem(title: AlbumType.titles[AlbumType.timeLapse.rawValue], albumType: AlbumType.timeLapse, image: self.lastImageFromCollection(smartAlbum), collection: smartAlbum)
                    break
                    
                default:
                    break
                }
                
                if item != nil {
                    self.items.append(item!)
                }
            }
            
            let topLevelUserCollections = PHCollectionList.fetchTopLevelUserCollections(with: nil)
            for i: Int in 0 ..< topLevelUserCollections.count {
                if let userCollection = topLevelUserCollections[i] as? PHAssetCollection {
                    let assetsCount = self.assetsCountFromCollection(userCollection)
                    if assetsCount == 0 {
                        continue
                    }
                    let item = RootListItem(title: userCollection.localizedTitle, albumType: AlbumType.userAlbum, image: self.lastImageFromCollection(userCollection), collection: userCollection)
                    self.items.append(item)
                }
            }
            
            DispatchQueue.main.async {
                self.tableViewAssetPicker.reloadData()
                self.tableViewAssetPicker.isUserInteractionEnabled = true
            }
        }
    }
    
    
    func cancelAction() {
        dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "AssetCell") as! AssetCell
        
        cell.imageViewObj?.image = items[(indexPath as NSIndexPath).row].image
        cell.nameLabelObj?.text = NSLocalizedString(items[(indexPath as NSIndexPath).row].title, comment: "")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let assetsGrid = AssetsPickerGridController(collectionViewLayout: UICollectionViewLayout())
        
        
        assetsGrid.collection = items[(indexPath as NSIndexPath).row].collection
        assetsGrid.didSelectAssets = didSelectAssets
        assetsGrid.title = items[(indexPath as NSIndexPath).row].title
        navigationController?.pushViewController(assetsGrid, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 100
    }
    
    // MARK: PHPhotoLibraryChangeObserver
    
    open func photoLibraryDidChange(_ changeInstance: PHChange) {
        loadData()
    }
    
    // MARK: Other
    
    func assetsCountFromCollection(_ collection: PHAssetCollection?) -> Int {
        let fetchResult = (collection == nil) ? PHAsset.fetchAssets(with: .image, options: nil) : PHAsset.fetchAssets(in: collection!, options: nil)
        return fetchResult.count
    }
    
    func lastImageFromCollection(_ collection: PHAssetCollection?) -> UIImage? {
        
        var returnImage: UIImage? = nil
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        
        let fetchResult = (collection == nil) ? PHAsset.fetchAssets(with: .image, options: fetchOptions) : PHAsset.fetchAssets(in: collection!, options: fetchOptions)
        if let lastAsset = fetchResult.lastObject {
            
            let imageRequestOptions = PHImageRequestOptions()
            imageRequestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.fastFormat
            imageRequestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
            imageRequestOptions.isSynchronous = true
            
            let retinaScale = UIScreen.main.scale
            let retinaSquare = CGSize(width: thumbnailSize.width * retinaScale, height: thumbnailSize.height * retinaScale)
            
            let cropSideLength = min(lastAsset.pixelWidth, lastAsset.pixelHeight)
            let square = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(cropSideLength), height: CGFloat(cropSideLength))
            let cropRect = square.applying(CGAffineTransform(scaleX: 1.0 / CGFloat(lastAsset.pixelWidth), y: 1.0 / CGFloat(lastAsset.pixelHeight)))
            
            imageRequestOptions.normalizedCropRect = cropRect
            
            PHImageManager.default().requestImage(for: lastAsset, targetSize: retinaSquare, contentMode: PHImageContentMode.aspectFit, options: imageRequestOptions, resultHandler: { (image: UIImage?, info :[AnyHashable: Any]?) -> Void in
                returnImage = image
            })
        }
        
        return returnImage
    }
    
    
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnTapped(_ sender: Any) {
    }
    
}
