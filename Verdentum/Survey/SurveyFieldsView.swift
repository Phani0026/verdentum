
//
//  SurveyFieldsView.swift
//  Verdentum
//
//  Created by Verdentum on 21/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD
import DropDown
import Photos
import CoreLocation
import FMDB
import Alamofire


class SurveyFieldsView: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var selectedIndex = NSIndexPath()
    var rootListAssets = SwiftAssetsViewController()
    var textcell = TextCell()
  
    @IBOutlet var minLabelHeight: NSLayoutConstraint!
    @IBOutlet var maxLabelHeight: NSLayoutConstraint!
    @IBOutlet var maxLabelObj: UILabel!
    @IBOutlet var minLabelObj: UILabel!
    @IBOutlet var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet var collectionViewObj: UICollectionView!
    @IBOutlet var uploadImageView: UIView!
    @IBOutlet var locationLabelObj: UILabel!
    @IBOutlet var locationViewObj: UIView!
    @IBOutlet var topViewTitle: UIView!
    
    @IBOutlet var tilteNameOBj: UILabel!
    @IBOutlet var BottomView: UIView!
    //@IBOutlet var tableViewPopObj: UITableView!
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    @IBOutlet var popViewObj: UIView!
    @IBOutlet var tableViewSurveyDetailsObj: UITableView!
    
    @IBOutlet var choosepicBtn: UIButton!
    @IBOutlet var takepicBtn: UIButton!
    
    var isTrue = Bool()
     var isTextTrue = Bool()
    var isTextAreaTrue = Bool()
    var survey_ID = NSString()
    var responseSurveyFieldsArray = NSArray()
    var checkTypeArray = NSMutableArray()
    var multipleArray = NSMutableArray()
    var form_IDArray = NSMutableArray()
    var optionArray = NSMutableArray()
    var requiredArray = NSMutableArray()
    var subTypeArray = NSMutableArray()
    var selectedString: String?
    let textType = "text"
    let selectType = "select"
    let numberType = "number"
    let radiogroupType = "radio-group"
    let textareaType = "textarea"
    let checkboxgroupType = "checkbox-group"
    let dateType = "date"
    var multipleTrueorNot = "true"
    var index = Int()
    var radioArray = NSMutableArray()
    var selectedArray = NSMutableArray()
    var checkBoxArray = NSMutableArray()
    var duplicatedArray : [String:[Int]] = [:]
    var checkCellStatus = NSString()

    // Cells
    
    var SelectPIckercell = SelectPIckerCell()
    var datecell = DateCell()
    var radiocell = RadioGroupCell()
    var checkboxcell = CheckBoxGroupCell()
    var numbercell = NumberCell()
    var textareacell = TextAreaCell()
    
    
    var dataArray = [String]()
    var selectString = NSString()
    var titleStr = NSString()
    var starMarkStr : String?
    var trueOrNOt: String?
    var surveyDict =  [String : AnyObject]()
    var surveyNO =  [String : AnyObject]()
    var fieldID_Array = NSMutableArray()
    var base64Images = [AnyObject]()
    var datePickerHidden = false    
    var minValueStr: String?
    var maxValueStr: String?
    var locationCheck: String?
    var surveyArray = NSDictionary()
    var imagePickerctr: UIImagePickerController?
    var imageArray = NSMutableArray()
    var profilepicImageStr = NSString()
    var galleryImages = [UIImage]()
    var camImageArray = [UIImage]()
    var radioValueDic =  [String : AnyObject]()
    var radioSubValueDic =  [String : AnyObject]()
    var surveyDic =  [String : AnyObject]()
    var checkboxSelectedArray = NSMutableArray()
    var SelectedboxSelectedArray = NSMutableArray()
    var checkboxDataDic = [String : AnyObject]()
    var SelectedboxDataDic = [String : AnyObject]()
    var SelectedRadioDataDic = [String : AnyObject]()
    var locationArray = [String : AnyObject]()
    var params = [String : AnyObject]()
    var addressStr = NSString()
    var stateStr = NSString()
    var countryStr = NSString()
    var continentStr = NSString()
    var textFillOrNot = NSString()
     var NoFillOrNot = NSString()
     var textAreaFillOrNot = NSString()
     var checkFillOrNot = NSString()
    var currentSelectedIndex = NSInteger()
    var textfieldObjData = NSString()
     var databasePath = String()
    //var checkboxDataDic = NSMutableDictionary()
    var mutlipleCheckORNot: String?
     let net = NetworkReachabilityManager()
    //MARK: - DropDown's
    
    let chooseArticleDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown,
            
            ]
    }()
    
    //MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // self.currentSelectedIndex = -1
        // homeServiceApiCalling
        
        self.createDatabase()
        
         self.isTrue = false
        self.isTextTrue = false
        self.isTextAreaTrue = false
        self.homeServiceApiCalling()
        
        self.locationLabelObj.layer.borderColor = UIColor.black.cgColor
        self.locationLabelObj.layer.borderWidth = 1
        self.starMarkStr = nil
        self.trueOrNOt = nil
        self.tilteNameOBj.text = self.titleStr as String
        self.tableViewSurveyDetailsObj.tableHeaderView = self.topViewTitle
        self.mutlipleCheckORNot = nil
        self.locationCheck = nil
        self.minValueStr = nil
        self.maxValueStr = nil
        self.locationCheck = self.surveyArray .value(forKey: "location") as? String
        self.minValueStr = self.surveyArray .value(forKey: "pic_min") as? String
        self.maxValueStr = self.surveyArray .value(forKey: "pic_max") as? String
        
        if self.locationCheck != nil
        {
            if (locationCheck? .isEqual("true"))! {
                
                // Ask for Authorisation from the User.
                self.locationManager.requestAlwaysAuthorization()
                
                // For use in foreground
                self.locationManager.requestWhenInUseAuthorization()
                
                if CLLocationManager.locationServicesEnabled() {
                    locationManager.delegate = self
                    locationManager.desiredAccuracy = kCLLocationAccuracyBest
                    locationManager.requestWhenInUseAuthorization()
                    locationManager.startUpdatingLocation()
                }
                else
                    
                {
                    let alertController = UIAlertController(title: "Location Accees Requested", message: "The location permission was not authorized. Please enable it in Settings to continue.",
                                                            preferredStyle: .alert)
                    
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
                        
                        // THIS IS WHERE THE MAGIC HAPPENS!!!!
                        if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/org.verdentum.verdentum") {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                    }
                    alertController.addAction(settingsAction)
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    
                    present(alertController, animated: true, completion: nil)
                    
                    
                }
            }
        }
        
        if self.locationCheck != nil && self.minValueStr != nil && self.maxValueStr != nil {
            if minValueStr! .isEqual("1") && maxValueStr! .isEqual("2") && locationCheck! .isEqual("true")
            {
                self.tableViewSurveyDetailsObj.tableFooterView = self.uploadImageView
            }
        }
        else
        {
            if self.locationCheck != nil
            {
                if (locationCheck? .isEqual("true"))! {
                    self.tableViewSurveyDetailsObj.tableFooterView = self.locationViewObj
                }
            }
            else
            {
                self.tableViewSurveyDetailsObj.tableFooterView = self.BottomView
            }
        }
        self.takepicBtn.layer.borderWidth = 1
        self.takepicBtn.layer.borderColor = UIColor.lightGray.cgColor
        self.choosepicBtn.layer.borderWidth = 1
        self.choosepicBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        self.collectionViewObj.register(UINib(nibName: "AddPostCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddPostCollectionCell")
        
        // HeaderFooterViewReuseIdentifier
        self.tableViewSurveyDetailsObj.register(UINib(nibName: "SurveyHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "SurveyHeader")
        self.tableViewSurveyDetailsObj.register(UINib(nibName: "FooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "FooterView")
        //CellReuseIdentifier
        
        self.tableViewSurveyDetailsObj .register(UINib (nibName: "TextCell", bundle: nil), forCellReuseIdentifier: "TextCell")
        self.tableViewSurveyDetailsObj .register(UINib (nibName: "RadioGroupCell", bundle: nil), forCellReuseIdentifier: "RadioGroupCell")
        self.tableViewSurveyDetailsObj .register(UINib (nibName: "NumberCell", bundle: nil), forCellReuseIdentifier: "NumberCell")
        self.tableViewSurveyDetailsObj .register(UINib (nibName: "SelectPIckerCell", bundle: nil), forCellReuseIdentifier: "SelectPIckerCell")
        self.tableViewSurveyDetailsObj .register(UINib (nibName: "TextAreaCell", bundle: nil), forCellReuseIdentifier: "TextAreaCell")
        self.tableViewSurveyDetailsObj .register(UINib (nibName: "CheckBoxGroupCell", bundle: nil), forCellReuseIdentifier: "CheckBoxGroupCell")
        self.tableViewSurveyDetailsObj .register(UINib (nibName: "DateCell", bundle: nil), forCellReuseIdentifier: "DateCell")
        self.tableViewSurveyDetailsObj.sectionHeaderHeight = UITableViewAutomaticDimension
        self.tableViewSurveyDetailsObj.estimatedSectionHeaderHeight = 40
        self.tableViewSurveyDetailsObj.keyboardDismissMode = .interactive
    }
    
     //MARK: - Create Sqlite Database
    
    func createDatabase()  {
        
        let filemgr = FileManager.default
        let dirPaths = filemgr.urls(for: .documentDirectory,
                                    in: .userDomainMask)
        
        databasePath = dirPaths[0].appendingPathComponent("survey.db").path
        print(self.databasePath)
        if !filemgr.fileExists(atPath: databasePath as String) {
            
            let contactDB = FMDatabase(path: databasePath as String)
            
            if contactDB == nil {
                print("Error: \(contactDB.lastErrorMessage())")
            }
            
            if (contactDB.open()) {
                let sql_stmt = "CREATE TABLE IF NOT EXISTS SURVEY (ID INTEGER PRIMARY KEY AUTOINCREMENT, SURVEY TEXT)"
                if !(contactDB.executeStatements(sql_stmt)) {
                    print("Error: \(contactDB.lastErrorMessage())")
                }
                contactDB.close()
            } else {
                print("Error: \(contactDB.lastErrorMessage())")
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - viewDidAppear
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
     // MARK: - viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.networkChanged()
    }
    
    
    func networkChanged() {
        
        net?.startListening()
        
        net?.listener = { status in
            if self.net?.isReachable ?? false {
                if (self.net?.isReachableOnEthernetOrWiFi) != nil {
                    
                } else if (self.net?.isReachableOnWWAN)! {
                    
                }
                print("Reachable")
                
                let contactDB = FMDatabase(path: self.databasePath as String)
                if (contactDB.open()) {
                    let querySQL = "SELECT * FROM SURVEY "
                    let results:FMResultSet? = contactDB.executeQuery(querySQL,
                                                                      withArgumentsIn: [])
                    if results?.next() == true {
                        
                         let surveyStr = results?.string(forColumn: "survey")
                        print(surveyStr!)
                        
                    } else {
                    }
                    contactDB.close()
                } else {
                    print("Error: \(contactDB.lastErrorMessage())")
                }
                
            } else {
                
                print("no connection")
            }
        }
    }
    
    
    func hideKeyboard() {
        self.tableViewSurveyDetailsObj.endEditing(true)
    }
   
    //MARK: - HomeServiceApiCalling
    
    func homeServiceApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
         
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "getfields" as AnyObject,
                "id" : userID as AnyObject,
                "formid" : survey_ID as AnyObject]
            let url = NSURL(string:BaseURL_SurveyFieldsAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                
                                self.checkTypeArray.removeAllObjects()
                                self.multipleArray.removeAllObjects()
                                self.fieldID_Array.removeAllObjects()
                                self.form_IDArray.removeAllObjects()
                                self.optionArray.removeAllObjects()
                                self.requiredArray.removeAllObjects()
                                self.subTypeArray.removeAllObjects()
                                self.responseSurveyFieldsArray = (responsejson["fields"] as AnyObject) as! NSArray as! NSMutableArray
                            self.surveyArray.setValue( self.responseSurveyFieldsArray, forKey: "fields")
                                
                                for var i in 0..<self.responseSurveyFieldsArray.count {
                                    self.checkTypeArray.add(((self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "type") as AnyObject))
                                    
                                    self.multipleArray.add(((self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "multiple") as AnyObject))
                                    
                                    self.fieldID_Array.add(((self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "field_id") as AnyObject))
                                    
                                    self.form_IDArray.add(((self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "form_id") as AnyObject))
                                    self.optionArray.add(((self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "options") as AnyObject))
                                    self.requiredArray.add(((self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "required") as AnyObject))
                                    self.subTypeArray.add(((self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "subtype") as AnyObject))
                                }
                               // self.viewWillAppear(true)
                               // print(self.optionArray)
                                self.tableViewSurveyDetailsObj.reloadData()
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.responseSurveyFieldsArray.count 
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.selectedString = self.checkTypeArray .object(at: section) as? String
        
        
        // print(self.selectedString!)
        if (selectedString?.isEqual(textType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textType)
                {
                    self.index = i
                    if section == self.index
                    {
                        return 1
                    }
                }
            }
        }
        if (selectedString?.isEqual(numberType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(numberType)
                {
                    self.index = i
                    if section == self.index
                    {
                        return 1
                    }
                }
            }
        }
        if (selectedString?.isEqual(textareaType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textareaType)
                {
                    self.index = i
                    if section == self.index
                    {
                        return 1
                    }
                }
            }
        }
        if (selectedString?.isEqual(radiogroupType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(radiogroupType)
                {
                    self.index = i
                    if section == self.index
                    {
                        return (self.optionArray[self.index] as AnyObject).count
                    }
                }
            }
        }
        if (selectedString?.isEqual(selectType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(selectType)
                {
                    self.index = i
                    for k in 0..<self.multipleArray.count {
                        trueOrNOt = self.multipleArray[k] as? String
                        if trueOrNOt != nil
                        {
                            if (trueOrNOt? .isEqual(multipleTrueorNot))!
                            {
                                if section == self.index
                                {
                                    return (self.optionArray[self.index] as AnyObject).count
                                }
                            }
                        }
                        else
                        {
                            if section == self.index
                            {
                                return 1
                            }
                        }
                    }
                }
            }
        }
        if (selectedString?.isEqual(checkboxgroupType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(checkboxgroupType)
                {
                    self.index = i
                    if section == self.index
                    {
                        return (self.optionArray[self.index] as AnyObject).count
                    }
                }
            }
        }
        if (selectedString?.isEqual(dateType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(dateType)
                {
                    self.index = i
                    if section == self.index
                    {
                        return 1
                    }
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.selectedString = self.checkTypeArray .object(at: indexPath.section) as? String
        //print(self.selectedString!)
        if (selectedString?.isEqual(textType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        textcell = tableView .dequeueReusableCell(withIdentifier: "TextCell") as! TextCell
                        textcell.textfieldOBj.delegate = self
                        textcell.textfieldOBj.tag = indexPath.section
                        
                        if isTextTrue == false {
                            textcell.textfieldOBj.text = ""
                        }
                        
                        return textcell
                    }
                }
            }
        }
        if (selectedString?.isEqual(numberType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(numberType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        numbercell = tableView .dequeueReusableCell(withIdentifier: "NumberCell") as! NumberCell
                        numbercell .textfieldNoObj.delegate = self
                        numbercell .textfieldNoObj.tag = indexPath.section
                        if isTrue == false {
                             numbercell .textfieldNoObj.text = ""
                        }
                        return numbercell
                    }
                }
            }
        }
        if (selectedString?.isEqual(textareaType))! {
            
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textareaType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        textareacell = tableView .dequeueReusableCell(withIdentifier: "TextAreaCell") as! TextAreaCell
                        textareacell.textViewObj.delegate = self
                         textareacell.textViewObj.tag = indexPath.section
                        textareacell.textViewObj.layer.borderWidth = 1
                        textareacell.textViewObj.layer.borderColor = UIColor.lightGray.cgColor
                        if isTextAreaTrue == false {
                            textareacell.textViewObj.text = ""
                        }
                        return textareacell
                    }
                }
            }
        }
        if (selectedString?.isEqual(radiogroupType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(radiogroupType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        radiocell = tableView .dequeueReusableCell(withIdentifier: "RadioGroupCell") as! RadioGroupCell
                        
                        radiocell.nameLabelObj.text = (((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "label") as AnyObject)  as? String
                        
                        let optionssArray = optionArray[indexPath.section] as! NSArray
                        let dictionary = optionssArray[indexPath.row] as! NSDictionary
                        print(dictionary)
                        if dictionary["selected"] is NSNull {
                            radiocell.radioImageViewObj.image = UIImage (named: "")
                        }else {
                            if dictionary["selected"] as! String  == "true" {
                                radiocell.radioImageViewObj.image = UIImage (named: "Checkmark Filled")
                                
                                self.radioSubValueDic.removeAll()
                            self.radioSubValueDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "multi_id") as AnyObject), forKey: "id")
                            self.radioSubValueDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "value") as AnyObject), forKey: "value")
                                
                                self.surveyDict .updateValue(self.radioSubValueDic as AnyObject, forKey: self.fieldID_Array .object(at: self.index) as! String)
                                
                               // print(self.surveyDict)
                                
                            }else {
                                radiocell.radioImageViewObj.image = UIImage (named: "")
                            }
                        }
                        
                        return radiocell
                    }
                }
            }
        }
        if (selectedString?.isEqual(selectType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(selectType)
                {
                    self.index = i
                    
                    for k in 0..<self.multipleArray.count {
                        trueOrNOt = self.multipleArray[k] as? String
                        if trueOrNOt != nil
                        {
                            if (trueOrNOt? .isEqual(multipleTrueorNot))!
                            {
                                if indexPath.section == self.index
                                {
                                    SelectPIckercell = tableView .dequeueReusableCell(withIdentifier: "SelectPIckerCell") as! SelectPIckerCell
                                    
                                    SelectPIckercell.nameLabelObj.text = (((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "label") as AnyObject)  as? String
                                    
                                    /* if indexPath.row == 0
                                     {
                                     SelectPIckercell.checkBoxImageObj.image = UIImage(named: "Checkbox Filled")
                                     self.radioSubValueDic.removeAll()
                                     self.radioSubValueDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: 0) as AnyObject) .value(forKey: "multi_id") as AnyObject), forKey: "id")
                                     self.radioSubValueDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: 0) as AnyObject) .value(forKey: "value") as AnyObject), forKey: "value")
                                     
                                     self.surveyDict .updateValue(self.radioSubValueDic as AnyObject, forKey: self.fieldID_Array .object(at: self.index) as! String)
                                     print(self.surveyDict)
                                     }else {
                                     SelectPIckercell.checkBoxImageObj.image = UIImage (named: "unchecked")
                                     }*/
                                    
                                    
                                    SelectPIckercell.nameLabelObj.isHidden = false
                                    SelectPIckercell.checkBoxImageObj.isHidden = false
                                    SelectPIckercell.selectedBtn.isHidden = true
                                    SelectPIckercell.textFieldSelectedObj.isHidden = true
                                    SelectPIckercell.imageviewObj.isHidden = true
                                    SelectPIckercell.contentView.backgroundColor = UIColor.white
                                    return SelectPIckercell
                                }
                            }
                        }
                        else
                        {
                            if indexPath.section == self.index
                            {
                                SelectPIckercell = tableView .dequeueReusableCell(withIdentifier: "SelectPIckerCell") as! SelectPIckerCell
                                SelectPIckercell.nameLabelObj.isHidden = true
                                SelectPIckercell.checkBoxImageObj.isHidden = true
                                SelectPIckercell.selectedBtn.isHidden = false
                                SelectPIckercell.imageviewObj.isHidden = false
                                SelectPIckercell.selectedBtn.tag = indexPath.section
                                SelectPIckercell.textFieldSelectedObj.isHidden = true
                                SelectPIckercell.selectedBtn.addTarget(self, action: #selector(self.selectedBtnText), for: .touchUpInside)
                                return SelectPIckercell
                            }
                        }
                    }
                }
            }
        }
        if (selectedString?.isEqual(checkboxgroupType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(checkboxgroupType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        checkboxcell = tableView .dequeueReusableCell(withIdentifier: "CheckBoxGroupCell") as! CheckBoxGroupCell
                        checkboxcell.nameLabelOBj.text = (((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "label") as AnyObject)  as? String
                        
                        
                        
                        let optionssArray = optionArray[indexPath.section] as! NSArray
                        let dictionary = optionssArray[indexPath.row] as! NSDictionary
                        print(dictionary)
                        if dictionary["selected"] is NSNull {
                            checkboxcell.checkBoxImageObj.image = UIImage (named: "unchecked")
                        }else {
                            if dictionary["selected"] as! String  == "true" {
                                checkboxcell.checkBoxImageObj.image = UIImage (named: "Checkbox Filled")
                                
                                self.checkboxDataDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "multi_id") as AnyObject), forKey: "id")
                                self.checkboxDataDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "value") as AnyObject), forKey: "value")
                                self.checkboxSelectedArray.add(self.checkboxDataDic)
                                self.surveyDict .updateValue(self.checkboxSelectedArray, forKey: ((self.fieldID_Array .object(at: indexPath.section) as AnyObject) as! String))
                                
                                
                                 print(self.surveyDict)
                                
                            }else {
                                checkboxcell.checkBoxImageObj.image = UIImage (named: "unchecked")
                            }
                        }
                        
                        
                        
                        
                        
                        
                        /* if indexPath.row == 0
                         {
                         checkboxcell.checkBoxImageObj.image = UIImage(named: "Checkbox Filled")
                         self.radioSubValueDic.removeAll()
                         self.radioSubValueDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: 0) as AnyObject) .value(forKey: "multi_id") as AnyObject), forKey: "id")
                         self.radioSubValueDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: 0) as AnyObject) .value(forKey: "value") as AnyObject), forKey: "value")
                         
                         self.surveyDict .updateValue(self.radioSubValueDic as AnyObject, forKey: self.fieldID_Array .object(at: self.index) as! String)
                         print(self.surveyDict)
                         }else {
                         checkboxcell.checkBoxImageObj.image = UIImage (named: "unchecked")
                         }*/
                        return checkboxcell
                    }
                }
            }
        }
        if (selectedString?.isEqual(dateType))! {
            //self.index = self.checkTypeArray.index(of: dateType)
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(dateType)
                {
                    self.index = i
                    
                    if indexPath.section == self.index
                    {
                        datecell = tableView .dequeueReusableCell(withIdentifier: "DateCell") as! DateCell
                        
                        datecell.dateTimePickerObj.maximumDate = Date()
                        let dateFormat = DateFormatter()
                        let eventDate: Date? = datecell.dateTimePickerObj.date
                        dateFormat.dateFormat = "dd/MM/yyyy"
                        let dateString: String = dateFormat.string(from: eventDate!)
                        datecell.dateLabelObj.text = "\(dateString)"
                        
                        datecell.dateTimePickerObj.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
                        return datecell
                    }
                }
            }
        }
        
        return datecell
        
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        datecell.dateTimePickerObj.maximumDate = Date()
        let dateFormat = DateFormatter()
        let eventDate: Date? = datecell.dateTimePickerObj.date
        dateFormat.dateFormat = "dd/MM/yyyy"
        let dateString: String = dateFormat.string(from: eventDate!)
        datecell.dateLabelObj.text = "\(dateString)"
        
        self.surveyDict .updateValue(datecell.dateLabelObj.text as AnyObject , forKey: ((self.fieldID_Array .object(at: sender.tag) as AnyObject) as! String))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedString = self.checkTypeArray .object(at: indexPath.section) as? String
        
        
        // ------- Selection Type ------- //
        
        if (selectedString?.isEqual(selectType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(selectType)
                {
                    self.index = i
                    for k in 0..<self.multipleArray.count {
                        trueOrNOt = self.multipleArray[k] as? String
                        if trueOrNOt != nil
                        {
                            if (trueOrNOt? .isEqual(multipleTrueorNot))!
                            {
                                if indexPath.section == self.index
                                {                                    
                                    let cell = tableView.cellForRow(at:indexPath) as! SelectPIckerCell
                                    
                                    if(cell.checkBoxImageObj.isHighlighted == false)
                                    {
                                        cell.checkBoxImageObj.isHighlighted = true
                                    }
                                    else if(cell.checkBoxImageObj.isHighlighted == true)
                                    {
                                        cell.checkBoxImageObj.isHighlighted = false
                                    }
                                    if(cell.checkBoxImageObj.image!.isEqual(UIImage(named: "unchecked")))
                                    {
                                        cell.checkBoxImageObj.image = UIImage(named: "Checkbox Filled")
                                       
                                       self.checkCellStatus = "1"
                                        self.SelectedboxDataDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "multi_id") as AnyObject), forKey: "id")
                                        self.SelectedboxDataDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "value") as AnyObject), forKey: "value")
                                         // self.SelectedboxSelectedArray.insert(self.SelectedboxDataDic, at: indexPath.row)
                                        self.SelectedboxSelectedArray.add(self.SelectedboxDataDic)
                                        
                                       
                                        
                                        print(self.SelectedboxDataDic)
                                        
                                        self.surveyDict .updateValue(self.SelectedboxSelectedArray, forKey: ((self.fieldID_Array .object(at: indexPath.section) as AnyObject) as! String))
                                        print(self.surveyDict)
                                        
                                    }
                                    else if(cell.checkBoxImageObj.image!.isEqual(UIImage(named: "Checkbox Filled")))
                                    {
                                        cell.checkBoxImageObj.image = UIImage(named: "unchecked")
                                        self.SelectedboxSelectedArray.removeObject(at: indexPath.row)
                                         print(self.SelectedboxDataDic)
                                        self.surveyDict .updateValue(self.SelectedboxSelectedArray, forKey: ((self.fieldID_Array .object(at: indexPath.section) as AnyObject) as! String))
                                        print(self.surveyDict)
                                    }
                                }
                            }
                            else
                            {
                                
                            }
                        }
                    }
                }
            }
        }
        
        // ------ Radio Type ------ //
        
        if (selectedString?.isEqual(radiogroupType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(radiogroupType)
                {
                    let optionssArray = optionArray[indexPath.section] as! NSMutableArray
                    for index in 0..<optionssArray.count {
                        let dictionary = optionssArray[index] as! NSMutableDictionary
                        dictionary["selected"] = "no"
                        optionssArray.replaceObject(at: index, with: dictionary)
                    }
                    let dictionary = optionssArray[indexPath.row] as! NSMutableDictionary
                    print(dictionary)
                    if dictionary["selected"] as! String  == "true" {
                        dictionary["selected"] = "no"
                    }else {
                        dictionary["selected"] = "true"
                    }
                    
                    optionssArray.replaceObject(at: indexPath.row, with: dictionary)
                    optionArray.replaceObject(at: indexPath.section, with: optionssArray)
                    self.tableViewSurveyDetailsObj.reloadData()
                    return
                    
                }
            }
        }
        
        /// ---- checkBox Type ----- //
        
        if (selectedString?.isEqual(checkboxgroupType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(checkboxgroupType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        
                        let cell = tableView.cellForRow(at:indexPath) as! CheckBoxGroupCell
                        
                        if(cell.checkBoxImageObj.isHighlighted == false)
                        {
                            cell.checkBoxImageObj.isHighlighted = true
                        }
                        else if(cell.checkBoxImageObj.isHighlighted == true)
                        {
                            cell.checkBoxImageObj.isHighlighted = false
                        }
                        if(cell.checkBoxImageObj.image!.isEqual(UIImage(named: "unchecked")))
                        {
                            cell.checkBoxImageObj.image = UIImage(named: "Checkbox Filled")
                            self.checkboxDataDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "multi_id") as AnyObject), forKey: "id")
                            self.checkboxDataDic .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "value") as AnyObject), forKey: "value")
                            self.checkboxSelectedArray.add(self.checkboxDataDic)
                            self.surveyDict .updateValue(self.checkboxSelectedArray, forKey: ((self.fieldID_Array .object(at: indexPath.section) as AnyObject) as! String))
                           // print(self.surveyDict)
                            
                        }
                        else if(cell.checkBoxImageObj.image!.isEqual(UIImage(named: "Checkbox Filled")))
                        {
                            cell.checkBoxImageObj.image = UIImage(named: "unchecked")
                            print (self.checkboxSelectedArray)
                            var dict = [String : AnyObject]()
                            dict .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "multi_id") as AnyObject), forKey: "id")
                            dict .updateValue((((self.optionArray .object(at: self.index) as AnyObject) .object(at: indexPath.row) as AnyObject) .value(forKey: "value") as AnyObject), forKey: "value")
                            if self.checkboxSelectedArray.count > 0 {
                                self.checkboxSelectedArray.remove(dict)
                            }
                            self.surveyDict .updateValue(self.checkboxSelectedArray, forKey: ((self.fieldID_Array .object(at: indexPath.section) as AnyObject) as! String))
                        }
                    }
                }
            }
        }
        
        // ----- Date Type ------ //
        if (selectedString?.isEqual(dateType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(dateType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        toggleDatepicker()
                        dpShowDateChanged()
                    }
                }
            }
        }
        //tableView.deselectRow(at: indexPath, animated: true)
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        self.selectedString = self.checkTypeArray .object(at: indexPath.section) as? String
        //print(self.selectedString!)
        if (selectedString?.isEqual(textType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        return 44
                    }
                }
            }
        }
        if (selectedString?.isEqual(numberType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(numberType)
                {
                    self.index = i
                    
                    if indexPath.section == self.index
                    {
                        return 44
                    }
                }
            }
        }
        if (selectedString?.isEqual(textareaType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textareaType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        return 90
                    }
                }
            }
        }
        if (selectedString?.isEqual(radiogroupType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(radiogroupType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        return 44
                    }
                }
            }
        }
        if (selectedString?.isEqual(selectType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(selectType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        return 44
                    }
                }
            }
        }
        if (selectedString?.isEqual(checkboxgroupType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(checkboxgroupType)
                {
                    self.index = i
                    if indexPath.section == self.index
                    {
                        return 44
                    }
                }
            }
        }
        if (selectedString?.isEqual(dateType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(dateType)
                {
                    //let height : CGFloat = self.tableViewSurveyDetailsObj.rowHeight
                    if datePickerHidden && indexPath.section == self.index
                    {
                        return 30
                    }
                    else
                    {
                        return self.tableViewSurveyDetailsObj.rowHeight
                    }
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = self.tableViewSurveyDetailsObj.dequeueReusableHeaderFooterView(withIdentifier: "SurveyHeader") as! SurveyHeader
        headerView.layer.borderWidth = 1.0
        headerView.layer.borderColor = UIColor.lightGray.cgColor
       
        self.starMarkStr = (self.responseSurveyFieldsArray .object(at: section) as AnyObject) .value(forKey: "required") as? String
        
        //if (starMarkStr? .isEqual("1"))! {
        if starMarkStr != nil {
            
            let Star : NSString = "*"
            let headerNameStr : NSString = (self.responseSurveyFieldsArray .object(at: section) as AnyObject) .value(forKey: "label") as! NSString
            headerView.nameLabelObj.text = headerNameStr .appending(" ") .appending(Star as String)
            let range = (headerView.nameLabelObj.text! as NSString).range(of: "*")
            let attributedString = NSMutableAttributedString(string:headerView.nameLabelObj.text!)
            attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
            //Apply to the label
            headerView.nameLabelObj.attributedText = attributedString;
        }
        else
        {
            headerView.nameLabelObj.text = (self.responseSurveyFieldsArray .object(at: section) as AnyObject) .value(forKey: "label") as? String
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        self.selectedString = self.checkTypeArray .object(at: section) as? String
        //print(self.selectedString!)
        if (selectedString?.isEqual(textType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textType)
                {
                    self.index = i
                    if section == self.index
                    {
                        self.starMarkStr = (self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "required") as? String
                        
                        if starMarkStr != nil {
                            let footerView = self.tableViewSurveyDetailsObj.dequeueReusableHeaderFooterView(withIdentifier: "FooterView") as! FooterView
                            footerView.layer.borderColor = UIColor.gray.cgColor
                            footerView.layer.borderWidth = 1.0
                            return footerView
                        }
                    }
                }
            }
        }
        if (selectedString?.isEqual(numberType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(numberType)
                {
                    self.index = i
                    if section == self.index
                    {
                        self.starMarkStr = (self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "required") as? String
                        
                        if starMarkStr != nil {
                            let footerView = self.tableViewSurveyDetailsObj.dequeueReusableHeaderFooterView(withIdentifier: "FooterView") as! FooterView
                            footerView.layer.borderColor = UIColor.gray.cgColor
                            footerView.layer.borderWidth = 1.0
                            return footerView
                        }
                    }
                }
            }
        }
        let footerView = self.tableViewSurveyDetailsObj.dequeueReusableHeaderFooterView(withIdentifier: "FooterView") as! FooterView
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        self.selectedString = self.checkTypeArray .object(at: section) as? String
        //print(self.selectedString!)
        if (selectedString?.isEqual(textType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textType)
                {
                    self.index = i
                    if section == self.index
                    {
                        self.starMarkStr = (self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "required") as? String
                        
                        if starMarkStr != nil {
                            
                            return 25
                        }
                    }
                }
            }
        }
        if (selectedString?.isEqual(numberType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(numberType)
                {
                    self.index = i
                    if section == self.index
                    {
                        self.starMarkStr = (self.responseSurveyFieldsArray .object(at: i) as AnyObject) .value(forKey: "required") as? String
                        
                        if starMarkStr != nil {
                            
                            return 25
                        }
                    }
                }
            }
        }
        return 0
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.selectedString = self.checkTypeArray .object(at: textField.tag) as? String
        
        if (selectedString?.isEqual(numberType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(numberType)
                {
                    if let cell = textField.superview?.superview as? NumberCell {
                        _ = self.tableViewSurveyDetailsObj.indexPath(for: cell)
                        
                        
                       self.isTextTrue = true
                        
                    //--- add UIToolBar on keyboard and Done button on UIToolBar ---//
                        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
                        doneToolbar.barStyle       = UIBarStyle.default
                        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
                        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
                        var items = [UIBarButtonItem]()
                        items.append(flexSpace)
                        items.append(done)
                        doneToolbar.items = items
                        doneToolbar.sizeToFit()
                        cell.textfieldNoObj.inputAccessoryView = doneToolbar
                    }
                }
            }
        }
        if (selectedString?.isEqual(dateType))! {
            
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(dateType)
                {
                    toggleDatepicker()
                }
            }
        }
    }
    
    func toggleDatepicker() {
        datePickerHidden = !datePickerHidden
        self.tableViewSurveyDetailsObj.beginUpdates()
        self.tableViewSurveyDetailsObj.endUpdates()
    }
    
    func doneButtonAction() {
        
        self.view .endEditing(true)
        numbercell.textfieldNoObj.resignFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.selectedString = self.checkTypeArray .object(at: textField.tag) as? String
        //  print(self.selectedString!)
        if (selectedString?.isEqual(textType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textType)
                {
                     self.isTrue = true
                    self.index = i
                    if let cell = textField.superview?.superview as? TextCell {
                        _ = self.tableViewSurveyDetailsObj.indexPath(for: cell)
                        textField.keyboardType = UIKeyboardType.alphabet
                        
                        self.surveyDict .updateValue(textField.text as AnyObject, forKey: ((self.fieldID_Array .object(at: textField.tag) as AnyObject) as! String))
                        print(self.surveyDict)
                    }
                }
            }
        }
        if (selectedString?.isEqual(numberType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(numberType)
                {
                    self.index = i
                    if let cell = textField.superview?.superview as? NumberCell {
                        _ = self.tableViewSurveyDetailsObj.indexPath(for: cell)
                        textField.keyboardType = UIKeyboardType.numberPad
                        self.surveyDict .updateValue(textField.text as AnyObject, forKey: ((self.fieldID_Array .object(at: textField.tag) as AnyObject) as! String))
                        print(self.surveyDict)
                    }
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
        // called when 'return' key pressed. return false to ignore.
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        self.selectedString = self.checkTypeArray .object(at: textField.tag) as? String
        
        if (selectedString?.isEqual(textType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textType)
                {
                        let Regex = "[A-Za-z0-9 ]*"
                        let TestResult = NSPredicate(format: "SELF MATCHES %@", Regex)
                        return TestResult.evaluate(with: string)
                }
            }
        }
        
        if (selectedString?.isEqual(numberType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(numberType)
                {
                    
                        // Create an `NSCharacterSet` set which includes everything *but* the digits
                        let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
                        
                        // At every character in this "inverseSet" contained in the string,
                        // split the string up into components which exclude the characters
                        // in this inverse set
                        let components = string.components(separatedBy: inverseSet)
                        
                        // Rejoin these components
                        let filtered = components.joined(separator: "")  // use join("", components) if you are using Swift 1.2
                        
                        // If the original string is equal to the filtered string, i.e. if no
                        // inverse characters were present to be eliminated, the input is valid
                        // and the statement returns true; else it returns false
                        return string == filtered
                   
                }
            }
        }
        
        return true
    }
    
    // MARK: - UITextViewDelegate
    
  /*  func textViewDidEndEditing(_ textView: UITextView) {
        self.selectedString = self.checkTypeArray .object(at: textView.tag) as? String
        if (selectedString?.isEqual(textareaType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textareaType)
                {
                     self.isTextAreaTrue = true
                    self.index = i
                    if let cell = textView.superview?.superview as? NumberCell {
                        _ = self.tableViewSurveyDetailsObj.indexPath(for: cell)
                        textView.keyboardType = UIKeyboardType.alphabet
                        self.surveyDict .updateValue(textView.text as AnyObject, forKey: ((self.fieldID_Array .object(at: textView.tag) as AnyObject) as! String))
                    }
                }
            }
        }
    }*/
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.selectedString = self.checkTypeArray .object(at: textView.tag) as? String
        if (selectedString?.isEqual(textareaType))! {
            for i in 0..<self.checkTypeArray.count {
                let stringInWordArray: String = self.checkTypeArray[i] as! String
                if stringInWordArray .isEqual(textareaType)
                {
                    self.isTextAreaTrue = true
                    self.index = i
                    if let cell = textView.superview?.superview as? TextAreaCell {
                        _ = self.tableViewSurveyDetailsObj.indexPath(for: cell)
                        textView.keyboardType = UIKeyboardType.alphabet
                        self.surveyDict .updateValue(textView.text as AnyObject, forKey: ((self.fieldID_Array .object(at: textView.tag) as AnyObject) as! String))
                    }
                }
            }
        }
    }
    
    // MARK: - dpShowDateChanged
    
    private func dpShowDateChanged () {
        datecell.dateTimePickerObj.maximumDate = Date()
        let dateFormat = DateFormatter()
        let eventDate: Date? = datecell.dateTimePickerObj.date
        dateFormat.dateFormat = "dd/MM/yyyy"
        let dateString: String = dateFormat.string(from: eventDate!)
        datecell.dateLabelObj.text = "\(dateString)"
    }
    
    func selectedBtnText(_ sender: UIButton) {
        for i in 0..<self.checkTypeArray.count {
            let stringInWordArray: String = self.checkTypeArray[i] as! String
            if stringInWordArray .isEqual(selectType)
            {
                chooseArticleDropDown.anchorView = sender
                chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
                self.dataArray.removeAll()
                for j in 0..<((self.optionArray .object(at: sender.tag) as AnyObject) .value(forKey: "label") as AnyObject).count
                {
                    self.dataArray.append(((((self.optionArray .object(at: sender.tag) as AnyObject) .object(at: j) as AnyObject) .value(forKey: "label") as AnyObject) as! String))
                }
                chooseArticleDropDown.dataSource = dataArray
                
                // Action triggered on selection
                chooseArticleDropDown.selectionAction = { [unowned self] (index, item) in
                    if let cell = sender.superview?.superview as? SelectPIckerCell {
                        _ = self.tableViewSurveyDetailsObj.indexPath(for: cell)
                        cell.selectedBtn.setTitleColor(UIColor.black, for: UIControlState.normal)
                        cell.selectedBtn.setTitle(item, for: UIControlState.normal)
                        self.checkCellStatus = "1"
                        self.SelectedRadioDataDic .updateValue((((self.optionArray .object(at: sender.tag) as AnyObject) .object(at: index) as AnyObject) .value(forKey: "multi_id") as AnyObject), forKey: "id")
                        self.SelectedRadioDataDic .updateValue((((self.optionArray .object(at: sender.tag) as AnyObject) .object(at: index) as AnyObject) .value(forKey: "value") as AnyObject), forKey: "value")
                        self.surveyDict .updateValue(self.SelectedRadioDataDic as AnyObject, forKey: ((self.fieldID_Array .object(at: sender.tag) as AnyObject) as! String))
                    }
                }
                chooseArticleDropDown.show()
                dropDowns.forEach { $0.dismissMode = .onTap }
                dropDowns.forEach { $0.direction = .any }
            }
        }
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        let mainViewController = self.sideMenuController!
        let CommentsVC = SurveysViewController()
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
    
    @IBAction func sideMenuAction(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
        
    }
    
    @IBAction func recallBtnTapped(_ sender: Any) {
        let mainViewController = self.sideMenuController!
        let CommentsVC = SurveysViewController()
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            // Print each key-value pair in a new row
            //  addressDict.forEach { print($0) }
            
            // Print fully formatted address
            if let formattedAddress = addressDict["FormattedAddressLines"] as? [String] {
                self.addressStr = formattedAddress.joined(separator: ", ") as NSString
            }
            
            // Access each element manually
            /*  if let locationName = addressDict["Name"] as? String {
             }
             if let street = addressDict["Thoroughfare"] as? String {
             }
             if let city = addressDict["City"] as? String {
             }
             if let zip = addressDict["ZIP"] as? String {
             }*/
            if let state = addressDict["State"] as? String {
                self.stateStr = state as NSString
            }
            if let country = addressDict["Country"] as? String {
                self.countryStr = country as NSString
            }
            if let continent = addressDict["CountryCode"] as? String {
                self.continentStr = continent as NSString
            }
            
            self.locationArray .updateValue(locValue.latitude as AnyObject, forKey:"lat")
            self.locationArray .updateValue(locValue.longitude as AnyObject, forKey:"long")
            self.locationArray .updateValue(self.addressStr as AnyObject, forKey:"address")
            self.locationArray .updateValue(self.stateStr as AnyObject, forKey:"state")
            self.locationArray .updateValue(self.countryStr as AnyObject, forKey:"country")
            self.locationArray .updateValue(self.continentStr as AnyObject, forKey:"continent")
        })
        manager .stopUpdatingLocation()
        manager.delegate = nil
    }
    
    @IBAction func completeSurveyBtn(_ sender: Any) {
        // print(self.requiredArray)
        for i in 0..<self.requiredArray.count {
            self.starMarkStr = (self.requiredArray .object(at: i) as AnyObject) as? String
            self.selectedString = self.checkTypeArray .object(at: i) as? String
            if starMarkStr != nil {
                //print(self.selectedString!)
                if (selectedString?.isEqual(textType))! && (starMarkStr? .isEqual("1"))! {
                    if (textcell.textfieldOBj.text?.isEmpty)! {
                        let alert=UIAlertController(title: "Required Fields", message: "Please Fill Required fields", preferredStyle: UIAlertControllerStyle.alert);
                        //no event handler (just close dialog box)
                        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil));
                        //event handler with closure
                        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in
                            
                        }));
                        present(alert, animated: true, completion: nil);
                    }
                    else {
                        self.textFillOrNot = "1"
                    }
                }
                else {
                   self.textFillOrNot = "0"
                }
                if (selectedString?.isEqual(numberType))! && (starMarkStr? .isEqual("1"))! {
                    
                    if (numbercell.textfieldNoObj.text?.isEmpty)! {
                        let alert=UIAlertController(title: "Required Fields", message: "Please Fill Required fields", preferredStyle: UIAlertControllerStyle.alert);
                        //no event handler (just close dialog box)
                        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil));
                        //event handler with closure
                        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in
                            
                        }));
                        present(alert, animated: true, completion: nil);
                    }
                    else {
                        self.NoFillOrNot = "1"
                    }
                }
                else {
                    self.NoFillOrNot = "0"
                }
                if (selectedString?.isEqual(textareaType))! && (starMarkStr? .isEqual("1"))! {
                    
                    if (textareacell.textViewObj.text?.isEmpty)! {
                    
                    let alert=UIAlertController(title: "Required Fields", message: "Please Fill Required fields", preferredStyle: UIAlertControllerStyle.alert);
                    //no event handler (just close dialog box)
                    alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil));
                    //event handler with closure
                    alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in
                        
                    }));
                    present(alert, animated: true, completion: nil);
                    }
                    else{
                       self.textAreaFillOrNot = "1"
                    }
                }
                else {
                  self.textAreaFillOrNot = "0"
                }
                if (selectedString?.isEqual(selectType))! && (starMarkStr? .isEqual("1"))! {
                    
                    if !self.checkCellStatus.isEqual(to: "1") {
                        let alert=UIAlertController(title: "Required Fields", message: "Please Fill Required fields", preferredStyle: UIAlertControllerStyle.alert);
                        //no event handler (just close dialog box)
                        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil));
                        //event handler with closure
                        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in
                            
                        }));
                        present(alert, animated: true, completion: nil);
                    }
                    else {
                       self.checkFillOrNot = "1"
                        
                    }
                }
                else {
                    self.checkFillOrNot = "0"
                }
                
               
              /*  if (selectedString?.isEqual(checkboxgroupType))! && (starMarkStr? .isEqual("1"))! {
                    
                    if (SelectPIckercell.selectedBtn.currentTitle?.isEmpty)! {
                        let alert=UIAlertController(title: "Required Fields", message: "Please Fill Required fields", preferredStyle: UIAlertControllerStyle.alert);
                        //no event handler (just close dialog box)
                        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil));
                        //event handler with closure
                        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in
                            
                        }));
                        present(alert, animated: true, completion: nil);
                    }
                    else {
                        
                    }
                }
                if (selectedString?.isEqual(radiogroupType))! && (starMarkStr? .isEqual("1"))! {
                    
                    if (SelectPIckercell.selectedBtn.currentTitle?.isEmpty)! {
                        let alert=UIAlertController(title: "Required Fields", message: "Please Fill Required fields", preferredStyle: UIAlertControllerStyle.alert);
                        //no event handler (just close dialog box)
                        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil));
                        //event handler with closure
                        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in
                            
                        }));
                        present(alert, animated: true, completion: nil);
                    }
                    else {
                        
                    }
                }
                if (selectedString?.isEqual(dateType))! && (starMarkStr? .isEqual("1"))! {
                    
                    if (SelectPIckercell.selectedBtn.currentTitle?.isEmpty)! {
                        let alert=UIAlertController(title: "Required Fields", message: "Please Fill Required fields", preferredStyle: UIAlertControllerStyle.alert);
                        //no event handler (just close dialog box)
                        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil));
                        //event handler with closure
                        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in
                            
                        }));
                        present(alert, animated: true, completion: nil);
                    }
                    else {
                        
                    }
                }*/
            }
        }
        
        if self.textFillOrNot .isEqual(to: "1") || self.NoFillOrNot .isEqual(to: "1") || self.textAreaFillOrNot .isEqual(to: "1") || self.checkFillOrNot .isEqual(to: "1") {
            
            self.completeSurveyAPICalling()
        }
        else {
            if self.textFillOrNot .isEqual(to: "0") || self.NoFillOrNot .isEqual(to: "0") || self.textAreaFillOrNot .isEqual(to: "0") || self.checkFillOrNot .isEqual(to: "0") {
                
            }
            else {
              self.completeSurveyAPICalling()
            }
           
        }
        
        
    }
    
    func completeSurveyAPICalling()  {
        
        
        /* NSDictionary to NSData */
        let surveydata = NSKeyedArchiver.archivedData(withRootObject: self.surveyDict)
        
        self.surveyDict = NSKeyedUnarchiver.unarchiveObject(with: surveydata) as! [String : AnyObject]
        
        let convertedStr = NSString(data: surveydata, encoding: String.Encoding.utf8.rawValue)
        print("Converted String = \(String(describing: convertedStr))")
        
        // --- For Loop the Survey Data ----- //
        
        self.surveyArray.setValue(self.base64Images, forKey: "imgURI")
        let max: Int? = self.fieldID_Array.value(forKeyPath: "@min.intValue") as? Int
        for counter in 0 ..< Int(max!) {
            let emptykeyStr =  String(counter)
            self.surveyDict .updateValue("null" as AnyObject, forKey: emptykeyStr)
        }
        
        
        
        if Reachability.isConnectedToNetwork() == true {
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            if self.locationCheck != nil
            {
                if (locationCheck? .isEqual("true"))! {
                    self.params = [
                        "purpose" : "completesurvey" as AnyObject,
                        "id" : userID as AnyObject,
                        "survey" : self.surveyArray as AnyObject,
                        "surveys" : self.surveyDict as AnyObject,
                        "location" : self.locationArray as AnyObject]
                }
                let jsonData = try? JSONSerialization.data(withJSONObject: self.params, options: [])
                let responceStr = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue)
                print(responceStr!)
            }
            else
            {
                self.params = [
                    "purpose" : "completesurvey" as AnyObject,
                    "id" : userID as AnyObject,
                    "survey" : self.surveyArray as AnyObject,
                    "surveys" : self.surveyDict as AnyObject]
                
                let jsonData = try? JSONSerialization.data(withJSONObject: self.params, options: [])
                let responceStr = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue)
                print(responceStr!)
                
                
               /* let contactDB = FMDatabase(path: databasePath as String)
                
                if (contactDB.open()) {
                    
                    let insertSQL = "INSERT INTO CONTACTS (survey) VALUES ('\(responceStr!)')"
                    
                    let result = contactDB.executeUpdate(insertSQL,
                                                         withArgumentsIn: [])
                    print(result)
                    if !result {
                        print("Error: \(contactDB.lastErrorMessage())")
                    } else {
                        
                    }
                } else {
                    print("Error: \(contactDB.lastErrorMessage())")
                }*/
            }
            let url = NSURL(string:BaseURL_CompleteSurveyAPI )
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: self.params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                // self.presentAlertWithTitle(title: "", message: (responsejson["msg"] as AnyObject) as! String)
                                
                                
                                let alertController = UIAlertController(title: "", message: ((responsejson["msg"] as AnyObject) as! String), preferredStyle: .alert)
                                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(cancelAction)
                                
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    
                                    /*let mainViewController = self.sideMenuController!
                                    let CommentsVC = SurveysViewController()
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)*/
                                    
                                    self.homeServiceApiCalling()
                                     self.isTrue = false
                                      self.isTextTrue = false
                                     self.isTextAreaTrue = false
                                   // self.tableViewSurveyDetailsObj.reloadData()
                                    
                                }
                                alertController.addAction(OKAction)
                                
                                self.present(alertController, animated: true, completion:nil)
                                
                                
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.presentAlertWithTitle(title: "", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
          //  self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
            presentAlertWithTitle(title: "Offline", message: "You are offline.")
            let userID = UserDefaults.standard.value(forKey: "userID")
            if self.locationCheck != nil
            {
                if (locationCheck? .isEqual("true"))! {
                    self.params = [
                        "purpose" : "completesurvey" as AnyObject,
                        "id" : userID as AnyObject,
                        "survey" : self.surveyArray as AnyObject,
                        "surveys" : self.surveyDict as AnyObject,
                        "location" : self.locationArray as AnyObject]
                }
                let jsonData = try? JSONSerialization.data(withJSONObject: self.params, options: [])
                let responceStr = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue)
                print(responceStr!)
                let contactDB = FMDatabase(path: databasePath as String)
                if (contactDB.open()) {
                    let insertSQL = "INSERT INTO SURVEY (survey) VALUES ('\(responceStr!)')"
                    let result = contactDB.executeUpdate(insertSQL,
                                                         withArgumentsIn: [])
                    print(result)
                    if !result {
                        print("Error: \(contactDB.lastErrorMessage())")
                    } else {
                    }
                } else {
                    print("Error: \(contactDB.lastErrorMessage())")
                }
            }
            else
            {
                self.params = [
                    "purpose" : "completesurvey" as AnyObject,
                    "id" : userID as AnyObject,
                    "survey" : self.surveyArray as AnyObject,
                    "surveys" : self.surveyDict as AnyObject]
                
                let jsonData = try? JSONSerialization.data(withJSONObject: self.params, options: [])
                let responceStr = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue)
                print(responceStr!)
                let contactDB = FMDatabase(path: databasePath as String)
                
                if (contactDB.open()) {
                    let insertSQL = "INSERT INTO SURVEY (survey) VALUES ('\(responceStr!)')"
                    let result = contactDB.executeUpdate(insertSQL,
                                                         withArgumentsIn: [])
                    print(result)
                    if !result {
                        print("Error: \(contactDB.lastErrorMessage())")
                    } else {
                    }
                } else {
                    print("Error: \(contactDB.lastErrorMessage())")
                }
            }
        }
    }
    
    @IBAction func takePhotoBtn(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate=self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self .present(imagePicker, animated: true, completion: nil)
        } else {
            noCamera()
        }
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, This device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    @IBAction func choosePhotoBtn(_ sender: Any) {
        
        self.rootListAssets.didSelectAssets = {(assets: Array<PHAsset?>) -> () in
            
            for var i in 0..<assets.count {
                let manager = PHImageManager.default()
                let asset = assets[i]
                let requestOptions = PHImageRequestOptions()
                requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
                requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
                // this one is key
                requestOptions.isSynchronous = true
                if ((asset as AnyObject).mediaType == PHAssetMediaType.image)
                {
                    PHImageManager.default().requestImage(for: asset!, targetSize: PHImageManagerMaximumSize, contentMode:  .aspectFill, options: requestOptions, resultHandler: { (pickedImage, info) in
                        self.galleryImages.append(pickedImage!)
                        // you can get image like this way
                    })
                }
            }
            for var i in 0..<self.galleryImages.count {
                if self.imageArray.count >= 2 {
                    self.imageArray.removeLastObject()
                }
                self.imageArray.insert(self.galleryImages[i] , at: 0)
            }
            if self.imageArray.count == 0
            {
                self.collectionViewHeight.constant = 0
                self.uploadImageView.frame = CGRect(x: 0, y: 0, width: self.tableViewSurveyDetailsObj.bounds.size.width, height: 253)
            }
            if self.imageArray.count == 1
            {
                self.collectionViewHeight.constant = 110
                self.uploadImageView.frame = CGRect(x: 0, y: 0, width: self.tableViewSurveyDetailsObj.bounds.size.width, height: 363)
            }
            if self.imageArray.count == 2
            {
                self.collectionViewHeight.constant = 205
                self.uploadImageView.frame = CGRect(x: 0, y: 0, width: self.tableViewSurveyDetailsObj.bounds.size.width, height: 458)
            }
            self.tableViewSurveyDetailsObj.tableFooterView = self.uploadImageView
            self.base64Images.removeAll()
            self.galleryImages.removeAll()
            self.collectionViewObj.reloadData()
            self.tableViewSurveyDetailsObj.reloadData()
        }
        let navigationController = UINavigationController(rootViewController:  self.rootListAssets)
        present(navigationController, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        picker .dismiss(animated: true, completion: nil)
        self.camImageArray.append((info[UIImagePickerControllerOriginalImage] as? UIImage)!)
        for var i in 0..<self.camImageArray.count
        {
            if self.imageArray.count >= 2 {
                self.imageArray.removeLastObject()
            }
            self.imageArray.insert(self.camImageArray[i] , at: 0)
        }
        self.base64Images.removeAll()
        self.galleryImages.removeAll()
        self.tableViewSurveyDetailsObj.reloadData()
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        defer {
            picker.dismiss(animated: true)
        }
    }
    
    /* override func viewDidLayoutSubviews() {
     super.viewDidLayoutSubviews()
     
     self.uploadImageView.setNeedsLayout()
     self.uploadImageView.layoutIfNeeded()
     
     let height = min(self.view.bounds.size.height, self.collectionViewObj.contentSize.height)
     self.collectionViewHeight.constant = height
     //self.view.layoutSubviews()
     }*/
    
    // MARK: - UICollectionViewDelegate and DataSource Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        var cell: AddPostCollectionCell?
        
        if self.imageArray.count == 0
        {
            self.collectionViewHeight.constant = 0
            self.uploadImageView.frame = CGRect(x: 0, y: 0, width: self.tableViewSurveyDetailsObj.bounds.size.width, height: 253)
        }
        if self.imageArray.count == 1
        {
            self.collectionViewHeight.constant = 110
            self.uploadImageView.frame = CGRect(x: 0, y: 0, width: self.tableViewSurveyDetailsObj.bounds.size.width, height: 363)
        }
        if self.imageArray.count == 2
        {
            self.collectionViewHeight.constant = 205
            self.uploadImageView.frame = CGRect(x: 0, y: 0, width: self.tableViewSurveyDetailsObj.bounds.size.width, height: 458)
        }
        self.tableViewSurveyDetailsObj.tableFooterView = self.uploadImageView
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPostCollectionCell", for: indexPath)as? AddPostCollectionCell
        cell?.removeBtnTapped.tag = indexPath.row
        cell?.removeBtnTapped.addTarget(self, action: #selector(self.removebtnTapped), for: .touchUpInside)
        cell?.layer.borderColor = UIColor.darkGray.cgColor
        cell?.layer.borderWidth = 1
        cell?.imageViewObj.image = self.imageArray[indexPath.row] as? UIImage
        let imageData : NSData = UIImageJPEGRepresentation((cell?.imageViewObj.image!)!, 0.6)! as NSData
        self.profilepicImageStr = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
        // let selectedImageSize:Int = imageData.length
        self.base64Images.append(self.profilepicImageStr)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: UIScreen.main.bounds.size.width - 40, height: 94.0)
    }
    
    func removebtnTapped(_ sender: UIButton) {
        self.base64Images.removeAll()
        self.galleryImages.removeAll()
        self.imageArray.removeObject(at: sender.tag)
        
        if self.imageArray.count == 0
        {
            self.collectionViewHeight.constant = 0
            self.uploadImageView.frame = CGRect(x: 0, y: 0, width: self.tableViewSurveyDetailsObj.bounds.size.width, height: 253)
        }
        if self.imageArray.count == 1
        {
            self.collectionViewHeight.constant = 110
            self.uploadImageView.frame = CGRect(x: 0, y: 0, width: self.tableViewSurveyDetailsObj.bounds.size.width, height: 363)
            
        }
        if self.imageArray.count == 2
        {
            self.collectionViewHeight.constant = 205
            self.uploadImageView.frame = CGRect(x: 0, y: 0, width: self.tableViewSurveyDetailsObj.bounds.size.width, height: 458)
        }
        self.collectionViewObj.reloadData()
        self.tableViewSurveyDetailsObj.reloadData()
    }
}


