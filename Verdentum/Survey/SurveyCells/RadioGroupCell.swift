//
//  RadioGroupCell.swift
//  Verdentum
//
//  Created by Verdentum on 23/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class RadioGroupCell: UITableViewCell {

    @IBOutlet var nameLabelObj: UILabel!
    
    
    @IBOutlet var radioImageViewObj: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
