//
//  SelectPIckerCell.swift
//  Verdentum
//
//  Created by Verdentum on 23/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class SelectPIckerCell: UITableViewCell {
    
    @IBOutlet var selectedBtn: UIButton!
    @IBOutlet var imageviewObj: UIImageView!
    @IBOutlet var textFieldSelectedObj: UITextField!
    
    @IBOutlet var nameLabelObj: UILabel!
    
    @IBOutlet var checkBoxImageObj: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
