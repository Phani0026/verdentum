//
//  SurveyHeader.swift
//  Verdentum
//
//  Created by Verdentum on 21/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class SurveyHeader: UITableViewHeaderFooterView {
    @IBOutlet var nameLabelObj: UILabel!
    
    override func awakeFromNib() {
        
        backgroundView = UIView()
        backgroundView?.backgroundColor = UIColor.init(red: 235.0/255.0, green: 235.0/255.0, blue: 241.0/255.0, alpha: 1.0)
    }

}
