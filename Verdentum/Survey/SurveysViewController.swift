//
//  SurveysViewController.swift
//  Verdentum
//
//  Created by Verdentum on 11/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import LGSideMenuController
import MBProgressHUD
class SurveysViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
   
    @IBOutlet var tableViewSurevyObj: UITableView!
    
    var responseSurveyArray = NSArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewSurevyObj.estimatedRowHeight = 70
        self.tableViewSurevyObj.rowHeight = UITableViewAutomaticDimension
        self.tableViewSurevyObj .register(UINib (nibName: "ProgramFollowCell", bundle: nil), forCellReuseIdentifier: "ProgramFollowCell")

         self.homeServiceApiCalling()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         sideMenuController?.isLeftViewSwipeGestureDisabled = false
    }
    
    //MARK: - HomeServiceApiCalling
    
    func homeServiceApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allsurvey" as AnyObject,
                "id" : userID as AnyObject ]
            let url = NSURL(string:BaseURL_SurveyAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                
                                
                                self.responseSurveyArray = (responsejson["surveys"] as AnyObject) as! NSArray as! NSMutableArray
                                
                                self.tableViewSurevyObj.reloadData()
                                
                                
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                               
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
     // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.responseSurveyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "ProgramFollowCell") as! ProgramFollowCell
        cell.descriptionLabel.text =  (self.responseSurveyArray[indexPath.row] as AnyObject).value(forKey: "subject") as? String
        
        cell.nameLabel.text = (self.responseSurveyArray[indexPath.row] as AnyObject).value(forKey: "title") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let mainViewController = self.sideMenuController!
        let SurveyFieldsVC = SurveyFieldsView()
        SurveyFieldsVC.survey_ID = (self.responseSurveyArray[indexPath.row] as! NSObject)  .value(forKey: "id") as! NSString
        SurveyFieldsVC.titleStr = (self.responseSurveyArray[indexPath.row] as! NSObject).value(forKey: "title") as! NSString

        SurveyFieldsVC.surveyArray = ((self.responseSurveyArray[indexPath.row] as AnyObject) as! NSDictionary)
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(SurveyFieldsVC, animated: true)
    }

    @IBAction func sideMenuBtnTapped(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
}
