//
//  ImagePostCell.swift
//  Verdentum
//
//  Created by Verdentum on 16/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class ImagePostCell: UITableViewCell {

    @IBOutlet var imageViewObj: UIImageView!
    @IBOutlet var commentActionBtn: UIButton!
    @IBOutlet var commentCOuntLabel: UILabel!
    @IBOutlet var commentCountBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    
}
