//
//  WallCell.swift
//  Task
//
//  Created by admin on 13/06/17.
//  Copyright © 2017 VNSoftech. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import SDWebImage
import WebKit
import Kingfisher

class WallCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var cellString = NSString()
    var imagesArrayURL = NSDictionary()
    var imageArray = NSMutableArray()
    var screenWidth = CGFloat()
    var appendStringPDF = NSString()
    var youtubeAppendStr = NSString()
    var imageorPDFstr = NSString()
    let postIDStr = NSString()
     var isPlayerTag = 0//0->pause,1->play
    
    
    @IBOutlet var imageViewAboveLIneObj: UILabel!
    
    @IBOutlet var imageTappedBtn: UIButton!
    @IBOutlet var imageBtnHeight: NSLayoutConstraint!
    @IBOutlet var userPostsBtnTapped: UIButton!
    @IBOutlet var dotMoreBtn: UIButton!
    @IBOutlet var textLabelObj: UILabel!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var imageCollectionViewObj: UICollectionView!
    @IBOutlet var dataTimeLabel: UILabel!
    
    @IBOutlet var supportLabel: UILabel!
    @IBOutlet var heightCollectionView: NSLayoutConstraint!
   
    @IBOutlet var imageViewObj: UIImageView!
    
    
    @IBOutlet var selectedLIkeLabel: UILabel!
    @IBOutlet var likeCountLabel: UILabel!
    
    @IBOutlet var viewAllCmtCount: UILabel!
    
    @IBOutlet var viewAllBtnTapped: UIButton!
    
    @IBOutlet var commentBtnTap: UIButton!
    
    @IBOutlet var commentImageView: UIImageView!
    @IBOutlet var heartImageview: UIImageView!
    @IBOutlet var likeCountTap: UIButton!
    @IBOutlet var likeBtnTapped: UIButton!
    // MARK: - awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
         NotificationCenter.default.removeObserver(self)
        
        
        
        let screenRect: CGRect = UIScreen.main.bounds
        screenWidth = screenRect.size.width
        self.imageCollectionViewObj.register(UINib(nibName: "NewsImagesCell", bundle: nil), forCellWithReuseIdentifier: "NewsImagesCell")
        self.imageCollectionViewObj.register(UINib(nibName: "TwoImageCell", bundle: nil), forCellWithReuseIdentifier: "TwoImageCell")
        self.imageCollectionViewObj.register(UINib(nibName: "NewsImages5Cell", bundle: nil), forCellWithReuseIdentifier: "NewsImages5Cell")
        self.imageCollectionViewObj.register(UINib(nibName: "ThreeImageCell", bundle: nil), forCellWithReuseIdentifier: "ThreeImageCell")
        self.imageCollectionViewObj.register(UINib(nibName: "FourImagesCell", bundle: nil), forCellWithReuseIdentifier: "FourImagesCell")
    }
    
    func setCollectionData (collection : NSMutableArray)    {
        self.imageArray = collection
        self.imageCollectionViewObj.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
     // MARK: - UICollectionViewDelegate and DataSource Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
       // let cell = UICollectionViewCell()
        
        if self.imageArray.count <= 5 {
            if self.imageArray.count == 5 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsImages5Cell", for: indexPath)as! NewsImages5Cell
                
                
                
                
                let imageURLstr : NSString = IMAGE_UPLOAD.appending(((self.imageArray[0] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url = URL(string: imageURLstr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.firstImageView.kf.indicatorType = .activity
                cell.firstImageView.kf.setImage(with: url,placeholder: nil,
                                                options: [.transition(ImageTransition.fade(1))],
                                                progressBlock: { receivedSize, totalSize in
                                                    //print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                                completionHandler: { image, error, cacheType, imageURL in
                                                   // print("\(indexPath.row + 1): Finished")
                })
                
                /*cell.firstImageView.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 if cell.firstImageView.image != nil
                 {
                 progressIcon.stopAnimating()
                 progressIcon.hidesWhenStopped = true
                 }
                 else
                 {
                 progressIcon.hidesWhenStopped = false
                 }
                 })*/
                
                
                
                let imageURLstr1 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[1] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url1 = URL(string: imageURLstr1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                
                cell.secondImageView.kf.indicatorType = .activity
                cell.secondImageView.kf.setImage(with: url1,placeholder: nil,
                                                 options: [.transition(ImageTransition.fade(1))],
                                                 progressBlock: { receivedSize, totalSize in
                                                   // print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                                 completionHandler: { image, error, cacheType, imageURL in
                                                   // print("\(indexPath.row + 1): Finished")
                })
                
                
                /* cell.secondImageView.sd_setImage(with: url1!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 if cell.secondImageView.image != nil
                 {
                 progressIcon.stopAnimating()
                 progressIcon.hidesWhenStopped = true
                 }
                 else
                 {
                 progressIcon.hidesWhenStopped = false
                 }
                 
                 })*/
                
                
                
                let imageURLstr2 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[2] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url2 = URL(string: imageURLstr2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.thirdImageView.kf.indicatorType = .activity
                cell.thirdImageView.kf.setImage(with: url2,placeholder: nil,
                                                options: [.transition(ImageTransition.fade(1))],
                                                progressBlock: { receivedSize, totalSize in
                                                  //  print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                                completionHandler: { image, error, cacheType, imageURL in
                                                  //  print("\(indexPath.row + 1): Finished")
                })
                
                
                /* cell.thirdImageView.sd_setImage(with: url2!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 
                 if cell.thirdImageView.image != nil
                 {
                 thirdprogressIcon.stopAnimating()
                 thirdprogressIcon.hidesWhenStopped = true
                 
                 }
                 else
                 {
                 thirdprogressIcon.hidesWhenStopped = false
                 }
                 })*/
                
                
                let imageURLstr3 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[3] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url3 = URL(string: imageURLstr3.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.fourthImageView.kf.indicatorType = .activity
                cell.fourthImageView.kf.setImage(with: url3,placeholder: nil,
                                                 options: [.transition(ImageTransition.fade(1))],
                                                 progressBlock: { receivedSize, totalSize in
                                                   // print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                                 completionHandler: { image, error, cacheType, imageURL in
                                                   // print("\(indexPath.row + 1): Finished")
                })
                
                
                /* cell.fourthImageView.sd_setImage(with: url3!, placeholderImage: UIImage(named: ""),options:SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 if cell.fourthImageView.image != nil
                 {
                 fourthprogressIcon.stopAnimating()
                 fourthprogressIcon.hidesWhenStopped = true
                 }
                 else
                 {
                 fourthprogressIcon.hidesWhenStopped = false
                 }
                 })*/
                
                
                let imageURLstr4 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[4] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url4 = URL(string: imageURLstr4.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.fivethImageView.kf.indicatorType = .activity
                cell.fivethImageView.kf.setImage(with: url4,placeholder: nil,
                                                 options: [.transition(ImageTransition.fade(1))],
                                                 progressBlock: { receivedSize, totalSize in
                                                  //  print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                                 completionHandler: { image, error, cacheType, imageURL in
                                                  //  print("\(indexPath.row + 1): Finished")
                })
                
                
                
                /* cell.fivethImageView.sd_setImage(with: url4!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 if cell.fivethImageView.image != nil
                 {
                 fivethprogressIcon.stopAnimating()
                 fivethprogressIcon.hidesWhenStopped = true
                 }
                 else
                 {
                 fivethprogressIcon.hidesWhenStopped = false
                 }
                 })*/
                
                return cell
            }
            if self.imageArray.count == 4
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FourImagesCell", for: indexPath)as! FourImagesCell
                
                
                
                let imageURLstr : NSString = IMAGE_UPLOAD.appending(((self.imageArray[0] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url = URL(string: imageURLstr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.imageViewone4.kf.indicatorType = .activity
                cell.imageViewone4.kf.setImage(with: url,placeholder: nil,
                                               options: [.transition(ImageTransition.fade(1))],
                                               progressBlock: { receivedSize, totalSize in
                                               // print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                               completionHandler: { image, error, cacheType, imageURL in
                                               // print("\(indexPath.row + 1): Finished")
                })
                
                /* cell.imageViewone4.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 
                 if cell.imageViewone4.image != nil
                 {
                 progressIcon4view.stopAnimating()
                 progressIcon4view.hidesWhenStopped = true
                 }
                 else
                 {
                 progressIcon4view.hidesWhenStopped = false
                 }
                 })*/
                
                
                let imageURLstr1 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[1] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url1 = URL(string: imageURLstr1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.imageViewTwo4.kf.indicatorType = .activity
                cell.imageViewTwo4.kf.setImage(with: url1,placeholder: nil,
                                               options: [.transition(ImageTransition.fade(1))],
                                               progressBlock: { receivedSize, totalSize in
                                               // print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                               completionHandler: { image, error, cacheType, imageURL in
                                               // print("\(indexPath.row + 1): /Finished")
                })
                
                
                /*cell.imageViewTwo4.sd_setImage(with: url1!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 
                 if cell.imageViewTwo4.image != nil
                 {
                 progressIcon4viewonew.stopAnimating()
                 progressIcon4viewonew.hidesWhenStopped = true
                 }
                 else
                 {
                 progressIcon4viewonew.hidesWhenStopped = false
                 }
                 })*/
                
                
                let imageURLstr2 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[2] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url2 = URL(string: imageURLstr2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.imageviewThree4.kf.indicatorType = .activity
                cell.imageviewThree4.kf.setImage(with: url2,placeholder: nil,
                                                 options: [.transition(ImageTransition.fade(1))],
                                                 progressBlock: { receivedSize, totalSize in
                                                  //  print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                                 completionHandler: { image, error, cacheType, imageURL in
                                                   // print("\(indexPath.row + 1): Finished")
                })
                
                /* cell.imageviewThree4.sd_setImage(with: url2!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 
                 if cell.imageviewThree4.image != nil
                 {
                 progressIcon4viewTwo.stopAnimating()
                 progressIcon4viewTwo.hidesWhenStopped = true
                 }
                 else
                 {
                 progressIcon4viewTwo.hidesWhenStopped = false
                 }
                 })*/
                
                let imageURLstr3 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[3] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url3 = URL(string: imageURLstr3.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                
                cell.imageViewfour4.kf.indicatorType = .activity
                cell.imageViewfour4.kf.setImage(with: url3,placeholder: nil,
                                                options: [.transition(ImageTransition.fade(1))],
                                                progressBlock: { receivedSize, totalSize in
                                                   // print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                                completionHandler: { image, error, cacheType, imageURL in
                                                  //  print("\(indexPath.row + 1): Finished")
                })
                
                /* cell.imageViewfour4.sd_setImage(with: url3!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 
                 if cell.imageViewfour4.image != nil
                 {
                 progressIcon4viewThree.stopAnimating()
                 progressIcon4viewThree.hidesWhenStopped = true
                 }
                 else
                 {
                 progressIcon4viewThree.hidesWhenStopped = false
                 }
                 })*/
                
                return cell
            }
            if self.imageArray.count == 3 {
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreeImageCell", for: indexPath)as! ThreeImageCell
                
                let imageURLstr : NSString = IMAGE_UPLOAD.appending(((self.imageArray[0] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url = URL(string: imageURLstr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.imageViewone3.kf.indicatorType = .activity
                cell.imageViewone3.kf.setImage(with: url,placeholder: nil,
                                               options: [.transition(ImageTransition.fade(1))],
                                               progressBlock: { receivedSize, totalSize in
                                               // print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                               completionHandler: { image, error, cacheType, imageURL in
                                               // print("\(indexPath.row + 1): Finished")
                })
                
                /*cell.imageViewone3.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 
                 if cell.imageViewone3.image != nil
                 {
                 progressIcon3view.stopAnimating()
                 progressIcon3view.hidesWhenStopped = true
                 
                 }
                 else
                 {
                 progressIcon3view.hidesWhenStopped = false
                 
                 }
                 })*/
                
                
                let imageURLstr1 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[1] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url1 = URL(string: imageURLstr1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.imageViewtwo3.kf.indicatorType = .activity
                cell.imageViewtwo3.kf.setImage(with: url1,placeholder: nil,
                                               options: [.transition(ImageTransition.fade(1))],
                                               progressBlock: { receivedSize, totalSize in
                                             //   print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                               completionHandler: { image, error, cacheType, imageURL in
                                              //  print("\(indexPath.row + 1): Finished")
                })
                
                /*cell.imageViewtwo3.sd_setImage(with: url1!, placeholderImage: UIImage(named: ""),options:SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 if cell.imageViewtwo3.image != nil
                 {
                 progressIcon3viewone.stopAnimating()
                 progressIcon3viewone.hidesWhenStopped = true
                 }
                 else
                 {
                 progressIcon3viewone.hidesWhenStopped = false
                 }
                 })*/
                
                
                let imageURLstr2 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[2] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url2 = URL(string: imageURLstr2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.imageviewthree3.kf.indicatorType = .activity
                cell.imageviewthree3.kf.setImage(with: url2,placeholder: nil,
                                                 options: [.transition(ImageTransition.fade(1))],
                                                 progressBlock: { receivedSize, totalSize in
                                                 //   print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                                 completionHandler: { image, error, cacheType, imageURL in
                                                 //   print("\(indexPath.row + 1): Finished")
                })
                
                /* cell.imageviewthree3.sd_setImage(with: url2!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 if cell.imageviewthree3.image != nil
                 {
                 progressIcon3viewtwo.stopAnimating()
                 progressIcon3viewtwo.hidesWhenStopped = true
                 }
                 else
                 {
                 progressIcon3viewtwo.hidesWhenStopped = false
                 }
                 })*/
                
                return cell
            }
            if self.imageArray.count == 2 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoImageCell", for: indexPath)as! TwoImageCell
                
                
                let imageURLstr : NSString = IMAGE_UPLOAD.appending(((self.imageArray[0] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url = URL(string: imageURLstr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                cell.imageViewOne2.kf.indicatorType = .activity
                cell.imageViewOne2.kf.setImage(with: url,placeholder: nil,
                                               options: [.transition(ImageTransition.fade(1))],
                                               progressBlock: { receivedSize, totalSize in
                                               // print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                               completionHandler: { image, error, cacheType, imageURL in
                                               // print("\(indexPath.row + 1): Finished")
                })
                
                /*  cell.imageViewOne2.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 
                 if cell.imageViewOne2.image != nil
                 {
                 firstprogressIcontwo.stopAnimating()
                 firstprogressIcontwo.hidesWhenStopped = true
                 }
                 else
                 {
                 firstprogressIcontwo.hidesWhenStopped = false
                 }
                 
                 })*/
                
                
                
                
                let imageURLstr1 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[1] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                
                let url1 = URL(string: imageURLstr1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                
                
                cell.imageViewTwo2.kf.indicatorType = .activity
                cell.imageViewTwo2.kf.setImage(with: url1,placeholder: nil,
                                               options: [.transition(ImageTransition.fade(1))],
                                               progressBlock: { receivedSize, totalSize in
                                              //  print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                },
                                               completionHandler: { image, error, cacheType, imageURL in
                                              //  print("\(indexPath.row + 1): Finished")
                })
                
                /*cell.imageViewTwo2.sd_setImage(with: url1!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                 if cell.imageViewOne2.image != nil
                 {
                 SecprogressIcontwo3.stopAnimating()
                 SecprogressIcontwo3.hidesWhenStopped = true
                 }
                 else
                 {
                 SecprogressIcontwo3.hidesWhenStopped = false
                 }
                 })*/
                
                return cell
            }
            
            if self.imageArray.count == 1
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsImagesCell", for: indexPath)as! NewsImagesCell
                cell.newsImageBtnTapped.addTarget(self, action: #selector(self.firstimageSecondBtn), for: .touchUpInside)
                self.imageorPDFstr = ((self.imageArray[indexPath.row] as AnyObject).value(forKey: "image_name") as? String!)!! as NSString
                let fileArray = self.imageorPDFstr.components(separatedBy: ".")
                let finalFileNameextension = fileArray.last
                let countStr : NSString = finalFileNameextension! as NSString
                let youtubestrpath  = countStr.length
                
                
                if youtubestrpath as NSInteger == 11
                {
                    cell.newsImageBtnTapped.isHidden = true
                    cell.imageViewObjCollectionview.isHidden = true
                    cell.mainWebViewObj.isHidden = true
                    cell.webViewBtnTapped.isHidden = true
                    self.youtubeAppendStr = "\(YOUTUBE_BASEURL)\(self.imageorPDFstr)" as NSString
                    let requestURL = URL(string: self.youtubeAppendStr as String)
                    let request = URLRequest(url: requestURL!)
                    cell.youtubeWebViewObj.scrollView.isScrollEnabled = false
                    cell.youtubeWebViewObj.scrollView.bounces = false 
                    cell.youtubeWebViewObj.loadRequest(request)
                }
                
                if finalFileNameextension == "pdf"
                {
                    cell.newsImageBtnTapped.isHidden = true
                    cell.imageViewObjCollectionview.isHidden = true
                    cell.mainWebViewObj.isHidden = false
                    cell.webViewBtnTapped.isHidden = false
                    self.appendStringPDF = "\(IMAGE_UPLOAD) \(imageorPDFstr)" as NSString
                    cell.webViewBtnTapped.addTarget(self, action: #selector(self.webViewBtntapped), for: .touchUpInside)
                }
                if finalFileNameextension == "jpeg" {
                    
                    cell.newsImageBtnTapped.isHidden = false
                    cell.imageViewObjCollectionview.isHidden = false
                    cell.mainWebViewObj.isHidden = true
                    cell.webViewBtnTapped.isHidden = true
                    
                    
                    let imageURLstr1 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[indexPath.row] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                    
                    let url = URL(string: imageURLstr1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                    
                    //let url = URL(string: IMAGE_UPLOAD.appending(((self.imageArray[indexPath.row] as AnyObject).value(forKey: "image_name") as? String!)!))
                    cell.btnHEightconst.constant = 40
                    cell.webViewBtnTapped.layer.borderWidth = 1
                    cell.webViewBtnTapped.layer.borderColor = UIColor.blue.cgColor
                    cell.mainWebViewObj.layer.borderWidth = 1
                    cell.mainWebViewObj.layer.borderColor = UIColor.init(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).cgColor
                    
                    
                    
                    
                    if url == nil {
                        
                    }
                    else
                    {
                        
                        cell.imageViewObjCollectionview.kf.indicatorType = .activity
                        cell.imageViewObjCollectionview.kf.setImage(with: url,placeholder: nil,
                                                                    options: [.transition(ImageTransition.fade(1))],
                                                                    progressBlock: { receivedSize, totalSize in
                                                                      //  print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                        },
                                                                    completionHandler: { image, error, cacheType, imageURL in
                                                                      //  print("\(indexPath.row + 1): Finished")
                        })
                        
                        /* cell.imageViewObjCollectionview.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                         
                         if cell.imageViewObjCollectionview.image != nil
                         {
                         FirstprogressIcon.stopAnimating()
                         FirstprogressIcon.hidesWhenStopped = true
                         
                         }
                         else
                         {
                         FirstprogressIcon.hidesWhenStopped = false
                         
                         }
                         })*/
                        
                    }
                    
                }
                
                if finalFileNameextension == "png" {
                    
                    cell.newsImageBtnTapped.isHidden = false
                    cell.imageViewObjCollectionview.isHidden = false
                    cell.mainWebViewObj.isHidden = true
                    cell.webViewBtnTapped.isHidden = true
                    let imageURLstr1 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[indexPath.row] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                    
                    let url = URL(string: imageURLstr1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                    
                    cell.btnHEightconst.constant = 40
                    cell.webViewBtnTapped.layer.borderWidth = 1
                    cell.webViewBtnTapped.layer.borderColor = UIColor.blue.cgColor
                    cell.mainWebViewObj.layer.borderWidth = 1
                    cell.mainWebViewObj.layer.borderColor = UIColor.init(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).cgColor
                    
                    if url == nil {
                        
                    }
                    else
                    {
                        
                        cell.imageViewObjCollectionview.kf.indicatorType = .activity
                        cell.imageViewObjCollectionview.kf.setImage(with: url,placeholder: nil,
                                                                    options: [.transition(ImageTransition.fade(1))],
                                                                    progressBlock: { receivedSize, totalSize in
                                                                      //  print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                        },
                                                                    completionHandler: { image, error, cacheType, imageURL in
                                                                      //  print("\(indexPath.row + 1): Finished")
                        })
                        
                        /* cell.imageViewObjCollectionview.sd_setImage(with: url!, placeholderImage: UIImage(named: "verdentumPlaceholder"),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                         if cell.imageViewObjCollectionview.image != nil
                         {
                         FirstprogressIcon.stopAnimating()
                         FirstprogressIcon.hidesWhenStopped = true
                         
                         }
                         else
                         {
                         FirstprogressIcon.hidesWhenStopped = false
                         
                         }
                         })*/
                        
                    }
                }
                if finalFileNameextension == "PNG" {
                    cell.newsImageBtnTapped.isHidden = false
                    cell.imageViewObjCollectionview.isHidden = false
                    cell.mainWebViewObj.isHidden = true
                    cell.webViewBtnTapped.isHidden = true
                    let imageURLstr1 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[indexPath.row] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                    
                    let url = URL(string: imageURLstr1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                    
                    cell.btnHEightconst.constant = 40
                    cell.webViewBtnTapped.layer.borderWidth = 1
                    cell.webViewBtnTapped.layer.borderColor = UIColor.blue.cgColor
                    cell.mainWebViewObj.layer.borderWidth = 1
                    cell.mainWebViewObj.layer.borderColor = UIColor.init(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).cgColor
                    if url == nil {
                    }
                    else
                    {
                        cell.imageViewObjCollectionview.kf.indicatorType = .activity
                        cell.imageViewObjCollectionview.kf.setImage(with: url,placeholder: nil,
                                                                    options: [.transition(ImageTransition.fade(1))],
                                                                    progressBlock: { receivedSize, totalSize in
                                                                       // print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                        },
                                                                    completionHandler: { image, error, cacheType, imageURL in
                                                                       // print("\(indexPath.row + 1): Finished")
                        })
                        
                        
                        /* cell.imageViewObjCollectionview.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                         if cell.imageViewObjCollectionview.image != nil
                         {
                         FirstprogressIcon.stopAnimating()
                         FirstprogressIcon.hidesWhenStopped = true
                         
                         }
                         else
                         {
                         FirstprogressIcon.hidesWhenStopped = false
                         
                         }
                         })*/
                        
                    }
                }
                if finalFileNameextension == "jpg" {
                    cell.newsImageBtnTapped.isHidden = false
                    cell.imageViewObjCollectionview.isHidden = false
                    cell.mainWebViewObj.isHidden = true
                    cell.webViewBtnTapped.isHidden = true
                    let imageURLstr1 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[indexPath.row] as AnyObject).value(forKey: "image_name") as? String!)!) as NSString
                    
                    let url = URL(string: imageURLstr1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                    
                    cell.btnHEightconst.constant = 40
                    cell.webViewBtnTapped.layer.borderWidth = 1
                    cell.webViewBtnTapped.layer.borderColor = UIColor.blue.cgColor
                    cell.mainWebViewObj.layer.borderWidth = 1
                    cell.mainWebViewObj.layer.borderColor = UIColor.init(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).cgColor
                    if url == nil {
                        
                    }
                    else
                    {
                        
                        cell.imageViewObjCollectionview.kf.indicatorType = .activity
                        cell.imageViewObjCollectionview.kf.setImage(with: url,placeholder: nil,
                                                                    options: [.transition(ImageTransition.fade(1))],
                                                                    progressBlock: { receivedSize, totalSize in
                                                                      //  print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
                        },
                                                                    completionHandler: { image, error, cacheType, imageURL in
                                                                       // print("\(indexPath.row + 1): Finished")
                        })
                        
                        
                        /*cell.imageViewObjCollectionview.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                         if cell.imageViewObjCollectionview.image != nil
                         {
                         FirstprogressIcon.stopAnimating()
                         FirstprogressIcon.hidesWhenStopped = true
                         }
                         else
                         {
                         FirstprogressIcon.hidesWhenStopped = false
                         }
                         })*/
                        
                    }
                }
                else
                {
                    
                }
                return cell
            }
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsImagesCell", for: indexPath)as! NewsImagesCell
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if screenWidth == 320
        {
            return CGSize(width: CGFloat(280.0), height: CGFloat(227.0))
        }
        if screenWidth == 375
        {
            return CGSize(width: CGFloat(310.0), height: CGFloat(227.0))
        }
        if screenWidth == 414
        {
            return CGSize(width: CGFloat(374.0), height: CGFloat(227.0))
        }
        if screenWidth == 768
        {
            return CGSize(width: CGFloat(700.0), height: CGFloat(227.0))
        }
        if screenWidth == 834.0
        {
            return CGSize(width: CGFloat(794.0), height: CGFloat(227.0))
        }
        if screenWidth == 1024
        {
            return CGSize(width: CGFloat(984.0), height: CGFloat(227.0))
        }
        return CGSize.zero
    }

    
    
    
     // MARK: - UIButton Actions
    
    func buttonClicked(_ sender: UIButton)
    {
        let notificationName = Notification.Name("didSelectItemFromCollectionView")
        cellString = ((self.imageArray[0] as AnyObject).value(forKey: "image_name") as AnyObject) as! NSString
        NotificationCenter.default.post(name: notificationName, object: cellString)
    }
    
    func firstimageSecondBtn(_ sender: UIButton) {
        let notificationName = Notification.Name("didSelectItemFromCollectionView")
        cellString = ((self.imageArray[0] as AnyObject).value(forKey: "image_name") as AnyObject) as! NSString
        NotificationCenter.default.post(name: notificationName, object: cellString)
    }
    
    func secondimageSecondBtn(_ sender: UIButton) {
        let notificationName = Notification.Name("didSelectItemFromCollectionView")
        cellString = ((self.imageArray[1] as AnyObject).value(forKey: "image_name") as AnyObject) as! NSString
        NotificationCenter.default.post(name: notificationName, object: cellString)
    }
    
    func thirdImageThreeBtn(_ sender: UIButton) {
        let notificationName = Notification.Name("didSelectItemFromCollectionView")
        cellString = ((self.imageArray[2] as AnyObject).value(forKey: "image_name") as AnyObject) as! NSString
        NotificationCenter.default.post(name: notificationName, object: cellString)
    }
    
    func fourthImageFourBtn(_ sender: UIButton) {
        let notificationName = Notification.Name("didSelectItemFromCollectionView")
        cellString = ((self.imageArray[3] as AnyObject).value(forKey: "image_name") as AnyObject) as! NSString
        NotificationCenter.default.post(name: notificationName, object: cellString)
    }
    
    func fivthimageBtn(_ sender: UIButton) {
        let notificationName = Notification.Name("didSelectItemFromCollectionView")
        cellString = ((self.imageArray[4] as AnyObject).value(forKey: "image_name") as AnyObject) as! NSString
        NotificationCenter.default.post(name: notificationName, object: cellString)
    }
    func webViewBtntapped(_ sender: UIButton) {
        let notificationName = Notification.Name("didSelectItemFromCollectionViewPDF")
        cellString = self.imageorPDFstr
        NotificationCenter.default.post(name: notificationName, object: cellString)
    }
    
}

extension UICollectionViewFlowLayout {
    
    var collectionViewWidthWithoutInsets: CGFloat {
        get {
            guard let collectionView = self.collectionView else { return 0 }
            let collectionViewSize = collectionView.bounds.size
            let widthWithoutInsets = collectionViewSize.width
                - self.sectionInset.left - self.sectionInset.right
                - collectionView.contentInset.left - collectionView.contentInset.right
            return widthWithoutInsets
        }
    }
}

class StickyHeaderCollectionViewLayout: UICollectionViewFlowLayout {
    
    // MARK: - Variables
    let cellAspectRatio: CGFloat = 3/1
    
    // MARK: - Layout
    override func prepare() {
        self.scrollDirection = .vertical
        self.minimumInteritemSpacing = 1
        self.minimumLineSpacing = 1
        self.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: self.minimumLineSpacing, right: 0)
        let collectionViewWidth = self.collectionView?.bounds.size.width ?? 0
        self.headerReferenceSize = CGSize(width: collectionViewWidth, height: 40)
        
        // cell size
        let itemWidth = collectionViewWidthWithoutInsets
        self.itemSize = CGSize(width: itemWidth, height: itemWidth/cellAspectRatio)
        
        // Note: call super last if we set itemSize
        super.prepare()
    }
    
    // ...
    
}
