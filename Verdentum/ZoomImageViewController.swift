//
//  ZoomImageViewController.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 31/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import SDWebImage
import AlamofireImage
import Alamofire

class ZoomImageViewController: UIViewController,UIScrollViewDelegate,UIGestureRecognizerDelegate {

    var newImageURlStr = NSString()
    var pdfStr = NSString()
    
    @IBOutlet var imageViewZoom: UIImageView!
    
    @IBOutlet var scrollViewZoomObj: UIScrollView!
    
    @IBOutlet var webViewOBj: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
      //  self.imageViewZoom.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        let fileArray = self.newImageURlStr.components(separatedBy: ".")
        let finalFileNameextension = fileArray.last
        
        if finalFileNameextension == "pdf"
        {
            self.webViewOBj.scalesPageToFit = true
            let url = URL(string: IMAGE_UPLOAD.appending(self.newImageURlStr as String))
           
            self.imageViewZoom.isHidden = true
            let urlRequest = URLRequest(url: url!)
            
            self.webViewOBj.loadRequest(urlRequest)
            let pgr = UIPinchGestureRecognizer(target: self, action: #selector(self.handlePinch))
            pgr.delegate = self
            self.webViewOBj.addGestureRecognizer(pgr)
        }
        else
        {
            self.imageViewZoom.isHidden = false
            let url = URL(string: IMAGE_UPLOAD.appending(self.newImageURlStr as String))
            self.imageViewZoom.sd_setImage(with: url!, placeholderImage: UIImage(named: "verdentumPlaceholder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
                
            })
            
            
            setZoomScale()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func handlePinch(_ recognizer: UIPinchGestureRecognizer) {
        recognizer.view?.transform = (recognizer.view?.transform.scaledBy(x: recognizer.scale, y: recognizer.scale))!
        recognizer.scale = 1
    }
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.imageViewZoom
    }
    
    
    func setZoomScale()
    {
        
        self.scrollViewZoomObj.minimumZoomScale = 1.0
        self.scrollViewZoomObj.maximumZoomScale = 6.0
    }
    
   
    

    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
}
