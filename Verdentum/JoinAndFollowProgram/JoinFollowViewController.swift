//
//  JoinFollowViewController.swift
//  Verdentum
//
//  Created by Verdentum on 10/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class JoinFollowViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var tableViewObj: UITableView!
    
    @IBOutlet var programLabelTitle: UILabel!
    var programArray = NSMutableArray()
    var followedstr = NSString()
    var requestedStr = NSString()
    var program_idstr = NSString()
    var joiningStr = NSString()
    var MyteamArray = NSMutableArray()
    var programTeamArray = NSMutableArray()
    var titleStr = NSString()
     var responsePostArray = NSMutableArray()
     var lastProgramIDStr = NSString()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(self.programArray)
        self.programLabelTitle.text = self.titleStr as String
        
        self.tableViewObj.estimatedRowHeight = 70
        self.tableViewObj.rowHeight = UITableViewAutomaticDimension
        self.tableViewObj .register(UINib (nibName: "DetailedProgramCell", bundle: nil), forCellReuseIdentifier: "DetailedProgramCell")
        self.FollowProgramDetailsServiceApiCalling()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
        
    }

    //MARK: - Follow/Program APi
    
    func FollowProgramDetailsServiceApiCalling() {
        
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "getteams" as AnyObject,
                "id" : userID as AnyObject,
                "progId" : self.program_idstr as AnyObject]
            let url = NSURL(string:BaseURL_GetTeamAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let requestStrNO = (responsejson["requested"] as AnyObject) as! NSNumber
                                self.requestedStr = requestStrNO .stringValue as NSString
                               // self.programTeamArray = ((responsejson["progteams"] as AnyObject) as! NSMutableArray)
                                
                               // self.MyteamArray = ((responsejson["myteams"] as AnyObject) as! NSMutableArray)
                                
                                self.tableViewObj.reloadData()
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.programArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView .dequeueReusableCell(withIdentifier: "DetailedProgramCell") as! DetailedProgramCell
         let url = URL(string: IMAGE_UPLOAD.appending(((self.programArray[indexPath.row] as AnyObject).value(forKey: "prog_banner") as? String!)!))
        
        cell.imageViewProgramObj.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
        
        })
        
        let url1 = URL(string: IMAGE_UPLOAD.appending(((self.programArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!))
        
        cell.imageViewLogoObj.sd_setImage(with: url1!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
            
        })
        cell.imageViewLogoObj.layer.borderWidth = 1.0
        cell.imageViewLogoObj.layer.borderColor = UIColor.lightGray.cgColor
        cell.imageViewLogoObj.layer.cornerRadius = cell.imageViewLogoObj.frame.size.width / 2;
        cell.imageViewLogoObj.layer.masksToBounds = true
        
        cell.NameLabel.text = (self.programArray[indexPath.row] as AnyObject).value(forKey: "pm_organization") as? String
        
        cell.aboutLabelProgram.text = (self.programArray[indexPath.row] as AnyObject).value(forKey: "prog_description") as? String
        cell.programObjectLabel.text = (self.programArray[indexPath.row] as AnyObject).value(forKey: "prog_objective") as? String
        
        let followCheckNo : NSNumber = (self.programArray[indexPath.row] as! NSObject).value(forKey: "following") as! NSNumber
        
        let followCheckStr : NSString = followCheckNo .stringValue as NSString
        
        
        if followCheckStr .isEqual(to: "1") && self.followedstr .isEqual(to: "false") {
            cell.followProgramBtnheight.constant = 0
            cell.joinBottomConstraint.constant = 30
        }
        else
        {
            cell.followProgramBtnheight.constant = 30
            cell.followBtnTapped.tag = indexPath.row
            cell.followBtnTapped.addTarget(self, action: #selector(self.followBtnTapped), for: .touchUpInside)
            cell.joinBottomConstraint.constant = 30
        }
        
       // let joiningNO : NSNumber = (self.programArray[indexPath.row] as! NSObject).value(forKey: "joining") as! NSNumber
        self.joiningStr = (self.programArray[indexPath.row] as AnyObject).value(forKey: "joining") as! NSString
        
        if self.requestedStr == "0"
        {
            cell.joinBtnTapped.setTitle("Join Program", for: .normal)
            cell.joinBtnTapped.backgroundColor = UIColor.init(red: 146/255.0, green: 208/255.0, blue: 78/255.0, alpha: 1.0)
            cell.joinBtnTapped.isEnabled = true
        }
        else
        {
            cell.joinBtnTapped.setTitle("Request Sent To Join Program", for: .normal)
            cell.joinBtnTapped.backgroundColor = UIColor.lightGray
            cell.joinBtnTapped.isEnabled = false
        }
        
        
//        if !(self.requestedStr .isEqual(to: "0")) {
//
//            cell.joinBtnTapped.setTitle("Request Sent To Join Program", for: .normal)
//            cell.joinBtnTapped.backgroundColor = UIColor.lightGray
//            cell.joinBtnTapped.isEnabled = false
//        }
         if self.joiningStr .isEqual(to: "2") && self.requestedStr .isEqual(to: "0")
        {
            cell.joinBtnTapped.tag = indexPath.row
            cell.joinBtnTapped.addTarget(self, action: #selector(self.joinBtnTapped), for: .touchUpInside)
             cell.joinBtnTapped.isEnabled = true
        }
        if self.joiningStr .isEqual(to: "1") && self.requestedStr .isEqual(to: "0")
        {
            cell.joinBtnTapped.tag = indexPath.row
            cell.joinBtnTapped.addTarget(self, action: #selector(self.createinditeams), for: .touchUpInside)
             cell.joinBtnTapped.isEnabled = true
        }
        
        
        return cell
    }
    
    func joinBtnTapped(_ sender: UIButton) {
        let mainViewController = self.sideMenuController!
        let MyTeamsvc = MyTeamsViewController()
        MyTeamsvc.programArray = self.programArray
        MyTeamsvc.followedstr = self.followedstr
        MyTeamsvc.titleStr = self.titleStr
       // MyTeamsvc.MyteamArray = self.MyteamArray
        MyTeamsvc.program_idstr = self.program_idstr
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(MyTeamsvc, animated: true)
    }
    
     func createinditeams(_ sender: UIButton) {
        let mainViewController = self.sideMenuController!
        let CommentsVC = JoinProgramViewController()
        CommentsVC.program_idstr = self.program_idstr
         CommentsVC.programArray = self.programArray
        CommentsVC.followedstr = self.followedstr
        CommentsVC.titleStr = self.titleStr
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
    
    func followBtnTapped(_ sender: UIButton) {
        let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "followprogram" as AnyObject,
            "id" : userID as AnyObject,
            "prog_id" : self.program_idstr as AnyObject]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.followApiCalling(param: params)
    }
    
    func followApiCalling(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
            
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_FollowBtnAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            let successMsgstr : NSString = (responsejson["msg"] as AnyObject) as! NSString
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.presentAlertWithTitle(title: "", message:successMsgstr as String)
                               // self.FollowProgramDetailsServiceApiCalling()
                                 self.FollowProgramServiceApiCalling()
                                //self.tableViewObj.reloadData()
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    //MARK: - Follow/Program APi
    
    func FollowProgramServiceApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "todaysprogram" as AnyObject,
                "id" : userID as AnyObject ]
            let url = NSURL(string:BaseURL_FollowProgramAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.responsePostArray = (responsejson["programs"] as AnyObject) as! NSMutableArray
                                self.programArray.removeAllObjects()
                               
                                let predicate = NSPredicate(format:"prog_id==%@",self.program_idstr)
                                let arr : NSArray = self.responsePostArray.filtered(using: predicate) as NSArray
                                
                                if arr.count > 0
                                {
                                    for item in arr {
                                        
                                        self.programArray.add(item)
                                    }
                                    print(self.programArray)
                                }
                                else
                                {
                                    let postarray : NSArray = self.responsePostArray .value(forKey: "prog_id") as! NSArray
                                    self.lastProgramIDStr = postarray.lastObject as! NSString
                                    let userID = UserDefaults.standard.value(forKey: "userID")
                                    let params:[String: AnyObject] = [
                                        "purpose" : "todaysprogram" as AnyObject,
                                        "id" : userID as AnyObject,
                                        "direction" : "down" as AnyObject,
                                        "limit" : self.lastProgramIDStr as AnyObject]
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    self.postsApiCalling(param: params)
                                }
                                self.tableViewObj.reloadData()
                                
                            }
                            else
                            {
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    
    
    //MARK: - SecondServiceApiCalling
    
    func postsApiCalling(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_FollowProgramAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let arra  = (responsejson["programs"] as AnyObject) as! NSArray
                                if arra.count == 0
                                {
                                    
                                }
                                else
                                {
                                    self.responsePostArray.addObjects(from: arra as! [Any])
                                    self.programArray.removeAllObjects()
                                    
                                    let predicate = NSPredicate(format:"prog_id==%@",self.program_idstr)
                                    let arr : NSArray = self.self.responsePostArray.filtered(using: predicate) as NSArray
                                    if arr.count > 0
                                    {
                                        for item in arr {
                                            
                                            self.programArray.add(item)
                                        }
                                        print(self.programArray)
                                    }
                                    else
                                    {
                                        let postarray : NSArray = self.responsePostArray .value(forKey: "prog_id") as! NSArray
                                        self.lastProgramIDStr = postarray.lastObject as! NSString
                                        let userID = UserDefaults.standard.value(forKey: "userID")
                                        let params:[String: AnyObject] = [
                                            "purpose" : "todaysprogram" as AnyObject,
                                            "id" : userID as AnyObject,
                                            "direction" : "down" as AnyObject,
                                            "limit" : self.lastProgramIDStr as AnyObject]
                                        MBProgressHUD.showAdded(to: self.view, animated: true)
                                        self.postsApiCalling(param: params)
                                    }
                                    self.tableViewObj.reloadData()
                                }
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0){
                                
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }

    @IBAction func backBtnTapped(_ sender: Any) {
        let mainViewController = self.sideMenuController!
        let CommentsVC = FollowProgramViewController()
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
        
    }
}
