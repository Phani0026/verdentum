//
//  MyTeamsViewController.swift
//  Verdentum
//
//  Created by Verdentum on 12/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class MyTeamsViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var MyteamArray = NSArray()
    var programTeamArray = NSMutableArray()
    var sectionHeaderArray = ["Join Through Other Group","Join Through Your Group"]
    var program_idstr = NSString()
    var programArray = NSMutableArray()
    var headerId = "headerView"
    var followedstr = NSString()
    var titleStr = NSString()
    var StatuStr = NSString()
    @IBOutlet var collectionViewObj: UICollectionView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(self.programTeamArray)
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        if #available(iOS 9.0, *) {
            layout.sectionHeadersPinToVisibleBounds = true
        } else {
            // Fallback on earlier versions
        }
        layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        layout.itemSize = CGSize(width: 100, height: 168)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        self.collectionViewObj!.collectionViewLayout = layout
        self.FollowProgramDetailsServiceApiCalling()
        
        self.collectionViewObj.register(UINib(nibName: "TeamCell", bundle: nil), forCellWithReuseIdentifier: "TeamCell")
        self.collectionViewObj.register(UINib(nibName: "MyGroupCell", bundle: nil), forCellWithReuseIdentifier: "MyGroupCell")
        
        self.collectionViewObj.register(UINib(nibName: "headerView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Follow/Program APi
    
    func FollowProgramDetailsServiceApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "getteams" as AnyObject,
                "id" : userID as AnyObject,
                "progId" : self.program_idstr as AnyObject]
            let url = NSURL(string:BaseURL_GetTeamAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                
                                self.programTeamArray = ((responsejson["progteams"] as AnyObject) as! NSMutableArray)
                                if self.programTeamArray.count == 0 {
                                    self.StatuStr = "You have not created any program."
                                    self.collectionViewObj.reloadData()
                                } else {
                                   self.collectionViewObj.reloadData()
                                }
                                self.MyteamArray = ((responsejson["myteams"] as AnyObject) as! NSArray)
                                if self.MyteamArray.count == 0 {
                                    self.StatuStr = "You have not created any teams."
                                    self.collectionViewObj.reloadData()

                                } else {
                                   self.collectionViewObj.reloadData()
                                    
                                }                                
                            }
                            else
                            {
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    //MARK: - UICollectionview Delegate and Datasource
    // number of sections is 2. Section above search and below search
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    //number of items for each section. Section above search will have only one and below search will be dynamic as per images we have
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return self.programTeamArray.count
        }
        if section == 1 {
           
            return self.MyteamArray.count
        }
        return 0
    }
    
    // cell for item at given indexpath: if section is 0 then return cell above search, if section is 1 then return cell below search
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeamCell", for: indexPath)as! TeamCell
            
            cell.layer.borderColor = UIColor.black.cgColor
            cell.layer.borderWidth = 1
            
            cell.nameObj.text = (self.programTeamArray[indexPath.row] as AnyObject) .value(forKey: "group_name") as? String
            
            let url = URL(string: IMAGE_UPLOAD.appending(((self.programTeamArray[indexPath.row] as AnyObject) .value(forKey: "group_banner") as? String!)!))
            
            cell.imageViewObj.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                
            })
            cell.joinTapped.tag = indexPath.row
            cell.joinTapped.addTarget(self, action: #selector(self.joinTapped), for: .touchUpInside)
            
            return cell
        }
        if indexPath.section == 1 {
            // below search cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyGroupCell", for: indexPath)as! MyGroupCell
            if self.MyteamArray.count == 0
            {
                let rect = CGRect(x: 0,
                                  y: 0,
                                  width: view.bounds.size.width,
                                  height: view.bounds.size.height)
                let noDataLabel: UILabel = UILabel(frame: rect)
                
                noDataLabel.text = self.StatuStr as String
                noDataLabel.textColor = UIColor.black
                noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
                noDataLabel.numberOfLines = 0
                noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                noDataLabel.sizeToFit()
                noDataLabel.textAlignment = NSTextAlignment.center
                self.collectionViewObj.backgroundView = noDataLabel
            }
            else
            {
                var borderColor: CGColor! = UIColor.lightGray.cgColor
                var borderWidth: CGFloat = 0
                borderColor = UIColor.brown.cgColor
                borderWidth = 1 //or whatever you please
                cell.layer.borderColor = borderColor
                cell.layer.borderWidth = borderWidth
                cell.nameObj.text = (self.MyteamArray[indexPath.row] as AnyObject) .value(forKey: "group_name") as? String
                let url = URL(string: IMAGE_UPLOAD.appending(((self.MyteamArray[indexPath.row] as AnyObject) .value(forKey: "group_banner") as? String!)!))
                cell.imageViewObj.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.progressiveDownload, completed: { (image, error, cacheType, imageURL) in
                })
                cell.joinTapped.tag = indexPath.row
                cell.joinTapped.addTarget(self, action: #selector(self.joinMyProgramTapped), for: .touchUpInside)
            }
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyGroupCell", for: indexPath)as! MyGroupCell
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            let Selected_GroupID : NSString = ((self.programTeamArray[indexPath.row] as AnyObject) .value(forKey: "group_id") as? NSString)!
            let Selected_GroupName : NSString = ((self.programTeamArray[indexPath.row] as AnyObject) .value(forKey: "group_name") as? NSString)!
            let Selected_GroupImage : NSString = ((self.programTeamArray[indexPath.row] as AnyObject) .value(forKey: "group_banner") as? NSString)!
            
            let mainViewController = self.sideMenuController!
            let CommentsVC = GroupNewsViewController()
            CommentsVC.groupIDStr = Selected_GroupID
            CommentsVC.groupImageStr = Selected_GroupImage
            CommentsVC.headerNameStr = Selected_GroupName
            CommentsVC.MyteamArray = self.MyteamArray
            CommentsVC.programTeamArray = self.programTeamArray
            CommentsVC.program_idstr = self.program_idstr
            CommentsVC.programArray = self.programArray
            CommentsVC.navStr = "joinProgram"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(CommentsVC, animated: true)
        }
        if indexPath.section == 1 {
            
            let Selected_GroupID : NSString = ((self.MyteamArray[indexPath.row] as AnyObject) .value(forKey: "group_id") as? NSString)!
            let Selected_GroupName : NSString = ((self.MyteamArray[indexPath.row] as AnyObject) .value(forKey: "group_name") as? NSString)!
            let Selected_GroupImage : NSString = ((self.MyteamArray[indexPath.row] as AnyObject) .value(forKey: "group_banner") as? NSString)!
            
            let mainViewController = self.sideMenuController!
            let CommentsVC = GroupNewsViewController()
            CommentsVC.groupIDStr = Selected_GroupID
            CommentsVC.groupImageStr = Selected_GroupImage
            CommentsVC.headerNameStr = Selected_GroupName
            CommentsVC.MyteamArray = self.MyteamArray
            CommentsVC.programTeamArray = self.programTeamArray
            CommentsVC.program_idstr = self.program_idstr
            CommentsVC.programArray = self.programArray
             CommentsVC.navStr = "joinProgram"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(CommentsVC, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            let headerViewcell = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: headerId,for: indexPath) as! headerView
            
            
            let headerStr : NSString = self.sectionHeaderArray[indexPath.section] as NSString
            
            headerViewcell.headerLabelOBj.text = headerStr as String as String
            
            return headerViewcell
            
        default:
            
            fatalError("Unexpected element kind")
        }
    }
    
    // size for header in section: since we have 2 sections, collectionView will ask size for header for both sections so we make section header of first section with height 0 and width 0 so it remains like invisible.
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        // if section is above search bar we need to make its height 0
        //        if section == 0 {
        //            return CGSize(width: 0, height: 0)
        //        }
        // for section header i.e. actual search bar
        return CGSize(width: collectionView.frame.width, height: 50)
    }
    
    //MARK: - UIButton Actions
    
    func joinMyProgramTapped(_ sender: UIButton) {
        
        // let gr_minNO : Int = (self.MyteamArray[sender.tag] as! NSObject) .value(forKey: "gr_min") as! Int
        let groupMyTeamID : NSString = (self.MyteamArray .object(at: sender.tag) as AnyObject) .value(forKey: "group_id") as! NSString
        let gr_minStr : NSString = (self.MyteamArray[sender.tag] as! NSObject) .value(forKey: "gr_min") as! NSString
        
        let message1 : NSString = "You need atleast"
        let message2 : NSString = "members to join this program."
        let messageStr : NSString = "\(message1) \(gr_minStr) \(message2)" as NSString
        
        let statuNo : NSString = (self.MyteamArray[sender.tag] as! NSObject) .value(forKey: "status") as! NSString
        
        let statuStr : NSString = statuNo as NSString
        
        if statuStr .isEqual(to: "0")
        {
            self.presentAlertWithTitle(title: "", message:messageStr as String)
        }
        else
        {
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "joinprogram" as AnyObject,
                "id" : userID as AnyObject,
                "grid" : groupMyTeamID as AnyObject,
                "progId" : self.program_idstr as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.joinMyProgramApiCalling(param: params)
        }
        
        
    }
    func joinMyProgramApiCalling(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_JoinMyProgramAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            let successMsgstr : NSString = (responsejson["msg"] as AnyObject) as! NSString
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.presentAlertWithTitle(title: "", message:successMsgstr as String)
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let alertController = UIAlertController(title: "", message: successMsgstr as String , preferredStyle: .alert)
                               
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = MyProgramsViewController()
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    func joinTapped(_ sender: UIButton) {
        let userID = UserDefaults.standard.value(forKey: "userID")
        let groupID : NSString = (self.programTeamArray .object(at: sender.tag) as AnyObject) .value(forKey: "group_id") as! NSString
        let params:[String: AnyObject] = [
            "purpose" : "joingroup" as AnyObject,
            "id" : userID as AnyObject,
            "grid" : groupID as AnyObject]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.joinProgramApiCalling(param: params)
    }
    
    func joinProgramApiCalling(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_JoinOtherProgramAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            let successMsgstr : NSString = (responsejson["msg"] as AnyObject) as! NSString
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.presentAlertWithTitle(title: "", message:successMsgstr as String)
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let mainViewController = self.sideMenuController!
                                let CommentsVC = MyProgramsViewController()
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(CommentsVC, animated: true)
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        let mainViewController = self.sideMenuController!
        let CommentsVC = JoinFollowViewController()
        CommentsVC.program_idstr = self.program_idstr
        CommentsVC.programArray = programArray
        CommentsVC.followedstr = self.followedstr
        CommentsVC.titleStr = self.titleStr
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
}

