//
//  FollowProgramViewController.swift
//  Verdentum
//
//  Created by Verdentum on 11/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD

class FollowProgramViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var responsePostArray = NSMutableArray()
    var usernameobjstr = NSString()
    var lastProgramIDStr = NSString()
      var refreshControl = UIRefreshControl()
    var verticalOffset: Int = 0
     var StatuStr = NSString()
    @IBOutlet var tableViewProgramObj: UITableView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewProgramObj.estimatedRowHeight = 70
        self.tableViewProgramObj.rowHeight = UITableViewAutomaticDimension
        
        // -----Refresh Control----
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(DashBoardViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        
        self.tableViewProgramObj.addSubview(refreshControl)
        
        self.tableViewProgramObj .register(UINib (nibName: "ProgramFollowCell", bundle: nil), forCellReuseIdentifier: "ProgramFollowCell")
        
        self.FollowProgramServiceApiCalling()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh(_ sender:AnyObject)
    {
        FollowProgramServiceApiCalling()
        // self.newTableViewObj.reloadData()
        self.refreshControl .endRefreshing()
    }
    
    //MARK: - Follow/Program APi
    
    func FollowProgramServiceApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "todaysprogram" as AnyObject,
                "id" : userID as AnyObject ]
            let url = NSURL(string:BaseURL_FollowProgramAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.responsePostArray = (responsejson["programs"] as AnyObject) as! NSArray as! NSMutableArray
                                
                                if self.responsePostArray.count == 0 {
                                    self.StatuStr = "No programs found... Please check back later"
                                    self.tableViewProgramObj.reloadData()
                                }
                                else {
                                   
                                    self.tableViewProgramObj.reloadData()
                                }
                                
                            }
                            else
                            {
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.responsePostArray.count == 0 {
            let rect = CGRect(x: 0,
                              y: 0,
                              width: view.bounds.size.width,
                              height: view.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            
            noDataLabel.text = self.StatuStr as String
            noDataLabel.textColor = UIColor.black
            noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            noDataLabel.sizeToFit()
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableViewProgramObj.backgroundView = noDataLabel
            
            return 0
        } else {
           return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.responsePostArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView .dequeueReusableCell(withIdentifier: "ProgramFollowCell") as! ProgramFollowCell
        
        let titlename: String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_lname") as! String
        
        
        self.usernameobjstr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        
        cell.descriptionLabel.text =  usernameobjstr as String
        
        cell.nameLabel.text = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "prog_name") as? String
        return cell        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let selectedProgramstr : NSDictionary = (self.responsePostArray[indexPath.row] as AnyObject) as! NSDictionary
        
        self.verticalOffset = indexPath.row
        
        let mainViewController = self.sideMenuController!
        let CommentsVC = JoinFollowViewController()
        CommentsVC.programArray.removeAllObjects()
          CommentsVC.programArray.add(selectedProgramstr)
        CommentsVC.followedstr = "false"
        CommentsVC.requestedStr = "1"
       
        CommentsVC.titleStr = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "prog_name") as! NSString
        CommentsVC.program_idstr = (self.responsePostArray[indexPath.row] as! NSObject)  .value(forKey: "prog_id") as! NSString
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
    
    func tableView(_ tableView:UITableView, willDisplay cell:UITableViewCell, forRowAt indexPath:IndexPath)
    {
        if indexPath.row == (self.responsePostArray.count - 1)
        {
            
            let postarray : NSArray = self.responsePostArray .value(forKey: "prog_id") as! NSArray
            self.lastProgramIDStr = postarray.lastObject as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "todaysprogram" as AnyObject,
                "id" : userID as AnyObject,
                "direction" : "down" as AnyObject,
                "limit" : self.lastProgramIDStr as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.postsApiCalling(param: params)
            
        }
    }
    
    //MARK: - SecondServiceApiCalling
    
    func postsApiCalling(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
            
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_FollowProgramAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let arra  = (responsejson["programs"] as AnyObject) as! NSArray
                                if arra.count == 0
                                {
                                    
                                }
                                else
                                {
                                    self.responsePostArray.addObjects(from: arra as! [Any])
                                    self.tableViewProgramObj.reloadData()
                                }
                                
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    @IBAction func sideMenuBtn(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
}
