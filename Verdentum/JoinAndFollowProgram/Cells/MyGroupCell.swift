//
//  MyGroupCell.swift
//  Verdentum
//
//  Created by Verdentum on 12/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class MyGroupCell: UICollectionViewCell {

    @IBOutlet var joinTapped: UIButton!
    @IBOutlet var nameObj: UILabel!
    @IBOutlet var imageViewObj: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
