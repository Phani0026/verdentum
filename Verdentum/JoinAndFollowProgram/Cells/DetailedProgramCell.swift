//
//  DetailedProgramCell.swift
//  Verdentum
//
//  Created by Verdentum on 10/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class DetailedProgramCell: UITableViewCell {

    
    @IBOutlet var NameLabel: UILabel!
    
    @IBOutlet var imageViewProgramObj: UIImageView!
    
    @IBOutlet var joinProgramBtnHeight: NSLayoutConstraint!
    @IBOutlet var followProgramBtnheight: NSLayoutConstraint!
    @IBOutlet var imageViewLogoObj: UIImageView!
    
    @IBOutlet var aboutLabelProgram: UILabel!
    
    @IBOutlet var joinBottomConstraint: NSLayoutConstraint!
    @IBOutlet var programObjectLabel: UILabel!
    
    @IBOutlet var followBtnTapped: UIButton!
    
    @IBOutlet var joinBtnTapped: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
