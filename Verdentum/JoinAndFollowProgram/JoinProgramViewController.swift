//
//  JoinProgramViewController.swift
//  Verdentum
//
//  Created by Verdentum on 11/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker
import MBProgressHUD
class JoinProgramViewController: UIViewController,UITextFieldDelegate {

    var latStr = NSString()
    var longStr = NSString()
    var addressstr = NSString()
    var program_idstr = NSString()
    var programArray = NSMutableArray()
    var followedstr = NSString()
      var titleStr = NSString()
    @IBOutlet var locationTextfieldObj: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        let mainViewController = self.sideMenuController!
        let JoinFollowVC = JoinFollowViewController()
        JoinFollowVC.programArray = programArray
        JoinFollowVC.program_idstr = self.program_idstr
        JoinFollowVC.followedstr = self.followedstr
        JoinFollowVC.titleStr = self.titleStr
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(JoinFollowVC, animated: true)
    }
    
    @IBAction func joinProgramBtnTapped(_ sender: Any) {
      
        if (self.locationTextfieldObj.text?.isEmpty)! {
           self.presentAlertWithTitle(title: "Your Location!", message:"Your Location is required")
        }
        else {
          
            let userID = UserDefaults.standard.value(forKey: "userID")
            let username = UserDefaults.standard.value(forKey: "userName")
            let userrole = UserDefaults.standard.value(forKey: "userRole")
            let userimage = UserDefaults.standard.value(forKey: "userImage")
            let useremailid = UserDefaults.standard.value(forKey: "userEmail")
            
            let params:[String: AnyObject] = [
                "purpose" : "createinditeam" as AnyObject,
                "user" : [
                    "id" : userID as AnyObject,
                    "name" : username as AnyObject,
                    "role" : userrole as AnyObject,
                    "image" : userimage as AnyObject,
                    "email" : useremailid as AnyObject] as AnyObject,
                "team" : [
                    "progid" : program_idstr as AnyObject ,
                    "location" : self.addressstr as AnyObject,
                    "lat" : self.latStr as AnyObject,
                    "lng" : self.longStr as AnyObject] as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.createinditeamApiCalling(param: params)
        }
    }
   
    
    func createinditeamApiCalling(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
              
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let url = NSURL(string:BaseURL_createinditeamAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            let successMsgstr : NSString = (responsejson["msg"] as AnyObject) as! NSString
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.presentAlertWithTitle(title: "", message:successMsgstr as String)
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let mainViewController = self.sideMenuController!
                                let CommentsVC = MyProgramsViewController()
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(CommentsVC, animated: true)
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    

}

extension JoinProgramViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
       // print("Place name: \(place.name)")
         //self.locationTextfieldObj.text = place.name
        let lat = place.coordinate.latitude
        let lon = place.coordinate.longitude
        //print("lat lon",lat,lon)
        let numLat = NSNumber(value: (place.coordinate.latitude) as Double)
        self.latStr = numLat.stringValue as NSString
        let numLang = NSNumber(value: (place.coordinate.longitude) as Double)
        self.longStr = numLang.stringValue as NSString
       
       // print("Place address: \(String(describing: place.formattedAddress))")
        self.locationTextfieldObj.text = place.formattedAddress
        self.addressstr = place.formattedAddress! as NSString
       // print("Place attributions: \(String(describing: place.attributions))")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
