//
//  ProgramSwipeViewController.swift
//  Verdentum
//
//  Created by Verdentum on 13/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
class ProgramSwipeViewController: UIViewController,UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate  {

    @IBOutlet var worldViewBtn: UIButton!
    @IBOutlet var feedBottomLine: UILabel!
    @IBOutlet var feedBtn: UIButton!
    @IBOutlet var swipeViewLIne: UILabel!
    @IBOutlet var headerNameLabel: UILabel!
    @IBOutlet weak var constantViewLeft: NSLayoutConstraint!
    
    var tab1VC:WorldViewController! = nil
    var tab2VC:FeedsViewController! = nil
    var program_idstr = NSString()
    var responsePostArray = NSMutableArray()
    var navStr = NSString()
    var programNamestr = NSString()
    var headerTitleStr = NSString()
    var allFeedcommentCountArray = NSMutableArray()
      var allsupportFeedArray = NSMutableArray()
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    var verticalOffset: Int = 0
     var allisLikesFeedArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.headerNameLabel.text = self.headerTitleStr as String
        
        if self.navStr == "MyProgramFeedVC" {
            currentPage = 1
            createPageViewController()
        }
        if self.navStr == "FeedCommentProfileVC" {
            currentPage = 1
            createPageViewController()
        }
        if self.navStr ==  "WorldVC"
        {
            currentPage = 0
            createPageViewController()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    @IBAction func worldViewAndFeedBtn(_ sender: Any) {
        
        pageController.setViewControllers([arrVC[(sender as AnyObject).tag-1]], direction: UIPageViewControllerNavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        resetTabBarForTag(tag: (sender as AnyObject).tag-1)
    }
    
    //MARK: - CreatePagination
    
    private func createPageViewController() {
        
        pageController = UIPageViewController.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.pageController.view.frame = CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        }
        tab1VC = WorldViewController(nibName: "WorldViewController", bundle: nil)
        tab1VC.program_idstr = self.program_idstr
        tab1VC.programNamestr = self.programNamestr
        
        tab2VC = FeedsViewController(nibName: "FeedsViewController", bundle: nil)
        if self.navStr == "MyProgramFeedVC" {
            tab2VC.program_idstr = self.program_idstr
            tab2VC.verticalOffset  = self.verticalOffset
            tab2VC.headerTitleStr = self.headerTitleStr
            tab2VC.responsePostArray = self.responsePostArray
            tab2VC.allFeedcommentCountArray = self.allFeedcommentCountArray
            tab2VC.allisLikesFeedArray = self.allisLikesFeedArray
            tab2VC.allsupportFeedArray = self.allsupportFeedArray
            tab2VC.navStr = "MyProgramFeedVC"
        }
        if self.navStr == "FeedCommentProfileVC" {
            tab2VC.program_idstr = self.program_idstr
            tab2VC.headerTitleStr = self.headerTitleStr
            tab2VC.navStr = "FeedCommentProfileVC"
        }
        if self.navStr ==  "WorldVC"
        {
          // tab2VC.program_idstr = self.program_idstr
           // tab2VC.navStr = "MyProgramFeedVC"
            tab1VC.programNamestr = self.programNamestr
            tab2VC.program_idstr = self.program_idstr
            tab1VC.headerTitleStr = self.headerTitleStr
            tab2VC.headerTitleStr = self.headerTitleStr
        }
        arrVC = [tab1VC, tab2VC]
        
         if self.navStr == "MyProgramFeedVC" {
            pageController.setViewControllers([tab2VC], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
            
            self.addChildViewController(pageController)
            self.view.addSubview(pageController.view)
            pageController.didMove(toParentViewController: self)
        }
         if self.navStr == "FeedCommentProfileVC" {
            pageController.setViewControllers([tab2VC], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)

            self.addChildViewController(pageController)
            self.view.addSubview(pageController.view)
            pageController.didMove(toParentViewController: self)

        }
        if self.navStr ==  "WorldVC"
         {
            pageController.setViewControllers([tab1VC], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
            
            self.addChildViewController(pageController)
            self.view.addSubview(pageController.view)
            pageController.didMove(toParentViewController: self)
        }       
    }
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        
        return -1
    }
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            resetTabBarForTag(tag: currentPage)
        }
    }
    
    //MARK: - Set Top bar after selecting Option from Top Tabbar
    
    private func resetTabBarForTag(tag: Int) {
        
        var sender: UIButton!
        
        if(tag == 0) {
            feedBtn.backgroundColor = UIColor(red: 146.0/255.0, green: 208.0/255.0, blue: 78.0/255.0, alpha: 1.0)
            feedBtn.setTitleColor(UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0), for: .normal)
            worldViewBtn.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            worldViewBtn.setTitleColor(UIColor.black, for: .normal)
            sender = worldViewBtn
        }
        else if(tag == 1) {
            worldViewBtn.backgroundColor = UIColor(red: 146.0/255.0, green: 208.0/255.0, blue: 78.0/255.0, alpha: 1.0)
            worldViewBtn.setTitleColor(UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0), for: .normal)
            feedBtn.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            feedBtn.setTitleColor(UIColor.black, for: .normal)
            sender = feedBtn
        }
        currentPage = tag
        unSelectedButton(btn: worldViewBtn)
        unSelectedButton(btn: feedBtn)
        selectedButton(btn: sender)
        
    }
    
    
    //MARK: - UIScrollView Delegate Methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let xFromCenter: CGFloat = self.view.frame.size.width-scrollView.contentOffset.x
        let xCoor: CGFloat = CGFloat(self.swipeViewLIne.frame.size.width) * CGFloat(currentPage)
        let xPosition: CGFloat = xCoor - xFromCenter/CGFloat(arrVC.count)
        self.constantViewLeft.constant = xPosition
    }
    
    //MARK: - Custom Methods
    
    private func selectedButton(btn: UIButton) {
        
        btn.setTitleColor(UIColor.black, for: .normal)
        
        constantViewLeft.constant = btn.frame.origin.x
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    private func unSelectedButton(btn: UIButton) {
        btn.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
    }
    
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        let mainViewController = self.sideMenuController!
        let CommentsVC = MyProgramsViewController()
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
    

}
