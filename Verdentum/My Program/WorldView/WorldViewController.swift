//
//  WorldViewController.swift
//  Verdentum
//
//  Created by Verdentum on 13/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD


class WorldViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    open var aaChartModel: AAChartModel?
    open var aaChartView: AAChartView?
    @IBOutlet var barChartView: AAChartView!
    @IBOutlet var pieChartTitle: UILabel!
    @IBOutlet var tableViewWorldObj: UITableView!
    @IBOutlet var chartHeaderView: UIView!
    
    @IBOutlet var activityIndicatorObj: UIActivityIndicatorView!
    @IBOutlet var popUpViewObj: UIView!
    
    @IBOutlet var textViewObj: UITextView!
    var imagesArray = NSArray()
    var responseWorldViewArray = NSArray()
    var imagesArray1 = NSArray()
    var groupArray = NSArray()
    var indiasPieArray = NSArray()
    var countStr = NSString()
    var taskNameStr = NSString()
    var height = CGFloat()
    var JoiningStr = NSString()
    var responseGetActArray = NSArray()
    var responseGetChartArray = NSArray()
    var responseGetChartArray1 = NSMutableArray()
    var responseIndisNameArray = NSArray()
    var cellimages = WorldImagesCell()
    var program_idstr = NSString()
    var programNamestr = NSString()
    var barChartArray  = [Any]()
    var userID_Registered = NSString()
    var headerTitleStr = NSString()
    var pieArray1 = [(Any).self , (Any).self] as [Any]
    var pieChartObjectName = NSArray()
    var pieChartObjectAppendName = NSMutableArray()
    var pieTitleStr = String()
    var piechartTitleStr = String()
    //MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicatorObj.startAnimating()
         self.activityIndicatorObj.hidesWhenStopped = false
        
        // self.tableViewWorldObj.estimatedRowHeight = 80
        // self.tableViewWorldObj.rowHeight = UITableViewAutomaticDimension
        self.WorldViewServiceApiCalling()
        self.tableViewWorldObj.tableHeaderView = self.chartHeaderView
        self.tableViewWorldObj .register(UINib (nibName: "ActivityPerformedCell", bundle: nil), forCellReuseIdentifier: "ActivityPerformedCell")
        self.tableViewWorldObj .register(UINib (nibName: "WorldImagesCell", bundle: nil), forCellReuseIdentifier: "WorldImagesCell")
        self.tableViewWorldObj .register(UINib (nibName: "GroupCell", bundle: nil), forCellReuseIdentifier: "GroupCell")
        self.tableViewWorldObj.register(UINib(nibName: "GroupHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "GroupHeader")
        self.tableViewWorldObj.register(UINib(nibName: "GroupTableHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "GroupTableHeader")
    }
    
    fileprivate var touchDistance: CGFloat = 0
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let location = touches.first?.location(in: view) else {
            return
        }
        
        touchDistance = location.x
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let location = touches.first?.location(in: view) else {
            return
        }
        
        touchDistance -= location.x
        
        if touchDistance > 100 {
            // print("Right")
        } else if touchDistance < -100 {
            //print("Left")
        }
        
        touchDistance = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - WorldViewServiceApiCalling
    
    func WorldViewServiceApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            // MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "worldview" as AnyObject,
                "id" : userID as AnyObject,
                "program" : ["prog_id" : self.program_idstr as AnyObject,
                             "prog_name": self.programNamestr as AnyObject] as AnyObject]
            let url = NSURL(string:BaseURL_WorldViewAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        // print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            // MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.groupArray = ((responsejson["program"] as AnyObject) .value(forKey: "groups") as AnyObject) as! NSArray
                                self.imagesArray = ((responsejson["program"] as AnyObject) .value(forKey: "images") as AnyObject) as! NSArray
                                self.responseIndisNameArray = ((responsejson["program"] as AnyObject) .value(forKey: "indis") as AnyObject) as! NSArray
                                
                                //let joiningNO : NSNumber = ((responsejson["program"] as AnyObject) .value(forKey: "joining") as AnyObject) as! NSNumber
                                self.JoiningStr = (responsejson["program"] as AnyObject) .value(forKey: "joining") as! NSString
                                //self.tableViewWorldObj.reloadData()
                                self.getactServiceApiCalling()
                                self.getchartServiceApiCalling()
                                self.tableViewWorldObj.reloadData()
                            }
                            else
                            {
                                
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    //MARK: - GetactServiceApiCalling
    
    func getactServiceApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            // MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            //let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "getact" as AnyObject,
                "progid" : self.program_idstr as AnyObject]
            
            let url = NSURL(string:BaseURL_GetActAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        //  print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            //  MBProgressHUD.hide(for: self.view, animated: true)
                            
                            self.responseGetActArray = (responsejson["acts"] as AnyObject) as! NSArray as! NSMutableArray
                            self.tableViewWorldObj.reloadData()
                            
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    //MARK: - GetchartServiceApiCalling
    
    func getchartServiceApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
             MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            //let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "getchart" as AnyObject,
                "progid" : self.program_idstr as AnyObject]
            
            let url = NSURL(string:BaseURL_GetChartAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        //   print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                             MBProgressHUD.hide(for: self.view, animated: true)
                            
                            self.barChartArray = (((responsejson["activities"] as AnyObject) .value(forKey: "totalhr") as AnyObject) as! [Any])
                            
                            
                            // self.barChartArray = ((responsejson["activities"] as AnyObject) .value(forKey: "totalhr") as AnyObject) as! NSArray
                            
                            if self.JoiningStr .isEqual(to: "1")
                            {
                                self.responseGetChartArray = ((responsejson["indisInCountry"] as AnyObject) .value(forKey: "total") as AnyObject) as! NSArray
                                
                                self.pieChartObjectName = ((responsejson["indisInCountry"] as AnyObject) .value(forKey: "country") as AnyObject) as! NSArray
                                
                                
                                if self.responseGetChartArray.count == 0 {
                                    
                                }
                                else {
                                    for var i in 0..<self.responseGetChartArray.count {
                                        
                                        let pieStr : NSString = self.pieChartObjectName .object(at: i) as! NSString
                                        let intToStr: NSNumber = self.responseGetChartArray .object(at: i) as! NSNumber
                                        let strValue : NSString = intToStr.stringValue as NSString
                                        self.pieChartObjectAppendName.add(pieStr .appending(":") .appending(strValue as String) )
                                        
                                        self.pieArray1.append([pieStr , strValue.doubleValue])
                                       // print(self.pieArray1)
                                        self.responseGetChartArray1.add(strValue)
                                    }
                                    self.pieArray1.remove(at: 0)
                                    self.pieArray1.remove(at: 0)
                                }
                            }
                            else
                            {
                                self.responseGetChartArray = ((responsejson["groups"] as AnyObject) .value(forKey: "totalhr") as AnyObject) as! NSArray
                                self.pieChartObjectName = ((responsejson["groups"] as AnyObject) .value(forKey: "group_name") as AnyObject) as! NSArray
                                
                                if self.responseGetChartArray.count == 0 {
                                    
                                }
                                else {
                                    for var i in 0..<self.responseGetChartArray.count {
                                        
                                        let pieStr : NSString = self.pieChartObjectName .object(at: i) as! NSString
                                        let strValue : NSString = self.responseGetChartArray .object(at: i) as! NSString
                                        self.pieChartObjectAppendName.add(pieStr .appending(":") .appending(strValue as String))
                                        self.pieArray1.append([pieStr , strValue.floatValue])
                                       // print(self.pieArray1)
                                        
                                    }
                                    self.pieArray1.remove(at: 0)
                                    self.pieArray1.remove(at: 0)
                                    //print(self.pieArray1)
                                }
                            }
                            if self.JoiningStr .isEqual(to: "1")
                            {
                                self.piechartTitleStr = "Individuals Joined By Region"
                                self.pieTitleStr = "Individuals Joined"
                            }
                            else
                            {
                                self.piechartTitleStr = "Time Spent By Each Team"
                                self.pieTitleStr = "Time Spent"
                            }
                            
                            self.setUpTheAAChartViewOne()
                            self.setUpThePieChartView()
                            self.activityIndicatorObj.stopAnimating()
                            self.activityIndicatorObj.hidesWhenStopped = true
                            self.tableViewWorldObj.reloadData()
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func setUpThePieChartView() {
        DispatchQueue.main.async {
            self.aaChartView = AAChartView()
        self.aaChartView?.frame = CGRect(x:0,y:320,width:self.view.frame.size.width,height:200)
        self.aaChartView?.contentHeight = 200
        self.aaChartView?.reloadInputViews()
        self.chartHeaderView.addSubview(self.aaChartView!)
        self.aaChartView?.scrollEnabled = false
        
        self.aaChartModel = AAChartModel.init()
            .chartType(AAChartType.Pie)
            .backgroundColor("#ffffff")
            .title(self.piechartTitleStr)
            // .subtitle("virtual data")
            .dataLabelEnabled(true) //Is the pie chart data displayed directly?
            //.yAxisTitle("℃")
           // .xAxisLabelsEnabled(true)
            .series(
                [
                    [
                        "name": self.pieTitleStr,
                        "innerSize":"40%",
                        // The radius of the inner ring size accounted for(Internal ring radius / fan radius)
                        "data": self.pieArray1
                    ],
                    ]
        )
       
        self.aaChartView?.aa_drawChartWithChartModel(self.aaChartModel!)
        }
    }
    
    func setUpTheAAChartViewOne() {
        DispatchQueue.main.async {
        var force = [Float]()
        for item in self.barChartArray {
            force.append((item as! NSString).floatValue)
        }
        print(force)
        let chartViewWidth  = self.view.frame.size.width
        let screenHeight = self.view.frame.size.height-60
        let aaChartView = AAChartView()
        aaChartView.frame = CGRect(x:0,y:30,width:chartViewWidth,height:screenHeight/2)
        // self.view.addSubview(aaChartView)
       
        self.chartHeaderView.addSubview(aaChartView)
        let  aaChartModel = AAChartModel.init()
            .chartType(AAChartType.Bar) //Graphic type
            .animationType(AAChartAnimationType.Bounce) // Graphics rendering animation type is "bounce"
            .title("Hours Spent Per Activity")
            .dataLabelEnabled(false)//Whether to display the number
            .markerRadius(5)
            //Polyline connection point radius length, the equivalent of 0:00 no polyline connection point
            .series([
                [
                    "name": "Hours Spent",
                    "data": force
                ], ])
       
        aaChartView.aa_drawChartWithChartModel(aaChartModel)
        }
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.responseGetActArray.count
        }
        if section == 1 {
            if self.JoiningStr .isEqual(to: "1")
            {
                return self.responseIndisNameArray.count
            }
            else
            {
                return self.groupArray.count
            }
        }
        if section == 2 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell : ActivityPerformedCell = tableView .dequeueReusableCell(withIdentifier: "ActivityPerformedCell") as! ActivityPerformedCell
            let taskName: String = (self.responseGetActArray[indexPath.row] as AnyObject).value(forKey: "task_name") as! String
            let descriptionStr : String = "(View Task Description)"
            self.taskNameStr = ("\(taskName) \(descriptionStr)" as NSString) as NSString
            cell.nameLabel.text = self.taskNameStr as String
            let count: String = (self.responseGetActArray[indexPath.row] as AnyObject).value(forKey: "compunit") as! String
            let number : String = (self.responseGetActArray[indexPath.row] as AnyObject).value(forKey: "task_unit") as! String
            self.countStr = ("\(count) \(number)" as NSString) as NSString
            cell.metricsCountlabel.text = self.countStr as String
            cell.viewPhotosBtn.tag = indexPath.row
            cell.viewPhotosBtn.addTarget(self, action: #selector(self.viewPhotosBtn), for: .touchUpInside)
            cell.discriptionBtnTapped.tag = indexPath.row
            cell.discriptionBtnTapped.addTarget(self, action: #selector(self.discriptionBtnTapped), for: .touchUpInside)
            
            return cell
        }
        if indexPath.section == 1 {
            let Cellgroup : GroupCell = tableView .dequeueReusableCell(withIdentifier: "GroupCell") as! GroupCell
            if self.JoiningStr .isEqual(to: "1")
            {
                
                let nameOfString : NSString = ((self.responseIndisNameArray[indexPath.row] as AnyObject).value(forKey: "group_name") as? NSString)!
                Cellgroup.nameLabel.text = nameOfString.capitalized
                Cellgroup.despHeightlabel.constant = 0
            }
            else
            {
                let nameOfString : NSString = (self.groupArray[indexPath.row] as AnyObject).value(forKey: "group_name") as! NSString
                Cellgroup.nameLabel.text = nameOfString.capitalized
                
                let descriptionOfString : NSString = (self.groupArray[indexPath.row] as AnyObject).value(forKey: "group_description") as! NSString
                Cellgroup.descLabelObj.text = descriptionOfString.capitalized
                 Cellgroup.despHeightlabel.constant = 21
            }
            
            return Cellgroup
        }
        
        if indexPath.section == 2 {
            cellimages = tableView .dequeueReusableCell(withIdentifier: "WorldImagesCell") as! WorldImagesCell
            cellimages.setCollectionData(collection: self.imagesArray)
            cellimages.didCollectionViewCellSelect = didCollectionViewCellSelect
            cellimages.frame = tableView.bounds
            cellimages.layoutIfNeeded()
            cellimages.setNeedsDisplay()
           // cellimages.setNeedsLayout()
            cellimages.collectionviewOBj.reloadData()
            cellimages.collectionViewHeight.constant = cellimages.collectionviewOBj.collectionViewLayout.collectionViewContentSize.height
            return cellimages
        }
        return cellimages
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 80
        }
        if indexPath.section == 1 {
            return 50
        }
        if indexPath.section == 2 {
            return 100 // <- Your Desired Height
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
        }
        if indexPath.section == 1 {
            return UITableViewAutomaticDimension
        }
        if indexPath.section == 2 {
            return UITableViewAutomaticDimension
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0 {
            let headerView = self.tableViewWorldObj.dequeueReusableHeaderFooterView(withIdentifier: "GroupHeader") as! GroupHeader
            
            return headerView
        }
        if section == 1 {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "GroupTableHeader") as! GroupTableHeader
            
            if self.JoiningStr .isEqual(to: "1")
            {
                headerView.nameLabelOBj.text = "Individuals Joined"
            }
            else
            {
                headerView.nameLabelOBj.text = "Teams Joined"
            }
            return headerView
        }
        if section == 2 {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "GroupTableHeader") as! GroupTableHeader
            
            
            headerView.nameLabelOBj.text = "Upload Images"
            
            return headerView
        }
        
        let headerView = self.tableViewWorldObj.dequeueReusableHeaderFooterView(withIdentifier: "GroupHeader") as! GroupHeader
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0 {
            return 90
        }
        if section == 1 {
            return 40
        }
        if section == 2 {
            return 40
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
        }
        if indexPath.section == 1 {
            
            if self.JoiningStr .isEqual(to: "1")
            {
                let Selected_GroupName : NSString = ((self.responseIndisNameArray[indexPath.row] as AnyObject) .value(forKey: "group_name") as? NSString)!
                self.userID_Registered = ((self.responseIndisNameArray[indexPath.row] as AnyObject) .value(forKey: "group_admin") as? NSString)!
                
                let mainViewController = self.sideMenuController!
                let PersonalAccountVC = PersonalAccountViewController()
                PersonalAccountVC.navStr = "WorldViewIndividualVC"
                PersonalAccountVC.userID_Registered = self.userID_Registered
                PersonalAccountVC.program_idstr = self.program_idstr
                PersonalAccountVC.programNamestr = self.programNamestr
                PersonalAccountVC.WorldViewheaderTitleStr = self.headerTitleStr
                PersonalAccountVC.headerTitleStr = Selected_GroupName
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(PersonalAccountVC, animated: true)
                
            }
            else
            {
                let Selected_GroupID : NSString = ((self.groupArray[indexPath.row] as AnyObject) .value(forKey: "group_id") as? NSString)!
                let Selected_GroupName : NSString = ((self.groupArray[indexPath.row] as AnyObject) .value(forKey: "group_name") as? NSString)!
                let Selected_GroupImage : NSString = ((self.groupArray[indexPath.row] as AnyObject) .value(forKey: "group_banner") as? NSString)!
                
                let mainViewController = self.sideMenuController!
                let GroupNewsVC = GroupNewsViewController()
                GroupNewsVC.groupIDStr = Selected_GroupID
                GroupNewsVC.navStr = "WorldViewGroup"
                GroupNewsVC.groupImageStr = Selected_GroupImage
                GroupNewsVC.headerNameStr = Selected_GroupName
                GroupNewsVC.program_idstr = self.program_idstr
                GroupNewsVC.programNamestr = self.programNamestr
                GroupNewsVC.WorldViewheaderTitleStr = self.headerTitleStr
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(GroupNewsVC, animated: true)
            }
        }
        if indexPath.section == 2 {
            
        }
        
    }
    
    // MARK: -  didCollectionViewCellSelect
    
    lazy var didCollectionViewCellSelect: (([UIImage]) -> Void)? = { (imagesArray) in
        
        let mainViewController = self.sideMenuController!
        let CommentsVC = MyProgramImagesView()
        CommentsVC.downloadImage = imagesArray
        CommentsVC.imagesArray = self.imagesArray
        CommentsVC.navStr = "WorldVC"
        CommentsVC.program_idstr = self.program_idstr
        CommentsVC.programNamestr = self.programNamestr
        CommentsVC.headerTitleStr = self.headerTitleStr
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
    
    @IBAction func cancelPopUpBtnTapped(_ sender: Any) {
        
        self.popUpViewObj.isHidden = true
    }
    
    
    
    func viewPhotosBtn(_ sender: UIButton) {
        
        let task_Id: String = (self.responseGetActArray[sender.tag] as AnyObject).value(forKey: "task_id") as! String
        let params:[String: AnyObject] = [
            "purpose" : "getimageoftask" as AnyObject,
            "taskid" : task_Id as AnyObject]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.userpostsApiCalling(param: params)
    }
    
    //MARK: - UserPostsAPI Calling
    
    func userpostsApiCalling(param:[String: AnyObject])
        
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
       if Reachability.isConnectedToNetwork() == true {
        
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let url = NSURL(string:BaseURL_ActiviyPhotoAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            self.imagesArray1 = (responsejson["images"] as AnyObject) as! NSArray
                            
                            if self.imagesArray1.count == 0
                            {
                                self.presentAlertWithTitle(title: "Activity Photos", message:"No Photos uploaded for this activity.")
                            }
                            else
                            {
                                let mainViewController = self.sideMenuController!
                                let CommentsVC = ActivityPhotosView()
                                CommentsVC.imageArray = self.imagesArray1
                                CommentsVC.navStr = "WorldVC"
                                CommentsVC.worldViewheaderStr = self.headerTitleStr
                                CommentsVC.program_idstr = self.program_idstr
                                CommentsVC.programNamestr = self.programNamestr
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(CommentsVC, animated: true)
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    func discriptionBtnTapped(_ sender: UIButton) {
        
        let taskDiscriptionStr: String = (self.responseGetActArray[sender.tag] as AnyObject).value(forKey: "task_description") as! String
        
        self.textViewObj.text = taskDiscriptionStr
        
        self.popUpViewObj.frame = CGRect(x: CGFloat(20), y: CGFloat(-350), width: CGFloat(view.frame.size.width - 40), height: CGFloat(0))
        self.popUpViewObj.isHidden = false
        view.addSubview(self.popUpViewObj)
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.popUpViewObj.layer.shadowOpacity = 0.2
            self.popUpViewObj.layer.masksToBounds = false
            self.popUpViewObj.layer.shadowOpacity = 0.5
            self.popUpViewObj.frame = CGRect(x: CGFloat(20), y: CGFloat(100), width: CGFloat(self.view.frame.size.width - 40), height: CGFloat(300))
            self.popUpViewObj.layer.cornerRadius = 8.0
            self.popUpViewObj.layer.borderColor = UIColor.black.cgColor
        },
                       completion:
            {(_ finished: Bool) -> Void in
        })
    }
}


