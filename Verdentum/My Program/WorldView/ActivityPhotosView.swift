//
//  ActivityPhotosView.swift
//  Verdentum
//
//  Created by Verdentum on 20/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import SDWebImage
import Kingfisher
class ActivityPhotosView: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var imageArray = NSArray()
    var navStr = NSString()
    var program_idstr = NSString()
    var programNamestr = NSString()
    var downloadImage = [UIImage]()
    var worldViewheaderStr = NSString()
    var headerTitleStr = NSString()
    
    
    @IBOutlet var collectionViewObj: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.downloadImage.removeAll()
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        // layout.sectionHeadersPinToVisibleBounds = true
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.collectionViewObj!.collectionViewLayout = layout
        self.collectionViewObj.register(UINib(nibName: "ImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath)as! ImageCollectionCell
        
        let imageURLstr : NSString = IMAGE_UPLOAD.appending(((self.imageArray[indexPath.row] as AnyObject).value(forKey: "upload") as? String!)!) as NSString
        
       let url = URL(string: imageURLstr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)

        //let url = URL(string: IMAGE_UPLOAD.appending(((self.imageArray[indexPath.row] as AnyObject).value(forKey: "upload") as? String!)!))
        if url != nil {
            cell.imageViewObj.sd_setImage(with: url!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.progressiveDownload, completed: { (image, error, cacheType, imageURL) in
                
                if image != nil {
                    self.downloadImage.append(image!)
                }
                else {
                    
                }
            })
            
           /* cell.imageViewObj.kf.indicatorType = .activity
            cell.imageViewObj.kf.setImage(with: url,placeholder: nil,
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
                                            // print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
            },
                                          completionHandler: { image, error, cacheType, imageURL in
                                            
                                            if image != nil{
                                                self.downloadImage.append(image!)
                                            }
                                            else {
                                                
                                            }
                                            //  print("\(indexPath.row + 1): Finished")
            })*/
            
            
            
        } else {
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let mainViewController = self.sideMenuController!
        let CommentsVC = MyProgramImagesView()
        CommentsVC.verticalOffset = indexPath.row
        CommentsVC.imagesArray = self.imageArray
        CommentsVC.downloadImage = self.downloadImage
        CommentsVC.navStr = "ActivityPhotoVC"
        CommentsVC.programSwipenavStr = self.navStr
        CommentsVC.program_idstr = self.program_idstr
        CommentsVC.programNamestr = self.programNamestr
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }

    @IBAction func backBtn(_ sender: Any) {
        
        let mainViewController = self.sideMenuController!
        let programVC = ProgramSwipeViewController()
        programVC.navStr = self.navStr
        programVC.program_idstr = self.program_idstr
        programVC.programNamestr = self.programNamestr
        programVC.headerTitleStr = self.worldViewheaderStr
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(programVC, animated: true)
    }
    

}
