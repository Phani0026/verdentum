
//
//  ActivityPerformedCell.swift
//  Verdentum
//
//  Created by Verdentum on 17/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class ActivityPerformedCell: UITableViewCell {
    @IBOutlet var metricsCountlabel: UILabel!
    
    @IBOutlet var discriptionBtnTapped: UIButton!
    @IBOutlet var viewPhotosBtn: UIButton!
    @IBOutlet var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
