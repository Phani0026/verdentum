//
//  WorldImagesCell.swift
//  Verdentum
//
//  Created by Verdentum on 17/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import SDWebImage
import Kingfisher

class WorldImagesCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var imageArray = NSArray()
    var cellIndexString = NSString()
     var verticalOffset: Int = 0
     var didCollectionViewCellSelect: (([UIImage]) -> Void)?
     var downloadImage = [UIImage]()
    @IBOutlet var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet var collectionviewOBj: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
       self.downloadImage.removeAll()
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
      
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        // layout.sectionHeadersPinToVisibleBounds = true
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.collectionviewOBj.collectionViewLayout = layout
         self.collectionviewOBj.register(UINib(nibName: "ImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionCell")
        self.collectionviewOBj.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCollectionData (collection : NSArray)    {
        self.imageArray = collection
        self.collectionviewOBj.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.imageArray.count <= 30 {
            return self.imageArray.count
        }
        else {
            return 30
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath)as! ImageCollectionCell
        
        let imageURLstr2 : NSString = IMAGE_UPLOAD.appending(((self.imageArray[indexPath.row] as AnyObject).value(forKey: "upload") as? String!)!) as NSString
        
        let url = URL(string: imageURLstr2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
      //  let url = URL(string: IMAGE_UPLOAD.appending(((self.imageArray[indexPath.row] as AnyObject).value(forKey: "upload") as? String!)!))
        if url != nil {
           /* cell.imageViewObj.sd_setImage(with: url!, placeholderImage: UIImage(named: "loading"),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                
                if image != nil{
                    self.downloadImage.append(image!)
                }
                else {
                    
                }
            })*/
 
            cell.imageViewObj.kf.indicatorType = .activity
            cell.imageViewObj.kf.setImage(with: url,placeholder: nil,
                                                        options: [.transition(ImageTransition.fade(1))],
                                                        progressBlock: { receivedSize, totalSize in
                                                           // print("\(indexPath.row + 1): \(receivedSize)/\(totalSize)")
            },
                                                        completionHandler: { image, error, cacheType, imageURL in
                                                            
                                                            if image != nil{
                                                                self.downloadImage.append(image!)
                                                            }
                                                            else {
                                                                
                                                            }
                                                          //  print("\(indexPath.row + 1): Finished")
            })
            
            
        }
        else {
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if let didCollectionViewCellSelect = didCollectionViewCellSelect {
            didCollectionViewCellSelect(self.downloadImage)
        }
    }
    
}
