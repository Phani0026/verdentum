//
//  ImagesAndNoteCell.swift
//  Verdentum
//
//  Created by Verdentum on 20/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class ImagesAndNoteCell: UITableViewCell {
    @IBOutlet var imageViewObj: UIImageView!
    
    @IBOutlet var nameLabelOBj: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
