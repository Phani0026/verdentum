//
//  GroupCell.swift
//  Verdentum
//
//  Created by Verdentum on 17/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class GroupCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descLabelObj: UILabel!
    
    @IBOutlet var despHeightlabel: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
