//
//  MyProgramImagesView.swift
//  Verdentum
//
//  Created by Verdentum on 20/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import SDWebImage
import SKPhotoBrowser
import AlamofireImage
import Alamofire

class MyProgramImagesView: UIViewController,UITableViewDelegate,UITableViewDataSource,SKPhotoBrowserDelegate {

    @IBOutlet var titleNameLabel: UILabel!
    @IBOutlet var tableViewObj: UITableView!
    var cellIndexStr = NSIndexPath()
    var imagesArray = NSArray()
    var usernameobjstr = NSString()
    var noteStr = NSString()
    var navStr = NSString()
     var programSwipenavStr = NSString()
    var program_idstr = NSString()
    var programNamestr = NSString()
    var verticalOffset: Int = 0
     var downloadImage = [UIImage]()
    var headerTitleStr = NSString()
     var images = [SKPhotoProtocol]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.titleNameLabel.text = self.programNamestr as String
        self.tableViewObj.estimatedRowHeight = 100
        self.tableViewObj.rowHeight = UITableViewAutomaticDimension
        self.tableViewObj .register(UINib (nibName: "ImagesAndNoteCell", bundle: nil), forCellReuseIdentifier: "ImagesAndNoteCell")
        self.tableViewObj.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        if self.navStr == "ActivityPhotoVC"
        {
            let indexPath = IndexPath(item: self.verticalOffset, section: 0)
            self.tableViewObj.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        else
        {
            let indexPath = IndexPath(item: self.verticalOffset, section: 0)
            self.tableViewObj.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
    

    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.imagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ImagesAndNoteCell = tableView .dequeueReusableCell(withIdentifier: "ImagesAndNoteCell") as! ImagesAndNoteCell
        
        let Str1 : String = "Image uploaded by "
        let Str2 : String = "of the Activity -"
        let Str3 : String = "for the Program -"
        
        let taskNameStr : String = (self.imagesArray[indexPath.row] as AnyObject).value(forKey: "task_name") as! String
         let programTaskstr : String = (self.imagesArray[indexPath.row] as AnyObject).value(forKey: "prog_name") as! String
        
        let note_str : String = (self.imagesArray[indexPath.row] as AnyObject).value(forKey: "note") as! String
        
        let titlename: String = (self.imagesArray[indexPath.row] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.imagesArray[indexPath.row] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.imagesArray[indexPath.row] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.imagesArray[indexPath.row] as AnyObject).value(forKey: "reg_lname") as! String
        
        
        self.usernameobjstr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        
        if note_str.count == 0 {
           self.noteStr = ("\(Str1) \(self.usernameobjstr) \(Str2) \(taskNameStr) \(Str3) \(programTaskstr)" as NSString) as NSString
        }
        else
        {
           self.noteStr = ("\(Str1) \(self.usernameobjstr) \(Str2) \(taskNameStr) \(Str3) \(programTaskstr) \(note_str)" as NSString) as NSString
        }
        
        cell.nameLabelOBj.text = self.noteStr as String
        cell.imageViewObj.image = self.downloadImage[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    
       // SKCache.sharedCache.imageCache = CustomImageCache()
        let browser = SKPhotoBrowser(photos: createWebPhotos())
        browser.initializePageIndex(indexPath.row)
        browser.delegate = self
        present(browser, animated: true, completion: nil)
    }
    
    func createWebPhotos() -> [SKPhotoProtocol] {
        
     // print(self.downloadImage.count)
        return (0..<self.downloadImage.count).map { (i: Int) -> SKPhotoProtocol in
            let photo = SKPhoto.photoWithImage(self.downloadImage[i])
            
            //let photo = SKPhoto.photoWithImageURL(IMAGE_UPLOAD .appending(self.responsePostimageArray[i] as! String))
            // photo.caption = caption[i%10]
           // photo.shouldCachePhotoURLImage = true
            
            return photo
        }
    }
 
    @IBAction func backBtnTapped(_ sender: Any) {
        
        if self.navStr == "ActivityPhotoVC"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ActivityPhotosView()
        
            dashboardVC.navStr = self.programSwipenavStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.imageArray = self.imagesArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        else
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ProgramSwipeViewController()
            dashboardVC.navStr = self.navStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.headerTitleStr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
    }
 
}

// MARK: - SKPhotoBrowserDelegate

extension MyProgramImagesView {
    func didDismissAtPageIndex(_ index: Int) {
    }
    
    func didDismissActionSheetWithButtonIndex(_ buttonIndex: Int, photoIndex: Int) {
    }
    
    func removePhoto(index: Int, reload: (() -> Void)) {
        SKCache.sharedCache.removeImageForKey("somekey")
        reload()
    }
}

class CustomImageCache: SKImageCacheable {
    var cache: SDImageCache
    
    init() {
        let cache = SDImageCache(namespace: "com.suzuki.custom.cache")
        self.cache = cache
    }
    
    func imageForKey(_ key: String) -> UIImage? {
        guard let image = cache.imageFromDiskCache(forKey: key) else { return nil }
        
        return image
    }
    
    func setImage(_ image: UIImage, forKey key: String) {
        cache.store(image, forKey: key)
    }
    
    func removeImageForKey(_ key: String) {
    }
}


