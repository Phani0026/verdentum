//
//  MyProgramsViewController.swift
//  Verdentum
//
//  Created by Verdentum on 11/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD

class MyProgramsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var responseProgramArray = NSArray()
    var lastProgramIDStr = NSString()
    var refreshControl = UIRefreshControl()
    var StatuStr = NSString()
    @IBOutlet var tableheaderViewObj: UIView!
    @IBOutlet var tableViewObj: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableViewObj.estimatedRowHeight = 70
        self.tableViewObj.rowHeight = UITableViewAutomaticDimension
         self.tableViewObj.tableHeaderView = self.tableheaderViewObj
        // -----Refresh Control----
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(MyProgramsViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        
        self.tableViewObj.addSubview(refreshControl)
        
        self.tableViewObj .register(UINib (nibName: "ProgramFollowCell", bundle: nil), forCellReuseIdentifier: "ProgramFollowCell")
        
        self.MyProgramServiceApiCalling()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sideMenuController?.isLeftViewSwipeGestureDisabled = false
    }
    
    // MARK: - refresh
    
    func refresh(_ sender:AnyObject)
    {
        
        MyProgramServiceApiCalling ()
        // self.newTableViewObj.reloadData()
        self.refreshControl .endRefreshing()
    }
    
    //MARK: - MyProgram APi
    
    func MyProgramServiceApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "allprograms" as AnyObject,
                "id" : userID as AnyObject ]
            let url = NSURL(string:BaseURL_MYProgramAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                       // print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.responseProgramArray = (responsejson["programs"] as AnyObject) as! NSArray as! NSMutableArray
                                
                                if self.responseProgramArray.count == 0 {
                                    
                                    self.StatuStr = "Sorry!It seems you have not joined any program."
                                    self.tableViewObj.reloadData()
                                }
                                else {
                                   self.tableViewObj.reloadData()
                                }
                                
                                
                            }
                            else
                            {
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.responseProgramArray.count == 0 {
            let rect = CGRect(x: 0,
                              y: 0,
                              width: view.bounds.size.width,
                              height: view.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            
            noDataLabel.text = self.StatuStr as String
            noDataLabel.textColor = UIColor.black
            noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            noDataLabel.sizeToFit()
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableViewObj.backgroundView = noDataLabel
            
            return 0
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.responseProgramArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView .dequeueReusableCell(withIdentifier: "ProgramFollowCell") as! ProgramFollowCell
        cell.descriptionLabel.text =  (self.responseProgramArray[indexPath.row] as AnyObject).value(forKey: "pm_organization") as? String
        
        cell.nameLabel.text = (self.responseProgramArray[indexPath.row] as AnyObject).value(forKey: "prog_name") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
       // let selectedProgramstr : NSDictionary = (self.responseProgramArray[indexPath.row] as AnyObject) as! NSDictionary
        
        let mainViewController = self.sideMenuController!
        let ProgramVC = ProgramSwipeViewController()
       
        ProgramVC.program_idstr = (self.responseProgramArray[indexPath.row] as! NSObject)  .value(forKey: "prog_id") as! NSString
        ProgramVC.programNamestr = (self.responseProgramArray[indexPath.row] as! NSObject)  .value(forKey: "prog_name") as! NSString
        ProgramVC.navStr = "WorldVC"
        ProgramVC.headerTitleStr = (self.responseProgramArray[indexPath.row] as! NSObject)  .value(forKey: "prog_name") as! NSString
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(ProgramVC, animated: true)
    }
    

    @IBAction func sideMenuBtntapped(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }

   
}
