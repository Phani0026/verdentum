//
//  SideMenuViewController.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 19/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import LGSideMenuController
import MBProgressHUD


class SideMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
   
    
    @IBOutlet var tableViewObj: UITableView!
    var friendRequestArray = NSMutableArray()
    var friendReqstr = NSString()
    var groupReqstr = NSString()
    var groupRequestArray = NSMutableArray()
    var pushArray = NSMutableArray()
    var pushdic = NSDictionary()
    var progIDuserDefault : String? = nil
    var frndReqidstr : String? = ""
    var groupReqidstr : String? = ""
    var pushstr : String? = ""
    var pushcount : String? = "0"
    var frndcount : String? = "0"
    var groupcount : String? = "0"
    var nameData: NSArray = [ "News","Upload Activity","Survey","My Programs","Join/Follow Program","people","Invite","Notifications"]
    var progidfilterstr = NSString()
    var notificationArray = NSArray()
    var frndArray = NSArray()
    var groupArray = NSArray()
    var frndDic = NSDictionary()
    var groupDic = NSDictionary()
    var progidstr = NSString()
    var notificationsArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.tableViewObj .register(UINib (nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
        self.tableViewObj .register(UINib (nibName: "NameCell", bundle: nil), forCellReuseIdentifier: "NameCell")
        self.tableViewObj .reloadData()
        
        //----- NSNotificationCenter ------ //
        NotificationCenter.default.addObserver(self, selector: #selector(self.push(fromCollectionView:)), name: NSNotification.Name(rawValue: "pushArrayNotifications"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.groupRequest(fromCollectionView:)), name: NSNotification.Name(rawValue: "groupRequestArrayNotifications"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.friendRequest(fromCollectionView:)), name: NSNotification.Name(rawValue: "friendRequestArrayNotifications"), object: nil)
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didSelectItemFromCollectionView"), object: nil)
    }
    
    func push(fromCollectionView notification: Notification)
    {
        self.pushdic = notification .object as! NSDictionary
        
        self.notificationArray = self.pushdic .value(forKey: "pushnotificationdata") as! NSArray
        if self.notificationArray.count == 0 {
             self.tableViewObj.reloadData()
        }
        else {
            let checkProgidDic : NSDictionary = (self.notificationArray  .firstObject) as! NSDictionary
            let keys = checkProgidDic.allKeys as! [String]
            let progidfiltered = keys.filter { $0.contains("progid") }
            if progidfiltered.count == 0 {
                let keys = checkProgidDic.allKeys as! [String]
                let postidfiltered = keys.filter { $0.contains("postid") }
                if postidfiltered.count == 0 {
                    let keys = checkProgidDic.allKeys as! [String]
                    let datafiltered = keys.filter { $0.contains("data") }
                    if datafiltered.count == 0 {
                    }
                    else {
                        if progidfiltered.count == 0 {
                            
                        }
                        else {
                        pushstr = progidfiltered[0]
                        }
                    }
                }
                else {
                    if progidfiltered.count == 0 {
                        
                    }
                    else {
                    pushstr = progidfiltered[0]
                    }
                }
            }
            else {
                if progidfiltered.count == 0 {
                    
                }
                else {
                pushstr = progidfiltered[0]
                }
            }
            
            if (pushstr? .isEqual("progid"))! {
                self.tableViewObj.reloadData()
            }
            else {
                if (pushstr? .isEqual("postid"))! {
                    self.tableViewObj.reloadData()
                }
                else {
                    if (pushstr? .isEqual("data"))! {
                        self.tableViewObj.reloadData()
                    }
                }
            }
        }
    }
    
    func groupRequest(fromCollectionView notification: Notification)
    {
        self.groupDic = notification .object as! NSDictionary
        self.groupArray = self.groupDic .value(forKey: "groupReq") as! NSArray
        if self.groupArray.count == 0 {
             self.tableViewObj.reloadData()
        } else {
            let checkProgidDic : NSDictionary = (self.groupArray  .firstObject) as! NSDictionary
            let keys = checkProgidDic.allKeys as! [String]
            let progidfiltered = keys.filter { $0.contains("req_id") }
            if progidfiltered.count == 0 {
                let keys = checkProgidDic.allKeys as! [String]
                
                let progidfiltered = keys.filter { $0.contains("data") }
                
                if progidfiltered.count == 0 {
                }
                else {
                    groupReqidstr = progidfiltered[0]
                }
            }
            else {
                groupReqidstr = progidfiltered[0]
            }
            if (groupReqidstr? .isEqual("req_id"))! {
                self.tableViewObj.reloadData()
            }
            else {
                if (groupReqidstr? .isEqual("data"))! {
                    self.tableViewObj.reloadData()
                }
            }
        }
    }
    func friendRequest(fromCollectionView notification: Notification)
    {
        self.frndDic = notification .object as! NSDictionary
        self.frndArray = self.frndDic .value(forKey: "frndreq") as! NSArray
        
        
        if self.frndArray.count == 0 {
             self.tableViewObj.reloadData()
        } else {
            let checkProgidDic : NSDictionary = (self.frndArray  .firstObject) as! NSDictionary
            let keys = checkProgidDic.allKeys as! [String]
            let progidfiltered = keys.filter { $0.contains("req_id") }
            if progidfiltered.count == 0 {
                let keys = checkProgidDic.allKeys as! [String]
                
                let progidfiltered = keys.filter { $0.contains("data") }
                
                if progidfiltered.count == 0 {
                }
                else {
                    frndReqidstr = progidfiltered[0]
                }
            }
            else {
                frndReqidstr = progidfiltered[0]
            }
            if (frndReqidstr? .isEqual("req_id"))! {
                self.tableViewObj.reloadData()
            }
            else {
                if (frndReqidstr? .isEqual("data"))! {
                    self.tableViewObj.reloadData()
                }
            }
        }
    }
    
    
     // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0  {
            return 1
        }
        if section == 1 {
            return self.nameData .count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView .dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
           // cell.backgroundColor = UIColor.init(red: 35/255.0, green: 38/255.0, blue: 42/255.0, alpha: 1.0)
            
            let userDefaults = UserDefaults.standard
            let userimage = userDefaults.value(forKey: "userImage")
            UserDefaults.standard.synchronize()
            if userimage != nil {
                let url = URL(string: IMAGE_UPLOAD.appending((userimage as? String!)!))
                cell.imageViewObj.sd_setImage(with: url, placeholderImage: UIImage(named: " "))
            }
            else {
                
            }
            cell.imageViewObj.layer.borderWidth = 2.0
            cell.imageViewObj.layer.borderColor = UIColor.darkGray.cgColor
            cell.imageViewObj.layer.cornerRadius = 40
            cell.imageViewObj.layer.masksToBounds = true
            let userName = UserDefaults.standard.value(forKey: "userName")
            let userEmail = UserDefaults.standard.value(forKey: "userEmail")
            cell.customerNameLabel.text = userName as? String
            cell.customerEmailIDLabel.text = userEmail as? String
            return cell
        }
        if indexPath.section == 1         {
            let cell = tableView .dequeueReusableCell(withIdentifier: "NameCell") as! NameCell
          //  cell.backgroundColor = UIColor.init(red: 35/255.0, green: 38/255.0, blue: 42/255.0, alpha: 1.0)
            let namelabelStr : String = self.nameData .object(at: indexPath.row) as! String 
            cell.nameLabelObj.text = namelabelStr.capitalized
            let namestr : NSString = cell.nameLabelObj.text! as NSString
            if namestr .isEqual(to: "Notifications") {
               cell.NotificationcountView.isHidden = false
                if (pushstr? .isEqual("progid"))! {
                    pushcount = String(self.notificationArray.count)
                }
                else {
                    if (pushstr? .isEqual("postid"))! {
                        pushcount = String(self.notificationArray.count)
                    }
                    else {
                        if (pushstr? .isEqual("data"))! {
                            pushcount = "0"
                        }
                    }
                }
                if (frndReqidstr? .isEqual("req_id"))! {
                    frndcount = String(self.frndArray.count)
                }
                else {
                    if (frndReqidstr? .isEqual("data"))! {
                      frndcount = "0"
                    }
                }
                if (groupReqidstr? .isEqual("req_id"))! {
                    groupcount = String(self.groupArray.count)
                }
                else {
                    if (groupReqidstr? .isEqual("data"))! {
                        groupcount = "0"
                    }
                }
                let str = "\(pushcount as! String) + \(frndcount as! String) + \(groupcount as! String)"
                let sum = str.components(separatedBy: " + ").flatMap{ Int($0) }.reduce(0) { $0 + $1 }
               
                let NotificationCount : String = String(sum)
               cell.notificationCountLabel.text = NotificationCount as String
            }
            else {
                 cell.NotificationcountView.isHidden = true
            }
            return cell
        }
        let cell = tableView .dequeueReusableCell(withIdentifier: "NameCell") as! NameCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 140
        }
        if indexPath.section == 1         {
            return 44
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mainViewController = sideMenuController!
        
        if indexPath.section == 0 {
          
            if indexPath.row == 0
            {
                let logoutVC = LogoutViewController()
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(logoutVC, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
        }
        if indexPath.section == 1
        {
            if indexPath.row == 0
            {
                let dashboardVC = DashBoardViewController()
                dashboardVC.navStr = "DashboardVC"
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(dashboardVC, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 1
            {
                let userID = UserDefaults.standard.value(forKey: "userID")
                let params:[String: AnyObject] = [
                    "purpose" : "fastprograms" as AnyObject,
                    "id" : userID as AnyObject]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.uploadActivityAPICalling(param: params)
            }
            if indexPath.row == 2
            {
                let SurveysVC = SurveysViewController()
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(SurveysVC, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 3
            {
                let MyProgramsVC = MyProgramsViewController()
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(MyProgramsVC, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 4
            {
                let FollowProgramVC = FollowProgramViewController()
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(FollowProgramVC, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 5
            {
                let PeopleVC = PeopleViewController()
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(PeopleVC, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 6
            {
                let InviteVC = InviteViewController()
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(InviteVC, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 7
            {
                let NotificationVC = NotificationsViewController()
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(NotificationVC, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
        }
    }
    
    
    func uploadActivityAPICalling(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_UPLOADACTIVITYAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                      //  print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.presentAlertWithTitle(title: "", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                            else
                            {
                                 //self.responseUploadActivityArray = (responsejson["programs"] as AnyObject) as! NSArray as! NSMutableArray
                                
                                
                                let mainViewController = self.sideMenuController!
                                let UploadActivityVC = UploadActivityViewController()
                                
                                
                                UploadActivityVC.mainProgramArray = (responsejson["programs"] as AnyObject) as! NSArray
                                UploadActivityVC.mainGroupArray = ((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject)  as! NSArray
                                UploadActivityVC.programArray = ((responsejson["programs"] as AnyObject) .value(forKey: "prog_name") as AnyObject) as! NSArray
                                UploadActivityVC.joiningArray = ((responsejson["programs"] as AnyObject) .value(forKey: "joining") as AnyObject) as! NSArray
                                UploadActivityVC.defaultProgramStr = (((responsejson["programs"] as AnyObject) .value(forKey: "prog_name") as AnyObject) .firstObject) as! NSString
                                UploadActivityVC.programIDArray = ((responsejson["programs"] as AnyObject) .value(forKey: "prog_id") as AnyObject) as! NSArray
                                UploadActivityVC.teamArray = ((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject)  as! NSArray
                                UploadActivityVC.defaultTeamStr = (((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject) .value(forKey: "group_name") as AnyObject) as! NSArray
                                UploadActivityVC.groupAdminArray = (((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject) .value(forKey: "group_admin") as AnyObject) as! NSArray
                                UploadActivityVC.teamIDArray = ((responsejson["programs"] as AnyObject) .value(forKey: "groups") as AnyObject)  as! NSArray
                                UploadActivityVC.selectActivityArray = ((responsejson["programs"] as AnyObject) .value(forKey: "tasks") as AnyObject)  as! NSArray
                                UploadActivityVC.selectActivityIDArray = ((responsejson["programs"] as AnyObject) .value(forKey: "tasks") as AnyObject)  as! NSArray
                                UploadActivityVC.navstr = "uploadVC"
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(UploadActivityVC, animated: true)
                                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                            }
                            
                            
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
