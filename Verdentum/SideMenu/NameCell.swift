//
//  NameCell.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 19/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//


import UIKit

class NameCell: UITableViewCell {

    @IBOutlet var notificationCountLabel: UILabel!
    @IBOutlet var NotificationcountView: UIView!
    @IBOutlet var nameLabelObj: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
