//
//  APIModal.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 17/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import Foundation

class APIModal: NSObject
{
    class func sharedAPIModel()
    {
        return sharedAPIModel()
    }
  
    func LoginURL(withUrl urlPath: String, withParameters parameters: String, withComplitionBlock responseBlock: @escaping (_ response: Dictionary <String, AnyObject>, _ errorString: String) -> Void)
    {
        
        let url = URL(string:urlPath.appending(parameters))
        
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(content_type, forHTTPHeaderField: "Content-Type")
        request.addValue(content_type, forHTTPHeaderField: "Accept")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {(data, response, error) in
            if (data != nil)
            {
                if let httpResponse = response as? HTTPURLResponse
                {
                    
                    if httpResponse.statusCode == 200
                    {
                        do
                        {
                            let json = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary <String, AnyObject>
                            
                            let error = ""
                            responseBlock(json,error)
                        } catch
                        {
                            
                        }
                    }
                    
                    
                }
                
            }
            
        }
        
        
        task.resume()
    }

    
    
    
    func signUpUrl(withUrl urlPath: String, withParameters parameters: String, withComplitionBlock responseBlock: @escaping (_ response: Dictionary <String, AnyObject>, _ errorString: String) -> Void)
    {
        
        let url = URL(string:urlPath)
        var request = URLRequest(url: url!)
        request.addValue(content_type, forHTTPHeaderField: "Content-Type")
        request.addValue(content_type, forHTTPHeaderField: "Accept")
       //// request.setValue(api_key, forHTTPHeaderField: "api_key")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
        request.httpMethod = "POST"
        request.httpBody = parameters.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {(data, response, error) in
            
            do
            {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary <String, AnyObject>
                
                let error = ""
                responseBlock(json,error)
            } catch
            {
                
            }
            
        }
        
        
        task.resume()
    }
    
    
    
    func homeGetURL(withUrl urlPath: String, withParameters parameters: String, withComplitionBlock responseBlock: @escaping (_ response: Dictionary <String, AnyObject>, _ errorString: String) -> Void)
    {
        
        let url = URL(string:urlPath.appending(parameters))
        
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(content_type, forHTTPHeaderField: "Content-Type")
        request.addValue(content_type, forHTTPHeaderField: "Accept")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {(data, response, error) in
            if (data != nil)
            {
                if let httpResponse = response as? HTTPURLResponse
                {
                    
                    if httpResponse.statusCode == 200
                    {
                        do
                        {
                            let json = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary <String, AnyObject>
                            
                            let error = ""
                            responseBlock(json,error)
                        } catch
                        {
                            
                        }
                    }
                    
                    
                }
                
            }
            
        }
        
        
        task.resume()
    }

    
    
 
}






