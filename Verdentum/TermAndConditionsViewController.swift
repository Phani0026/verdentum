//
//  TermAndConditionsViewController.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 17/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class TermAndConditionsViewController: UIViewController {

    @IBOutlet var webViewObj: UIWebView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /// http://verdentum.org/privacy_policy
        let url = URL (string: "http://verdentum.org/privacy_policy")
        let requestObj = URLRequest(url: url!)
        self.webViewObj.loadRequest(requestObj)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
}
