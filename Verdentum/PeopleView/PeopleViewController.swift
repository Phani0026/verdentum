//
//  PeopleViewController.swift
//  Verdentum
//
//  Created by Verdentum on 11/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class PeopleViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {
    
    
    @IBOutlet weak var constantViewLeft: NSLayoutConstraint!
    
    @IBOutlet weak var findPeopleBtn: UIButton!
    @IBOutlet weak var mynetworkBtn: UIButton!
    
    @IBOutlet weak var findBtnLine: UILabel!
    
    @IBOutlet weak var findBtnLine1: UILabel!
    
    var tab1VC:MyNetworkViewController! = nil
    var tab2VC:FindPeopleViewController! = nil
    var navStr = NSString()
    
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.navStr == "peopleVC" {
            currentPage = 1
            createPageViewController()
        } else {
            currentPage = 0
            createPageViewController()
        }
        
       
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func sideMenuBtnTapped(_ sender: Any) {
         sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       
    }
    
     @IBAction func btnOptionClicked(btn: UIButton) {
        
    }
    @IBAction func BtnActionTapped(_ sender: UIButton) {
        
        pageController.setViewControllers([arrVC[sender.tag-1]], direction: UIPageViewControllerNavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        
        resetTabBarForTag(tag: sender.tag-1)
    }

    @IBAction func mynetworkBtnTapped(_ sender: Any) {
       
        pageController.setViewControllers([arrVC[(sender as AnyObject).tag-1]], direction: UIPageViewControllerNavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        
        resetTabBarForTag(tag: (sender as AnyObject).tag-1)
    }
    //MARK: - CreatePagination
    
    private func createPageViewController() {
        
        pageController = UIPageViewController.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.pageController.view.frame = CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        }
        tab1VC = MyNetworkViewController(nibName: "MyNetworkViewController", bundle: nil)
        tab2VC = FindPeopleViewController(nibName: "FindPeopleViewController", bundle: nil)
        arrVC = [tab1VC, tab2VC]
        pageController.setViewControllers([tab1VC], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        self.addChildViewController(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParentViewController: self)
    }
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        return -1
    }
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = indexofviewController(viewCOntroller: viewController)
        if(index != -1) {
            index = index - 1
        }
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = indexofviewController(viewCOntroller: viewController)
        if(index != -1) {
            index = index + 1
        }
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(completed) {
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            resetTabBarForTag(tag: currentPage)
        }
    }
    
    //MARK: - Set Top bar after selecting Option from Top Tabbar
    
    private func resetTabBarForTag(tag: Int) {
        var sender: UIButton!
        if(tag == 0) {
            findPeopleBtn.backgroundColor = UIColor(red: 146.0/255.0, green: 208.0/255.0, blue: 78.0/255.0, alpha: 1.0)
            findPeopleBtn.setTitleColor(UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0), for: .normal)
            mynetworkBtn.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            mynetworkBtn.setTitleColor(UIColor.black, for: .normal)
            sender = mynetworkBtn
        }
        else if(tag == 1) {
            mynetworkBtn.backgroundColor = UIColor(red: 146.0/255.0, green: 208.0/255.0, blue: 78.0/255.0, alpha: 1.0)
            mynetworkBtn.setTitleColor(UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0), for: .normal)
            findPeopleBtn.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            findPeopleBtn.setTitleColor(UIColor.black, for: .normal)
            sender = findPeopleBtn
        }
        currentPage = tag
        unSelectedButton(btn: mynetworkBtn)
        unSelectedButton(btn: findPeopleBtn)
        selectedButton(btn: sender)
    }
    
    
    //MARK: - UIScrollView Delegate Methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let xFromCenter: CGFloat = self.view.frame.size.width-scrollView.contentOffset.x
        let xCoor: CGFloat = CGFloat(self.findBtnLine.frame.size.width) * CGFloat(currentPage)
        let xPosition: CGFloat = xCoor - xFromCenter/CGFloat(arrVC.count)
        self.constantViewLeft.constant = xPosition
    }
    
    //MARK: - Custom Methods
    
    private func selectedButton(btn: UIButton) {
        btn.setTitleColor(UIColor.black, for: .normal)
        constantViewLeft.constant = btn.frame.origin.x
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func unSelectedButton(btn: UIButton) {
        btn.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
    }
}
