//
//  ConnectCell.swift
//  Verdentum
//
//  Created by Verdentum on 06/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class ConnectCell: UITableViewCell {

    @IBOutlet weak var imageViewProfile: UIImageView!
    
    @IBOutlet weak var disConnectBtn: UIButton!
    
    @IBOutlet weak var nameLabelObj: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
