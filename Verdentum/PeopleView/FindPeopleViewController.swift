//
//  FindPeopleViewController.swift
//  Verdentum
//
//  Created by Verdentum on 06/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD


class FindPeopleViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var responseVolunteersArray = NSMutableArray()
    var usernameobjstr = NSString()
    var responsevolunteersID = NSMutableArray()
    var responseFriendID = NSMutableArray()
    var user_one_idStr = NSString()
    var refreshControl = UIRefreshControl()
    var selectedRow = NSIndexPath()
    var statusArray = NSMutableArray()
    var cell = ConnectCell()
    var responsePostArray = NSMutableArray()
    var reg_IDstr = NSString()
     var StatuStr = NSString()
    var allsupportPeopleArray = NSMutableArray()
    
     var allisLikesPeopleArray = NSMutableArray()
    @IBOutlet weak var tableViewFindpeopleObj: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableViewFindpeopleObj.estimatedRowHeight = 80
        self.tableViewFindpeopleObj.rowHeight = UITableViewAutomaticDimension
        
        self.tableViewFindpeopleObj .register(UINib (nibName: "ConnectCell", bundle: nil), forCellReuseIdentifier: "ConnectCell")
        
        // -----Refresh Control----
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(DashBoardViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        
        self.tableViewFindpeopleObj.addSubview(refreshControl)
        
        self.FindPeopleAPICalling()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh(_ sender:AnyObject)
    {
        self.FindPeopleAPICalling()
        // self.newTableViewObj.reloadData()
        self.refreshControl .endRefreshing()
    }
    
    //MARK: - FindPeopleAPICalling
    
    func FindPeopleAPICalling() {
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "findvoluns" as AnyObject,
                "id" : userID as AnyObject ]
            let url = NSURL(string:BaseURL_FindPeopleAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                       // print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.responseVolunteersArray = (responsejson["volunteers"] as AnyObject) as! NSMutableArray
                                
                                if self.responseVolunteersArray.count == 0
                                {
                                   // self.presentAlertWithTitle(title: "", message:"No people in your Network... you can connect to people in the Find People tab.")
                                    self.StatuStr = "No people currently registered."
                                    self.tableViewFindpeopleObj.reloadData()
                                }
                                else
                                {
                                    let volunteerUserIDArray : NSArray = (self.responseVolunteersArray .value(forKey: "reg_id") as AnyObject) as! NSArray
                                    self.responsevolunteersID = NSMutableArray(array: volunteerUserIDArray)
                                   
                                    let statusCheckArray : NSArray = (self.responseVolunteersArray .value(forKey: "status") as AnyObject) as! NSArray
                                    self.statusArray = NSMutableArray(array: statusCheckArray)
                                    for i in 0..<self.statusArray.count {
                                        let obj = self.statusArray[i]
                                        if (obj is NSNull) {
                                            self.statusArray [i] = "1"
                                        }
                                    }
                                    self.tableViewFindpeopleObj.reloadData()
                                }
                            }
                            else
                            {
                                self.presentAlertWithTitle(title: "", message:"Please Try again.")
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.responseVolunteersArray.count == 0 {
        let rect = CGRect(x: 0,
                          y: 0,
                          width: view.bounds.size.width,
                          height: view.bounds.size.height)
        let noDataLabel: UILabel = UILabel(frame: rect)
        
        noDataLabel.text = self.StatuStr as String
        noDataLabel.textColor = UIColor.black
        noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
        noDataLabel.numberOfLines = 0
        noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        noDataLabel.sizeToFit()
        noDataLabel.textAlignment = NSTextAlignment.center
        self.tableViewFindpeopleObj.backgroundView = noDataLabel
        return 0
        } else {
        return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.responseVolunteersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = tableView .dequeueReusableCell(withIdentifier: "ConnectCell") as! ConnectCell
        let url = URL(string: IMAGE_UPLOAD.appending(((self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!))
        cell.imageViewProfile.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        cell.imageViewProfile.layer.borderWidth = 2.0
        cell.imageViewProfile.layer.borderColor = UIColor.white.cgColor
        cell.imageViewProfile.layer.cornerRadius = 25
        cell.imageViewProfile.layer.masksToBounds = true
        
        
        let titlename: String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_lname") as! String
        
        
        self.usernameobjstr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        
        cell.nameLabelObj.text = usernameobjstr as String
        cell.disConnectBtn.tag = indexPath.row
        
       
        
        
        let statuCheckstr =  (self.statusArray .object(at: indexPath.row) as AnyObject) as! NSString
        
        if statuCheckstr .isEqual(to: "0") {
            
            cell.disConnectBtn.setTitle("Request sent", for: .normal)
            cell.disConnectBtn.setTitleColor(UIColor.black, for: .normal)
            cell.disConnectBtn.backgroundColor = UIColor (red: 211/255.0, green: 211/255.0, blue: 211/255.0, alpha:  CGFloat(1.0))
            cell.disConnectBtn.layer.borderWidth = 0.5
            cell.disConnectBtn.layer.borderColor = UIColor.black.cgColor
        }
        if statuCheckstr .isEqual(to: "1")
        {
            cell.disConnectBtn.setTitle("Connect", for: .normal)
             cell.disConnectBtn.setTitleColor(UIColor.white, for: .normal)
            cell.disConnectBtn.backgroundColor = UIColor (red: 146/255.0, green: 208/255.0, blue: 78/255.0, alpha:  CGFloat(1.0))
           
        }
        
        
        cell.disConnectBtn.addTarget(self, action: #selector(self.ConnectBtnTapped), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.reg_IDstr = (self.responsevolunteersID .object(at: indexPath.row) as AnyObject) as! NSString
        
        let titlename: String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_lname") as! String
        
        
        self.usernameobjstr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        
         let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "userpost" as AnyObject,
            "id" : reg_IDstr as AnyObject,
            "myid" : userID as AnyObject]
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.postsApiCalling(param: params)
    }
    
    func tableView(_ tableView:UITableView, willDisplay cell:UITableViewCell, forRowAt indexPath:IndexPath)
    {
        if indexPath.row == (self.responseVolunteersArray.count - 1)
        {
            
            let postarray : NSArray = self.responseVolunteersArray .value(forKey: "reg_id") as! NSArray
            self.user_one_idStr = postarray.lastObject as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "findvoluns" as AnyObject,
                "id" : userID as AnyObject,
                "direction" : "down" as AnyObject,
                "limit" : self.user_one_idStr as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.volunteerPostsApiCalling(param: params)
            
        }
    }
    
    func postsApiCalling(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
            
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_USERPOSTSAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                           // print(responsejson)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.responsePostArray = (responsejson["posts"] as AnyObject) as! NSArray as! NSMutableArray
                                
                                
                                
                                for var i in 0..<self.responsePostArray.count
                                {
                                    
                                    let likesStr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "likes") as? String)!
                                    self.allsupportPeopleArray.add(likesStr)
                                    let islikestr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                    self.allisLikesPeopleArray.add(islikestr)
                                }
                                
                                if self.responsePostArray.count == 0
                                {
                                    
                                    let mainViewController = self.sideMenuController!
                                    let PeoplePostsVC = PeoplePostsViewController()
                                    PeoplePostsVC.responsePostArray = self.responsePostArray
                                    PeoplePostsVC.navStr = "peopleVC"
                                    PeoplePostsVC.peopleReg_idStr = self.reg_IDstr
                                    PeoplePostsVC.usernameObjstr = self.usernameobjstr
                                    PeoplePostsVC.allsupportPeopleArray = self.allsupportPeopleArray
                                    PeoplePostsVC.allisLikesPeopleArray = self.allisLikesPeopleArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(PeoplePostsVC, animated: true)
                                }
                                else
                                {
                                    let titlename: String = (self.responsePostArray .object(at: 0) as AnyObject).value(forKey: "reg_title") as! String
                                    let firstname : String = (self.responsePostArray .object(at: 0) as AnyObject).value(forKey: "reg_fname") as! String
                                    let middlename: String = (self.responsePostArray .object(at: 0) as AnyObject).value(forKey: "reg_mname") as! String
                                    let lastname : String = (self.responsePostArray .object(at: 0) as AnyObject).value(forKey: "reg_lname") as! String
                                    
                                    
                                    self.usernameobjstr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
                                    
                                    
                                    let mainViewController = self.sideMenuController!
                                    let PeoplePostsVC = PeoplePostsViewController()
                                    PeoplePostsVC.responsePostArray = self.responsePostArray
                                    PeoplePostsVC.navStr = "peopleVC"
                                    PeoplePostsVC.peopleReg_idStr = self.reg_IDstr
                                    PeoplePostsVC.allsupportPeopleArray = self.allsupportPeopleArray
                                    PeoplePostsVC.allisLikesPeopleArray = self.allisLikesPeopleArray
                                    PeoplePostsVC.usernameObjstr = self.usernameobjstr
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(PeoplePostsVC, animated: true)
                                }
                                
                                
                                
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                self.presentAlertWithTitle(title: "", message:(responsejson["msg"] as AnyObject) as! String )
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func ConnectBtnTapped(_ sender: UIButton) {
        
        let statusCheckStr : NSString = self.statusArray .object(at: sender.tag) as! NSString
        
        if statusCheckStr .isEqual(to: "0") {
           self.presentAlertWithTitle(title: "", message:"Already sent a request")
        }
        if statusCheckStr .isEqual(to: "1") {
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewFindpeopleObj)
            let indexPathRow = self.tableViewFindpeopleObj.indexPathForRow(at: buttonPosition)
            self.selectedRow = indexPathRow! as NSIndexPath
            let reg_idstr : NSString = self.responsevolunteersID .object(at: sender.tag) as! NSString
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "connect" as AnyObject,
                "id" : userID as AnyObject,
                "volun" : reg_idstr as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.ConnectFrndAPI(param: params)
        }
    }
    
    func ConnectFrndAPI(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_ConnectAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                                
                            {
                                self.FindPeopleAPICalling()
                                self.presentAlertWithTitle(title: "", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                            else
                            {
                                self.presentAlertWithTitle(title: "", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
    }
    
    //MARK: - SecondServiceApiCalling
    
    func volunteerPostsApiCalling(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_FindPeopleAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let arra  = (responsejson["volunteers"] as AnyObject) as! NSArray
                                if arra.count == 0
                                {
                                    
                                }
                                else
                                {
                                    self.responseVolunteersArray.addObjects(from: arra as! [Any])
                                    let volunteerUserIDArray : NSArray = (arra .value(forKey: "reg_id") as AnyObject) as! NSArray
                                    self.responsevolunteersID.addObjects(from: volunteerUserIDArray as! [Any])
                                    
                                    let statusCheckArray : NSArray = (arra .value(forKey: "status") as AnyObject) as! NSArray
                                    self.statusArray.addObjects(from: statusCheckArray as! [Any])
                                    for i in 0..<self.statusArray.count {
                                        let obj = self.statusArray[i]
                                        if (obj is NSNull) {
                                             self.statusArray [i] = "1"
                                        }
                                    }
                                    
                                    self.tableViewFindpeopleObj.reloadData()
                                }
                                
                            }
                            else {
                                self.presentAlertWithTitle(title: "", message:"Please try again.")
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }

   

}
