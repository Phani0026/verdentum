//
//  MyNetworkViewController.swift
//  Verdentum
//
//  Created by Verdentum on 06/10/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class MyNetworkViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableViewMyNetworkObj: UITableView!
    
    var responseVolunteersArray = NSMutableArray()
    var usernameobjstr = NSString()
    var responsevolunteersID = NSMutableArray()
    var responseFriendID = NSMutableArray()
    var user_one_idStr = NSString()
      var refreshControl = UIRefreshControl()
     var selectedRow = NSIndexPath()
    var volunteers_IDstr = NSString()
    var responsePostArray = NSMutableArray()
    var StatuStr = NSString()
    var allsupportPeopleArray = NSMutableArray()
    var allisLikesPeopleArray = NSMutableArray()
   
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableViewMyNetworkObj.estimatedRowHeight = 80
        self.tableViewMyNetworkObj.rowHeight = UITableViewAutomaticDimension
        
         self.tableViewMyNetworkObj .register(UINib (nibName: "NetworkCell", bundle: nil), forCellReuseIdentifier: "NetworkCell")
        
        // -----Refresh Control----
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(DashBoardViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        
       self.tableViewMyNetworkObj.addSubview(refreshControl)
        
         self.myNetworkApiCalling()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh(_ sender:AnyObject)
    {
        self.myNetworkApiCalling()
        // self.newTableViewObj.reloadData()
        self.refreshControl .endRefreshing()
    }
    
    //MARK: - HomeServiceApiCalling
    
    func myNetworkApiCalling() {
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "myvoluns" as AnyObject,
                "id" : userID as AnyObject ]
            let url = NSURL(string:BaseURL_MyNetworkAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        //print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                            self.responseVolunteersArray = (responsejson["volunteers"] as AnyObject) as! NSMutableArray
                                
                                if self.responseVolunteersArray.count == 0
                                {
                                   //self.presentAlertWithTitle(title: "", message:"No people in your Network... you can connect to people in the Find People tab.")
                                    
                                    self.StatuStr = "No people in your Network... You can connect to people in the Find People tab."
                                    self.tableViewMyNetworkObj.reloadData()
                                }
                                else
                                {
                                    let volunteerUserIDArray : NSArray = (self.responseVolunteersArray .value(forKey: "user_one_id") as AnyObject) as! NSArray
                                    self.responsevolunteersID = NSMutableArray(array: volunteerUserIDArray)
                                    let volunteerfriend_idArray : NSArray = (self.responseVolunteersArray .value(forKey: "friend_id") as AnyObject) as! NSArray
                                    self.responseFriendID = NSMutableArray(array: volunteerfriend_idArray)
                                    self.tableViewMyNetworkObj.reloadData()
                                }
                            }
                            else
                            {
                                self.presentAlertWithTitle(title: "", message:"Please Try again.")
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
        
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.responseVolunteersArray.count == 0 {
            let rect = CGRect(x: 0,
                              y: 0,
                              width: view.bounds.size.width,
                              height: view.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            
            noDataLabel.text = self.StatuStr as String
            noDataLabel.textColor = UIColor.black
            noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            noDataLabel.sizeToFit()
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableViewMyNetworkObj.backgroundView = noDataLabel
            return 0
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.responseVolunteersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView .dequeueReusableCell(withIdentifier: "NetworkCell") as! NetworkCell
        
        
        let url = URL(string: IMAGE_UPLOAD.appending(((self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!))
        cell.imageViewProfile.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        cell.imageViewProfile.layer.borderWidth = 2.0
        cell.imageViewProfile.layer.borderColor = UIColor.white.cgColor
        cell.imageViewProfile.layer.cornerRadius = 25
        cell.imageViewProfile.layer.masksToBounds = true
        
        
        let titlename: String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.responseVolunteersArray[indexPath.row] as AnyObject).value(forKey: "reg_lname") as! String
        
        
        self.usernameobjstr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        
        cell.nameLabelObj.text = usernameobjstr as String
        
        cell.disConnectBtn.tag = indexPath.row
        cell.disConnectBtn.addTarget(self, action: #selector(self.disConnectBtnTapped), for: .touchUpInside)
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        self.volunteers_IDstr = (self.responsevolunteersID .object(at: indexPath.row) as AnyObject) as! NSString
         let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "userpost" as AnyObject,
            "id" : self.volunteers_IDstr as AnyObject,
            "myid" : userID as AnyObject]        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.postsApiCalling(param: params)
        
    }
    
    func tableView(_ tableView:UITableView, willDisplay cell:UITableViewCell, forRowAt indexPath:IndexPath)
    {
        if indexPath.row == (self.responseVolunteersArray.count - 1)
        {
            
                let postarray : NSArray = self.responseVolunteersArray .value(forKey: "user_one_id") as! NSArray
                self.user_one_idStr = postarray.lastObject as! NSString
                let userID = UserDefaults.standard.value(forKey: "userID")
                let params:[String: AnyObject] = [
                    "purpose" : "myvoluns" as AnyObject,
                    "id" : userID as AnyObject,
                    "direction" : "down" as AnyObject,
                    "limit" : self.user_one_idStr as AnyObject]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.volunteerPostsApiCalling(param: params)
            
        }
    }
    
    func disConnectBtnTapped(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewMyNetworkObj)
        let indexPathRow = self.tableViewMyNetworkObj.indexPathForRow(at: buttonPosition)
        self.selectedRow = indexPathRow! as NSIndexPath
        let friendIDstr : NSString = self.responseFriendID .object(at: sender.tag) as! NSString
        let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "unfriend" as AnyObject,
            "id" : userID as AnyObject,
            "volun" : friendIDstr as AnyObject]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.disConnectFrndAPI(param: params)
    }
    
    func disConnectFrndAPI(param:[String: AnyObject])
    {
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_DisConnectFrndAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                                
                            {
                                self.responseVolunteersArray .removeObject(at: self.selectedRow.row)
                                self.tableViewMyNetworkObj.reloadData()
                            }
                            else
                            {
                                self.presentAlertWithTitle(title: " Disconnect Failed!", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
    }
    
    //MARK: - SecondServiceApiCalling
    
    func volunteerPostsApiCalling(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
            
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_MyNetworkAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let arra  = (responsejson["volunteers"] as AnyObject) as! NSArray
                                if arra.count == 0
                                {
                                    
                                }
                                else
                                {
                                    self.responseVolunteersArray.addObjects(from: arra as! [Any])
                                    let volunteerUserIDArray : NSArray = (arra .value(forKey: "user_one_id") as AnyObject) as! NSArray
                                    self.responsevolunteersID.addObjects(from: volunteerUserIDArray as! [Any])
                                    let volunteerfriend_idArray : NSArray = (arra .value(forKey: "friend_id") as AnyObject) as! NSArray
                                    self.responseFriendID.addObjects(from: volunteerfriend_idArray as! [Any])
                                    self.tableViewMyNetworkObj.reloadData()
                                }
                                
                            }
                            else {
                                self.presentAlertWithTitle(title: "", message:"Please try again.")
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func postsApiCalling(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_USERPOSTSAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.responsePostArray = (responsejson["posts"] as AnyObject) as! NSArray as! NSMutableArray
                                for var i in 0..<self.responsePostArray.count
                                {
                                    let likesStr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "likes") as? String)!
                                    self.allsupportPeopleArray.add(likesStr)
                                    
                                    let islikestr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                    self.allisLikesPeopleArray.add(islikestr)
                                }
                                if self.responsePostArray.count == 0
                                {
                                    
                                    
                                }
                                else
                                {
                                
                                let titlename: String = (self.responsePostArray .object(at: 0) as AnyObject).value(forKey: "reg_title") as! String
                                let firstname : String = (self.responsePostArray .object(at: 0) as AnyObject).value(forKey: "reg_fname") as! String
                                let middlename: String = (self.responsePostArray .object(at: 0) as AnyObject).value(forKey: "reg_mname") as! String
                                let lastname : String = (self.responsePostArray .object(at: 0) as AnyObject).value(forKey: "reg_lname") as! String
                                self.usernameobjstr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
                                
                                let mainViewController = self.sideMenuController!
                                let PeoplePostsVC = PeoplePostsViewController()
                                PeoplePostsVC.responsePostArray = self.responsePostArray
                                PeoplePostsVC.allsupportPeopleArray = self.allsupportPeopleArray
                                PeoplePostsVC.allisLikesPeopleArray = self.allisLikesPeopleArray
                                PeoplePostsVC.navStr = "networkpeople"
                                PeoplePostsVC.peopleReg_idStr = self.volunteers_IDstr
                                PeoplePostsVC.usernameObjstr = self.usernameobjstr
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(PeoplePostsVC, animated: true)
                                }
                            }
                           if (responsejson["status"] as AnyObject) .isEqual(to: 0) {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            let messagestr : NSString = ((responsejson["msg"] as AnyObject) as! NSString)
                            self.presentAlertWithTitle(title: "", message:messagestr as String)
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }

}
