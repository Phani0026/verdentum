//
//  AddPostViewController.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 23/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import Photos
import AVKit
//import Photos
import MBProgressHUD
import Alamofire
import LGSideMenuController

class AddPostViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIPickerViewDataSource,UIPickerViewDelegate,UITextViewDelegate,UITextFieldDelegate
{
    
    
    var cellString = NSString()
    // var rootListAssets = AssetsPickerController()
    var rootListAssets = SwiftAssetsViewController()
    var imageArray = NSMutableArray()
    //var imageArray : NSMutableArray = []
    var galleryImages = [UIImage]()
    var popoverController: UIPopoverController?
    var imagePicker = UIImagePickerController()
    var profilepicImageStr = NSString()
    // var imageArray : NSMutableArray = []
    var saveGalleryImage = UIImage()
    var screenWidth = CGFloat()
    var datePicker: UIDatePicker!
    var groupnameArray = NSArray()
    var groupNamePicker = [AnyObject]()
    var groupIDObjects = [AnyObject]()
    var selectedRow = NSIndexPath()
    var base64Images = [AnyObject]()
    var JsonimageArray = NSArray()
    var selectedGroupName  = NSString()
    var selectedGroupID = NSString()
    var groupAdminID = NSArray()
    var camImageArray = [UIImage]()
    var groupAdminStr = NSString()
    var statusID = NSArray()
    var statusStr = NSString()
    var placeholderLabel : UILabel!
    var wallArray = NSArray()
    var cameraMediaType = AVMediaTypeVideo
    var toolBar = UIToolbar()
    var navStr = NSString()
    var allSupportArray = NSMutableArray()
    var allcommentCountArray = NSMutableArray()
    var responsePostArray = NSMutableArray()
    var allisLikeArray = NSMutableArray()
    
    
    @IBOutlet var scrollviewObj: UIScrollView!
    @IBOutlet var subViewScrollObj: UIView!
    @IBOutlet var ViewscrollviewHeight: NSLayoutConstraint!
    @IBOutlet var collectionViewObj: UICollectionView!
    @IBOutlet var wallTextfieldObj: UITextField!
    @IBOutlet var pickerViewObj: UIPickerView!
    @IBOutlet var commentTextViewObj: UITextView!
    @IBOutlet var textViewHeight: NSLayoutConstraint!
    @IBOutlet var imageTableViewOBj: UITableView!
    @IBOutlet var heightTableViewObj: NSLayoutConstraint!
    // @IBOutlet var heightTableViewObj: NSLayoutConstraint!
    
    // MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
        
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = UIBarStyle.default
        toolBar.items = [UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(Done))]
        toolBar.sizeToFit()
        self.commentTextViewObj.inputAccessoryView = toolBar
        let screenRect: CGRect = UIScreen.main.bounds
        screenWidth = screenRect.size.width
        self.groupNamePicker = self.groupnameArray .value(forKey: "group_name") as! [AnyObject]
        self.groupAdminID = self.groupnameArray .value(forKey: "group_admin") as! NSArray
        self.statusID = self.groupnameArray .value(forKey: "status") as! NSArray
        self.groupIDObjects = self.groupnameArray .value(forKey: "group_id") as! [AnyObject]
        self.selectedGroupID = "0"
        self.collectionViewObj.delegate = self;
        self.collectionViewObj.dataSource = self;
        self.groupIDObjects.insert("0" as AnyObject, at: 0)
        self.groupNamePicker.insert("Wall" as AnyObject, at: 0)
        let str = NSAttributedString(string: "Wall", attributes: [NSForegroundColorAttributeName:UIColor.darkGray])
        self.wallTextfieldObj.attributedPlaceholder = str
        self.imagePicker.delegate = self
        
        // PlaceholderLabel
        
        placeholderLabel = UILabel()
        placeholderLabel.text = "Update Status"
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: (self.commentTextViewObj.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        self.commentTextViewObj.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (self.commentTextViewObj.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !self.commentTextViewObj.text.isEmpty
        
        // UIPickerView
        self.pickerViewObj = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.pickerViewObj.delegate = self
        self.pickerViewObj.dataSource = self
        self.pickerViewObj.backgroundColor = UIColor.white
        self.wallTextfieldObj.inputView = self.pickerViewObj
        self.pickerViewObj.reloadAllComponents()
        self.pickerViewObj.showsSelectionIndicator = true
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.backgroundColor = UIColor(red: 169.0/255.0, green: 169.0/255.0, blue: 169.0/255.0, alpha: 1.0)
        //toolBar.tintColor = UIColor(red: 104/255, green: 104/255, blue: 104/255, alpha: 1)
        toolBar.sizeToFit()
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        doneButton.setTitleTextAttributes([
            NSFontAttributeName : UIFont(name: "HelveticaNeue", size: 17)!,
            NSForegroundColorAttributeName : UIColor.black,
            ], for: .normal)
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        self.wallTextfieldObj.inputAccessoryView = toolBar
        self.collectionViewObj.register(UINib(nibName: "AddPostCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddPostCollectionCell")
        self.collectionViewObj.reloadData()
    }
    
    func Done(_ sender: UIBarButtonItem) {
        self.commentTextViewObj.resignFirstResponder()
    }
    
     // MARK:- ViewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        
         sideMenuController?.isLeftViewSwipeGestureDisabled = true
        self.collectionViewObj.reloadData()
    }
    
    
    // MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
       
       // self.collectionViewObj.reloadData()
    }
    
//    func textViewDidChange(_ textView: UITextView) {
//        placeholderLabel.isHidden = !self.commentTextViewObj.text.isEmpty
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func donePicker(_ sender: UIBarButtonItem) {
        self.pickerViewObj .removeFromSuperview()
        view.endEditing(true)
    }
    
    internal func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if self.wallTextfieldObj == textField {
            // self.selectedString = state
            //  toolBar.isHidden = false
            self.pickerViewObj.isHidden = false
            self.wallTextfieldObj.inputView = self.pickerViewObj
            //self.stateTexfieldObj.inputAccessoryView! = toolBar
            self.pickerViewObj.reloadAllComponents()
        }
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
       textView.resignFirstResponder()
    }
    
    func textViewDidChange(_ textView: UITextView) {
       
    self.placeholderLabel.isHidden = !self.commentTextViewObj.text.isEmpty
        
//        if ((textView.text as NSString).range(of:
//            "\n")).location == NSNotFound {
//            return
//        }
//        let resultStr: String = textView.text.replacingOccurrences(of: "\n", with: "")
//        self.commentTextViewObj.text = resultStr
//        print(self.commentTextViewObj.text)
    }
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//
//        if(text == "\n") {
//            textView.resignFirstResponder()
//            return false
//        }
//        return true
//    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    // #pragma mark - Picker View Data source
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.groupNamePicker.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return self.groupNamePicker[row] as? String
        
    }
    // MARK:- Picker View Delegate
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
   
        self.wallTextfieldObj.text = self.groupNamePicker[row] as? String
        self.selectedGroupID = self.groupIDObjects[row] as! NSString
        self.selectedGroupName = self.groupNamePicker[row] as! NSString
       // self.wallTextfieldObj.resignFirstResponder()
    }
    
    // MARK: - UICollectionViewDelegate and DataSource Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        var cell: AddPostCollectionCell?
        if self.imageArray.count == 0
        {
            self.heightTableViewObj.constant = 0
        }
        if self.imageArray.count == 1
        {
            self.heightTableViewObj.constant = 110
            self.ViewscrollviewHeight.constant = 600
        }
        if self.imageArray.count == 2
        {
            self.heightTableViewObj.constant = 205
            self.ViewscrollviewHeight.constant = 640
        }
        if self.imageArray.count == 3
        {
            self.heightTableViewObj.constant = 305
            self.ViewscrollviewHeight.constant = 770
        }
        if self.imageArray.count == 4
        {
            self.heightTableViewObj.constant = 420
            self.ViewscrollviewHeight.constant = 900
        }
        if self.imageArray.count == 5
        {
            self.heightTableViewObj.constant = 520
            self.ViewscrollviewHeight.constant = 1000
        }
        if self.imageArray.count == 6
        {
            self.heightTableViewObj.constant = 0
            // self.ViewscrollviewHeight.constant = 1000
        }
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPostCollectionCell", for: indexPath)as? AddPostCollectionCell
        cell?.removeBtnTapped.tag = indexPath.row
        cell?.removeBtnTapped.addTarget(self, action: #selector(self.removebtnTapped), for: .touchUpInside)
        cell?.layer.borderColor = UIColor.darkGray.cgColor
        cell?.layer.borderWidth = 1
        cell?.imageViewObj.image = self.imageArray[indexPath.row] as? UIImage
        return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: UIScreen.main.bounds.size.width - 40, height: 94.0)
    }
    
    func removebtnTapped(_ sender: UIButton) {
       /* self.base64Images.removeAll()
        self.galleryImages.removeAll()
        self.imageArray.removeObject(at: sender.tag)
        
        
        if self.imageArray.count == 0
        {
            self.heightTableViewObj.constant = 0
            self.imageArray.removeAllObjects()
            self.camImageArray.removeAll()
            self.collectionViewObj.reloadData()
            
        }
        if self.imageArray.count == 1
        {
            self.heightTableViewObj.constant = 110
            self.ViewscrollviewHeight.constant = 600
           
        }
        if self.imageArray.count == 2
        {
            self.heightTableViewObj.constant = 205
            self.ViewscrollviewHeight.constant = 640
            
        }
        if self.imageArray.count == 3
        {
            self.heightTableViewObj.constant = 305
            self.ViewscrollviewHeight.constant = 770
            
        }
        if self.imageArray.count == 4
        {
            self.heightTableViewObj.constant = 420
            self.ViewscrollviewHeight.constant = 900
            
        }
        if self.imageArray.count == 5
        {
            self.heightTableViewObj.constant = 520
            self.ViewscrollviewHeight.constant = 1000
            
        }
        if self.imageArray.count == 6
        {
            self.heightTableViewObj.constant = 0
            // self.ViewscrollviewHeight.constant = 1000
        }
        
        self.collectionViewObj.reloadData()*/
       
        self.base64Images.remove(at: sender.tag)
        self.galleryImages.removeAll()
        self.imageArray.removeObject(at: sender.tag)
        collectionViewConstantHeight()
    }
    
    func collectionViewConstantHeight(){
        let count = self.imageArray.count
        switch count {
        case 0:
            self.heightTableViewObj.constant = 0
            break
        case 1:
            self.heightTableViewObj.constant = 110
            self.ViewscrollviewHeight.constant = 600
            break
        case 2:
            self.heightTableViewObj.constant = 205
            self.ViewscrollviewHeight.constant = 640
            break
        case 3:
            self.heightTableViewObj.constant = 305
            self.ViewscrollviewHeight.constant = 770
            break
        case 4:
            self.heightTableViewObj.constant = 420
            self.ViewscrollviewHeight.constant = 900
            break
        case 5:
            self.heightTableViewObj.constant = 520
            self.ViewscrollviewHeight.constant = 1000
            break
        case 6:
            self.heightTableViewObj.constant = 0
            break
        default:
            break
        }
        self.collectionViewObj.reloadData()
    }
    
    @IBAction func backBtnTapped(_ sender: Any)
    {
        self.imageArray = []
       /* let mainViewController = self.sideMenuController!
        let DashBoardVC = DashBoardViewController()
        DashBoardVC.responsePostArray = self.responsePostArray
        DashBoardVC.allcommentCountArray = self.allcommentCountArray
        DashBoardVC.allSupportArray = self.allSupportArray
        DashBoardVC.allisLikeArray = self.allisLikeArray
        DashBoardVC.navStr = "HomereportPostNav"
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(DashBoardVC, animated: false)*/
       
        for DashBoardVC in (self.navigationController?.viewControllers ?? []) {
            if DashBoardVC is DashBoardViewController {
                _ = self.navigationController?.popToViewController(DashBoardVC, animated: true)
                break
            }
        }
    }
    
    @IBAction func walldropDownBtnTapped(_ sender: Any) {
    }
    
    @IBAction func takePhotoBtnTapped(_ sender: Any)
    {
        if self.imageArray.count == 5  {
            let alertSuccess = UIAlertController(title: "Maximum 5 Photos", message: "You can only take 5 photos", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction (title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
            alertSuccess.addAction(okAction)
            
            self.present(alertSuccess, animated: true, completion: nil)
        }
        else {
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: self.cameraMediaType)
            
            switch cameraAuthorizationStatus {
            case .denied:
                alertPromptToAllowCameraAccessViaSetting()
                break
            case .authorized:
                if UIImagePickerController.isSourceTypeAvailable(.camera)
                {
                    self.imagePicker.delegate=self
                    self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                    self.imagePicker.allowsEditing = true
                    self.present(self.imagePicker, animated: true, completion: nil)
                } else {
                    noCamera()
                }
                break
            case .restricted: break
                
            case .notDetermined:
                // Prompting user for the permission to use the camera.
                AVCaptureDevice.requestAccess(forMediaType: cameraMediaType) { granted in
                    if granted {
                        print("Granted access to \(self.cameraMediaType)")
                        
                    } else {
                        print("Denied access to \(self.cameraMediaType)")
                        self.alertPromptToAllowCameraAccessViaSetting()
                    }
                }
            }
        }
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        let alert = UIAlertController(title: "Alert", message: "Photos access required to...", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel) { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        
        present(alert, animated: true)
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
      
        let cameraImage = info[UIImagePickerControllerEditedImage] as? UIImage!
        if let updatedImage = cameraImage?.updateImageOrientionUpSide() {
            
            
            if self.imageArray.count >= 5 {
                let alertSuccess = UIAlertController(title: "Maximum 5 Photos", message: "You can only take 5 photos", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction (title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                alertSuccess.addAction(okAction)
                
                self.present(alertSuccess, animated: true, completion: nil)
            }
            else {
                self.imageArray.insert(updatedImage , at: 0)
                let imageData : NSData = UIImageJPEGRepresentation(updatedImage, 0.4)! as NSData
                let base64Image : NSString = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
                self.base64Images.insert(base64Image, at: 0)
                // print(self.imageArray.count)
                // self.base64Images.removeAll()
                self.galleryImages.removeAll()
                collectionViewConstantHeight()
            }
            
          /*  if self.imageArray.count >= 5 {   // if images more than 5 , remove last one
                self.imageArray.removeLastObject()
                self.base64Images.removeLast()
            }
            self.imageArray.insert(updatedImage , at: 0)
            let imageData : NSData = UIImageJPEGRepresentation(updatedImage, 0.4)! as NSData
            let base64Image : NSString = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
            self.base64Images.insert(base64Image, at: 0)
           // print(self.imageArray.count)
           // self.base64Images.removeAll()
            self.galleryImages.removeAll()
            collectionViewConstantHeight()*/
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        defer {
            picker.dismiss(animated: true)
        }
    }
    
    func noCamera()
    {
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, This device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    @IBAction func chooseBtnTapped(_ sender: Any) {
        
        if self.imageArray.count == 5  {
            let alertSuccess = UIAlertController(title: "Maximum 5 Photos", message: "You can only take 5 photos", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction (title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
            alertSuccess.addAction(okAction)
            
            self.present(alertSuccess, animated: true, completion: nil)
        }
        else {
        
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            print("Access has been granted")
        }
            
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
            print("Access has been denied")
             self.alertPromptToAllowCameraAccessViaSetting()
        }
            
        else if (status == PHAuthorizationStatus.notDetermined) {
            
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    print("Access has been granted.")
                }
                else {
                     print(" Access has been denied.")
                     self.alertPromptToAllowCameraAccessViaSetting()
                }
            })
        }
            
        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
            print(" Restricted access - normally won't happen")
        }
        
        
        self.rootListAssets.didSelectAssets = {(assets: Array<PHAsset?>) -> () in
            for var i in 0..<assets.count {
                
                
                let manager = PHImageManager.default()
                let asset = assets[i]
                let requestOptions = PHImageRequestOptions()
                requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
                requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
                // this one is key
                requestOptions.isSynchronous = true
                if ((asset as AnyObject).mediaType == PHAssetMediaType.image)
                {
                    PHImageManager.default().requestImage(for: asset!, targetSize: PHImageManagerMaximumSize, contentMode:  .aspectFill, options: requestOptions, resultHandler: { (pickedImage, info) in
                        self.galleryImages.append(pickedImage!)
                        // you can get image like this way
                    })
                }
            }
            for var i in 0..<self.galleryImages.count {
                if self.imageArray.count >= 5 {
                   // self.imageArray.removeLastObject()
                   // self.base64Images.removeLast()
                } else {
                self.imageArray.insert(self.galleryImages[i] , at: 0)
                
                let imageData : NSData = UIImageJPEGRepresentation(self.galleryImages[i], 0.4)! as NSData
                let base64Image : NSString = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
                self.base64Images.insert(base64Image, at: 0)
                }
            }
            if self.imageArray.count == 0
            {
                self.heightTableViewObj.constant = 0
            }
            if self.imageArray.count == 1
            {
                self.heightTableViewObj.constant = 110
                self.ViewscrollviewHeight.constant = 600
            }
            if self.imageArray.count == 2
            {
                self.heightTableViewObj.constant = 205
                self.ViewscrollviewHeight.constant = 640
                
            }
            if self.imageArray.count == 3
            {
                self.heightTableViewObj.constant = 305
                self.ViewscrollviewHeight.constant = 770
                
            }
            if self.imageArray.count == 4
            {
                self.heightTableViewObj.constant = 420
                self.ViewscrollviewHeight.constant = 900
                
            }
            if self.imageArray.count == 5
            {
                self.heightTableViewObj.constant = 520
                self.ViewscrollviewHeight.constant = 1000
                
            }
            if self.imageArray.count == 6
            {
                self.heightTableViewObj.constant = 0
                // self.ViewscrollviewHeight.constant = 1000
            }
            
           // self.base64Images.removeAll()
            self.galleryImages.removeAll()
            //self.imagePicker.removeFromParentViewController()
            self.collectionViewObj.reloadData()
            
        }
        let navigationController = UINavigationController(rootViewController:  self.rootListAssets)
        present(navigationController, animated: true, completion: nil)
        }
    }
    
    
    
    @IBAction func postBtnTapped(_ sender: Any) {
        
        let commentstr : String = self.commentTextViewObj.text!
        let trimmedString = commentstr.trimmingCharacters(in: .whitespacesAndNewlines)
        print(trimmedString)
            if trimmedString == ""
        {
            if self.imageArray.count == 0
            {
                let alertController = UIAlertController(title: "", message: "Cannot post empty data.", preferredStyle: .actionSheet)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
            else {
                let usedID = UserDefaults.standard.value(forKey: "userID")
                let dict = ["purpose": "addpost", "id": usedID as Any,"imgURI" : self.base64Images,"group": [
                    "group_id" : self.selectedGroupID ,
                    "group_name" : self.selectedGroupName],
                            "status":self.commentTextViewObj.text] as [String: Any]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.AddpostsApiCalling(param: dict as [String : AnyObject])
            }
            
        } else {
            if self.commentTextViewObj.text.count == 0  {
                
                if self.imageArray.count == 0
                {
                    let alertController = UIAlertController(title: "", message: "Cannot Post Empty Data.", preferredStyle: .actionSheet)
                    //let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                    //}
                    //alertController.addAction(cancelAction)
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                }
                else
                {
                    let usedID = UserDefaults.standard.value(forKey: "userID")
                    if self.imageArray.count == 0
                    {
                        let params:[String: AnyObject] = [
                            "purpose" : "addpost" as AnyObject,
                            "id" : usedID as AnyObject,
                            "imgURI" : [] as AnyObject,
                            "group" : [
                                "group_id" : self.selectedGroupID as AnyObject,
                                "group_name" : self.selectedGroupName as AnyObject] as AnyObject,
                            "status" : self.commentTextViewObj.text as AnyObject]
                        MBProgressHUD.showAdded(to: self.view, animated: true)
                        self.AddpostsApiCalling(param: params)
                    }
                    else
                    {
                        if self.base64Images.count == 0 {
                            let alertController = UIAlertController(title: "", message: "Cannot Post Empty Data.", preferredStyle: .alert)
                            //let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                            //}
                            //alertController.addAction(cancelAction)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                        else {
                            let dict = ["purpose": "addpost", "id": usedID as Any,"imgURI" : self.base64Images,"group": [
                                "group_id" : self.selectedGroupID ,
                                "group_name" : self.selectedGroupName],
                                        "status":self.commentTextViewObj.text] as [String: Any]
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            self.AddpostsApiCalling(param: dict as [String : AnyObject])
                        }
                        
                    }
                }
                
            }
            else {
                
                let usedID = UserDefaults.standard.value(forKey: "userID")
                if self.imageArray.count == 0
                {
                    let params:[String: AnyObject] = [
                        "purpose" : "addpost" as AnyObject,
                        "id" : usedID as AnyObject,
                        "imgURI" : [] as AnyObject,
                        "group" : [
                            "group_id" : self.selectedGroupID as AnyObject,
                            "group_name" : self.selectedGroupName as AnyObject] as AnyObject,
                        "status" : self.commentTextViewObj.text as AnyObject]
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    self.AddpostsApiCalling(param: params)
                }
                else
                {
                    if self.base64Images.count == 0 {
                        let alertController = UIAlertController(title: "", message: "Cannot post empty data.", preferredStyle: .alert)
                        //let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                        //}
                        //alertController.addAction(cancelAction)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                    else {
                        let dict = ["purpose": "addpost", "id": usedID as Any,"imgURI" : self.base64Images,"group": [
                            "group_id" : self.selectedGroupID ,
                            "group_name" : self.selectedGroupName],
                                    "status":self.commentTextViewObj.text] as [String: Any]
                        MBProgressHUD.showAdded(to: self.view, animated: true)
                        self.AddpostsApiCalling(param: dict as [String : AnyObject])
                    }
                    
                }
            }
        }
        
        
       
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func AddpostsApiCalling(param:[String: AnyObject])
    {
        
        if Reachability.isConnectedToNetwork() == true {
            
            Alamofire.request(BaseURL_ADDPostAPI, method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    if let JSON = response.result.value as? NSDictionary {
                       MBProgressHUD.hide(for: self.view, animated: true)
                        if (JSON["status"] as AnyObject) .isEqual(to: 1)
                        {
                                let mainViewController = self.sideMenuController!
                                let dashboardVC = DashBoardViewController()
                                dashboardVC.navStr = "AddPostVC"
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(dashboardVC, animated: true)
                        }
                        if (JSON["status"] as AnyObject) .isEqual(to: 0)
                        {
                            let alertController = UIAlertController(title: "", message: ((JSON["msg"] as AnyObject) as! String), preferredStyle: .actionSheet)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                        
                    }
            }
        }
        else {
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    
    
}
