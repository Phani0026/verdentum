//
//  NetworkingManager.swift
//  DgDiary
//
//  Created by Vijay+Mohit on 6/18/17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration

class NetworkingManager{
    
       /// called when a response from a server is received
    typealias didFinishHandler = (NSArray) -> Void
    
    /// called when failed to connect to the server
    typealias didFailHandler = (Error) -> Void
    
    /**
     Generates a URL for an API by converting the parameters to JSON
     - parameter API: URL in string format
     - parameter parameters: Parameters as a dictionary
     */
    class func generateURLForAPI(_ API: String, parameters: AnyObject) -> String {
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions(rawValue: 0))
            guard let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) else { return "" }
            guard let url = (API + jsonString).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { return "" }
            return url
        }catch{
            let error = error as NSError
            print("JSON Conversion Error: \(error)")
            return ""
            
        }
    }
    
    
    /// POST DATA
    class func postDataToServer(_ Parameters: AnyObject, serviceURL: String, didFinish: @escaping didFinishHandler, didFail: @escaping didFailHandler) {
        let url = generateURLForAPI(serviceURL, parameters: Parameters)
        print(url)
        Alamofire.request(url).responseJSON { response in
            guard let jsonData = response.result.value ,response.result.isSuccess else {
                didFail(response.result.error!)
                return
            }
            let json = jsonData
            didFinish(json as! NSArray)
        }
    }
    
}
