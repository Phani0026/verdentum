//
//  NewsImagesCell.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 21/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class NewsImagesCell: UICollectionViewCell {

   
   
   
    @IBOutlet var youtubeWebViewObj: UIWebView!
    
    @IBOutlet var webViewBtnTapped: UIButton!
    @IBOutlet var mainWebViewObj: UIView!
    
    @IBOutlet var viewHEightCOnst: UIView!
    @IBOutlet var btnHEightconst: NSLayoutConstraint!
   
    @IBOutlet var newsImageBtnTapped: UIButton!
    @IBOutlet var imageViewObjCollectionview: UIImageView!
     #if os(tvOS)
    override func awakeFromNib() {
        super.awakeFromNib()
         playerview.delegate = self
        imageViewObjCollectionview.adjustsImageWhenAncestorFocused = true
    }
    #endif
}


