//
//  FooterCell.swift
//  Verdentum
//
//  Created by Verdentum on 25/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class FooterCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextViewDelegate {

    var base64Images = [AnyObject]()
    var profilepicImageStr = NSString()
    var imageArray = NSMutableArray()
    var galleryImages = [UIImage]()
    var placeholderLabel = UILabel()
    var cellString = NSString()
    
     var delegate: CellInfoDelegate?
    @IBOutlet var camBtnTaped: UIButton!
    @IBOutlet var youtubeTextfieldUrl: UITextField!
    @IBOutlet var commentTextviewObj: UITextView!
    @IBOutlet var heightColView: NSLayoutConstraint!
    @IBOutlet var collectionViewObj: UICollectionView!
    @IBOutlet var uploadProgramActivityBtn: UIButton!
    
    @IBOutlet var textViewHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
       
       
        placeholderLabel.text = "Additinal Notes/Comments"
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: (self.commentTextviewObj.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        self.commentTextviewObj.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (self.commentTextviewObj.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !self.commentTextviewObj.text.isEmpty
        
        self.commentTextviewObj.layer.borderWidth = 1
        self.commentTextviewObj.layer.borderColor = UIColor.white.cgColor
        self.collectionViewObj.delegate = self
        self.collectionViewObj.dataSource = self
        self.collectionViewObj.register(UINib(nibName: "AddPostCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddPostCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  

    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !self.commentTextviewObj.text.isEmpty
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if(text == "\n") {
//            textView.resignFirstResponder()
//            return false
//        }
        return true
    }
    
    func setCollectionData (collection : NSMutableArray)    {
        self.imageArray = collection
        self.collectionViewObj.reloadData()
    }
    
    // MARK: - UICollectionViewDelegate and DataSource Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.imageArray.count
    }    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        var cell: AddPostCollectionCell?
        // self.heightTableViewObj.constant = CGFloat(self.imageArray.count * 110)
        if self.imageArray.count == 0
        {
            self.heightColView.constant = 0
           // self.textViewHeight.constant = 70
        }
        if self.imageArray.count == 1
        {
            self.heightColView.constant = 110
            //self.textViewHeight.constant = 70
        }
        if self.imageArray.count == 2
        {
            self.heightColView.constant = 205
            //self.textViewHeight.constant = 70
            
        }
        if self.imageArray.count == 3
        {
            self.heightColView.constant = 305
           // self.textViewHeight.constant = 70
            
        }
        if self.imageArray.count == 4
        {
            self.heightColView.constant = 420
            //self.textViewHeight.constant = 70
            
        }
        if self.imageArray.count == 5
        {
            self.heightColView.constant = 520
            //self.textViewHeight.constant = 70
            
        }
        if self.imageArray.count == 6
        {
            self.heightColView.constant = 0
           //self.textViewHeight.constant = 70
        }
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPostCollectionCell", for: indexPath)as? AddPostCollectionCell
        cell?.removeBtnTapped.tag = indexPath.row
        cell?.removeBtnTapped.addTarget(self, action: #selector(self.removebtnTapped), for: .touchUpInside)
        cell?.layer.borderColor = UIColor.darkGray.cgColor
        cell?.layer.borderWidth = 1
        cell?.imageViewObj.image = self.imageArray[indexPath.row] as? UIImage
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: UIScreen.main.bounds.size.width - 40, height: 94.0)
    }
    
    func removebtnTapped(_ sender: UIButton) {
       // self.base64Images.removeAll()
        self.galleryImages.removeAll()
        self.imageArray.removeObject(at: sender.tag)
        self.base64Images.remove(at: sender.tag)
        if self.imageArray.count == 0
        {
            self.heightColView.constant = 0
             self.imageArray.removeAllObjects()
             self.base64Images.removeAll()
            if let delegate = self.delegate {
                delegate.Base64images(Base64Images: self.base64Images)
            }
            let notificationName = Notification.Name("didSelectImageFromCollectionViewHeight")
            cellString = "\(self.imageArray.count)" as NSString
            NotificationCenter.default.post(name: notificationName, object: cellString)
            self.collectionViewObj.reloadData()
        }
        if self.imageArray.count == 1
        {
            self.heightColView.constant = 110
            if let delegate = self.delegate {
                delegate.Base64images(Base64Images: self.base64Images)
            }
            let notificationName = Notification.Name("didSelectImageFromCollectionViewHeight")
            cellString = "\(self.imageArray.count)" as NSString
            NotificationCenter.default.post(name: notificationName, object: cellString)
        }
        if self.imageArray.count == 2
        {
            self.heightColView.constant = 205
            if let delegate = self.delegate {
                delegate.Base64images(Base64Images: self.base64Images)
            }
            let notificationName = Notification.Name("didSelectImageFromCollectionViewHeight")
            cellString = "\(self.imageArray.count)" as NSString
            NotificationCenter.default.post(name: notificationName, object: cellString)
            
        }
        if self.imageArray.count == 3
        {
            self.heightColView.constant = 305
            if let delegate = self.delegate {
                delegate.Base64images(Base64Images: self.base64Images)
            }
            let notificationName = Notification.Name("didSelectImageFromCollectionViewHeight")
            cellString = "\(self.imageArray.count)" as NSString
            NotificationCenter.default.post(name: notificationName, object: cellString)
            
        }
        if self.imageArray.count == 4
        {
            self.heightColView.constant = 420
            if let delegate = self.delegate {
                delegate.Base64images(Base64Images: self.base64Images)
            }
            let notificationName = Notification.Name("didSelectImageFromCollectionViewHeight")
            cellString = "\(self.imageArray.count)" as NSString
            NotificationCenter.default.post(name: notificationName, object: cellString)
            
        }
        if self.imageArray.count == 5
        {
            self.heightColView.constant = 535
            if let delegate = self.delegate {
                delegate.Base64images(Base64Images: self.base64Images)
            }
            let notificationName = Notification.Name("didSelectImageFromCollectionViewHeight")
            cellString = "\(self.imageArray.count)" as NSString
            NotificationCenter.default.post(name: notificationName, object: cellString)
            
        }
        if self.imageArray.count == 6
        {
            self.heightColView.constant = 0
            //self.textViewHeight.constant = 70
            // self.ViewscrollviewHeight.constant = 1000
        }
        
        self.collectionViewObj.reloadData()
    }

    
}
