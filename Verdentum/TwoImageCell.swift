//
//  TwoImageCell.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 22/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit

class TwoImageCell: UICollectionViewCell {

    @IBOutlet var firstimageSecondBtn: UIButton!
    
    @IBOutlet var secondimageSecondBtn: UIButton!
    @IBOutlet var imageViewOne2: UIImageView!
    
    @IBOutlet var imageViewTwo2: UIImageView!
     #if os(tvOS)
    override func awakeFromNib() {
        super.awakeFromNib()
         imageViewOne2.adjustsImageWhenAncestorFocused = true
        imageViewTwo2.adjustsImageWhenAncestorFocused = true
    }
    #endif
}
