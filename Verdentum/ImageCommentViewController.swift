//
//  ImageCommentViewController.swift
//  Verdentum
//
//  Created by Verdentum on 18/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import LGSideMenuController

class ImageCommentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate {
    
    @IBOutlet var chatTableViewObj: UITableView!
    @IBOutlet var dummyHeightConstarint: NSLayoutConstraint!
    @IBOutlet var chatTextViewOBj: UITextField!
    
    @IBOutlet var imageViewsentObj: UIImageView!
    @IBOutlet var sendBtnObj: UIButton!
    
    var headerStr = String()
    var allisLikesPersonalArray = NSMutableArray()
    var allsupportPeopleArray = NSMutableArray()
    var WorldViewheaderTitleStr = NSString()
    var CommentCountMutableArr = NSMutableArray()
    var commentsArray = NSMutableArray()
    var responsePostArray = NSMutableArray()
    var post_idStr = NSString()
    var image_Idstr = NSString()
    var placeholderLabel : UILabel!
    var ButtonIndex: Int = 0
    var lastpostIDStr = NSString()
    var downloadImage = [UIImage]()
    var verticalOffset: Int = 0
    var addCommentArray = NSArray()
    var verticalOffsetIndex: Int = 0
    var commentsImagesArray = NSArray()
    var commentcoutnstr = [NSString]()
    var imageResponseArray = NSArray()
    var usernameobjstr = NSString()
    var navStr = NSString()
    var responseUserPostsArray = NSMutableArray()
    var userNameObjStr = NSString()
    var headerNameStr = NSString()
    var groupImageStr = NSString()
    var groupIDStr = NSString()
    var programArray = NSMutableArray()
    var program_idstr = NSString()
    var userID_Registered = NSString()
    var programNamestr = NSString()
    var commentPost_ID = NSString()
    var peopleReg_idStr = NSString()
    var allcommentCountArray = NSMutableArray()
    var allPersonalcommentsArray = NSMutableArray()
    var allFeedcommentCountArray = NSMutableArray()
    var allSupportArray = NSMutableArray()
    var headerTitleStr = NSString()
    var allSupportPersonalArray = NSMutableArray()
    var allsupportFeedArray = NSMutableArray()
    var allsupportGroupArray = NSMutableArray()
    var allisLikesGroupArray = NSMutableArray()
    var allisLikesPeopleArray = NSMutableArray()
    var allisLikesFeedArray = NSMutableArray()
    var personalTitleName = NSString()
    var commentUsedID = String()
    var allisLikeArray = NSMutableArray()
   

     // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        if self.commentsArray.count == 0 {
        //            self.presentAlertWithTitle(title: "", message:"No Comments Found, Please write Comments")
        //        }
        
        self.sendBtnObj.isEnabled = false
        self.imageViewsentObj.image = UIImage (named: "sentHide")
        self.navigationController?.isNavigationBarHidden = true
        self.chatTableViewObj.estimatedRowHeight = 113
        self.chatTableViewObj.rowHeight = UITableViewAutomaticDimension
        
        self.chatTableViewObj.register(UINib(nibName: "ConversationCell", bundle: nil), forCellReuseIdentifier: "ConversationCell")
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CommentsViewController.dismissKeyboard))
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        self.chatTableViewObj.reloadData()
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
     // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sideMenuController?.isLeftViewSwipeGestureDisabled = true
        chatTableViewObj.estimatedRowHeight = 44
        chatTableViewObj.rowHeight = UITableViewAutomaticDimension
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Show and Hide Keyboard
    
    func keyboardWillShow(_ notification: Notification) {
        let info: [AnyHashable: Any]? = notification.userInfo
        let animationDuration = CDouble(info?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? TimeInterval())
        let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect ?? CGRect.zero
        let window = UIApplication.shared.windows[0] as? UIWindow ?? UIWindow()
        let mainSubviewOfWindow: UIView? = window.rootViewController?.view
        let keyboardFrameConverted: CGRect = (mainSubviewOfWindow?.convert(keyboardFrame, from: window as? UIView ?? UIView()))!
        let tableSize: CGSize = chatTableViewObj.contentSize
        chatTableViewObj.contentOffset = CGPoint(x: 0, y: tableSize.height)
        dummyHeightConstarint.constant = keyboardFrameConverted.size.height
        UIView.animate(withDuration: animationDuration, animations: {() -> Void in
            self.view.layoutIfNeeded()
            
        })
    }
    
    func keyboardWillHide(_ notification: Notification) {
        let info: [AnyHashable: Any]? = notification.userInfo
        let animationDuration = CDouble(info?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? TimeInterval())
        view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.layoutIfNeeded()
        dummyHeightConstarint.constant = 0
        UIView.animate(withDuration: animationDuration, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    
    // MARK: - UITableViewDelegate and DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.commentsArray.count == 0 {
            let rect = CGRect(x: 0,
                              y: 0,
                              width: view.bounds.size.width,
                              height: view.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            
            noDataLabel.text = "No Comment found"
            noDataLabel.textColor = UIColor.black
            noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            noDataLabel.sizeToFit()
            noDataLabel.textAlignment = NSTextAlignment.center
            self.chatTableViewObj.backgroundView = noDataLabel
            
            return 0
        }
        else
        {
            let rect = CGRect(x: 0,
                              y: 0,
                              width: view.bounds.size.width,
                              height: view.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            
            noDataLabel.text = ""
            noDataLabel.textColor = UIColor.black
            noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            noDataLabel.sizeToFit()
            noDataLabel.textAlignment = NSTextAlignment.center
            self.chatTableViewObj.backgroundView = noDataLabel
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ConversationCell = tableView.dequeueReusableCell(withIdentifier: "ConversationCell") as! ConversationCell
        
        let url = URL(string: IMAGE_UPLOAD.appending(((self.commentsArray[indexPath.row] as AnyObject).value(forKey: "avatar") as? String!)!))
        cell.imageViewObj.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        cell.imageViewObj.layer.borderWidth = 1.0
        cell.imageViewObj.layer.borderColor = UIColor.gray.cgColor
        cell.imageViewObj.layer.cornerRadius = cell.imageViewObj.bounds.width/2
        cell.imageViewObj.layer.masksToBounds = true
        
        
        let titlename: String = (self.commentsArray[indexPath.row] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.commentsArray[indexPath.row] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.commentsArray[indexPath.row] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.commentsArray[indexPath.row] as AnyObject).value(forKey: "reg_lname") as! String
        
        
        
        let imagePathstr : String = (self.commentsArray[indexPath.row] as AnyObject).value(forKey: "image") as! String
        
        if imagePathstr.isEqual("") {
            cell.imageHeightConstant.constant = 0
        }
        else {
            cell.imageHeightConstant.constant = 150
            let imageURLstr : String = IMAGE_UPLOAD.appending(((self.commentsArray[indexPath.row]  as AnyObject).value(forKey: "image") as? String!)!) 
            let imageurl = URL(string: imageURLstr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            
            cell.commentImageObj.sd_setImage(with: imageurl!, placeholderImage: UIImage(named: ""),options: SDWebImageOptions.highPriority, completed: { (image, error, cacheType, imageURL) in
                
            })
        }
        
        
        
        let usernameobjstr : String = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as String
        
        cell.nameLabelObj.text = usernameobjstr
        
        let capitalized:String = (cell.nameLabelObj.text!.capitalized)
        cell.nameLabelObj.text = capitalized
        
        let myDate = (self.commentsArray[indexPath.row] as AnyObject).value(forKey: "date") as? String
        let myDateString = myDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        let Dateobj = dateFormatter.date(from: myDateString!)!
        dateFormatter.dateFormat = "MMM dd, YYYY HH:mm a"
        let somedateString = dateFormatter.string(from: Dateobj)
        cell.timeAndDateObj.text = somedateString
        
        cell.commentLabelObj.text = (self.commentsArray[indexPath.row] as AnyObject).value(forKey: "text") as? String
        cell.profileViewBtn.tag = indexPath.row
        cell.profileViewBtn.addTarget(self, action: #selector(self.commentProfileView), for: .touchUpInside)
        return cell
    }
    
    //MARK: - Comment Profile API Calling
    
    func commentProfileView(_ sender: UIButton) {
        
        let titlename: String = (self.commentsArray[sender.tag] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.commentsArray[sender.tag] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.commentsArray[sender.tag] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.commentsArray[sender.tag] as AnyObject).value(forKey: "reg_lname") as! String
        
        
        let usernameobjstr : String = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as String
        
        let comment_userID : String = ((self.commentsArray .object(at: sender.tag) as AnyObject) .value(forKey: "user_id") as AnyObject) as! String
        
        let userID : NSString = UserDefaults.standard.value(forKey: "userID") as! NSString
        
        if  userID .isEqual(to: comment_userID){
            let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
            CommentsVC.commentUsedID = comment_userID
            CommentsVC.headerStr = usernameobjstr
            CommentsVC.navStr = "dashBoardComment"
            self.navigationController?.pushViewController(CommentsVC , animated: true)
        }
        else {
            if self.navStr == "dashBoardComment" {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "dashboarpersonalprofileComment" {
                
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "worldViewFeedprofileComment" {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "WorldViewGroupCommentPF" {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "userPostStr"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "NewPage" {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "PeoplesNewPage"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "GroupPageVC"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
                
            }
            if self.navStr == "MyProgramFeedVC"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "MyProgramPrsnlVC"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "joinProgram"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "joinProgramVC"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "WorldViewGroup"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
                
            }
            if self.navStr == "WorldViewpersonalGroup"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
            if self.navStr == "WorldViewIndividualVC"
            {
                let CommentsVC = CommentProfileVC(nibName: "CommentProfileVC", bundle: nil)
                CommentsVC.commentUsedID = comment_userID
                CommentsVC.headerStr = usernameobjstr
                CommentsVC.navStr = "dashBoardComment"
                self.navigationController?.pushViewController(CommentsVC , animated: true)
            }
        }
    }
    
    // MARK: - ScrollView
    
    func scrollTableView(atCurrentMessage arrayObj: [Any]) {
        // [self.converstionTableView reloadData];
        let lastRowNumber = Int(chatTableViewObj.numberOfRows(inSection: 0)) - 1
        if lastRowNumber >= 0 {
            let ip = IndexPath(row: lastRowNumber, section: 0)
            chatTableViewObj.scrollToRow(at: ip, at: .bottom, animated: false)
            //   [self.converstionTableView reloadData];
        }
    }
    
    func scrollingTableView(atCurrentMessage arrayObj: [Any]) {
        chatTableViewObj.reloadData()
        let lastRowNumber = Int(chatTableViewObj.numberOfRows(inSection: 0)) - 1
        if lastRowNumber >= 0 {
            let ip = IndexPath(row: lastRowNumber, section: 0)
            chatTableViewObj.scrollToRow(at: ip, at: .bottom, animated: false)
            //   [self.converstionTableView reloadData];
        }
    }
    
    
    // MARK: - UITextfield Delegate Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.sendBtnObj.isEnabled = true
        self.imageViewsentObj.image = UIImage (named: "sentShow")
        if (string == "\n") {
            textField.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    
    
    func getTextFieldString(_ string: String) {
        var searchString: String
        if (string.characters.count ) > 0 {
            searchString = "\(chatTextViewOBj.text)\(string)"
        }
        else {
            searchString = ((chatTextViewOBj.text as NSString?)?.substring(to: (chatTextViewOBj.text?.characters.count ?? 0) - 1))!
        }
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Back Button
    
    @IBAction func BackBtnTapped(_ sender: Any) {
        
        if self.navStr == "dashBoardComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.headerStr = self.headerStr
            dashboardVC.navstr = "dashBoardComment"
            dashboardVC.commentsArray = self.commentsArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.userNameObjStr = self.userNameObjStr
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "dashboarpersonalprofileComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.headerStr = self.headerStr
            dashboardVC.navstr = "dashboarpersonalprofileComment"
            dashboardVC.commentsArray = self.commentsArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.userNameObjStr = self.userNameObjStr
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered  = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "worldViewFeedprofileComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.navstr = "worldViewFeedprofileComment"
            dashboardVC.commentsArray = self.commentsArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.userNameObjStr = self.userNameObjStr
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered  = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "WorldViewGroupCommentPF" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.navstr = "WorldViewGroupCommentPF"
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.commentsArray = self.commentsArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.userNameObjStr = self.userNameObjStr
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered  = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "WorldIndividualCMTView" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.navstr = "WorldIndividualCMTView"
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.commentsArray = self.commentsArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.userNameObjStr = self.userNameObjStr
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered  = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navStr == "networkpeopleCMTView" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.navstr = "networkpeopleCMTView"
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.commentsArray = self.commentsArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.userNameObjStr = self.userNameObjStr
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered  = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navStr == "PeoplesNewPageCMTView" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.commentUsedID = self.commentUsedID
            dashboardVC.headerStr = self.headerStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.navstr = "PeoplesNewPageCMTView" 
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.commentsArray = self.commentsArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.userNameObjStr = self.userNameObjStr
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.personalTitleName = self.personalTitleName
            dashboardVC.userID_Registered  = self.userID_Registered
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "userPostStr"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.allSupportArray = self.allSupportArray
            dashboardVC.allisLikeArray = self.allisLikeArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.headerTitleStr = self.headerTitleStr
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.navstr = "personalUserPostUpdateComment"
            dashboardVC.commentsArray = self.commentsArray
            dashboardVC.responseUserPostsArray = self.responseUserPostsArray
            dashboardVC.userNameObjStr = self.userNameObjStr
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.allcommentCountArray = self.allcommentCountArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "NewPage"
        {
            let mainViewController = self.sideMenuController!
            let ImageTableVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            ImageTableVC.downloadImage = self.downloadImage
            ImageTableVC.responsePostArray = self.responsePostArray
           // ImageTableVC.allcommentCountArray = self.allcommentCountArray
            
            ImageTableVC.PostStr_ID = self.post_idStr
            ImageTableVC.imageResponseArray = self.imageResponseArray
            //dashboardVC.navstr = "imageCommentStr"
            ImageTableVC.navstr = "updateDashboardNewsImage"
            ImageTableVC.commentsArray = self.commentsArray
          //  ImageTableVC.allSupportArray = allSupportArray
          //  ImageTableVC.allisLikeArray = self.allisLikeArray
            // let commentCount = self.commentsArray.count as NSNumber
            
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            ImageTableVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            ImageTableVC.verticalOffsetIndex = self.verticalOffsetIndex
            ImageTableVC.usernameobjstr = self.usernameobjstr
            ImageTableVC.verticalOffset = self.verticalOffset
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ImageTableVC, animated: true)
        }
        if self.navStr == "PeoplesNewPage"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.headerTitleStr = self.headerTitleStr
            dashboardVC.commentPost_ID = self.commentPost_ID
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.allsupportPeopleArray = allsupportPeopleArray
            dashboardVC.allisLikesPeopleArray = self.allisLikesPeopleArray
            dashboardVC.allsupportFeedArray = self.allsupportFeedArray
            dashboardVC.allisLikesFeedArray = self.allisLikesFeedArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.peopleReg_idStr = self.peopleReg_idStr
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.navstr = "PeoplesimageCommentStr"
            dashboardVC.commentsArray = self.commentsArray
            //  let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "GroupPageVC"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.navstr = "GroupPageimageCommentStr"
            dashboardVC.commentsArray = self.commentsArray
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programArray = self.programArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "MyProgramFeedVC"
        {
            let mainViewController = self.sideMenuController!
            let ImageTableVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            ImageTableVC.downloadImage = self.downloadImage
            ImageTableVC.responsePostArray = self.responsePostArray
            ImageTableVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            ImageTableVC.PostStr_ID = self.post_idStr
            ImageTableVC.allsupportFeedArray = self.allsupportFeedArray
            ImageTableVC.allisLikesFeedArray = self.allisLikesFeedArray
            ImageTableVC.imageResponseArray = self.imageResponseArray
            ImageTableVC.navstr = "MyProgramFeedCommentStr"
            ImageTableVC.commentsArray = self.commentsArray
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            ImageTableVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            ImageTableVC.verticalOffsetIndex = self.verticalOffsetIndex
            ImageTableVC.usernameobjstr = self.usernameobjstr
            ImageTableVC.verticalOffset = self.verticalOffset
            ImageTableVC.groupIDStr = self.groupIDStr
            ImageTableVC.headerNameStr = self.headerNameStr
            ImageTableVC.groupImageStr = self.groupImageStr
            ImageTableVC.program_idstr = self.program_idstr
            ImageTableVC.programArray = self.programArray
            ImageTableVC.headerTitleStr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ImageTableVC, animated: true)
            
        }
        if self.navStr == "MyProgramPrsnlVC"
        {
            let mainViewController = self.sideMenuController!
            let ImageTableVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            ImageTableVC.downloadImage = self.downloadImage
            ImageTableVC.responsePostArray = self.responsePostArray
            ImageTableVC.allsupportFeedArray = self.allsupportFeedArray
            ImageTableVC.allisLikesFeedArray = self.allisLikesFeedArray
            //ImageTableVC.allisLikesGroupArray = self.allisLikesGroupArray
            ImageTableVC.allFeedcommentCountArray = self.allFeedcommentCountArray
            ImageTableVC.headerTitleStr = self.headerTitleStr
            ImageTableVC.userNameObjStr = self.userNameObjStr
            ImageTableVC.PostStr_ID = self.post_idStr
            ImageTableVC.imageResponseArray = self.imageResponseArray
            ImageTableVC.navstr = "MyProgramPrsnlVC"
            ImageTableVC.commentsArray = self.commentsArray
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            ImageTableVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            ImageTableVC.verticalOffsetIndex = self.verticalOffsetIndex
            ImageTableVC.usernameobjstr = self.usernameobjstr
            ImageTableVC.verticalOffset = self.verticalOffset
            ImageTableVC.groupIDStr = self.groupIDStr
            ImageTableVC.headerNameStr = self.headerNameStr
            ImageTableVC.groupImageStr = self.groupImageStr
            ImageTableVC.program_idstr = self.program_idstr
            ImageTableVC.programArray = self.programArray
            ImageTableVC.userID_Registered = self.userID_Registered
            ImageTableVC.allcommentCountArray =  self.allcommentCountArray
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ImageTableVC, animated: true)
            
        }
        if self.navStr == "joinProgram"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.navstr = "joinProgram"
            dashboardVC.commentsArray = self.commentsArray
            //  let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.programArray = self.programArray
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.programArray = self.programArray
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.allsupportGroupArray = self.allsupportGroupArray
            dashboardVC.allisLikesGroupArray = self.allisLikesGroupArray
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navStr == "joinProgramVC"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.navstr = "joinProgramVC"
            dashboardVC.commentsArray = self.commentsArray
            //  let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.programArray = self.programArray
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.programArray = self.programArray
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.allsupportGroupArray = self.allsupportGroupArray
            dashboardVC.allisLikesGroupArray = self.allisLikesGroupArray
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navStr == "WorldViewGroup"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.navstr = "WorldViewGroupImageVC"
            dashboardVC.commentsArray = self.commentsArray
            //  let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.programArray = self.programArray
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.allsupportGroupArray = self.allsupportGroupArray
            dashboardVC.allisLikesGroupArray = self.allisLikesGroupArray
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        
        if self.navStr == "WorldViewpersonalGroup"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.navstr = "WorldViewGrouppersonalImageVC"
            dashboardVC.commentsArray = self.commentsArray
            // let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.userNameObjStr = self.userNameObjStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.programArray = self.programArray
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.allsupportGroupArray = self.allsupportGroupArray
            dashboardVC.allisLikesGroupArray = self.allisLikesGroupArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.WorldViewheaderTitleStr = self.WorldViewheaderTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
            
        }
        if self.navStr == "WorldViewIndividualVC"
        {
            let mainViewController = self.sideMenuController!
            let dashboardVC = ImageTableViewController()
            // dashboardVC.navStr = "Commentvc"
            dashboardVC.downloadImage = self.downloadImage
            dashboardVC.responsePostArray = self.responsePostArray
            dashboardVC.PostStr_ID = self.post_idStr
            dashboardVC.imageResponseArray = self.imageResponseArray
            dashboardVC.navstr = "WorldViewIndividualpersonalImageVC"
            dashboardVC.commentsArray = self.commentsArray
            //  let commentCount = self.commentsArray.count as NSNumber
            let CInt : Int = self.commentsArray.count
            let commentStrupdate : NSString = String(CInt) as NSString
            let updatecommentCount : NSNumber =  NSNumber(value: Int(commentStrupdate as String)!)
            self.CommentCountMutableArr.replaceObject(at: self.verticalOffsetIndex, with: updatecommentCount)
            dashboardVC.commentsImagesArray =  self.CommentCountMutableArr as NSArray
            dashboardVC.allSupportPersonalArray = self.allSupportPersonalArray
            dashboardVC.allPersonalcommentsArray = self.allPersonalcommentsArray
            dashboardVC.allisLikesPersonalArray = self.allisLikesPersonalArray
            dashboardVC.verticalOffsetIndex = self.verticalOffsetIndex
            dashboardVC.usernameobjstr = self.usernameobjstr
            // dashboardVC.usernameobjStr = self.usernameobjStr
            dashboardVC.verticalOffset = self.verticalOffset
            dashboardVC.groupIDStr = self.groupIDStr
            dashboardVC.headerNameStr = self.headerNameStr
            dashboardVC.headerTitleStr = self.headerTitleStr
            dashboardVC.groupImageStr = self.groupImageStr
            dashboardVC.program_idstr = self.program_idstr
            dashboardVC.programNamestr = self.programNamestr
            dashboardVC.programArray = self.programArray
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.headerTitleStr = self.headerTitleStr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
    }
    
    @IBAction func sendBtnTapped(_ sender: Any) {
        
        if !(self.chatTextViewOBj.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "addcomment" as AnyObject,
                "id" : userID as AnyObject,
                "post_id" : self.post_idStr as AnyObject,
                "image_id" : self.image_Idstr as AnyObject,
                "comment" : self.chatTextViewOBj.text as AnyObject]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CommentsApiCalling(param: params)
            self.chatTextViewOBj.text = ""
        }
        else {
            self.presentAlertWithTitle(title: "", message:"Please Enter Comment")
        }
    }
    
    func CommentsApiCalling(param:[String: AnyObject])
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_AddCommentsAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            //  print(responsejson)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.commentsArray.insert((responsejson["comment"] as AnyObject), at: 0)
                                
                                self.chatTableViewObj.reloadData()
                                self.sendBtnObj.isEnabled = false
                                self.imageViewsentObj.image = UIImage (named: "sentHide")
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    
}
