//
//  LogoutViewController.swift
//  Verdentum
//
//  Created by Verdentum on 12/09/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import AVFoundation
import MBProgressHUD
import SDWebImage
import IKEventSource
//import FBSDKLoginKit
import Kingfisher
import Photos


class LogoutViewController: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PhotoTweaksViewControllerDelegate {
    var window: UIWindow?
    @IBOutlet var profileupdateBtn: UIButton!
    @IBOutlet var imageViewProfilePic: UIImageView!
    @IBOutlet var nameLabelObj: UILabel!
    @IBOutlet var emailNameObj: UILabel!
    var imagePicker = UIImagePickerController()
    var popoverController: UIPopoverController?
    var profilepicImageStr = NSString()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imagePicker.delegate = self
        let userName = UserDefaults.standard.value(forKey: "userName")
        let userEmail = UserDefaults.standard.value(forKey: "userEmail")
        self.nameLabelObj.text = userName as? String
        self.emailNameObj.text = userEmail as? String
        
        let userimage = UserDefaults.standard.value(forKey: "userImage")
        
        if userimage != nil {
            let url = URL(string: IMAGE_UPLOAD.appending((userimage as? String!)!))
            self.imageViewProfilePic.sd_setImage(with: url, placeholderImage: UIImage(named: " "))
            self.imageViewProfilePic.layer.borderWidth = 2.0
            self.imageViewProfilePic.layer.borderColor = UIColor.white.cgColor
            self.imageViewProfilePic.layer.cornerRadius = 50
            self.imageViewProfilePic.layer.masksToBounds = true
        } else {
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(true)
        
        let userimage = UserDefaults.standard.value(forKey: "userImage")
        
        if userimage != nil {
            let url = URL(string: IMAGE_UPLOAD.appending((userimage as? String!)!))
            self.imageViewProfilePic.sd_setImage(with: url, placeholderImage: UIImage(named: " "))
            self.imageViewProfilePic.layer.borderWidth = 2.0
            self.imageViewProfilePic.layer.borderColor = UIColor.white.cgColor
            self.imageViewProfilePic.layer.cornerRadius = 50
            self.imageViewProfilePic.layer.masksToBounds = true
        } else {
            
        }
    }
   
     // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        let image = info[UIImagePickerControllerEditedImage] as? UIImage
        self.imageViewProfilePic.image = image
        
        if image != nil {
            
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                self.imageViewProfilePic.image = pickedImage
//                let photoTweaksViewController = PhotoTweaksViewController(image: image )
//                photoTweaksViewController?.delegate = self
//                // photoTweaksViewController.autoSaveToLibray = YES;
//                 photoTweaksViewController?.maxRotationAngle = CGFloat(Double.pi / 2)
//                picker.pushViewController(photoTweaksViewController!, animated: true)
            }
            dismiss(animated: true, completion: nil)
            
            if let updatedImage = self.imageViewProfilePic.image?.updateImageOrientionUpSide() {
                uploadGalleryImage(image: updatedImage)
            } else {
                
            }
        }
        else {
            let image = info[UIImagePickerControllerOriginalImage] as? UIImage
            let photoTweaksViewController = PhotoTweaksViewController(image: image )
            photoTweaksViewController?.delegate = self
            // photoTweaksViewController.autoSaveToLibray = YES;
           // photoTweaksViewController?.maxRotationAngle = CGFloat(Double.pi / 2)
            picker.pushViewController(photoTweaksViewController!, animated: true)
        }
    }
    
    func uploadGalleryImage( image:UIImage)
    {
        let imageData : NSData = UIImageJPEGRepresentation((self.imageViewProfilePic.image)!, 0.6)! as NSData
        //        let selectedImageSize:Int = imageData.length
        //        print("size of image in KB: %f ", Double(selectedImageSize) / 1024.0)
        self.profilepicImageStr = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
        let usedID = UserDefaults.standard.value(forKey: "userID")
        let dict = ["id": usedID as Any,"img" : self.profilepicImageStr,"purpose": "editavatar"] as [String: Any]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.editProfileImageApiCalling(param: dict as [String : AnyObject])
    }

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: -PhotoTweaksViewControllerDelegate
    
    
    func photoTweaksController(_ controller: PhotoTweaksViewController!, didFinishWithCroppedImage croppedImage: UIImage!) {
        
        self.imageViewProfilePic.image = croppedImage
        let imageData : NSData = UIImageJPEGRepresentation((self.imageViewProfilePic.image)!, 0.6)! as NSData
        //        let selectedImageSize:Int = imageData.length
        //        print("size of image in KB: %f ", Double(selectedImageSize) / 1024.0)
        self.profilepicImageStr = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
        let usedID = UserDefaults.standard.value(forKey: "userID")
        let dict = ["id": usedID as Any,"img" : self.profilepicImageStr,"purpose": "editavatar"] as [String: Any]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.editProfileImageApiCalling(param: dict as [String : AnyObject])
        controller.navigationController?.popViewController(animated: true)
    }
    
    func photoTweaksControllerDidCancel(_ controller: PhotoTweaksViewController!) {
        controller.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -editProfileImageApiCalling
    
    func editProfileImageApiCalling(param:[String: AnyObject])
    {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let url = NSURL(string:BaseURL_EditProfileImageAPI)
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
        }
        catch let error
        {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request as URLRequest)
        {
            data, response, error in
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode != 200
                {
                    print("response was not 200: \(String(describing: response))")
                    return
                }
            }
            if (error != nil)
            {
                print("error submitting request: \(String(describing: error))")
                return
            }
            do {
                if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                {
                    DispatchQueue.main.async (execute:{ () -> Void in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print(responsejson)
                       
                        if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                        {
                            
                            UserDefaults.standard.removeObject(forKey: "userImage")
                            UserDefaults.standard.synchronize()
                            UserDefaults.standard.set((responsejson["img"] as AnyObject) , forKey: "userImage")
                            UserDefaults.standard.synchronize()
                            let userimage = UserDefaults.standard.value(forKey: "userImage")
                            let url = URL(string: IMAGE_UPLOAD.appending((userimage as? String!)!))
                            
                            self.imageViewProfilePic.sd_setImage(with: url! as URL, placeholderImage: nil ,options: SDWebImageOptions.progressiveDownload, completed: { (image, error, cacheType, imageURL) in
                            
                              })
                        }
                        else
                        {
                            
                        }
                    })
                }
            }
            catch let error
            {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    
    
  
    @IBAction func tapMoreImageBtn(_ sender: Any) {
    }

    @IBAction func sideMenuBtn(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    func checkNotifications() {
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let now = dateformatter.string(from: NSDate() as Date)
        let fullName    = now
        let fullNameArr = fullName.components(separatedBy: " ")
       // let date    = fullNameArr[0]
       // let time = fullNameArr[1]
        // print(date,time)
        let timestamp = fullNameArr[2]
        
        
        let nsMutableString = NSMutableString(string: timestamp)
        let timestamp1 : NSMutableString = nsMutableString
        timestamp1.insert(":", at: 3)
        // print(timestamp1)
        
        let emailAddressText = timestamp1
        
        let encodedtime = emailAddressText.addingPercentEncoding(withAllowedCharacters:.rfc3986Unreserved)
        let currentUsedID = UserDefaults.standard.value(forKey: "userID")
        //  print(encodedtime!)
        
        let urlPath : NSString = "http://52.40.207.123/cyc/api/notification/startstream/\(currentUsedID!)/\(encodedtime!)" as NSString
        
        let eventSource : EventSource = EventSource(url: urlPath as String)
        
        eventSource.onOpen {
            // When opened
        }
        
        eventSource.onError { (error) in
            // When errors
        }
        
        eventSource.onMessage { (id, event, data) in
            // Here you get an event without event name!
            /// print(data!)
        }
        
        eventSource.addEventListener("groupRequest") { (id, event, data) in
            // Here you get an event 'event-name'
           // print(data!)
          
        }
        eventSource.addEventListener("friendRequest") { (id, event, data) in
            // Here you get an event 'event-name'
            // print(data!)
          
        }
        eventSource.addEventListener("pushRequest") { (id, event, data) in
            // Here you get an event 'event-name'
            //print(data!)
        }
        eventSource.close()
    }
    
    
    @IBAction func logoutBtnTapped(_ sender: Any) {
        KingfisherManager.shared.cache.clearMemoryCache()
        KingfisherManager.shared.cache.clearDiskCache()
        
        let alertController = UIAlertController(title: "", message: "Are you sure you want to logout?", preferredStyle: UIAlertControllerStyle.actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
           
        }
        let okAction = UIAlertAction(title: "Logout", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.checkNotifications()
            UserDefaults.standard.removeObject(forKey: "userID")
           // let loginManager = FBSDKLoginManager()
           // loginManager.logOut()
            
            let appDomain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
            let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            dictionary.keys.forEach { key in
                defaults.removeObject(forKey: key)
            }
            defaults.synchronize()
            
            
            
          //  let vc = HomePageViewController(nibName: "HomePageViewController",
                //bundle: nil)
           // self.navigationController?.pushViewController(vc,animated: true)
           
            let mainViewController = self.sideMenuController!
            let HomePageVC = HomePageViewController()
           /* let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            
            let appDomain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
            let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            dictionary.keys.forEach { key in
                defaults.removeObject(forKey: key)
            }
            defaults.synchronize()*/
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(HomePageVC, animated: true)
            //navigationController.popViewController(animated: true)
            mainViewController.hideLeftView(animated: true, completionHandler: nil)
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
      
    }
    
    
    
    @IBAction func changeProfileImageView(_ sender: Any) {
        
       /* let actionSheet = UIAlertController.init(title: "Please choose a source type", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Take Photo", style: UIAlertActionStyle.default, handler: { (action) in
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Choose Photo", style: UIAlertActionStyle.default, handler: { (action) in
            self.showPhotoLibrary()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) in
            // self.dismissViewControllerAnimated(true, completion: nil) is not needed, this is handled automatically,
            //Plus whatever method you define here, gets called,
            //If you tap outside the UIAlertController action buttons area, then also this handler gets called.
        }))
        //Present the controller
        self.present(actionSheet, animated: true, completion: nil)*/
        
        
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please choose a source type", message: nil, preferredStyle: .actionSheet)
        
        let editAction: UIAlertAction = UIAlertAction(title: "Take Photo", style: .default) { action -> Void in
            
            self.openCamera()
        }
        
        let deleteAction: UIAlertAction = UIAlertAction(title: "Choose Photo", style: .default) { action -> Void in
            
            self.showPhotoLibrary()
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        actionSheetController.addAction(editAction)
        actionSheetController.addAction(deleteAction)
        actionSheetController.addAction(cancelAction)
        
        //        present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad
        
        actionSheetController.popoverPresentationController?.sourceView = self.profileupdateBtn // works for both iPhone & iPad
        
        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
      
    }
    
    func openCamera() {
      
         let cameraMediaType = AVMediaTypeVideo
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .denied:
            alertPromptToAllowCameraAccessViaSetting()
            break
        case .authorized:
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.delegate=self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imagePicker.allowsEditing = true
                present(self.imagePicker,animated: true,completion: nil)
            } else {
                noCamera()
            }
            break
        case .restricted: break
            
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(forMediaType: cameraMediaType) { granted in
                if granted {
                    print("Granted access to \(cameraMediaType)")
                    
                } else {
                    print("Denied access to \(cameraMediaType)")
                    self.alertPromptToAllowCameraAccessViaSetting()
                }
            }
        }
        
        
    }
    
    func showPhotoLibrary() {
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            print("Access has been granted")
        }
            
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
            print("Access has been denied")
            self.alertPromptToAllowCameraAccessViaSetting()
        }
            
        else if (status == PHAuthorizationStatus.notDetermined) {
            
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    print("Access has been granted.")
                }
                else {
                    print(" Access has been denied.")
                    self.alertPromptToAllowCameraAccessViaSetting()
                }
            })
        }
            
        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
            print(" Restricted access - normally won't happen")
        }
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            self.imagePicker.allowsEditing = false
             self.imagePicker.sourceType = .photoLibrary
            present( self.imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        let alert = UIAlertController(title: "Alert", message: "Photo access required to...", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel) { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        
        present(alert, animated: true)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, This device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }

    
}

// Image extension
extension UIImage {
    
    func updateImageOrientionUpSide() -> UIImage? {
        if self.imageOrientation == .up {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: 200, height: 200))
        if let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        }
        UIGraphicsEndImageContext()
        return nil
    }
}
