//
//  CommentProfileVC.swift
//  Verdentum
//
//  Created by Verdentum on 21/12/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD
import AlamofireImage
import Alamofire
import SDWebImage
import SKPhotoBrowser
import FirebaseMessaging
//import Reachability


class CommentProfileVC: UIViewController,UIActionSheetDelegate,SKPhotoBrowserDelegate {
    
    
    @IBOutlet var headerTitleNameObj: UILabel!
    @IBOutlet var tableviewObj: UITableView!
    var cell = WallCell()
    var program_idstr = NSString()
    var downloadImage = [UIImage]()
    var selected = Bool()
    var storeImagesURl = [AnyObject]()
    var images = [SKPhotoProtocol]()
    var verticalOffset: Int = 0
    var likeIndexOffSet: Int = 0
    var imageArrayURLCol = NSMutableArray()
    var refreshControl = UIRefreshControl()
    var responsePostArray = NSMutableArray()
    var responseUploadActivityArray = NSMutableArray()
    var responseUserPostsArray = NSMutableArray()
    var newimagesArray = NSMutableArray()
    private var lastContentOffset: CGFloat = 0
    var lastpostIDStr = NSString()
    var firstPostIDstr = NSString()
    var postusedID = NSString()
    var postID = NSString()
    var commentPost_ID = NSString()
    var commentUsedID = String()
    var result = NSString()
    var selectedRow = NSIndexPath()
    var navStr = NSString()
    var groupNameArray = NSArray()
    var commentsArray = NSMutableArray()
    var imagesArrayURl = NSArray()
    var commentsImagesArray = NSArray()
    var arrcontainstate = NSMutableArray()
    var imageResponseArray = NSArray()
    var usernameobjstr = NSString()
    var postUserNamestr = NSString()
    var checkImageORNotStr : String?
    var StatuStr = NSString()
    var allcommentCountArray = NSMutableArray()
    var allPersonalcommentsArray = NSMutableArray()
    var timer = Timer()
    var supportselectedStr = NSString()
    var allSupportArray = NSMutableArray()
    var allSupportPersonalArray = NSMutableArray()
    var allisLikeArray = NSMutableArray()
    var likeStr = NSString()
    var headerStr = String()
    var userID_Registered = NSString()
    var personalTitleName = NSString()
    var programNamestr = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkImageORNotStr = nil
        
        self.headerTitleNameObj.text = self.headerStr.capitalized
        self.selected = true
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.navigationController?.isNavigationBarHidden = true
        self.tableviewObj.estimatedRowHeight = 100
        self.tableviewObj.rowHeight = UITableViewAutomaticDimension
       
        // -----Refresh Control----
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(DashBoardViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        self.tableviewObj.addSubview(refreshControl)
        
        if self.navStr == "dashBoardComment" {
            self.homeServiceApiCalling()
        }
        if self.navStr == "dashboarpersonalprofileComment" {
            self.homeServiceApiCalling()
        }
        if self.navStr == "worldViewFeedprofileComment" {
            self.homeServiceApiCalling()
        }
        if self.navStr == "WorldViewGroupCommentPF" {
           self.homeServiceApiCalling()
        }
        if self.navStr == "WorldViewIndividualVC" {
             self.homeServiceApiCalling()
        }
        if self.navStr == "networkpeople" {
            self.homeServiceApiCalling()
        }
        if self.navStr == "PeoplesNewPage" {
            self.homeServiceApiCalling()
        }
        
        self.tableviewObj .register(UINib (nibName: "WallCell", bundle: nil), forCellReuseIdentifier: "WallCell")
        
        //----- NSNotificationCenter ------ //
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectItem(fromCollectionView:)), name: NSNotification.Name(rawValue: "didSelectItemFromCollectionView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectItemPDF(fromCollectionView:)), name: NSNotification.Name(rawValue: "didSelectItemFromCollectionViewPDF"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectAddPostView(fromCollectionView:)), name: NSNotification.Name(rawValue: "AddPostView"), object: nil)
    }
    
    // MARK: - refresh
    
    func refresh(_ sender:AnyObject)
    {
        
        homeServiceApiCalling()
        
        self.refreshControl .endRefreshing()
    }
    
    
    func applicationWillResign(notification : NSNotification) {
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didSelectItemFromCollectionView"), object: nil)
    }
    
    // MARK: - viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    // MARK: - viewDidAppear
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - HomeServiceApiCalling
    
    func homeServiceApiCalling() {
        
        if Reachability.isConnectedToNetwork() == true {
        
            // Second Method Calling Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let userID = UserDefaults.standard.value(forKey: "userID")
            let params:[String: AnyObject] = [
                "purpose" : "userpost" as AnyObject,
                "id" : self.commentUsedID as AnyObject,
                "myid" : userID as AnyObject]
            let url = NSURL(string:BaseURL_USERPOSTSAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        // print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                self.allSupportArray.removeAllObjects()
                                self.allcommentCountArray.removeAllObjects()
                                self.responsePostArray.removeAllObjects()
                                self.responsePostArray = (responsejson["posts"] as AnyObject) as! NSArray as! NSMutableArray
                                
                                if self.responsePostArray.count == 0
                                {
                                    
                                    self.StatuStr = "This is your development dashboard -a place where you can find updates from your network around the word, friends, and organizations you interact with."
                                    self.tableviewObj.reloadData()
                                }
                                else
                                {
                                    UserDefaults.standard.set((((responsejson["posts"] as AnyObject) .value(forKey: "post_id") as AnyObject) .firstObject) as Any, forKey: "firstPostID")
                                    
                                    for var i in 0..<self.responsePostArray.count
                                    {
                                        let commentsArr : NSArray = ((self.responsePostArray[i] as AnyObject).value(forKey: "comments") as? NSArray)!
                                        self.allcommentCountArray.add(String(commentsArr.count))
                                        let likesStr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "likes") as? String)!
                                        self.allSupportArray.add(likesStr)
                                        
                                        let islikestr : String = ((self.responsePostArray[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                        self.allisLikeArray.add(islikestr)
                                    }
                                    self.tableviewObj.reloadData()
                                }
                            }
                            if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.StatuStr = ((responsejson["msg"] as AnyObject) as! String as NSString)
                                self.tableviewObj.reloadData()
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
            
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    //MARK: - SecondServiceApiCalling
    
    func postsApiCalling(param:[String: AnyObject])
    {
        //  MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_USERPOSTSAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        //  print(responsejson)
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                let arra  = (responsejson["posts"] as AnyObject) as! NSArray
                                if arra.count == 0
                                {
                                    
                                }
                                else
                                {
                                    self.responsePostArray.addObjects(from: arra as! [Any])
                                    for var i in 0..<arra.count
                                    {
                                        let commentsArr : NSArray = ((arra[i] as AnyObject).value(forKey: "comments") as? NSArray)!
                                        self.allcommentCountArray.add(String(commentsArr.count))
                                        let likesStr : String = ((arra[i] as AnyObject).value(forKey: "likes") as? String)!
                                        self.allSupportArray.add(likesStr)
                                        let islikestr : String = ((arra[i] as AnyObject).value(forKey: "is_liked") as? String)!
                                        self.allisLikeArray.add(islikestr)
                                    }
                                    // print(self.allcommentCountArray)
                                    self.tableviewObj.reloadData()
                                }
                                
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
    }
    
    func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        cell.imageCollectionViewObj.collectionViewLayout.invalidateLayout()
    }
    
    func likeBtnTapped(_ sender: UIButton)
    {
        let likesStr : NSString = ((self.allisLikeArray[sender.tag] as AnyObject) as? NSString)!
        if likesStr.isEqual(to: "1") {
            
        }
        if likesStr.isEqual(to: "0")
        {
            let indexCheck1 : Int = sender.tag
            let supportselectedStr1 : String = String(indexCheck1)
            if self.supportselectedStr .isEqual(to: supportselectedStr1) {
                
            } else {
                var message = NSNumber()
                let button: UIButton? = sender
                button?.backgroundColor = UIColor.clear
                button?.setTitleColor(UIColor.clear, for: UIControlState.normal)
                button?.isSelected = !(button?.isSelected)!
                if (button?.isSelected)!
                {
                    let post_IDStr : NSString = (self.responsePostArray .object(at: sender.tag) as AnyObject) .value(forKey: "post_id") as! NSString
                    let userID = UserDefaults.standard.value(forKey: "userID")
                    let params:[String: AnyObject] = [
                        "purpose" : "support" as AnyObject,
                        "post_id" : post_IDStr as AnyObject,
                        "user_id" : userID as AnyObject]
                    
                    if Reachability.isConnectedToNetwork() == true {
                        Alamofire.request(BaseURL_SupportAPI, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                            .responseJSON { response in
                                // print(response.request as Any)  // original URL request
                                // print(response.response as Any) // URL response
                                // print(response.result.value as Any)   // result of response serialization
                                if let JSON = response.result.value as? NSDictionary {
                                    print(JSON)
                                    message = JSON .value(forKey: "status")  as! NSNumber
                                    //message = JSON["likes"] as! NSNumber
                                    //print(message)
                                    
                                    //self.homeServiceApiCalling()
                                    let indexPath = IndexPath(row: sender.tag, section: 0)
                                    self.cell = (self.tableviewObj.cellForRow(at: indexPath) as? WallCell)!
                                    let indexCheck : Int = sender.tag
                                    let updatecommentCount : String =  "1"
                                    self.allisLikeArray.replaceObject(at: indexCheck, with: updatecommentCount)
                                    
                                    // Replace AllSupportArray
                                    
                                    var likesStr:String?
                                    
                                    if let likesStrNo = JSON["likes"] as? NSNumber {
                                        likesStr = "\(likesStrNo)"
                                    } else if let likesString = JSON["likes"] as? String {
                                        likesStr =  likesString
                                    }
                                    
                                    self.allSupportArray.replaceObject(at: indexCheck, with: likesStr!)
                                    self.supportselectedStr = String(indexCheck) as NSString
                                    self.cell.heartImageview.tag = sender.tag
                                    self.cell.heartImageview.image = UIImage (named: "Hand Filled")
                                    self.cell.likeCountLabel.text = message.stringValue as String
                                    self.cell.likeCountLabel.tag = sender.tag
                                    self.cell.supportLabel.text = "Supported"
                                    self.cell.supportLabel.tag = sender.tag
                                    self.tableviewObj.reloadData()
                                }
                                
                        }
                    }
                    else {
                        self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
                    }
                    
                }
            }
        }
        if likesStr.isEqual(to: "2")
        {
            let indexCheck1 : Int = sender.tag
            let supportselectedStr1 : String = String(indexCheck1)
            if self.supportselectedStr .isEqual(to: supportselectedStr1) {
                
            } else {
                var message = NSNumber()
                let button: UIButton? = sender
                button?.backgroundColor = UIColor.clear
                button?.setTitleColor(UIColor.clear, for: UIControlState.normal)
                button?.isSelected = !(button?.isSelected)!
                if (button?.isSelected)!
                {
                    let post_IDStr : NSString = (self.responsePostArray .object(at: sender.tag) as AnyObject) .value(forKey: "post_id") as! NSString
                    let userID = UserDefaults.standard.value(forKey: "userID")
                    let params:[String: AnyObject] = [
                        "purpose" : "support" as AnyObject,
                        "post_id" : post_IDStr as AnyObject,
                        "user_id" : userID as AnyObject]
                    
                    if Reachability.isConnectedToNetwork() == true {
                        Alamofire.request(BaseURL_SupportAPI, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
                            .responseJSON { response in
                                // print(response.request as Any)  // original URL request
                                // print(response.response as Any) // URL response
                                // print(response.result.value as Any)   // result of response serialization
                                if let JSON = response.result.value as? NSDictionary {
                                    print(JSON)
                                    message = JSON .value(forKey: "status")  as! NSNumber
                                    //message = JSON["likes"] as! NSNumber
                                    //print(message)
                                }
                                //self.homeServiceApiCalling()
                                let indexPath = IndexPath(row: sender.tag, section: 0)
                                self.cell = (self.tableviewObj.cellForRow(at: indexPath) as? WallCell)!
                                let indexCheck : Int = sender.tag
                                let updatecommentCount : String =  "1"
                                self.allSupportArray.replaceObject(at: indexCheck, with: updatecommentCount)
                                self.supportselectedStr = String(indexCheck) as NSString
                                self.cell.heartImageview.tag = sender.tag
                                self.cell.heartImageview.image = UIImage (named: "Hand Filled")
                                self.cell.likeCountLabel.text = message.stringValue as String
                                self.cell.likeCountLabel.tag = sender.tag
                                self.cell.supportLabel.text = "Supported"
                                self.cell.supportLabel.tag = sender.tag
                                self.tableviewObj.reloadData()
                        }
                    }
                    else {
                        self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
                    }
                    
                }
            }
        }
    }
    
    func viewAllCommentBtnTapped(_ sender: UIButton) {
        self.verticalOffset = sender.tag
        self.commentPost_ID = ((self.responsePostArray[sender.tag] as! NSObject) .value(forKey: "post_id") as AnyObject) as! NSString
        let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "allcomment" as AnyObject,
            "id" : userID as AnyObject,
            "post_id" : self.commentPost_ID as AnyObject]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CommentsApiCalling(param: params, andSelectedRow: sender.tag)
    }
    
    func CommentsApiCalling(param:[String: AnyObject],andSelectedRow selectedRow: Int)
    {
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true
        {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:BaseURL_AllCommentsAPI)
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            {
                                if self.navStr == "dashBoardComment" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.headerStr = self.headerStr
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    CommentsVC.navstr = "dashBoardComment"
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.allisLikeArray = self.allisLikeArray
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                 if self.navStr == "dashboarpersonalprofileComment" {
                                    
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.headerStr = self.headerStr
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    CommentsVC.navstr = "dashboarpersonalprofileComment"
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                     CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                 if self.navStr == "worldViewFeedprofileComment" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.headerStr = self.headerStr
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    CommentsVC.navstr = "worldViewFeedprofileComment"
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                     CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                 if self.navStr ==  "WorldViewGroupCommentPF"  {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.headerStr = self.headerStr
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    CommentsVC.navstr = "WorldViewGroupCommentPF"
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                     CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navStr == "WorldViewIndividualVC" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.headerStr = self.headerStr
                                    CommentsVC.allcommentCountArray = self.allcommentCountArray
                                    CommentsVC.navstr = "WorldIndividualCMTView"
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.responsePostArray = self.responsePostArray
                                    CommentsVC.allSupportArray = self.allSupportArray
                                    CommentsVC.allisLikeArray = self.allisLikeArray
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                if self.navStr == "networkpeople" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.headerStr = self.headerStr
                                    CommentsVC.navstr = "networkpeopleCMTView"
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                }
                                 if self.navStr == "PeoplesNewPage" {
                                    self.commentsArray = (responsejson["comments"] as AnyObject)  as! NSMutableArray
                                    let mainViewController = self.sideMenuController!
                                    let CommentsVC = CommentsViewController()
                                    CommentsVC .commentArray = self.commentsArray
                                    CommentsVC.post_id = self.commentPost_ID
                                    CommentsVC.commentUsedID = self.commentUsedID
                                    CommentsVC.program_idstr = self.program_idstr
                                    CommentsVC.headerStr = self.headerStr
                                    CommentsVC.navstr = "PeoplesNewPageCMTView"
                                    CommentsVC.verticalOffset = self.verticalOffset
                                    CommentsVC.userID_Registered = self.userID_Registered
                                    let navigationController = mainViewController.rootViewController as! NavigationController
                                    navigationController.pushViewController(CommentsVC, animated: true)
                                    
                                }
                                
                                
                            }
                            else {}
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    func imageTappedBtn(_ sender: UIButton)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableviewObj)
        let indexPathRow = self.tableviewObj.indexPathForRow(at: buttonPosition)
        self.selectedRow = indexPathRow! as NSIndexPath
        self.verticalOffset = sender.tag
        
        let titlename: String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.responsePostArray[sender.tag] as AnyObject).value(forKey: "reg_lname") as! String
        
        self.postUserNamestr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        
        self.commentsImagesArray = (((self.responsePostArray[self.selectedRow.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "comments") as AnyObject) as! NSArray
        self.imagesArrayURl = (((self.responsePostArray[self.selectedRow.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "image_name") as AnyObject) as! NSArray
        self.imageResponseArray = ((self.responsePostArray[self.selectedRow.row] as AnyObject).value(forKey: "images") as AnyObject)  as! NSArray
        self.downloadImage.removeAll()
        for var i in (0..<imagesArrayURl.count)
        {
            let url = URL(string: IMAGE_UPLOAD.appending((self.imagesArrayURl[i] as AnyObject) as! String))
            if url != nil {
                Alamofire.request(url!).responseImage { response in
                    /*  debugPrint(response)
                     print(response.request as Any)
                     print(response.response as Any)
                     debugPrint(response.result)*/
                    
                    if let image = response.result.value {
                        
                        self.downloadImage.append(image)
                        if self.downloadImage.count == self.imagesArrayURl.count
                        {
                            if self.navStr == "dashBoardComment" {
                                
                                let mainViewController = self.sideMenuController!
                                let ImageTableVC = ImageTableViewController()
                                ImageTableVC.commentUsedID = self.commentUsedID
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.downloadImage = self.downloadImage
                                ImageTableVC.selectedRow = self.selectedRow
                                ImageTableVC.verticalOffset = self.verticalOffset
                                ImageTableVC.allSupportArray = self.allSupportArray
                                ImageTableVC.usernameobjstr = self.postUserNamestr
                                ImageTableVC.navstr = "dashBoardComment"
                                ImageTableVC.imageResponseArray = self.imageResponseArray
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.responsePostArray = self.responsePostArray
                                ImageTableVC.commentsImagesArray = self.commentsImagesArray
                                ImageTableVC.allcommentCountArray = self.allcommentCountArray
                                 ImageTableVC.allisLikeArray = self.allisLikeArray
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(ImageTableVC, animated: true)
                            }
                            
                            if self.navStr == "dashboarpersonalprofileComment" {
                                let mainViewController = self.sideMenuController!
                                let ImageTableVC = ImageTableViewController()
                                ImageTableVC.commentUsedID = self.commentUsedID
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.downloadImage = self.downloadImage
                                ImageTableVC.selectedRow = self.selectedRow
                                ImageTableVC.verticalOffset = self.verticalOffset
                                ImageTableVC.allSupportArray = self.allSupportArray
                                ImageTableVC.usernameobjstr = self.postUserNamestr
                                ImageTableVC.navstr = "dashboarpersonalprofileComment"
                                ImageTableVC.imageResponseArray = self.imageResponseArray
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.responsePostArray = self.responsePostArray
                                ImageTableVC.commentsImagesArray = self.commentsImagesArray
                                ImageTableVC.allcommentCountArray = self.allcommentCountArray
                                ImageTableVC.allisLikeArray = self.allisLikeArray
                                 ImageTableVC.userID_Registered = self.userID_Registered
                                ImageTableVC.personalTitleName = self.personalTitleName
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(ImageTableVC, animated: true)
                                
                            }
                            if self.navStr == "worldViewFeedprofileComment" {
                                let mainViewController = self.sideMenuController!
                                let ImageTableVC = ImageTableViewController()
                                ImageTableVC.commentUsedID = self.commentUsedID
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.downloadImage = self.downloadImage
                                ImageTableVC.selectedRow = self.selectedRow
                                ImageTableVC.verticalOffset = self.verticalOffset
                                ImageTableVC.allSupportArray = self.allSupportArray
                                ImageTableVC.usernameobjstr = self.postUserNamestr
                                ImageTableVC.navstr = "worldViewFeedprofileComment"
                                ImageTableVC.imageResponseArray = self.imageResponseArray
                                ImageTableVC.program_idstr = self.program_idstr
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.responsePostArray = self.responsePostArray
                                ImageTableVC.commentsImagesArray = self.commentsImagesArray
                                ImageTableVC.allcommentCountArray = self.allcommentCountArray
                                ImageTableVC.allisLikeArray = self.allisLikeArray
                                ImageTableVC.userID_Registered = self.userID_Registered
                                ImageTableVC.personalTitleName = self.personalTitleName
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(ImageTableVC, animated: true)
                            }
                            if self.navStr ==  "WorldViewGroupCommentPF"  {
                                let mainViewController = self.sideMenuController!
                                let ImageTableVC = ImageTableViewController()
                                ImageTableVC.commentUsedID = self.commentUsedID
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.downloadImage = self.downloadImage
                                ImageTableVC.selectedRow = self.selectedRow
                                ImageTableVC.verticalOffset = self.verticalOffset
                                ImageTableVC.allSupportArray = self.allSupportArray
                                ImageTableVC.usernameobjstr = self.postUserNamestr
                                ImageTableVC.navstr = "WorldViewGroupCommentPF"
                                ImageTableVC.imageResponseArray = self.imageResponseArray
                                ImageTableVC.program_idstr = self.program_idstr
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.responsePostArray = self.responsePostArray
                                ImageTableVC.programNamestr = self.programNamestr
                                ImageTableVC.commentsImagesArray = self.commentsImagesArray
                                ImageTableVC.allcommentCountArray = self.allcommentCountArray
                                ImageTableVC.allisLikeArray = self.allisLikeArray
                                ImageTableVC.userID_Registered = self.userID_Registered
                                ImageTableVC.personalTitleName = self.personalTitleName
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(ImageTableVC, animated: true)
                            }
                            if self.navStr == "WorldViewIndividualVC" {
                                
                                let mainViewController = self.sideMenuController!
                                let ImageTableVC = ImageTableViewController()
                                ImageTableVC.commentUsedID = self.commentUsedID
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.downloadImage = self.downloadImage
                                ImageTableVC.selectedRow = self.selectedRow
                                ImageTableVC.verticalOffset = self.verticalOffset
                                ImageTableVC.allSupportArray = self.allSupportArray
                                ImageTableVC.usernameobjstr = self.postUserNamestr
                                ImageTableVC.navstr = "WorldIndividualCMTView"
                                ImageTableVC.imageResponseArray = self.imageResponseArray
                                ImageTableVC.program_idstr = self.program_idstr
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.responsePostArray = self.responsePostArray
                                ImageTableVC.programNamestr = self.programNamestr
                                ImageTableVC.commentsImagesArray = self.commentsImagesArray
                                ImageTableVC.allcommentCountArray = self.allcommentCountArray
                                ImageTableVC.allisLikeArray = self.allisLikeArray
                                ImageTableVC.userID_Registered = self.userID_Registered
                                ImageTableVC.personalTitleName = self.personalTitleName
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(ImageTableVC, animated: true)
                            }
                               if self.navStr == "networkpeople" {
                                let mainViewController = self.sideMenuController!
                                let ImageTableVC = ImageTableViewController()
                                ImageTableVC.commentUsedID = self.commentUsedID
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.downloadImage = self.downloadImage
                                ImageTableVC.selectedRow = self.selectedRow
                                ImageTableVC.verticalOffset = self.verticalOffset
                                ImageTableVC.allSupportArray = self.allSupportArray
                                ImageTableVC.usernameobjstr = self.postUserNamestr
                                ImageTableVC.navstr = "networkpeopleCMTView"
                                ImageTableVC.imageResponseArray = self.imageResponseArray
                                ImageTableVC.program_idstr = self.program_idstr
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.responsePostArray = self.responsePostArray
                                ImageTableVC.programNamestr = self.programNamestr
                                ImageTableVC.commentsImagesArray = self.commentsImagesArray
                                ImageTableVC.allcommentCountArray = self.allcommentCountArray
                                ImageTableVC.allisLikeArray = self.allisLikeArray
                                ImageTableVC.userID_Registered = self.userID_Registered
                                ImageTableVC.personalTitleName = self.personalTitleName
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(ImageTableVC, animated: true)
                            }
                            if self.navStr == "PeoplesNewPage" {
                                let mainViewController = self.sideMenuController!
                                let ImageTableVC = ImageTableViewController()
                                ImageTableVC.commentUsedID = self.commentUsedID
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.downloadImage = self.downloadImage
                                ImageTableVC.selectedRow = self.selectedRow
                                ImageTableVC.verticalOffset = self.verticalOffset
                                ImageTableVC.allSupportArray = self.allSupportArray
                                ImageTableVC.usernameobjstr = self.postUserNamestr
                                ImageTableVC.navstr = "PeoplesNewPageCMTView"
                                ImageTableVC.imageResponseArray = self.imageResponseArray
                                ImageTableVC.program_idstr = self.program_idstr
                                ImageTableVC.headerStr = self.headerStr
                                ImageTableVC.responsePostArray = self.responsePostArray
                                ImageTableVC.programNamestr = self.programNamestr
                                ImageTableVC.commentsImagesArray = self.commentsImagesArray
                                ImageTableVC.allcommentCountArray = self.allcommentCountArray
                                ImageTableVC.allisLikeArray = self.allisLikeArray
                                ImageTableVC.userID_Registered = self.userID_Registered
                                ImageTableVC.personalTitleName = self.personalTitleName
                                let navigationController = mainViewController.rootViewController as! NavigationController
                                navigationController.pushViewController(ImageTableVC, animated: true)
                            }
                            
                        }
                        else
                        {
                            
                        }
                    }
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.presentAlertWithTitle(title: "", message:"Please Try again later.")
            }
            
        }
    }
    
    func createWebPhotos() -> [SKPhotoProtocol] {
        return (0..<self.imagesArrayURl.count).map { (i: Int) -> SKPhotoProtocol in
            let photo = SKPhoto.photoWithImageURL(IMAGE_UPLOAD .appending(self.imagesArrayURl[i] as! String))
            //photo.caption = caption[i%10]
            photo.shouldCachePhotoURLImage = true
            return photo
        }
    }
    
    func DotbuttonClicked(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableviewObj)
        let indexPathRow = self.tableviewObj.indexPathForRow(at: buttonPosition)
        self.selectedRow = indexPathRow! as NSIndexPath
        self.verticalOffset = sender.tag
        self.result = UserDefaults.standard.value(forKey: "userID") as! NSString
        self.postusedID = ((self.responsePostArray[sender.tag] as! NSObject) .value(forKey: "user_id") as AnyObject) as! NSString
        self.postID = ((self.responsePostArray[sender.tag] as! NSObject) .value(forKey: "post_id") as AnyObject) as! NSString
        if self.result == self.postusedID
        {
            // Finding Array Index
            let indexPath = IndexPath(row: sender.tag, section: 0)
            cell = (self.tableviewObj.cellForRow(at: indexPath) as? WallCell)!
            let quoteDictionary = self.responsePostArray[sender.tag] as! Dictionary<String,AnyObject>
            print(quoteDictionary)
            if indexPathRow != nil
            {
                let actionSheet = UIActionSheet(title: "Delete Post", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Delete")
                
                actionSheet.show(in: self.view)
            }
        }
        else
        {
            let actionSheet = UIActionSheet(title: "Report Post", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Report")
            actionSheet.show(in: self.view)
        }
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        print("\(buttonIndex)")
        switch (buttonIndex){
        case 0:
            print("Cancel")
        case 1:
            if self.result == self.postusedID
            {
                let alertController = UIAlertController(title: "Delete Post", message: "Are you sure you want to delete this post?", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                    
                }
                alertController.addAction(cancelAction)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    let userID = UserDefaults.standard.value(forKey: "userID")
                    let params:[String: AnyObject] = [
                        "purpose" : "deletepost" as AnyObject,
                        "post_id" : self.postID as AnyObject,
                        "id" : userID as AnyObject]
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    self.deletePostID(param: params)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
            else
            {
                if self.navStr == "dashBoardComment" {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.allSupportArray = allSupportArray
                    ReportuserVC.allisLikeArray = self.allisLikeArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.verticalOffset = self.verticalOffset
                    ReportuserVC.commentUsedID = self.commentUsedID
                    ReportuserVC.headerStr = self.headerStr
                    ReportuserVC.navstr = "dashBoardComment"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                }
                if self.navStr == "dashboarpersonalprofileComment" {
                    
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.allSupportArray = allSupportArray
                    ReportuserVC.allisLikeArray = self.allisLikeArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.verticalOffset = self.verticalOffset
                    ReportuserVC.commentUsedID = self.commentUsedID
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.personalTitleName = self.personalTitleName
                    ReportuserVC.headerStr = self.headerStr
                    ReportuserVC.navstr = "dashboarpersonalprofileComment"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                }
                 if self.navStr == "worldViewFeedprofileComment" {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.allSupportArray = allSupportArray
                    ReportuserVC.allisLikeArray = self.allisLikeArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.verticalOffset = self.verticalOffset
                    ReportuserVC.commentUsedID = self.commentUsedID
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.personalTitleName = self.personalTitleName
                    ReportuserVC.headerStr = self.headerStr
                    ReportuserVC.program_idstr = self.program_idstr
                    ReportuserVC.navstr = "worldViewFeedprofileComment"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                }
                if self.navStr == "WorldViewGroupCommentPF" {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.allSupportArray = allSupportArray
                    ReportuserVC.allisLikeArray = self.allisLikeArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.verticalOffset = self.verticalOffset
                    ReportuserVC.commentUsedID = self.commentUsedID
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.personalTitleName = self.personalTitleName
                    ReportuserVC.headerStr = self.headerStr
                    ReportuserVC.program_idstr = self.program_idstr
                    ReportuserVC.programNamestr = self.programNamestr
                    ReportuserVC.navstr = "WorldViewGroupCommentPF"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                }
                 if self.navStr == "WorldViewIndividualVC" {
                    
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.allSupportArray = allSupportArray
                    ReportuserVC.allisLikeArray = self.allisLikeArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.verticalOffset = self.verticalOffset
                    ReportuserVC.commentUsedID = self.commentUsedID
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.personalTitleName = self.personalTitleName
                    ReportuserVC.headerStr = self.headerStr
                    ReportuserVC.program_idstr = self.program_idstr
                    ReportuserVC.programNamestr = self.programNamestr
                    ReportuserVC.navstr = "WorldIndividualCMTView"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                }
                
                if self.navStr == "networkpeople" {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.allSupportArray = allSupportArray
                    ReportuserVC.allisLikeArray = self.allisLikeArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.verticalOffset = self.verticalOffset
                    ReportuserVC.commentUsedID = self.commentUsedID
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.personalTitleName = self.personalTitleName
                    ReportuserVC.headerStr = self.headerStr
                    ReportuserVC.program_idstr = self.program_idstr
                    ReportuserVC.programNamestr = self.programNamestr
                    ReportuserVC.navstr = "networkpeopleCMTView"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                }
                 if self.navStr == "PeoplesNewPage" {
                    let ReportuserVC = ReportViewController(nibName: "ReportViewController", bundle: nil)
                    ReportuserVC.userpostIdstr = self.postusedID
                    ReportuserVC.post_idStr = self.postID
                    ReportuserVC.responseArray = self.responsePostArray
                    ReportuserVC.allcommentCountArray = self.allcommentCountArray
                    ReportuserVC.allSupportArray = allSupportArray
                    ReportuserVC.allisLikeArray = self.allisLikeArray
                    ReportuserVC.selectedRow = self.selectedRow
                    ReportuserVC.verticalOffset = self.verticalOffset
                    ReportuserVC.commentUsedID = self.commentUsedID
                    ReportuserVC.userID_Registered = self.userID_Registered
                    ReportuserVC.personalTitleName = self.personalTitleName
                    ReportuserVC.headerStr = self.headerStr
                    ReportuserVC.program_idstr = self.program_idstr
                    ReportuserVC.programNamestr = self.programNamestr
                    ReportuserVC.navstr = "PeoplesNewPageCMTView"
                    self.navigationController?.pushViewController(ReportuserVC, animated: true)
                    
                }
            }
        default:
            print("Default")
        }
    }
    
    
    func deletePostID(param:[String: AnyObject])
    {
        //  MBProgressHUD.showAdded(to: self.view, animated: true)
        if Reachability.isConnectedToNetwork() == true {
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let url = NSURL(string:VERDENTUMB_BaseURL.appending(BaseURL_VERDENTUMDeletepostAPI))
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            }
            catch let error
            {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest)
            {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode != 200
                    {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil)
                {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                        
                    {
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                                
                            {
                                //self.presentAlertWithTitle(title: "", message:"Deleted Successfully.")
                                self.responsePostArray .removeObject(at: self.selectedRow.row)
                                self.tableviewObj.reloadData()
                            }
                            else
                            {
                                self.presentAlertWithTitle(title: " Delete Failed!", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                        })
                    }
                }
                catch let error
                {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
        }
        
    }
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func didSelectItem(fromCollectionView notification: Notification)
    {
        let newsImageURLstr = notification .object as! NSString
        
        self.imageArrayURLCol.add(newsImageURLstr)
        print(self.imageArrayURLCol)
        if newsImageURLstr.isEqual(to: "")
        {
            let alertController = UIAlertController(title: "", message: "News Image unavailable. Please try again.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            let ZoomImageVC = ZoomImageViewController(nibName: "ZoomImageViewController", bundle: nil)
            ZoomImageVC.newImageURlStr =  newsImageURLstr
            self.navigationController?.pushViewController(ZoomImageVC, animated: true)
        }
    }
    
    func didSelectItemPDF(fromCollectionView notification: Notification)
    {
        let newsImageURLstr = notification .object as! NSString
        if newsImageURLstr.isEqual(to: "")
        {
            let alertController = UIAlertController(title: "", message: "PDF unavailable. Please try again.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            let ZoomImageVC = ZoomImageViewController(nibName: "ZoomImageViewController", bundle: nil)
            ZoomImageVC.newImageURlStr =  newsImageURLstr
            self.navigationController?.pushViewController(ZoomImageVC, animated: true)
        }
    }
    func didSelectAddPostView(fromCollectionView notification: Notification)
    {
        let newsImageURLstr = notification .object as! NSString
        if newsImageURLstr.isEqual(to: "")
        {
            let alertController = UIAlertController(title: "", message: "Please Try again Later.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            self.allSupportArray.removeAllObjects()
            self.allcommentCountArray.removeAllObjects()
            self.responsePostArray.removeAllObjects()
            self.homeServiceApiCalling()
            
        }
    }
    
    
    func UPnewApiCalling() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let userID = UserDefaults.standard.value(forKey: "userID")
        let params:[String: AnyObject] = [
            "purpose" : "allpost" as AnyObject,
            "id" : userID as AnyObject,
            "direction" : "up" as AnyObject,
            "limit" : self.lastpostIDStr as AnyObject]
        
        let url = NSURL(string:"http://52.40.207.123/api/mobpost")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        // var err: NSError?
        //request.HTTPBody = JSONSerialization.dataWithJSONObject(params, options: JSONSerialization.WritingOptions.allZeros, error: &err)
        
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = session.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    print("response was not 200: \(String(describing: response))")
                    return
                }
            }
            if (error != nil) {
                print("error submitting request: \(String(describing: error))")
                return
            }
            
            do {
                if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                {
                    
                    DispatchQueue.main.async (execute:{ () -> Void in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                            
                        {
                            self.responsePostArray = (responsejson["posts"] as AnyObject) as! NSArray as! NSMutableArray
                            self.tableviewObj.reloadData()
                        }
                        else
                        {
                            
                        }
                    })
                }
                // pass dictionary to nsdata object and set it as request body
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        if self.navStr == "dashBoardComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = DashBoardViewController()
            dashboardVC.navStr = "DashboardVC"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
        if self.navStr == "dashboarpersonalprofileComment" {
            let mainViewController = self.sideMenuController!
            let dashboardVC = PersonalAccountViewController()
            dashboardVC.userID_Registered = self.userID_Registered
            dashboardVC.headerTitleStr = self.personalTitleName
            dashboardVC.navStr = "dashboarpersonalprofileComment"
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(dashboardVC, animated: true)
        }
         if self.navStr == "worldViewFeedprofileComment" {
            
            let mainViewController = self.sideMenuController!
            let ProgramSwipeVC = ProgramSwipeViewController()
            ProgramSwipeVC.navStr = "FeedCommentProfileVC"
            ProgramSwipeVC.program_idstr = self.program_idstr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ProgramSwipeVC, animated: true)
        }
        if self.navStr == "WorldViewGroupCommentPF" {
            let mainViewController = self.sideMenuController!
            let ProgramSwipeVC = ProgramSwipeViewController()
            ProgramSwipeVC.navStr = "WorldVC"
            ProgramSwipeVC.programNamestr = self.programNamestr
            ProgramSwipeVC.program_idstr = self.program_idstr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ProgramSwipeVC, animated: true)
        }
        if self.navStr == "WorldViewIndividualVC" {
            let mainViewController = self.sideMenuController!
            let ProgramSwipeVC = ProgramSwipeViewController()
            ProgramSwipeVC.navStr = "WorldVC"
            ProgramSwipeVC.programNamestr = self.programNamestr
            ProgramSwipeVC.program_idstr = self.program_idstr
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(ProgramSwipeVC, animated: true)
        }
        if self.navStr ==  "networkpeople" {
            let mainViewController = self.sideMenuController!
            let FindPeopleVC = PeopleViewController()
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(FindPeopleVC, animated: true)
        }
         if self.navStr == "PeoplesNewPage" {
            let mainViewController = self.sideMenuController!
            let FindPeopleVC = PeopleViewController()
            let navigationController = mainViewController.rootViewController as! NavigationController
            navigationController.pushViewController(FindPeopleVC, animated: true)
        }
    }
}

extension CommentProfileVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.responsePostArray.count == 0 {
            let rect = CGRect(x: 0,
                              y: 0,
                              width: view.bounds.size.width,
                              height: view.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            
            noDataLabel.text = self.StatuStr as String
            noDataLabel.textColor = UIColor.black
            noDataLabel.font = UIFont(name: "Palatino-Italic", size: 17) ?? UIFont()
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            noDataLabel.sizeToFit()
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableviewObj.backgroundView = noDataLabel
            
            return 0
        }
        else
        {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.responsePostArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        cell = tableView .dequeueReusableCell(withIdentifier: "WallCell") as! WallCell
        let url = URL(string: IMAGE_UPLOAD.appending(((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "image") as? String!)!))
        cell.imageViewObj.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
        cell.imageViewObj.layer.borderWidth = 2.0
        cell.imageViewObj.layer.borderColor = UIColor.white.cgColor
        cell.imageViewObj.layer.cornerRadius = 25
        cell.imageViewObj.layer.masksToBounds = true
         cell.imageViewAboveLIneObj.isHidden = true
        
        let titlename: String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_title") as! String
        let firstname : String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_fname") as! String
        let middlename: String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_mname") as! String
        let lastname : String = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "reg_lname") as! String
        self.usernameobjstr = ("\(titlename) \(firstname) \(middlename) \(lastname)" as NSString) as NSString
        cell.userNameLabel.text = usernameobjstr as String
        let capitalized:String = (cell.userNameLabel.text!.capitalized)
        cell.userNameLabel.text = capitalized
        
        let htmlStr  = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "text") as? String
        let attributeString1 = try? NSAttributedString(data: (htmlStr?.data(using: String.Encoding.unicode)!)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil) as NSAttributedString
        
        cell.textLabelObj.attributedText = attributeString1
        
        let newAttributedString = NSMutableAttributedString(attributedString: cell.textLabelObj.attributedText!)
        
        // Enumerate through all the font ranges
        newAttributedString.enumerateAttribute(NSFontAttributeName, in: NSMakeRange(0, newAttributedString.length), options: []) { value, range, stop in
            guard let currentFont = value as? UIFont else {
                return
            }
            
            // An NSFontDescriptor describes the attributes of a font: family name, face name, point size, etc.
            // Here we describe the replacement font as coming from the "Hoefler Text" family
            let fontDescriptor = currentFont.fontDescriptor.addingAttributes([UIFontDescriptorFamilyAttribute: "Helvetica"])
            
            // Ask the OS for an actual font that most closely matches the description above
            if let newFontDescriptor = fontDescriptor.matchingFontDescriptors(withMandatoryKeys: [UIFontDescriptorFamilyAttribute]).first {
                let newFont = UIFont(descriptor: newFontDescriptor, size: currentFont.pointSize)
                newAttributedString.addAttributes([NSFontAttributeName: newFont], range: range)
            }
        }
        
        cell.textLabelObj.attributedText = newAttributedString
        
        // cell.textLabelObj.text =
        
        let myDate = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "date") as? String
        let myDateString = myDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        let Dateobj = dateFormatter.date(from: myDateString!)!
        dateFormatter.dateFormat = "MMM dd, YYYY HH:mm a"
        let somedateString = dateFormatter.string(from: Dateobj)
        cell.dataTimeLabel.text = somedateString
        
        // ---- Show All Comments --- //
        
        // let commentsArr : NSArray = ((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "comments") as? NSArray)!
        //  cell.viewAllCmtCount.text =  String(commentsArr.count)
        
        //------ Add New Comments ------ //
        
        cell.viewAllCmtCount.text = self.allcommentCountArray[indexPath.row] as? String
        
        
        cell.likeCountLabel.tag = indexPath.row
        cell.supportLabel.tag = indexPath.row
        //let likesStr : NSString = ((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "is_liked") as? NSString)!
        
        let likesStr : NSString = ((self.allisLikeArray[indexPath.row] as AnyObject) as? NSString)!
        
        if likesStr.isEqual(to: "1") {
            
            cell.heartImageview.tag = indexPath.row
            cell.heartImageview.image = UIImage (named: "Hand Filled")
            // cell.likeCountLabel.text = (self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "likes") as? String
            cell.likeCountLabel.text = self.allSupportArray[indexPath.row] as? String
            cell.supportLabel.text = "Supported"
        }
        if likesStr.isEqual(to: "0")
        {
            cell.heartImageview.tag = indexPath.row
            cell.heartImageview.image = UIImage (named: "Hand")
            cell.likeCountLabel.text = "0"
            cell.supportLabel.text = "Support"
        }
        
        // ------ Sending Images to CollectionView(WallCell) ----//
        
        // cell.setCollectionData(collection: ((self.responsePostArray[indexPath.row] as! AnyObject) .value(forKey: "images") as? NSMutableArray)!)
        
        cell.setCollectionData(collection: ((self.responsePostArray[indexPath.row] as AnyObject) .value(forKey: "images") as? NSMutableArray)!)
        
        //-----------------------------//
        
        if (((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "images") as? NSMutableArray)!.count) == 0
        {
            cell.heightCollectionView.constant = 0
            cell.imageBtnHeight.constant = 0
        }
        else
        {
            let imageorPDFstr = ((((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "image_name") as AnyObject) .firstObject) as! NSString
          //  print(imageorPDFstr as Any)
            let fileArray = imageorPDFstr.components(separatedBy: ".")
            let finalFileNameextension = fileArray.last
            if finalFileNameextension == "pdf"
            {
                cell.heightCollectionView.constant = 80
                cell.imageBtnHeight.constant = 0
            }
            if finalFileNameextension == "jpg"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
            if finalFileNameextension == "PNG"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
            if finalFileNameextension == "png"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
            if finalFileNameextension == "jpeg"
            {
                cell.heightCollectionView.constant = 227
                cell.imageBtnHeight.constant = 227
            }
        }
        cell.dotMoreBtn.tag = indexPath.row
        cell.dotMoreBtn.addTarget(self, action: #selector(self.DotbuttonClicked), for: .touchUpInside)
        
        self.checkImageORNotStr = ((((self.responsePostArray[indexPath.row] as AnyObject).value(forKey: "images") as AnyObject) .value(forKey: "image_name") as AnyObject) .firstObject) as? String
        
        if  self.checkImageORNotStr != nil {
            let fileArray = self.checkImageORNotStr?.components(separatedBy: ".")
            let finalFileNameextension = fileArray?.last
            let countStr : NSString = finalFileNameextension! as NSString
            let youtubestrpath  = countStr.length
            
            if youtubestrpath as NSInteger == 11
            {
                cell.imageTappedBtn.isHidden = true
            }
            if finalFileNameextension == "pdf" {
                cell.imageTappedBtn.isHidden = true
            }
            if finalFileNameextension == "jpeg"{
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
            if finalFileNameextension == "png" {
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
            
            if finalFileNameextension == "PNG" {
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
            
            if finalFileNameextension == "jpg" {
                cell.imageTappedBtn.isHidden = false
                cell.imageTappedBtn.tag = indexPath.row
                cell.imageTappedBtn.addTarget(self, action: #selector(self.imageTappedBtn), for: .touchUpInside)
            }
        }
        else
        {
            
        }
        
        cell.likeBtnTapped.tag = indexPath.row
        cell.likeBtnTapped.addTarget(self, action: #selector(self.likeBtnTapped), for: .touchUpInside)
        cell.viewAllBtnTapped.tag = indexPath.row
        cell.viewAllBtnTapped.addTarget(self, action: #selector(self.viewAllCommentBtnTapped), for: .touchUpInside)
        cell.commentBtnTap.tag = indexPath.row
        cell.commentBtnTap.addTarget(self, action: #selector(self.viewAllCommentBtnTapped), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView:UITableView, willDisplay cell:UITableViewCell, forRowAt indexPath:IndexPath)
    {
        /// print(self.responsePostArray.count - 1)
        if indexPath.row == (self.responsePostArray.count - 1)
        {
            if self.navStr == "Commentvc"
            {
                let postarray : NSArray = self.responsePostArray .value(forKey: "post_id") as! NSArray
                self.lastpostIDStr = postarray.lastObject as! NSString
                let userID = UserDefaults.standard.value(forKey: "userID")
                let params:[String: AnyObject] = [
                    "purpose" : "userpost" as AnyObject,
                    "id" : self.commentUsedID as AnyObject,
                    "direction" : "down" as AnyObject,
                    "myid" : userID as AnyObject,
                    "limit" : self.lastpostIDStr as AnyObject]
                // MBProgressHUD.showAdded(to: self.view, animated: true)
                self.postsApiCalling(param: params)
            }
            else
            {
                let postarray : NSArray = self.responsePostArray .value(forKey: "post_id") as! NSArray
                self.lastpostIDStr = postarray.lastObject as! NSString
                let userID = UserDefaults.standard.value(forKey: "userID")
                let params:[String: AnyObject] = [
                    "purpose" : "userpost" as AnyObject,
                    "id" : self.commentUsedID as AnyObject,
                    "direction" : "down" as AnyObject,
                    "myid" : userID as AnyObject,
                    "limit" : self.lastpostIDStr as AnyObject]
                
                self.postsApiCalling(param: params)
            }
        }
    }
    
}

