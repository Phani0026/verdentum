//
//  LoginViewController.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 17/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD
import LGSideMenuController
import IKEventSource

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    
    var pushDict = NSMutableDictionary()
    var pushArray = NSMutableArray()
    var groupRequestDict = NSMutableDictionary()
    var groupRequestArray = NSMutableArray()
    var friendRequestDict = NSMutableDictionary()
    var friendRequestArray = NSMutableArray()
    var iconClick : Bool!
    @IBOutlet var showpasswordBtn: UIButton!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var emailUsernameTextfield: UITextField!
    @IBOutlet var backtoHomeLabel: UILabel!
    @IBOutlet var createAccountLabel: UILabel!
    @IBOutlet var passwordhideImageViewObj: UIImageView!
    @IBOutlet var backHomeBtnTapped: UIButton!
    
    @IBOutlet var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.loginBtn.isEnabled = false
        self.loginBtn.backgroundColor = UIColor.init(red: 145.0/255.0, green: 180.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        iconClick = true
        let backHome = NSMutableAttributedString(string: self.backtoHomeLabel.text!)
        backHome.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: NSRange(location: 7, length: 5))
        self.backtoHomeLabel.attributedText = backHome
        let createaccount = NSMutableAttributedString(string: self.createAccountLabel.text!)
        createaccount.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: NSRange(location: 21, length: 7))
        self.createAccountLabel.attributedText = createaccount
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func checkNotifications() {
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let now = dateformatter.string(from: NSDate() as Date)
        let fullName    = now
        let fullNameArr = fullName.components(separatedBy: " ")
        let timestamp = fullNameArr[2]
        let nsMutableString = NSMutableString(string: timestamp)
        let timestamp1 : NSMutableString = nsMutableString
        timestamp1.insert(":", at: 3)
        let emailAddressText = timestamp1
        let encodedtime = emailAddressText.addingPercentEncoding(withAllowedCharacters:.rfc3986Unreserved)
        let currentUsedID = UserDefaults.standard.value(forKey: "userID")
        let urlPath : NSString = "http://52.40.207.123/cyc/api/notification/startstream/\(currentUsedID!)/\(encodedtime!)" as NSString
        let eventSource : EventSource = EventSource(url: urlPath as String)
        eventSource.onOpen {
            // When opened
        }
        eventSource.onError { (error) in
            // When errors
        }
        eventSource.onMessage { (id, event, data) in
            // Here you get an event without event name!
        }
        eventSource.addEventListener("groupRequest") { (id, event, data) in
            // Here you get an event 'event-name'
          
            let data = data?.data(using: .utf8)!
            let json = try? JSONSerialization.jsonObject(with: data!)
            let groupArray : NSArray = (json as AnyObject) as! NSArray
            self.groupRequestDict .setValue(groupArray, forKey: "groupReq")
            let notificationName = Notification.Name("groupRequestArrayNotifications")
            NotificationCenter.default.post(name: notificationName, object: self.groupRequestDict)
        }
        eventSource.addEventListener("friendRequest") { (id, event, data) in
            let data = data?.data(using: .utf8)!
            let json = try? JSONSerialization.jsonObject(with: data!)
            let friendReqArray : NSArray = (json as AnyObject) as! NSArray
            self.friendRequestDict .setValue(friendReqArray, forKey: "frndreq")
            let notificationName = Notification.Name("friendRequestArrayNotifications")
            NotificationCenter.default.post(name: notificationName, object: self.friendRequestDict)
        }
        eventSource.addEventListener("pushRequest") { (id, event, data) in
            let data = data?.data(using: .utf8)!
            let json = try? JSONSerialization.jsonObject(with: data!)
            let pushdataArray : NSArray = (json as AnyObject) as! NSArray
            self.pushDict .setValue(pushdataArray, forKey: "pushnotificationdata")
            let notificationName = Notification.Name("pushArrayNotifications")
            NotificationCenter.default.post(name: notificationName, object: self.pushDict)
        }
    }
    
    // MARK: - UITextFieldDelegate Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        if textField == self.emailUsernameTextfield
        {
            if (self.emailUsernameTextfield.text?.characters.count)! >= 100 && range.length == 0
            {
                return false
            }
            if range.location == 0 && (string == " ") {
                
                return false
            }
            let Regex = "[A-Za-z0-9@. ]*"
            let TestResult = NSPredicate(format: "SELF MATCHES %@", Regex)
            return TestResult.evaluate(with: string)
        }
        if textField == self.passwordTextfield
        {
            self.loginBtn.isEnabled = true
            self.loginBtn.backgroundColor = UIColor.init(red: 79.0/255.0, green: 91.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return false to ignore.
    {
        textField.resignFirstResponder()
        return true
    }
    
    // UIAlertViewController
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func hidePasswordBtnTapped(_ sender: Any) {
        
        if(iconClick == true) {
            self.passwordTextfield .isSecureTextEntry = false
            self.passwordhideImageViewObj.image = UIImage (named: "cross-eye")
            iconClick = false
        } else {
            self.passwordTextfield .isSecureTextEntry = true
            self.passwordhideImageViewObj.image = UIImage (named: "eye")
            iconClick = true
        }
    }
    
    
    @IBAction func loginBtnTapped(_ sender: Any) {
        
        if self.emailUsernameTextfield.text == "" || self.passwordTextfield.text == ""
        {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title:"" , message:"Please Enter EmailID & Password")
        }
        else
        {
             if Reachability.isConnectedToNetwork() == true {
            
            //  Calling Login Web Service
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let params:[String: AnyObject] = [
                "purpose" : "login" as AnyObject,
                "email" : self.emailUsernameTextfield.text! as AnyObject,
                "password" : self.passwordTextfield.text! as AnyObject ]
            
            let url = NSURL(string:BaseURL.appending(BaseURL_VERDENTUMLoginAPI))
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                // pass dictionary to nsdata object and set it as request body
                
            } catch let error {
                print(error.localizedDescription)
            }
            
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    return
                }
                
                do {
                    if let responsejson = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: AnyObject]
                    {
                        
                        DispatchQueue.main.async (execute:{ () -> Void in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                                
                            {
                                
                                UserDefaults.standard.set(((responsejson["user"] as AnyObject) .value(forKey: "id") as AnyObject), forKey: "userID")
                                UserDefaults.standard.set(((responsejson["user"] as AnyObject) .value(forKey: "name") as AnyObject), forKey: "userName")
                                UserDefaults.standard.set(((responsejson["user"] as AnyObject) .value(forKey: "role") as AnyObject), forKey: "userRole")
                                UserDefaults.standard.set(((responsejson["user"] as AnyObject) .value(forKey: "image") as AnyObject), forKey: "userImage")
                                UserDefaults.standard.set(((responsejson["user"] as AnyObject) .value(forKey: "email") as AnyObject), forKey: "userEmail")
                                self.checkNotifications()
                                //  let viewController: UIViewController
                                let viewController = DashBoardViewController()
                                viewController.navStr = "DashboardVC"
                                let navigationController1 = NavigationController(rootViewController: viewController)
                                let mainViewController = MainSideViewController()
                                mainViewController.rootViewController = navigationController1
                                mainViewController.setup(type: UInt(2))
                                let mainView = self.sideMenuController!
                                let nav = mainView.rootViewController as! NavigationController
                                nav.pushViewController(mainViewController, animated: true)
                                
                                
                                
                            }
                            else if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                            {
                                self.presentAlertWithTitle(title: "Login failed!", message: (responsejson["msg"] as AnyObject) as! String)
                            }
                                
                            else
                            {
                                
                            }
                        })
                    }
                    // pass dictionary to nsdata object and set it as request body
                    
                } catch let error {
                    print(error.localizedDescription)
                }
            } 
            task.resume()
             } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
            }
        }
        
    }
    
    
    func passwordValidation(_ checkPassword: String) -> Bool {
        let stricterFilterString = "^.{6,15}$"
        //[A-Za-z0-9]{8,50}
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        return passwordTest.evaluate(with: checkPassword)
    }
    
    
    @IBAction func backhomeBtn(_ sender: Any)
    {
        /* let HomeVC = HomePageViewController(nibName: "HomePageViewController", bundle: nil)
         navigationController?.pushViewController(HomeVC, animated: true)*/
        
        let mainViewController = self.sideMenuController!
        let CommentsVC = HomePageViewController()
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
    
    @IBAction func accountCreateBtn(_ sender: Any) {
        
        /* let SignUpVC = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
         navigationController?.pushViewController(SignUpVC, animated: true)*/
        
        let mainViewController = self.sideMenuController!
        let CommentsVC = SignUpViewController()
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
    
}
extension CharacterSet {
    static let rfc3986Unreserved = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~")
}
