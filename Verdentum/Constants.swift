//
//  Constants.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 17/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import Foundation

let BaseURL =  "http://52.40.207.123/cyc/api/auth"
let BaseURL_Login = ""
let BaseURl_SignUp = "/signup/"
let IMAGE_UPLOAD  = "http://52.40.207.123/cyc/upload/"
let VERDENTUMB_BaseURL = "http://52.40.207.123/cyc/api/mobpost"

let BaseURL_FacebookLoginAPI  =  "http://52.40.207.123/cyc/api/auth/fblogin"

let BaseURL_ADDPostGroupAPI  = "http://52.40.207.123/cyc/api/addpost/group"

let BaseURL_ADDPostAPI =  "http://52.40.207.123/cyc/api/addpost"

let BaseURL_AllCommentsAPI = "http://52.40.207.123/cyc/api/mobpost/comment"

let BaseURL_AddCommentsAPI = "http://52.40.207.123/cyc/api/mobpost/addcomment"

let BaseURL_USERPOSTSAPI = "http://52.40.207.123/cyc/api/mobpost/userpost"

let BaseURL_UPLOADACTIVITYAPI  = "http://52.40.207.123/cyc/api/mobprogram/fastprograms"

let BaseURL_SubActivitiesAPI  = "http://52.40.207.123/cyc/api/mobprogram/getsubtasks"

let BaseURL_FastUPloadActivitiesAPI  = "http://52.40.207.123/cyc/api/mobprogram/fastupload"

let BaseURL_EditProfileImageAPI  = "http://52.40.207.123/cyc/api/mobprofile"

let BaseURL_MyNetworkAPI  =  "http://52.40.207.123/cyc/api/mobvolunteer/myvoluns"

let BaseURL_DisConnectFrndAPI  = "http://52.40.207.123/cyc/api/mobvolunteer/unfriend"

let BaseURL_FindPeopleAPI  = "http://52.40.207.123/cyc/api/mobvolunteer"

let BaseURL_ConnectAPI  =  "http://52.40.207.123/cyc/api/mobvolunteer/connect"

let BaseURL_InvitedAPI  = "http://52.40.207.123/cyc/api/mobinvite"

let BaseURL_InvitedPeopleAPI  = "http://52.40.207.123/cyc/api/mobinvite/invite"

let BaseURL_FollowProgramAPI  = "http://52.40.207.123/cyc/api/mobprogram/todaysprogram"

let BaseURL_GetTeamAPI  = "http://52.40.207.123/cyc/api/mobprogram/getteams"

let BaseURL_FollowBtnAPI  = "http://52.40.207.123/cyc/api/mobprogram/followprogram"

let BaseURL_JoinOtherProgramAPI  = "http://52.40.207.123/cyc/api/mobgroup/joingroup"

let BaseURL_JoinMyProgramAPI  = "http://52.40.207.123/cyc/api/mobprogram/joinprogram"

let BaseURL_createinditeamAPI  = "http://52.40.207.123/cyc/api/mobgroup/createinditeam"

let BaseURL_GroupAllPostAPI  = "http://52.40.207.123/cyc/api/mobgroup/allpost"

let BaseURL_MYProgramAPI  =  "http://52.40.207.123/cyc/api/mobprogram"

let BaseURL_MYProgramFeedAPI  = "http://52.40.207.123/cyc/api/mobprogram/feeds"

let BaseURL_WorldViewAPI  = "http://52.40.207.123/cyc/api/mobprogram/worldview"

let BaseURL_GetActAPI  = "http://52.40.207.123/cyc/api/mobworldview/getact"

let BaseURL_GetChartAPI  = "http://52.40.207.123/cyc/api/mobprogram/getchart"

let BaseURL_ActiviyPhotoAPI  = "http://52.40.207.123/cyc/api/mobworldview/getimageoftask"

let BaseURL_SurveyAPI  = "http://52.40.207.123/cyc/api/mobsurvey"
let BaseURL_SurveyFieldsAPI  = "http://52.40.207.123/cyc/api/mobsurvey/getfields"

let BaseURL_CompleteSurveyAPI  = "http://52.40.207.123/cyc/api/mobsurvey/completesurvey"

let BaseURL_SupportAPI = "http://52.40.207.123/cyc/api/mobpost/support"

let BaseURL_PushPostAPI = "http://52.40.207.123/cyc/api/mobpost/viewpostdetails"

let BaseURL_FriendReqAcceptAPI = "http://52.40.207.123/cyc/api/notification/updatefriend"
let BaseURL_GroupReqAcceptAPI = "http://52.40.207.123/cyc/api/notification/updategroup"


let BaseURL_VERDENTUMLoginAPI = "/login/"
let BaseURL_VERDENTUMSignupAPI = "/signup/"


let BaseURL_VERDENTUMDeletepostAPI = "/deletepost"
let BaseURL_VERDENTUMReportpostAPI = "/reportpost"
let YOUTUBE_BASEURL = "https://www.youtube.com/watch?v="

let content_type = "application/json; charset=utf-8"

class Constants: NSObject
{
   /* static var sharedConstant: Constants? = nil
    
    class func sharedConstant() -> Constants {
        if sharedConstant == nil {
            sharedConstant = Constants()
        }
        return sharedConstant!
    }
   
    func write(_ string: String, withKey key: String) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: key)
        defaults.set(string, forKey: key)
        defaults.synchronize()
    }
    
    func readString(withKey key: String) -> String {
        let defaults = UserDefaults.standard
        return defaults.object(forKey: key)! as! String
    }
    
    func writeAnObject(toDefaults userDetails: UserDetails, withKey key: String) {
        let defaults = UserDefaults.standard
        let myEncodedObject = NSKeyedArchiver.archivedData(withRootObject: userDetails)
        defaults.removeObject(forKey: key)
        defaults.set(myEncodedObject, forKey: key)
        defaults.synchronize()
    }
    
    func logoutObject(toDefaults userDetails: UserDetails, withKey key: String) {
        let userDefaults = UserDefaults.standard
        let dict: [AnyHashable: Any] = userDefaults.dictionaryRepresentation()
        for key: Any in dict {
            userDefaults.removeObject(forKey: key as! String)
        }
        userDefaults.synchronize()
    }
    
    func getUserDetails(forKey key: String) -> UserDetails {
        var userDefaults = UserDefaults.standard
        var myEncodedObject: Data? = userDefaults.object(forKey: key) as! Data?
        var details: UserDetails? = (NSKeyedUnarchiver.unarchiveObject(withData: myEncodedObject) as? UserDetails)
        return details!
    }*/
  
}
