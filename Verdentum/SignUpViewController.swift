//
//  SignUpViewController.swift
//  Verdentum
//
//  Created by Praveen Kuruganti on 17/08/17.
//  Copyright © 2017 Verdentum. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import AVFoundation
import Photos

class SignUpViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,PhotoTweaksViewControllerDelegate {
   
  
    
    var  responseArray  = NSArray()
    var screenWidth = CGFloat()
    var popoverController: UIPopoverController?
     var imagePicker = UIImagePickerController()
    var profilepicImageStr = NSString()
    //var imagePicker = UIImagePickerController()
    @IBOutlet var indicatesReqFieldLabel: UILabel!
    
    @IBOutlet var uploadPhotoLabelObj: UILabel!
    @IBOutlet var heightViewScrollOBj: NSLayoutConstraint!
    @IBOutlet weak var backHomeLabel: UILabel!
    @IBOutlet weak var termsConditionslabel: UILabel!
    @IBOutlet weak var alreadyLoginLabel: UILabel!
    @IBOutlet var firstNameTextfield: UITextField!
    @IBOutlet var middleNameTextfield: UITextField!
    @IBOutlet var lastNameTextfield: UITextField!
    @IBOutlet var emailTextfieldObj: UITextField!
    @IBOutlet var imageViewPorfilePIcObj: UIImageView!
    @IBOutlet var ageTextfieldObj: UITextField!
    @IBOutlet var profilePicViewObj: UIView!
    @IBOutlet var viewHeightProfilePIc: NSLayoutConstraint!
    @IBOutlet var imageHeightobj: NSLayoutConstraint!
    @IBOutlet var removeHeightObj: NSLayoutConstraint!
    @IBOutlet var takepictureObjBtn: UIButton!
    
    @IBOutlet var choosePictureBtnTapped: UIButton!
    
    
    // MARK: viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenRect: CGRect = UIScreen.main.bounds
        screenWidth = screenRect.size.width
//        if screenWidth == 320
//        {
//            self.heightViewScrollOBj.constant = 550
//        }
        
        
        self.imagePicker.delegate = self
        self.profilePicViewObj.layer.borderWidth = 1
        
        self.profilePicViewObj.layer.borderColor = UIColor.gray.cgColor
        self.takepictureObjBtn.layer.borderColor = UIColor.black.cgColor
        self.takepictureObjBtn.layer.borderWidth = 1.0
        self.choosePictureBtnTapped.layer.borderColor = UIColor.black.cgColor
        self.choosePictureBtnTapped.layer.borderWidth = 1.0
        
        let indicateLabel = NSMutableAttributedString(string: self.indicatesReqFieldLabel.text!)
        
        indicateLabel.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: NSRange(location: 0, length: 1))
        self.indicatesReqFieldLabel.attributedText = indicateLabel
        
        self.firstNameTextfield.attributedPlaceholder = NSAttributedString(string: "First Name *",attributes: [NSForegroundColorAttributeName: UIColor.red])

        
        self.emailTextfieldObj.attributedPlaceholder = NSAttributedString(string: "Email *",attributes: [NSForegroundColorAttributeName: UIColor.red])

        
        let uploadpic = NSMutableAttributedString(string: self.uploadPhotoLabelObj.text!)
        
        uploadpic.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: NSRange(location: 15, length: 1))
        self.uploadPhotoLabelObj.attributedText = uploadpic
        
        let termsConditions = NSMutableAttributedString(string: self.termsConditionslabel.text!)
        
        termsConditions.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: NSRange(location: 36, length: 33))
        self.termsConditionslabel.attributedText = termsConditions
        
        let backHome = NSMutableAttributedString(string: self.backHomeLabel.text!)
        
        backHome.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: NSRange(location: 7, length: 5))
        self.backHomeLabel.attributedText = backHome
        
        let alreadyLogin = NSMutableAttributedString(string: self.alreadyLoginLabel.text!)
        
        alreadyLogin.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: NSRange(location: 23, length: 7))
        self.alreadyLoginLabel.attributedText = alreadyLogin
        
        // self.choosePicbtn.addTarget(self, action: #selector(self.showResizablePicker), for: .touchUpInside)
        
      //  self.choosePictureBtnTapped.addTarget(self, action:#selector(self.choosePictureBtnTapped), for: .touchUpInside)
        
        
        
        if self.imageViewPorfilePIcObj.image == nil
        {
            self.imageHeightobj.constant = 0
            self.removeHeightObj.constant = 0
            self.viewHeightProfilePIc.constant = 0
        }
        else
        {
            self.imageHeightobj.constant = 90
            self.removeHeightObj.constant = 30
            self.viewHeightProfilePIc.constant = 135
        }
        
        
    }
    
     // MARK: viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.imageViewPorfilePIcObj.image == nil
        {
            self.imageHeightobj.constant = 0
            self.removeHeightObj.constant = 0
            self.viewHeightProfilePIc.constant = 0
        }
        else
        {
            self.imageHeightobj.constant = 90
            self.removeHeightObj.constant = 30
            self.viewHeightProfilePIc.constant = 135
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     // MARK: -choosePictureBtnTapped
    

    @IBAction func choooseImageBtn(_ sender: Any) {
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            print("Access has been granted")
        }
            
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
            print("Access has been denied")
            self.alertPromptToAllowCameraAccessViaSetting()
        }
            
        else if (status == PHAuthorizationStatus.notDetermined) {
            
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    print("Access has been granted.")
                }
                else {
                    print(" Access has been denied.")
                    self.alertPromptToAllowCameraAccessViaSetting()
                }
            })
        }
            
        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
            print(" Restricted access - normally won't happen")
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            present( self.imagePicker, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - UITextFieldDelegate Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == self.firstNameTextfield
        {
            
            if (self.firstNameTextfield.text?.characters.count)! >= 100 && range.length == 0
            {
                self.presentAlertWithTitle(title:"First Name!" , message:"First Name Should Be Less Than 100 Characters")
                return false
            }
            if range.location == 0 && (string == " ") {
                self.presentAlertWithTitle(title:"First Name!" , message:"First Name Should Only Contain Letters and Space and Should Start With a Letter")
                return false
            }
            let Regex = "[A-Za-z ]*"
            let TestResult = NSPredicate(format: "SELF MATCHES %@", Regex)
            return TestResult.evaluate(with: string)
        }
        if textField == self.middleNameTextfield
        {
            if (self.firstNameTextfield.text?.characters.count)! >= 100 && range.length == 0
            {
                self.presentAlertWithTitle(title:"Middle Name!" , message:"Middle Name Should Be Less Than 100 Characters")
                return false
            }
            if range.location == 0 && (string == " ")
            {
                self.presentAlertWithTitle(title:"Middle Name!" , message:"Middle Name Should Only Contain Letters and Space and Should Start With a Letter")
                return false
            }
            let Regex = "[A-Za-z ]*"
            let TestResult = NSPredicate(format: "SELF MATCHES %@", Regex)
            return TestResult.evaluate(with: string)
        }
        if textField == self.lastNameTextfield
        {
            if (self.firstNameTextfield.text?.characters.count)! >= 100 && range.length == 0
            {
                self.presentAlertWithTitle(title:"Last Name!" , message:"Last Name Should Be Less Than 100 Characters")
                return false
            }
            if range.location == 0 && (string == " ") {
                self.presentAlertWithTitle(title:"Last Name!" , message:"Last Name Should Only Contain Letters and Space and Should Start With a Letter")
                return false
            }
            let Regex = "[A-Za-z ]*"
            let TestResult = NSPredicate(format: "SELF MATCHES %@", Regex)
            return TestResult.evaluate(with: string)
        }
        if textField == self.ageTextfieldObj {
            
            
            
            if range.location == 0 && (string == " ") {
                return false
            }
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return false to ignore.
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField  == self.ageTextfieldObj  {
            let x = self.ageTextfieldObj.text
            
            if x == "" {
                
            }
            else
            {
                if CInt(x!)! >= 13 && CInt(x!)! <= 100 {
                    
                    
                    
                }
                else
                {
                    self.presentAlertWithTitle(title:"Age!" , message:"Age should be between 13 to 100 Years")
                }
            }
            
            
        }
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        let image = info[UIImagePickerControllerEditedImage] as? UIImage
        self.imageViewPorfilePIcObj.image = image
        
        if image != nil {
            
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                self.imageViewPorfilePIcObj.image = pickedImage
                let photoTweaksViewController = PhotoTweaksViewController(image: image )
                photoTweaksViewController?.delegate = self
                // photoTweaksViewController.autoSaveToLibray = YES;
               // photoTweaksViewController?.maxRotationAngle = CGFloat(Double.pi / 2)
                picker.pushViewController(photoTweaksViewController!, animated: true)
            }
            dismiss(animated: true, completion: nil)
            
            if let updatedImage = self.imageViewPorfilePIcObj.image?.updateImageOrientionUpSide() {
                // Covert UIImage to Base64
                let imageData : NSData = UIImageJPEGRepresentation(updatedImage, 0.7)! as NSData
                
                self.profilepicImageStr = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
            } else {
                
            }
        }
        else {
            let image = info[UIImagePickerControllerOriginalImage] as? UIImage
            let photoTweaksViewController = PhotoTweaksViewController(image: image )
            photoTweaksViewController?.delegate = self
            // photoTweaksViewController.autoSaveToLibray = YES;
            photoTweaksViewController?.maxRotationAngle = CGFloat(Double.pi / 2)
            picker.pushViewController(photoTweaksViewController!, animated: true)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
     // MARK: PhotoTweaksViewController
    
    func photoTweaksController(_ controller: PhotoTweaksViewController!, didFinishWithCroppedImage croppedImage: UIImage!) {
        self.imageViewPorfilePIcObj.image = croppedImage
        // Covert UIImage to Base64
        
        let imageData : NSData = UIImageJPEGRepresentation(self.imageViewPorfilePIcObj.image!, 0.7)! as NSData
        
        self.profilepicImageStr = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
    controller.navigationController?.popViewController(animated: true)
    }
    
    func photoTweaksControllerDidCancel(_ controller: PhotoTweaksViewController!) {
    controller.navigationController?.popViewController(animated: true)
    }
    
    // MARK: takePictureBtnTapped
    @IBAction func takePictureBtnTapped(_ sender: Any) {
        
        let cameraMediaType = AVMediaTypeVideo
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .denied:
            self.alertPromptToAllowCameraAccessViaSetting()
            break
        case .authorized:
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                //let imagePicker = UIImagePickerController()
                self.imagePicker.delegate=self
                 self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                 self.imagePicker.allowsEditing = true
                self .present( self.imagePicker, animated: true, completion: nil)
            } else {
                noCamera()
            }
            break
        case .restricted: break
            
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(forMediaType: cameraMediaType) { granted in
                if granted {
                    print("Granted access to \(cameraMediaType)")
                } else {
                    print("Denied access to \(cameraMediaType)")
                     self.alertPromptToAllowCameraAccessViaSetting()
                }
            }
        }
        
        
    }
    func alertPromptToAllowCameraAccessViaSetting() {
        let alert = UIAlertController(title: "Alert", message: "Photo access required to...", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel) { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        
        present(alert, animated: true)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, This device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    @IBAction func removeImageBtnTapped(_ sender: Any) {
        
        self.imageViewPorfilePIcObj.image = nil
        
        if self.imageViewPorfilePIcObj.image == nil
        {
            self.imageHeightobj.constant = 0
            self.removeHeightObj.constant = 0
            self.viewHeightProfilePIc.constant = 0
        }
        else
        {
            self.imageHeightobj.constant = 90
            self.removeHeightObj.constant = 30
            self.viewHeightProfilePIc.constant = 135
        }
        
        
    }
    
    
    
    @IBAction func termsConditinsBtnTapped(_ sender: Any) {
        
        let termsConditionVC = TermAndConditionsViewController(nibName: "TermAndConditionsViewController", bundle: nil)
        navigationController?.pushViewController(termsConditionVC, animated: true)
    }
    
    
    @IBAction func signUpBtnTapped(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if self.firstNameTextfield.text == "" || self.emailTextfieldObj.text == "" || self.imageViewPorfilePIcObj.image == nil
        {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.presentAlertWithTitle(title:"" , message:"* Indication required fields")
            
        }
        else
        {
            
            if self.firstNameTextfield.text == "" {
                
                MBProgressHUD.hide(for: self.view, animated: true)
                self.presentAlertWithTitle(title:"First Name!" , message:"First Name Is Required")
            }
            else
            {
                
                if self.emailTextfieldObj.text == ""
                {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.presentAlertWithTitle(title:"Email-id!" , message:"Email id Iis Required")
                }
                else
                {
                    if emailValidation(self.emailTextfieldObj.text!) {
                        
                        if self.imageViewPorfilePIcObj.image == nil
                        {
                            self.presentAlertWithTitle(title:"Profile Imageasdf!" , message:"Profile Image Is Required!")
                        }
                        else
                        {
                            
                            
                            
                            //declare parameter as a dictionary which contains string as key and value combination.
                            
                            // prepare json data
                            let parameters: [String: Any] = [
                                "purpose" : "signup",
                                "fname" : self.firstNameTextfield.text!,
                                "mname" : self.lastNameTextfield.text!,
                                "lname" : self.middleNameTextfield.text!,
                                "email" : self.emailTextfieldObj.text!,
                                "age" : self.ageTextfieldObj.text!,
                                "imgURI" : self.profilepicImageStr]
                            
                             if Reachability.isConnectedToNetwork() == true {
                            
                            //create the url with NSURL
                            let url = NSURL(string: BaseURL.appending(BaseURL_VERDENTUMSignupAPI))
                            
                            //create the session object
                            let session = URLSession.shared
                            
                            //now create the NSMutableRequest object using the url object
                            let request = NSMutableURLRequest(url: url! as URL)
                            request.httpMethod = "POST" //set http method as POST
                            
                            do {
                                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                                
                            } catch let error {
                                print(error.localizedDescription)
                            }
                            
                            //HTTP Headers
                            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                            request.addValue("application/json", forHTTPHeaderField: "Accept")
                            
                            //create dataTask using the session object to send data to the server
                            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                                
                                guard error == nil else {
                                    return
                                }
                                
                                guard let data = data else {
                                    return
                                }
                                
                                do {
                                    //create json object from data
                                    if let responsejson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                                        DispatchQueue.main.async (execute:{ () -> Void in
                                            MBProgressHUD.hide(for: self.view, animated: true)
                                            
                                            let successMsgstr : NSString = (responsejson["msg"] as AnyObject) as! NSString
                                            
                                             if (responsejson["status"] as AnyObject) .isEqual(to: 1)
                                                //|| (responsejson["status"] as AnyObject) .isEqual(to: 0))
                                            {
                                                
                                                let alertController = UIAlertController(title: "Successfully Registered", message: (successMsgstr as String), preferredStyle: .alert)
                                                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                                                }
                                                alertController.addAction(cancelAction)
                                                
                                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                                    let sampleVC = LoginViewController(nibName: "LoginViewController", bundle: nil)
                                                    self.navigationController?.pushViewController(sampleVC, animated: true)
                                                }
                                                alertController.addAction(OKAction)
                                                
                                                self.present(alertController, animated: true, completion:nil)
                                                
                                                
                                            }
                                            else if (responsejson["status"] as AnyObject) .isEqual(to: 0)
                                            {
                                                self.presentAlertWithTitle(title: "Signup failed", message: successMsgstr as String)
                                                self.firstNameTextfield.text = ""
                                                self.middleNameTextfield.text = ""
                                                self.lastNameTextfield.text = ""
                                                self.emailTextfieldObj.text = ""
                                                self.ageTextfieldObj.text = ""
                                                self.imageViewPorfilePIcObj.image = nil
                                                self.imageHeightobj.constant = 0
                                                self.removeHeightObj.constant = 0
                                                self.viewHeightProfilePIc.constant = 0
                                            }
                                            else
                                            {
                                                
                                            }
                                        })
                                    }
                                    
                                }
                                catch let error
                                {
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    print(error.localizedDescription)
                                }
                            })
                            
                            
                            task.resume()
                             } else {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                self.presentAlertWithTitle(title: "No Internet Connection", message:"Make sure your device is connected to the internet.")
                            }
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.presentAlertWithTitle(title:"Email-Id!" , message:"You have entered an invalid email id!")
                    }
                }
            }
        }
    }
    
    
    func emailValidation(_ checkString: String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: checkString)
        return result
    }
    
    
    func presentAlertWithTitle(title: String, message : String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func gotoHomeBtnTapped(_ sender: Any)
    {
      /*  let loginVC = HomePageViewController(nibName: "HomePageViewController", bundle: nil)
        navigationController?.pushViewController(loginVC, animated: true)*/
        
        let mainViewController = self.sideMenuController!
        let CommentsVC = HomePageViewController()
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
    @IBAction func alreadyLoginBtnTapped(_ sender: Any) {
        
       /* let loginVC = LoginViewController(nibName: "LoginViewController", bundle: nil)
        navigationController?.pushViewController(loginVC, animated: true)*/
        
        let mainViewController = self.sideMenuController!
        let CommentsVC = LoginViewController()
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(CommentsVC, animated: true)
    }
    
}

